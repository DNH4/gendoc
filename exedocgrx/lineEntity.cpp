#include "pointEntity.h"
#include "lineEntity.h"
#include "dataBase.h"


long getSsLine(
    ads_name&         ssName,
    const AcString&   sLayerName )
{
    return getSelectionSet( ssName, "", "LINE", sLayerName );
}



long getSsOneLine(
    ads_name&         ssName,
    const AcString&   sLayerName )
{
    return getSingleSelection( ssName, "LINE", sLayerName );
}



long getSsAllLine(
    ads_name&         ssName,
    const AcString&   sLayerName )
{
    return getSelectionSet( ssName, "X", "LINE", sLayerName );
}



AcDbLine* getLineFromSs(
    const ads_name&     ssName,
    const long&   iObject,
    const AcDb::OpenMode& opMode )
{
    //Recuperer l'entiter
    AcDbEntity* l_pEntityLine = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entiter en ligne trace
    return AcDbLine::cast( l_pEntityLine );
}




AcDbLine* readLineFromSs(
    const ads_name&     ssName,
    const long&   iObject )
{
    //recuperer l'entite
    return getLineFromSs( ssName, iObject, AcDb::kForRead );
}



AcDbLine* writeLineFromSs(
    const ads_name&     ssName,
    const long&   iObject )
{
    //recuperer l'entite
    return getLineFromSs( ssName, iObject, AcDb::kForWrite );
}


void drawLine(
    const AcGePoint3d& ptStart,
    const AcGePoint3d& ptEnd )
{
    //Creer un objet line
    AcDbLine* line = new AcDbLine( ptStart, ptEnd );
    
    //ajouter la ligne au model space
    addToModelSpace( line );
    
    // fermer la ligne
    line->close();
}


void drawLine(
    const AcGePoint3d& ptStart,
    const AcGePoint3d& ptEnd,
    AcDbBlockTableRecord* blocktable,
    AcDbEntity* entity )
{
    //Creer un objet line
    AcDbLine* line = new AcDbLine( ptStart, ptEnd );
    
    //recuperer la propriete de l'entite
    if( entity )line->setPropertiesFrom( entity );
    
    //ajouter la ligne au model space
    addToModelSpaceAlreadyOpened( blocktable, line );
    
    // fermer la ligne
    line->close();
}


void drawLine(
    const AcGePoint2d& ptStart,
    const AcGePoint2d& ptEnd,
    AcDbBlockTableRecord* blocktable,
    AcDbEntity* entity )
{
    //Creer un objet line
    AcDbLine* line = new AcDbLine( getPoint3d( ptStart ), getPoint3d( ptEnd ) );
    
    //recuperer la propriete de l'entite
    if( entity )line->setPropertiesFrom( entity );
    
    //ajouter la ligne au model space
    addToModelSpaceAlreadyOpened( blocktable, line );
    
    // fermer la ligne
    line->close();
}

AcDbLine* insertLine(
    const AcGePoint3d& ptStart,
    const AcGePoint3d& ptEnd )
{
    //Creer un objet line
    AcDbLine* line = new AcDbLine( ptStart, ptEnd );
    
    //ajouter la ligne au model space
    addToModelSpace( line );
    
    // Retourner la ligne
    return line;
}