#include "print.h"
#include "gcestext.h"
#include "boundingBox.h"


Acad::ErrorStatus sortBoundingBox( vector<AcDbExtents>& bbVec,
    vector<double>& bbVecXMinPoint,
    vector<Segment>& segVec )
{
    //Resultat
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Recuperer la taille du vecteur
    int taille = bbVec.size();
    
    //Verifier avant de trier
    if( taille != bbVecXMinPoint.size() ||
        taille != segVec.size() ||
        taille == 0 )
        return er;
        
    //Initialiser la barre de progression
    ProgressBar prog = ProgressBar( _T( "Tri des BoundingsBoxes" ), taille );
    
    //Boucle de tri
    for( int i = 0; i < taille - 1; i++ )
    {
        //Recuperer l'index du min
        int min = i;
        
        //Boucle de test
        for( int j = i + 1; j < taille; j++ )
        {
            if( bbVecXMinPoint[j] < bbVecXMinPoint[min] )
                min = j;
        }
        
        //Swapper les elements des vecteurs
        if( min != i )
        {
            std::swap( bbVecXMinPoint[i], bbVecXMinPoint[min] );
            std::swap( bbVec[i], bbVec[min] );
            std::swap( segVec[i], segVec[min] );
        }
        
        //Progresser
        prog.moveUp( i );
    }
    
    //Retourner eOk
    return Acad::eOk;
}


Acad::ErrorStatus getIntersectedBoundingBoxes( AcDbCurve* curve,
    vector<int>& indexesVec,
    const vector<Segment>& segVec )
{
    //Resultat
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //BoundingBox de la curve
    AcDbExtents curveExtents;
    er = curve->getGeomExtents( curveExtents );
    
    //Verifier
    if( er != Acad::eOk )
        return er;
        
    //Recuperer les deux points de la bounding box de la curve
    AcGePoint3d pt1 = curveExtents.minPoint();
    AcGePoint3d pt2 = curveExtents.maxPoint();
    
    //Appeller la fonction qui va recuperer l'index du segment
    er = dichotomBoundingBox( pt1,
            pt2,
            indexesVec,
            segVec );
            
    //Verifier
    if( er != Acad::eOk )
        return er;
        
    //Retourner eOk
    return Acad::eOk;
}


Acad::ErrorStatus dichotomBoundingBox( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    vector<int>& indexesVec,
    const vector<Segment>& segVec )
{
    //Resultat
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Initialisation des variables
    int debut = 0;
    bool trouve = false;
    int fin = segVec.size();
    int mil = 0;
    
    //Boucle de d�coupage vers la gauche
    while( !trouve && debut <= fin )
    {
        //Recuperer l'index du milieu du vecteur
        mil = ( debut + fin ) / 2;
        
        //Recuperer le segment
        Segment currentSeg = segVec[mil];
        
        //Cr�er une polyligne 2D � partir du segment
        AcDbPolyline* polyTemp = new AcDbPolyline();
        
        if( Acad::eOk != polyTemp->addVertexAt( 0, getPoint2d( currentSeg.pt1 ), currentSeg.bulge )
            || Acad::eOk !=  polyTemp->addVertexAt( 1, getPoint2d( currentSeg.pt2 ) ) )
        {
            delete polyTemp;
            return er;
        }
        
        //Recuperer le bounding box de cette nouvelle polyligne
        AcDbExtents bb;
        
        if( Acad::eOk  != polyTemp->getGeomExtents( bb ) )
        {
            delete polyTemp;
            return er;
        }
        
        delete polyTemp;
        
        
        //Tester si les boundingsBox s'intersectent
        if( isBoxIntersected( getPoint2d( pt1 ),
                getPoint2d( pt2 ),
                getPoint2d( bb.minPoint() ),
                getPoint2d( bb.maxPoint() ) ) )
        {
            //Ajouter dans le vecteur
            indexesVec.push_back( mil );
            
            //Changer le booleen
            trouve = true;
        }
        
        //Sinon on passe sur la deuxieme portion du vecteur
        else if( min( pt1.x, pt2.x ) > min( bb.minPoint().x, bb.maxPoint().x ) ||
            max( pt1.x, pt2.x ) > max( bb.minPoint().x, bb.maxPoint().x ) )
            debut = mil + 1;
            
        //Ou sur l'autre portion
        else
            fin = mil - 1;
    }
    
    if( indexesVec.size() > 0 )
    {
        //Boucle sur l'index trouv� vers le sens +
        for( int i = mil + 1; i <= fin; i++ )
        {
            //Recuperer le segment
            Segment currentSeg = segVec[i];
            
            
            //Cr�er une polyligne 2D � partir du segment
            AcDbPolyline* polyTemp = new AcDbPolyline();
            
            if( Acad::eOk != polyTemp->addVertexAt( 0, getPoint2d( currentSeg.pt1 ), currentSeg.bulge )
                || Acad::eOk !=  polyTemp->addVertexAt( 1, getPoint2d( currentSeg.pt2 ) ) )
            {
                delete polyTemp;
                return er;
            }
            
            //Recuperer le bounding box de cette nouvelle polyligne
            AcDbExtents bb;
            
            if( Acad::eOk  != polyTemp->getGeomExtents( bb ) )
            {
                delete polyTemp;
                return er;
            }
            
            delete polyTemp;
            
            //Ajouter l'index du segment dans le vecteur s'il y a une intersection
            if( isBoxIntersected( getPoint2d( pt1 ),
                    getPoint2d( pt2 ),
                    getPoint2d( bb.minPoint() ),
                    getPoint2d( bb.maxPoint() ) ) )
                indexesVec.push_back( i );
                
            else
                break;
        }
        
        //Boucle sur l'index trouv� dans le sens -
        if( mil > 0 )
        {
            for( int i = mil - 1 ; i >= 0 ; i-- )
            {
                //Recuperer le segment
                Segment currentSeg = segVec[i];
                
                //Cr�er une polyligne 2D � partir du segment
                AcDbPolyline* polyTemp = new AcDbPolyline();
                
                if( Acad::eOk != polyTemp->addVertexAt( 0, getPoint2d( currentSeg.pt1 ), currentSeg.bulge )
                    || Acad::eOk !=  polyTemp->addVertexAt( 1, getPoint2d( currentSeg.pt2 ) ) )
                {
                    delete polyTemp;
                    return er;
                }
                
                //Recuperer le bounding box de cette nouvelle polyligne
                AcDbExtents bb;
                
                if( Acad::eOk  != polyTemp->getGeomExtents( bb ) )
                {
                    delete polyTemp;
                    return er;
                }
                
                delete polyTemp;
                
                //Ajouter l'index du segment dans le vecteur s'il y a une intersection
                if( isBoxIntersected( getPoint2d( pt1 ),
                        getPoint2d( pt2 ),
                        getPoint2d( bb.minPoint() ),
                        getPoint2d( bb.maxPoint() ) ) )
                    indexesVec.insert( indexesVec.begin(), i );
                    
                else
                    break;
            }
        }
        
        //Retourner eOk
        return Acad::eOk;
    }
    
    //Sinon on n'a pas d'intersection
    return er;
}


bool isContainedBy( vector<AcDbExtents>& boundingBoxes,
    int& isContained,
    int& index,
    const AcDbExtents& bB )
{
    int size = boundingBoxes.size();
    
    // Si le vecteur de bounding boxes est vide on renvoie false
    if( boundingBoxes.size() == 0 )
        return false;
        
    // On r�cup�re les coordonn�es de la bounding box � traiter
    int bBminX = bB.minPoint().x;
    int bBminY = bB.minPoint().y;
    int bBmaxX = bB.maxPoint().x;
    int bBmaxY = bB.maxPoint().y;
    
    // On parcourt le vecteur de bounding boxes
    for( int i = 0; i < boundingBoxes.size(); i++ )
    {
        // On r�cup�re les coordonn�es de la bounding box courante
        AcDbExtents currentBb = boundingBoxes[i];
        int currentMinX = currentBb.minPoint().x;
        int currentMinY = currentBb.minPoint().y;
        int currentMaxX = currentBb.maxPoint().x;
        int currentMaxY = currentBb.maxPoint().y;
        
        // Si la bb �tudi�e est contenue dans la bb courante
        if( ( bBminX >= currentMinX ) &&
            ( bBmaxX <= currentMaxX ) &&
            ( bBminY >= currentMinY ) &&
            ( bBmaxY <= currentMaxY ) )
        {
            // On renvoie true et on met � jour l'index
            isContained = 1;
            index = i;
            return true;
        }
        
        // Si la bb �tudi�e contient la bb courante
        else if( ( bBminX <= currentMinX ) &&
            ( bBmaxX >= currentMaxX ) &&
            ( bBminY <= currentMinY ) &&
            ( bBmaxY >= currentMaxY ) )
        {
            // On renvoie true et on met � jour l'index
            isContained = -1;
            index = i;
            return true;
        }
        
        // Sinon, on renvoie false
        else
        {
            isContained = 0;
            index = -1;
            return false;
        }
    }
}


bool are2BoxIntersected( const AcDbExtents& box1, const AcDbExtents& box2 )
{
    // Recuperer le point min et le max de ses deux bounding box
    AcGePoint3d min1, max1, min2, max2;
    min1 = box1.minPoint();
    max1 = box1.maxPoint();
    min2 = box2.minPoint();
    max2 = box2.maxPoint();
    
    
    
    // Lancer la fonction qui verifie s'il y a une intersection entre deux rectangles
    if( are2RectIntersected( min1, max1, min2, max2 ) )
        return true;
    else return false;
}

bool isBoundingBoxEqual( const AcDbExtents& box1,
    const AcDbExtents& box2,
    const double& tolXYZ )
{
    AcGePoint3d min1, max1, min2, max2;
    
    //On recupere les coordone des deux boundingbox
    min1 = box1.minPoint();
    max1 = box1.maxPoint();
    min2 = box2.minPoint();
    max2 = box2.maxPoint();
    
    //On teste si les points se superpose
    if( isEqual3d( min1, min2, tolXYZ ) &&
        isEqual3d( max1, max2, tolXYZ ) )
        return true;
        
    else
        return false;
}

Acad::ErrorStatus drawBoundingBox( AcDbExtents box,
    AcDbDatabase* pDb )
{
    //Declarer l'errerur status
    Acad::ErrorStatus es = eOk;
    
    //Les points des cotes du box
    AcGePoint3dArray ptArr;
    
    //Dessiner un polyligne rectangle du box
    ptArr.append( box.maxPoint() );
    ptArr.append( box.minPoint() );
    AcDbPolyline* poly = new AcDbPolyline();
    
    poly->addVertexAt( 0, getPoint2d( ptArr[0] ) );
    poly->addVertexAt( 1, AcGePoint2d( ptArr[1].x, ptArr[0].y ) );
    poly->addVertexAt( 2, getPoint2d( ptArr[1] ) );
    poly->addVertexAt( 3, AcGePoint2d( ptArr[0].x, ptArr[1].y ) );
    
    //Dessiner le polyligne dans la base de donne
    // recupere la liste des blocks
    AcDbBlockTable* pBlockTable = NULL;
    pDb->getSymbolTable( pBlockTable, AcDb::kForRead );
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    
    // recupere le model space, puis ferme la liste des blocks
    pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite );
    pBlockTable->close();
    
    // Ajouter la modelspace
    poly->setClosed( Adesk::kTrue );
    addToModelSpaceAlreadyOpened( pBlockTableRecord, poly );
    
    closeModelSpace( pBlockTableRecord );
    poly->close();
    
    return es;
}


Acad::ErrorStatus drawBoundingBox3d( const AcDbExtents& box,
    const AcCmColor& color )
{
    //Les points des cotes du box
    AcGePoint3dArray ptArr;
    
    //Recuperer les points
    AcGePoint3d minPoint = box.minPoint();
    AcGePoint3d maxPoint = box.maxPoint();
    
    //Recuperer les 8 points du cube
    AcGePoint3d pt1 = minPoint;
    AcGePoint3d pt2 = AcGePoint3d( minPoint.x, maxPoint.y, minPoint.z );
    AcGePoint3d pt3 = AcGePoint3d( maxPoint.x, maxPoint.y, minPoint.z );
    AcGePoint3d pt4 = AcGePoint3d( maxPoint.x, minPoint.y, minPoint.z );
    AcGePoint3d pt5 = AcGePoint3d( minPoint.x, minPoint.y, maxPoint.z );
    AcGePoint3d pt6 = AcGePoint3d( minPoint.x, maxPoint.y, maxPoint.z );
    AcGePoint3d pt7 = maxPoint;
    AcGePoint3d pt8 = AcGePoint3d( maxPoint.x, minPoint.y, maxPoint.z );
    
    ptArr.push_back( pt1 );
    ptArr.push_back( pt2 );
    ptArr.push_back( pt3 );
    ptArr.push_back( pt4 );
    ptArr.push_back( pt1 );
    ptArr.push_back( pt5 );
    ptArr.push_back( pt6 );
    ptArr.push_back( pt7 );
    ptArr.push_back( pt3 );
    ptArr.push_back( pt4 );
    ptArr.push_back( pt8 );
    ptArr.push_back( pt5 );
    ptArr.push_back( pt6 );
    ptArr.push_back( pt2 );
    ptArr.push_back( pt3 );
    ptArr.push_back( pt7 );
    ptArr.push_back( pt8 );
    
    AcDb3dPolyline* cubeBox = new AcDb3dPolyline( AcDb::k3dSimplePoly, ptArr );
    addToModelSpace( cubeBox );
    cubeBox->setColor( color );
    cubeBox->close();
    
    return Acad::eOk;
}


bool isInBoundingBox( const AcDbExtents& box,
    const AcGePoint3d& pt,
    const bool& is3d )
{
    //Tester si le point est � l'interieur de la bounding box
    if( pt.x >= box.minPoint().x && pt.x <= box.maxPoint().x &&
        pt.y >= box.minPoint().y && pt.y <= box.maxPoint().y )
    {
        if( !is3d )
            return true;
            
        if( pt.z >= box.minPoint().z && pt.z <= box.maxPoint().z )
            return true;
    }
    
    return false;
}