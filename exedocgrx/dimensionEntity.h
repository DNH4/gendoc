/**
 * @file dimensionEntity.h
 * @brief Fichier contenant des fonctions utiles pour travailler avec des sélections sur les dimensions et cotations
 */


#pragma once
#include <dbents.h>
#include "selectEntity.h"
#include "pointEntity.h"
#include "progressBar.h"
#include "dataBase.h"
#include "layer.h"
#include "circleEntity.h"
#include "acString.h"
#include <dbgrip.h>

#include "dbdynblk.h"




/**
  * \brief Permet de faire une selection quelconque sur les cotations d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsDimension(
    ads_name        ssName,
    const AcString  layerName  = "" );


/**
  * \brief Permet de faire une selection UNIQUE sur les cotations d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsOneDimension(
    ads_name        ssName,
    const AcString  layerName  = "" );



/**
  * \brief Permet de selectionner TOUS les cotations d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsAllDimension(
    ads_name        ssName,
    const AcString  layerName  = "" );


/**
 * \brief Renvoie un pointeur sur la cotation numero N dans la selection
 * \param ssName Pointeur sur la selection
 * \param iObject Index dans la selection de la cotation a recuperer, par defaut 0
 * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
 * \return Pointeur sur une cotation
 */
AcDbDimension* getDimensionFromSs(
    const ads_name&             ssName,
    const long&           iObject   =   0,
    const AcDb::OpenMode& opMode = AcDb::kForRead );


/**
 * \brief Renvoie un pointeur sur la cotation numero N dans la selection
 * \param ssName Pointeur sur la selection
 * \param iObject Index dans la selection de la cotation a recuperer, par defaut 0
 * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
 * \return Pointeur sur une cotation
 */
AcDbAlignedDimension* getAlignedDimensionFromSs(
    const ads_name&             ssName,
    const long&           iObject   =   0,
    const AcDb::OpenMode& opMode = AcDb::kForRead );

/**
 * \brief
 * \param
 * \return
 */
AcString getDimensionText( AcDbDimension* dimension );