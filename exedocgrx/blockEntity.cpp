﻿#pragma once
#include "config.h"
#include "blockEntity.h"
#include "list.h"
#include "print.h"

#define FMAP_ELEVATION_PATH strToAcStr( GABARIT_FOLDER_STR ).concat( _T( "Utilitaires\\FMAP_Elevation.dwt" ) );


long getSsBlock(
    ads_name&         ssName,
    const AcString&  layerName,
    const AcString&  blockName )
{
    return getSelectionSet( ssName, "", "INSERT", layerName, blockName, "" );
}



long getSsOneBlock(
    ads_name&         ssName,
    const AcString&  layerName,
    const AcString&  blockName )
{
    return getSingleSelection( ssName, "INSERT", layerName, blockName, "" );
}



long getSsAllBlock(
    ads_name&               ssName,
    const AcString&        layerName,
    const AcString&        blockName )
{
    return getSelectionSet( ssName, "X", "INSERT", layerName, blockName, "" );
}



AcDbBlockReference* getBlockFromSs(
    const ads_name& ssName,
    const long& iObject,
    const AcDb::OpenMode opMode )
{
    //Recuperer l'entite
    AcDbEntity* pEntityBlock = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbBlockReference::cast( pEntityBlock );
}



AcDbBlockReference* readBlockFromSs(
    const ads_name&     ssName,
    const long         iObject )
{
    //Recuperer l'entite
    return getBlockFromSs( ssName, iObject, AcDb::kForRead );
}



AcDbBlockReference* writeBlockFromSs(
    const ads_name&  ssName,
    const long&      iObject )
{
    //Recuperer l'entite
    return getBlockFromSs( ssName, iObject, AcDb::kForWrite );
}



AcDbAttribute* getAttributObject(
    AcDbBlockReference*   blockRef,
    const AcString&        attName,
    const AcDb::OpenMode&  opMode )
{
    //Creer un iterateur sur les attributs
    AcDbObjectIterator* iterAttribut = blockRef->attributeIterator();
    
    //Initialiser l'attribut a renvoyer
    AcDbAttribute* obAttTest   = NULL;
    AcDbAttribute* obAttReturn = NULL;
    
    //Iitialiser l'iteratuer
    iterAttribut->start();
    
    //Boucler sur les attribt
    while( !obAttReturn && !iterAttribut->done() )
    {
        //Recuperer l'objectId
        if( Acad::eOk == blockRef->openAttribute( obAttTest, iterAttribut->objectId(), opMode ) )
        {
            //Controler le nom de l'attribut
            if( attName == obAttTest->tag() )
            {
                obAttReturn = obAttTest;
                break;
            }
            
            else
                obAttTest->close();
        }
        
        //Iterer
        iterAttribut->step();
    }
    
    delete iterAttribut;
    iterAttribut = NULL;
    
    //Sortir
    return obAttReturn;
}


AcDbPolyline* getPolyMemberFromBlock( AcDbBlockReference* pBlock )
{
    //Ouvrir la definition du bloc
    AcDbBlockTableRecord* pBlockDef;
    acdbOpenObject( pBlockDef,  pBlock->blockTableRecord(), AcDb::kForRead );
    
    //Initialiser l'iterateur
    AcDbBlockTableRecordIterator* pIterator;
    pBlockDef->newIterator( pIterator );
    pIterator->start();
    
    //Initialiser les objets
    AcDbEntity* pEnt = NULL;
    AcDbPolyline* pPoly = NULL;
    
    //Iterer sur les objets
    while( !pPoly && !pIterator->done() )
    {
        //Recuperer l'entite
        pIterator->getEntity( pEnt, AcDb::kForRead );
        
        //Recuperer la polylign
        pPoly =  AcDbPolyline::cast( pEnt );
        
        //Cas d'une polyligne, on retourne un pointeur sur l'objet
        if( pPoly != NULL )
            return pPoly;
            
        //Autre objet
        else
        {
            //Fermer l'objet
            pEnt->close();
        }
        
        //Iterer
        pIterator->step();
    }
    
    //Effacer l'iterateur
    delete pIterator;
    pBlockDef->close();
    
    // Retourner la polyligne
    return NULL;
}


AcString getAttributValue(
    AcDbBlockReference*   p_obBlock,
    const AcString&        p_sAttName )
{
    //Recupeer l'attribut
    AcDbAttribute* l_obAtt = getAttributObject( p_obBlock, p_sAttName, AcDb::kForRead );
    AcString l_sAttValue = _T( "empty" );
    
    //Verifier que l'attribut existe
    if( l_obAtt )
    {
        //Recuperer la valeur
        l_sAttValue = l_obAtt->textString();
        
        //Fermer l'attribut
        l_obAtt->close();
    }
    
    //Sortir
    return l_sAttValue;
}



bool getStringAttributValue(
    AcDbBlockReference* p_obBlock,
    const AcString&     p_sAttName,
    AcString&           p_dAttValue )
{
    //Recupeer l'attribut
    AcDbAttribute* l_obAtt = getAttributObject( p_obBlock, p_sAttName, AcDb::kForRead );
    
    //Verifier que l'attribut existe
    if( l_obAtt )
    {
        //Recuperer la valeur
        p_dAttValue = l_obAtt->textString();
        
        //Fermer l'attribut
        l_obAtt->close();
        
        //Sortir
        return true;
    }
    
    //Sortir
    return false;
}


bool getDoubleAttributValue(
    AcDbBlockReference* p_obBlock,
    const AcString&     p_sAttName,
    double&             p_dAttValue )
{
    //Recuperer l'attribut sous forme de chaine
    AcString l_sAttValue = getAttributValue( p_obBlock, p_sAttName );
    
    //Recuperer l'attribut sous forme de nombre
    return stringToDouble( l_sAttValue, &p_dAttValue );
}


bool getIntAttributValue(
    AcDbBlockReference* p_obBlock,
    const AcString&     p_sAttName,
    int&                p_dAttValue )
{
    //Recuperer l'attribut sous forme de chaine
    AcString l_sAttValue = getAttributValue( p_obBlock, p_sAttName );
    
    //Recuperer l'attribut sous forme de nombre
    return stringToInt( l_sAttValue, &p_dAttValue );
}

bool setAttributValue(
    AcDbBlockReference*   blockRef,
    const AcString&        attName,
    const AcString&        value )
{
    //Recupeer l'attribut
    AcDbAttribute* att = getAttributObject( blockRef, attName, AcDb::kForWrite );
    
    //Verifier que l'attribut existe
    if( att )
    {
        //changer la valeur
        Acad::ErrorStatus es =  att->setTextString( value );
        
        
        //Fermer l'attribut
        att->close();
        
        return true;
    }
    
    //Sortir
    return false;
}

bool setAttributValue(
    AcDbBlockReference*   blockRef,
    const AcString&        attName,
    const double&          value,
    const int&             iAttDec )
{
    //Declarer le texte
    ACHAR valStr[ 255 ];
    
    //Recupoerer la valeur
    acdbRToS( value, 2, iAttDec, valStr );
    
    //Modifier l'attribut
    return setAttributValue( blockRef, attName, valStr );
}


Acad::ErrorStatus setPropertyFromBlock( AcDbBlockReference* pBlkRef,
    const AcString& propertyName,
    const double& value )
{
    Acad::ErrorStatus es;
    /// Pour recuperer les proprietes d'un block dynamique
    /// il faut creer un AcDbDynBlockReference qui prend en param l'Id d'un AcDbBlockReference
    
    AcDbDynBlockReference* pDynBlkRef = new AcDbDynBlockReference( pBlkRef );
    
    // on verifie que le bloc est bien dynamique
    if( pDynBlkRef->isDynamicBlock() )
    {
        // on recupere les proprietes du bloc
        AcDbDynBlockReferencePropertyArray propArray;
        pDynBlkRef->getBlockProperties( propArray );
        
        // on boucle sur les proprietes
        for( int j = 0; j < propArray.length(); j++ )
        {
            AcDbDynBlockReferenceProperty prop = propArray.at( j );
            
            // on recupere la propriete
            if( prop.propertyName() == propertyName )
            {
                AcDbEvalVariant eval( value );
                
                if( es = prop.setValue( eval ) )
                    return es;
            }
        }
    }
    
    delete pDynBlkRef;
    return pBlkRef->upgradeOpen();
}


Acad::ErrorStatus setPropertyFromBlock( AcDbBlockReference* pBlkRef,
    const AcString& propertyName,
    const AcString& value )
{
    Acad::ErrorStatus es;
    /// Pour recuperer les proprietes d'un block dynamique
    /// il faut creer un AcDbDynBlockReference qui prend en param l'Id d'un AcDbBlockReference
    
    AcDbDynBlockReference* pDynBlkRef = new  AcDbDynBlockReference( pBlkRef );
    
    // on verifie que le bloc est bien dynamique
    if( pDynBlkRef->isDynamicBlock() )
    {
        // on recupere les proprietes du bloc
        AcDbDynBlockReferencePropertyArray propArray;
        pDynBlkRef->getBlockProperties( propArray );
        
        // on boucle sur les proprietes
        for( int j = 0; j < propArray.length(); j++ )
        {
            AcDbDynBlockReferenceProperty prop = propArray.at( j );
            
            // on recupere la propriete
            if( prop.propertyName() == propertyName )
            {
                AcDbEvalVariant eval( value );
                
                if( es = prop.setValue( eval ) )
                    return es;
            }
        }
    }
    
    
    delete pDynBlkRef;
    return pBlkRef->upgradeOpen();
}

Acad::ErrorStatus setPropertyFromBlock( AcDbBlockReference* pBlkRef,
    const AcString& propertyName,
    const int& value )
{
    Acad::ErrorStatus es;
    /// Pour recuperer les proprietes d'un block dynamique
    /// il faut creer un AcDbDynBlockReference qui prend en param l'Id d'un AcDbBlockReference
    
    AcDbDynBlockReference* pDynBlkRef = new  AcDbDynBlockReference( pBlkRef );
    
    // on verifie que le bloc est bien dynamique
    if( pDynBlkRef->isDynamicBlock() )
    {
        // on recupere les proprietes du bloc
        AcDbDynBlockReferencePropertyArray propArray;
        pDynBlkRef->getBlockProperties( propArray );
        
        // on boucle sur les proprietes
        for( int j = 0; j < propArray.length(); j++ )
        {
            AcDbDynBlockReferenceProperty prop = propArray.at( j );
            
            // on recupere la propriete
            if( prop.propertyName() == propertyName )
            {
                Adesk::Int32 value32 = Adesk::Int32( value );
                AcDbEvalVariant eval( value32 );
                
                if( es = prop.setValue( eval ) )
                    return es;
            }
        }
    }
    
    
    delete pDynBlkRef;
    return pBlkRef->upgradeOpen();
}

double getPropertyFromBlock( AcDbBlockReference* pBlkRef,
    const AcString& propertyName )
{

    /// Pour recuperer les proprietes d'un block dynamique
    /// il faut creer un AcDbDynBlockReference qui prend en param l'Id d'un AcDbBlockReference
    
    AcDbDynBlockReference pDynBlkRef( pBlkRef );
    
    double value = 0.0;
    
    // on verifie que le bloc est bien dynamique
    if( pDynBlkRef.isDynamicBlock() )
    {
        // on recupere les proprietes du bloc
        AcDbDynBlockReferencePropertyArray propArray;
        pDynBlkRef.getBlockProperties( propArray );
        
        // on boucle sur les proprietes
        for( int j = 0; j < propArray.length(); j++ )
        {
            AcDbDynBlockReferenceProperty prop = propArray.at( j );
            
            // on recupere la propriete
            if( prop.propertyName() == propertyName )
            {
                AcDbEvalVariant eval = prop.value();
                eval.getValue( value );
                break;
            }
        }
    }
    
    else value = -1;
    
    
    Acad::ErrorStatus es = pBlkRef->upgradeOpen();
    
    return value;
}



AcString getPropertyStrFromBlock( AcDbBlockReference* pBlkRef,
    const AcString& propertyName )
{

    /// Pour recuperer les proprietes d'un block dynamique
    /// il faut creer un AcDbDynBlockReference qui prend en param l'Id d'un AcDbBlockReference
    
    AcDbDynBlockReference pDynBlkRef( pBlkRef );
    
    AcString value = _T( "" );
    
    // on verifie que le bloc est bien dynamique
    if( pDynBlkRef.isDynamicBlock() )
    {
        // on recupere les proprietes du bloc
        AcDbDynBlockReferencePropertyArray propArray;
        pDynBlkRef.getBlockProperties( propArray );
        
        // on boucle sur les proprietes
        for( int j = 0; j < propArray.length(); j++ )
        {
            AcDbDynBlockReferenceProperty prop = propArray.at( j );
            
            // on recupere la propriete
            if( prop.propertyName() == propertyName )
            {
                AcDbEvalVariant eval = prop.value();
                eval.getValue( value );
                break;
            }
        }
    }
    
    else value = "empty";
    
    
    Acad::ErrorStatus es = pBlkRef->upgradeOpen();
    
    return value;
}



AcDbObjectId getBlockDefId(
    const AcString&       blockName )
{
    //Recuperer la table des blocks
    AcDbBlockTable* blockTable = getBlockTable();
    
    //Verifier la liste des blocs
    if( !blockTable )
        return AcDbObjectId::kNull;
        
    //Declarer l'identifiant de la definition de bloc
    AcDbObjectId idBlockDefinition;
    
    //Recuperer la defintion du bloc
    if( Acad::eOk != blockTable->getAt( blockName, idBlockDefinition ) )
        idBlockDefinition.setNull();
        
    //Fermer la liste des blocs
    blockTable->close();
    
    //Sortir
    return idBlockDefinition;
}


AcDbObjectId getBlockDefId(
    const AcString&       blockName,
    AcDbDatabase* database )
{
    //Recuperer la table des blocks
    AcDbBlockTable* blockTable = getBlockTable( AcDb::kForRead, database );
    
    //Verifier la liste des blocs
    if( !blockTable )
        return AcDbObjectId::kNull;
        
    //Declarer l'identifiant de la definition de bloc
    AcDbObjectId idBlockDefinition;
    
    //Recuperer la defintion du bloc
    if( Acad::eOk != blockTable->getAt( blockName, idBlockDefinition ) )
        idBlockDefinition.setNull();
        
    //Fermer la liste des blocs
    blockTable->close();
    
    //Sortir
    return idBlockDefinition;
}


AcDbBlockTableRecord* getBlockDef(
    const AcString& blockName,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer la table des blocks
    AcDbBlockTable* blockTable = getBlockTable();
    
    //Verifier la liste des blocs
    if( !blockTable )
        return NULL;
        
    //Recuperer la defintion du bloc
    AcDbBlockTableRecord* blockTableRecord = NULL;
    
    if( Acad::eOk != blockTable->getAt( blockName, blockTableRecord, opMode ) )
        blockTableRecord = NULL;
        
    //Fermer la liste des blocs
    blockTable->close();
    
    //Sortir
    return blockTableRecord;
}


AcDbBlockTableRecord* getBlockDef(
    AcDbBlockReference* blockRef,
    const AcDb::OpenMode&  opMode )
{
    //Defintion du bloc
    AcDbBlockTableRecord* blockTableRecord = NULL;
    
    //Recuperer le nom du bloc
    AcString blockName;
    
    //Recuperer la definition du bloc en utilisant la methode par nom
    if( getStaticBlockName( blockName, blockRef ) )
        blockTableRecord = getBlockDef( blockName, opMode );
        
    return blockTableRecord;
}



AcDbBlockTableRecord* readBlockDef(
    const AcString&        blockName )
{
    return getBlockDef( blockName, AcDb::kForRead );
}



AcDbBlockTableRecord* writeBlockDef(
    const AcString&        blockName )
{
    return getBlockDef( blockName, AcDb::kForWrite );
}


void drawBlock(
    const AcString& blockName,
    const AcGePoint3d& position,
    const double& angle,
    const int& colorIndex )
{
    // On crée la référence de bloc
    AcDbBlockReference* blockRef = insertBlockReference( blockName,
            position );
            
    // Safeguard pour si la définition du bloc n'est pas dans le dessin
    if( !blockRef )
        return;
        
    // On tourne le bloc
    blockRef->setRotation( angle );
    
    //Couleur
    AcCmColor colorBlock;
    
    //Setter la couleur du block
    colorBlock.setColorIndex( colorIndex );
    blockRef->setColor( colorBlock );
    
    // On la ferme
    blockRef->close();
}


void drawBlock(
    const AcString& blockName,
    const AcString& blockLayer,
    const AcGePoint3d& position,
    const double& angle,
    const int& colorIndex )

{
    // On crée la référence de bloc
    AcDbBlockReference* blockRef = insertBlockReference( blockName, blockLayer, position );
    
    // Safeguard pour si la définition du bloc n'est pas dans le dessin
    if( !blockRef )
        return;
        
    // On tourne le bloc
    blockRef->setRotation( angle );
    
    //Couleur
    AcCmColor colorBlock;
    
    //Setter la couleur du block
    colorBlock.setColorIndex( colorIndex );
    blockRef->setColor( colorBlock );
    
    // On la ferme
    blockRef->close();
}

Acad::ErrorStatus drawBlockReference(
    AcDbBlockReference* pBlkRef,
    AcDbObjectId blockId,
    AcGePoint3d position,
    double angle,
    double scaleX,
    double scaleY,
    double scaleZ,
    const AcGeVector3d& vecNormal )
{
    // on lui attribue la definition de bloc qu'on a recupere un peu plus haut
    pBlkRef->setBlockTableRecord( blockId );
    
    // On applique la normal avant les autres modifications
    pBlkRef->setNormal( vecNormal );
    
    // mofifier le point d'insertion
    pBlkRef->setPosition( position );
    
    // mofifier l'angle d'insertion
    pBlkRef->setRotation( angle );
    
    // mofifier l'echelle
    setScaleFactors( pBlkRef, scaleX, scaleY, scaleZ );
    
    // on l'ajoute a la DataBase
    addToModelSpace( pBlkRef );
    
    //Ajouter tosu els attibuts au bloc
    return ( addAllAttributeToBlock( pBlkRef ) == true ) ? eOk : eInvalidInput;
}



Acad::ErrorStatus drawBlockReference(
    AcDbBlockReference* pBlkRef,
    AcString blockName,
    AcGePoint3d position,
    double angle,
    double scaleX,
    double scaleY,
    double scaleZ,
    const AcGeVector3d& vecNormal )
{
    //Recuperer l'identifiant de la definition de bloc
    AcDbObjectId blockId = getBlockDefId( blockName );
    
    //Verifier si la definition
    if( blockId.isNull() )
    {
        //Initialiser le message
        AcString sMessage = "\nLa definition du bloc " + blockName + " est introuvable.";
        
        //Affricher le message
        acutPrintf( sMessage );
        
        //Sortir
        return eNullEntityPointer;
    }
    
    //Insererle bloc
    return drawBlockReference( pBlkRef,
            blockId,
            position,
            angle,
            scaleX,
            scaleY,
            scaleZ,
            vecNormal );
}


AcDbBlockReference* insertBlockReference(
    AcDbObjectId blockId,
    AcGePoint3d position,
    double angle,
    double scaleX,
    double scaleY,
    double scaleZ,
    const AcGeVector3d& vecNormal )
{
    //Initialiser le bloc
    AcDbBlockReference* pBlkRef = new AcDbBlockReference( position, blockId );
    
    if( !pBlkRef )
        return NULL;
        
    //On applique la normal avant les autres modifications
    pBlkRef->setNormal( vecNormal );
    
    //Après un setNormal il faut refaire un setPosition car le setNormal déplace le bloc ...
    pBlkRef->setPosition( position );
    
    //Modifier l'angle d'insertion
    pBlkRef->setRotation( angle );
    
    //Mofifier l'echelle
    setScaleFactors( pBlkRef, scaleX, scaleY, scaleZ );
    
    //On l'ajoute a la DataBase
    addToModelSpace( pBlkRef );
    
    //Ajouter tous les attibuts au bloc
    if( !addAllAttributeToBlock( pBlkRef ) )
        return NULL;
        
    //Sortir
    return pBlkRef;
}


AcDbBlockReference* insertBlockReference(
    AcString blockName,
    AcGePoint3d position,
    double angle,
    double scaleX,
    double scaleY,
    double scaleZ,
    const AcGeVector3d& vecNormal )
{
    //Recuperer l'identifiant de la definition de bloc
    AcDbObjectId blockId = getBlockDefId( blockName );
    
    //Verifier si la definition existe
    if( blockId.isNull() )
    {
        //Si c'est un bloc FMAP_ELEVATION on recupere le dwt
        if( blockName == _T( "FMAP_Elevation" ) )
        {
            //Recuperer le nom du fichier
            AcString blockGabaritPath = FMAP_ELEVATION_PATH;
            
            //Inserer une definition du bloc dans le dessin
            AcDbObjectId idFmapElevation = importExternBlockDefinition( blockGabaritPath,
                    blockName );
                    
            //Verifier
            if( !idFmapElevation.isValid() )
            {
                //Afficher message
                acutPrintf( _T( "\nErreur pendant l'insertion du definition du block FMAP_ELEVATION externe" ) );
                
                return NULL;
            }
            
            //Inserer le bloc
            return insertBlockReference( idFmapElevation,
                    position,
                    angle,
                    scaleX,
                    scaleY,
                    scaleZ,
                    vecNormal );
        }
        
        //Sinon retourner NULL
        else
            return NULL;
    }
    
    //Insere le bloc
    return insertBlockReference(
            blockId,
            position,
            angle,
            scaleX,
            scaleY,
            scaleZ,
            vecNormal );
}

AcDbBlockReference* insertBlockReference(
    const AcString& blockName,
    const AcString& blockLayer,
    AcGePoint3d position,
    double angle,
    double scaleX,
    double scaleY,
    double scaleZ,
    const AcGeVector3d& vecNormal )

{
    //Recuperer l'identifiant de la definition de bloc
    AcDbObjectId blockId = getBlockDefId( blockName );
    
    //Verifier si la definition
    if( blockId.isNull() )
    {
        //Si c'est un bloc FMAP_ELEVATION on recupere le dwt
        if( blockName == _T( "FMAP_Elevation" ) )
        {
            //Recuperer le nom du fichier
            AcString blockGabaritPath = FMAP_ELEVATION_PATH;
            
            //Inserer une definition du bloc dans le dessin
            AcDbObjectId idFmapElevation = importExternBlockDefinition( blockGabaritPath,
                    blockName );
                    
            //Verifier
            if( !idFmapElevation.isValid() )
            {
                //Afficher message
                acutPrintf( _T( "\nErreur pendant l'insertion du definition du block FMAP_ELEVATION externe" ) );
                
                return NULL;
            }
            
            //Inserer le bloc
            return insertBlockReference( idFmapElevation,
                    position,
                    angle,
                    scaleX,
                    scaleY,
                    scaleZ,
                    vecNormal );
        }
        
        //Sinon retourner NULL
        else
            return NULL;
    }
    
    //Inserer le bloc
    AcDbBlockReference* pBlockRef = insertBlockReference(
            blockId,
            position,
            angle,
            scaleX,
            scaleY,
            scaleZ,
            vecNormal );
            
    if( pBlockRef )
    {
        // Vérification du nom de calque
        if( !blockLayer.isEmpty() )
            pBlockRef->setLayer( blockLayer );
    }
    
    return pBlockRef;
}

bool addAllAttributeToBlock(
    AcDbBlockReference* pBlkRef )
{

    AcString name = _T( "" );
    
    ErrorStatus es = eNotApplicable;
    
    if( isDynamicBlock( pBlkRef ) )
        es = getDynamicBlockName( name, pBlkRef );
        
    else
        es = getStaticBlockName( name, pBlkRef );
        
    if( es != eOk )
        return false;
        
    // Récuperer la base de donées du dessin courant
    AcDbDatabase* pDB = acdbHostApplicationServices()->workingDatabase();
    
    // Récupérer la table des blocs
    AcDbBlockTable* pBlockTable = NULL;
    pDB->getSymbolTable( pBlockTable, AcDb::kForRead );
    
    // Ouvrir la definition du block
    AcDbBlockTableRecord* pBlockDef;
    pBlockTable->getAt( name,
        pBlockDef,
        AcDb::kForRead );
        
    // Fermer la table des blocs
    pBlockTable->close();
    
    if( !pBlockDef )
        return false;
        
    // on set un iterateur qui va iterer sur les attributs du bloc
    AcDbBlockTableRecordIterator* pIterator = NULL;
    pBlockDef->newIterator( pIterator );
    
    AcDbEntity* pEnt = NULL;
    AcDbAttributeDefinition* pAttdef = NULL;
    
    // Recuperer l'angle du bloc
    double angle = pBlkRef->rotation();
    
    // On récupère la position de la référence
    AcGeMatrix3d transformation = pBlkRef->blockTransform();
    
    // On itere sur les attributs
    for( pIterator->start(); !pIterator->done(); pIterator->step() )
    {
    
        // On recupere l'entite pointee par l'it
        pIterator->getEntity( pEnt, AcDb::kForRead );
        
        // On s'assure que l'entite est bien un attribut et pas une constante
        pAttdef = AcDbAttributeDefinition::cast( pEnt );
        
        if( pAttdef != NULL && !pAttdef->isConstant() )
        {
            // On cree un attribut
            AcDbAttribute* pAtt = new AcDbAttribute();
            
            // On copie l'attribut de la définition dans le nouvel attribut
            pAtt->setAttributeFromBlock( pAttdef, transformation );
            
            // On ajoute l'attribut a la reference de block
            AcDbObjectId attId;
            pBlkRef->appendAttribute( attId, pAtt );
            pAtt->close();
        }
        
        pEnt->close();
    }
    
    // On n'oublie aps de supprimer l'it
    delete pIterator;
    pIterator = NULL;
    
    // On n'oublie aps de fermer la definition du bloc
    pBlockDef->close();
    
    // Sortir
    return true;
}

bool setScaleFactors(
    AcDbBlockReference* pBlkRef,
    double scaleX,
    double scaleY,
    double scaleZ )
{
    //Verifier l'echelle X
    if( scaleX == 0.0 )
        return false;
        
    //Verifier l'echelle Y
    if( scaleY == 0.0 )
        scaleY = scaleX;
        
    //Verifier l'echelle Z
    if( scaleZ == 0.0 )
        scaleZ = scaleX;
        
    //Changer l'echelle
    return Acad::eOk == pBlkRef->setScaleFactors( AcGeScale3d( scaleX, scaleY, scaleZ ) );
}


AcDbBlockTable* getBlockTable( const AcDb::OpenMode& p_openMode )
{
    //Declarer la table des calques
    AcDbBlockTable* l_blockTable;
    
    //Recuperer la liste
    if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->getSymbolTable( l_blockTable, p_openMode ) )
        return NULL;
        
    //Sorir
    return l_blockTable;
}


AcDbBlockTable* getBlockTable(
    const AcDb::OpenMode& opMode,
    AcDbDatabase* database )
{
    //Declarer la table des calques
    AcDbBlockTable* l_blockTable;
    
    if( Acad::eOk != database->getSymbolTable( l_blockTable, opMode ) )
        return NULL;
        
    //Sorir
    return l_blockTable;
}


AcDbObjectId importExternBlockDefinition(
    const AcString& sFilePath,
    const AcString& sBlockName )
{
    //Tanter de recuperer l'identifiant du bloc
    AcDbObjectId idBlockDef = getBlockDefId( sBlockName );
    
    //Verifier si le bloc existe
    if( !idBlockDef.isNull() )
    {
        //Sortir
        return idBlockDef;
    }
    
    //Declarer la base de données externe
    AcDbDatabase* dbExtern = new AcDbDatabase( Adesk::kFalse );
    
    //Ouvrir la base de données externe
    if( Acad::eOk != dbExtern->readDwgFile( sFilePath ) )
    {
        //Sortir
        dbExtern->close();
        return AcDbObjectId::kNull;
    }
    
    //Inserer la definition de bloc
    if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->insert( idBlockDef, sBlockName, sBlockName, dbExtern ) )
    {
        //Sortir
        dbExtern->close();
        return AcDbObjectId::kNull;
    }
    
    //Sortir
    return idBlockDef;
}



bool setBlockPosition(
    AcDbBlockReference* obBlockRef,
    const AcGePoint3d&  ptInsert )
{
    //Creer la matrice de rotation
    AcGeMatrix3d matTranslation = AcGeMatrix3d::translation(
            ptInsert - obBlockRef->position() );
            
    //Transformer le bloc
    return Acad::eOk == obBlockRef->transformBy( matTranslation );
}


bool setBlockRotation(
    AcDbBlockReference* obBlockRef,
    const double&        dRotation )
{
    //Creer la matrice de rotation
    AcGeMatrix3d matRotation = AcGeMatrix3d::rotation(
            dRotation - obBlockRef->rotation(),
            obBlockRef->normal(),
            obBlockRef->position() );
            
    //Transformer le bloc
    return Acad::eOk == obBlockRef->transformBy( matRotation );
    
}

bool setBlockPositionRotation(
    AcDbBlockReference* obBlockRef,
    const AcGePoint3d&  ptInsert,
    const double&       dRotation )
{
    //Creer la matrice de rotation
    AcGeMatrix3d matRotation = AcGeMatrix3d::rotation(
            dRotation - obBlockRef->rotation(),
            obBlockRef->normal(),
            obBlockRef->position() );
            
    //Creer la matrice de rotation
    AcGeMatrix3d matTranslation = AcGeMatrix3d::translation(
            ptInsert - obBlockRef->position() );
            
    //Transformer le bloc
    return Acad::eOk == obBlockRef->transformBy( matTranslation * matRotation );
}


ErrorStatus getStaticBlockName(
    AcString& name,
    AcDbBlockReference* obBlockRef )
{
    ErrorStatus es = eOk;
    
    //Recuperer le nom du bloc à partir de l'id de la définition du bloc
    const AcDbObjectId idBlockRecord = obBlockRef->blockTableRecord();
    return getStaticBlockName( name, idBlockRecord );
}


ErrorStatus getStaticBlockName(
    AcString& name,
    const AcDbObjectId& idBlockRecord )
{
    ErrorStatus es = eOk;
    
    // Ouvre la definition de bloc
    AcDbBlockTableRecord* blockRecord;
    
    if( es = acdbOpenObject( ( AcDbObject*& )blockRecord, idBlockRecord, AcDb::kForRead ) )
        return es;
        
    //on recupère le nom du bloc
    if( es = blockRecord->getName( name ) )
        return es;
        
    //on ferme la definition du bloc
    blockRecord->close();
    
    //on retourne le resultat
    return es;
}


AcDbBlockReference* insertDynamicBlockReference( const AcString& blockName,
    const AcGePoint3d& position,
    const AcStringArray& propNames,
    vector< void* >& values,
    const AcString& blockLayer,
    const double& rotation,
    const double& scaleX,
    const double& scaleY,
    const double& scaleZ,
    const AcGeVector3d& vecNormal )
{
    // On vérifie qu'il n'y ait pas de problème avec les paramètres
    int size = propNames.length();
    
    if( size != values.size() )
    {
        acutPrintf( _T( "Le nombre de valeurs et de propriétés passées dans la fonction insertDynamicBlock est différent" ) );
        return NULL;
    }
    
    // On insère la référence de bloc
    AcDbBlockReference* pBlock = insertBlockReference( blockName,
            blockLayer,
            position,
            rotation,
            scaleX,
            scaleY,
            scaleZ,
            vecNormal );
            
    if( !pBlock )
        return NULL;
        
    // On vérfie que le bloc soit bien dynamique
    if( !isDynamicBlock( pBlock ) )
    {
        acutPrintf( _T( "\nBlock <%s> n'est pas un bloc dynamique" ), blockName );
        pBlock->erase();
        pBlock->close();
        return NULL;
    }
    
    // On crée le bloc dynamique
    AcDbDynBlockReference pDynRef( pBlock );
    
    // On récupère les propriétés du bloc dynamique
    AcDbDynBlockReferencePropertyArray propArr;
    pDynRef.getBlockProperties( propArr );
    
    // On boucle sur les propriétés du bloc
    int length = propArr.length();
    ACHAR buf[ 512 ];
    AcDbDynBlockReferenceProperty prop;
    
    for( int i = 0; i < length; i++ )
    {
        // On récupère la propriété courante
        prop = propArr[ i ];
        AcDbEvalVariant pValVar = prop.value();
        int iVar = 0;
        
        // On regarde si une des propriétés que l'on a demandé de setter
        // correspond à la propriété courante
        if( propNames.find( prop.propertyName(), iVar ) )
        {
            short rtype = pValVar.restype;
            
            // On est sur un double
            if( rtype >= 40 && rtype <= 59 && rtype != 48 ||
                rtype >= 140 && rtype <= 147 )
            {
                // On set la valeur du double sur la propriété
                pValVar.resval.rreal = *( ( double* )( values[ iVar ] ) );
            }
            
            // On est sur un int
            else if( rtype >= 61 && rtype <= 79 && rtype != 62 && rtype != -7 ||
                rtype >= 170 && rtype <= 175 )
            {
                // On set la valeur de int sur la propriété
                pValVar.resval.rint = *( ( int* )( values[ iVar ] ) );
            }
            
            // On est sur une string
            else if( rtype >= 1 && rtype <= 4 ||
                rtype == 7 ||
                rtype == 9 ||
                rtype >= 300 && rtype <= 309 )
                pValVar.resval.rstring = ( ACHAR* )( values[ iVar ] );
                
            // On est sur un AcGePoint3d
            else if( rtype >= 10 && rtype <= 17 )
                pValVar = AcDbEvalVariant( * ( AcGePoint3d* )( values[ iVar ] ) );
                
            prop.setValue( pValVar );
        }
    }
    
    return pBlock;
}


vector <AcString> getBlockList()
{
    //Vecteur contenant les noms du bloc dans le dessin
    vector <AcString> blockList;
    
    //on recupère la table des blocs
    AcDbBlockTable* bTable = getBlockTable();
    
    //Iterateur sur les blocs dans la table
    AcDbBlockTableIterator* bTableIter;
    bTable->newIterator( bTableIter );
    
    AcDbObject* obj = NULL;
    
    //on parcourt la liste des blocs dans la table
    for( bTableIter; !bTableIter->done(); bTableIter->step() )
    {
        //On recupère l'id du bloc
        AcDbObjectId blockId = bTableIter->getRecordId();
        
        //On recupère la définition de bloc à partir de son id
        AcDbObject* obj = NULL;
        
        if( Acad::eOk != acdbOpenAcDbObject( obj, blockId, AcDb::kForWrite ) )
            continue;
            
        AcDbBlockTableRecord* bRec = AcDbBlockTableRecord::cast( obj );
        
        if( !bRec ) continue;
        
        //On recupère le nom du bloc
        AcString blcName;
        bRec->getName( blcName );
        
        //On ajoute le nom du bloc dans le vecteur s'il ne commence pas par *
        int pos = blcName.find( _T( "*" ) );
        
        if( -1 == pos )
            blockList.push_back( blcName );
            
        //on ferme la définition du bloc
        bRec->close();
    }
    
    //on supprime l'iterateur
    delete bTableIter;
    
    //on ferme l'objet
    bTable->close();
    
    //on renvoie le résultat
    return blockList;
}



map <AcString, AcString> getBlockAttWithValuesList( AcDbBlockReference* bRef )
{
    //on initialise le map
    map <AcString, AcString> mapAttVal;
    
    //On crée un itérateur pour itérer les attribut dans le bloc
    AcDbObjectIterator* attIt = bRef->attributeIterator();
    
    //on initialise l'attribut
    AcDbAttribute* obAttName   = NULL;
    
    //on boucle sur tous les attributs du bloc
    for( attIt; !attIt->done(); attIt->step() )
    {
        //On ouvre l'attribut
        if( Acad::eOk != bRef->openAttribute( obAttName, attIt->objectId(), AcDb::kForRead ) )
            continue;
            
        //on recupère l'attribut et sa valeur
        mapAttVal.insert( pair<AcString, AcString>( obAttName->tag(), obAttName->textString() ) );
        
        //on ferme l'attribut
        obAttName->close();
    }
    
    //on supprime l'itérateur
    delete attIt;
    
    //on renvoie le résultat
    return mapAttVal;
}


std::vector<AcGePoint3d> getInsertPointOfBlockInLayer( const AcString& layerName )
{
    //Declaration du vecteur de sortie
    std::vector<AcGePoint3d> vectRes;
    
    //Declarer la selection sur les blocs
    ads_name ssBlock;
    
    //Recuperer les blocs dans la calque
    long nbrBlock = getSsAllBlock( ssBlock, layerName );
    
    //Boucle sur la selection des blocs
    for( int i = 0; i < nbrBlock; i++ )
    {
        //Declaration du reference de bloc
        AcDbBlockReference* obBlock3D = NULL;
        
        //Recuperer le bloc
        if( obBlock3D = readBlockFromSs( ssBlock, i ) )
        {
            //Recuperer le point d'insertion du bloc
            AcGePoint3d ptInBlock = obBlock3D->position();
            
            //Ajouter le position du bloc dans le vecteur
            vectRes.push_back( ptInBlock );
            
            //Fermer le bloc
            obBlock3D->close();
            obBlock3D = NULL;
            
            
        }
    }
    
    //Liberer la selection sur les blocs
    acedSSFree( ssBlock );
    
    //Retourner le vecteur de point d'insertion du bloc 3d
    return vectRes;
}



std::vector<AcGePoint3d> getInsertPointOfBlockInLayer( const AcString& layerName,
    std::vector<double>& vectX )
{
    //Declaration du vecteur de sortie
    std::vector<AcGePoint3d> vectRes;
    
    //Declarer la selection sur les blocs
    ads_name ssBlock;
    
    //Recuperer les blocs dans la calque
    long nbrBlock = getSsAllBlock( ssBlock, layerName );
    
    //Boucle sur la selection des blocs
    for( int i = 0; i < nbrBlock; i++ )
    {
        //Declaration du reference de bloc
        AcDbBlockReference* obBlock3D = NULL;
        
        //Recuperer le bloc
        if( obBlock3D = readBlockFromSs( ssBlock, i ) )
        {
            //Recuperer le point d'insertion du bloc
            AcGePoint3d ptInBlock = obBlock3D->position();
            
            //Ajouter le position du bloc dans le vecteur
            vectRes.push_back( ptInBlock );
            
            //Ajouter le x du bloc dans le vecteur de double
            vectX.push_back( ptInBlock.x );
            
            //Fermer le bloc
            obBlock3D->close();
            obBlock3D = NULL;
        }
    }
    
    //Liberer la selection sur les blocs
    acedSSFree( ssBlock );
    
    //Retourner le vecteur de point d'insertion du bloc 3d
    return vectRes;
}



std::vector<AcGePoint3d> getInsertPointOfSSBlock( const ads_name& ssBlock )
{
    //Declaration du resultat
    std::vector<AcGePoint3d> vectRes;
    
    //Recuperer la taille de la selection
    long taille = getSsLength( ssBlock );
    
    //Boucle pour ajouter les sommets des blocs dans le vecteur
    for( long counter = 0; counter < taille; counter++ )
    {
        // Récuperer la poly3D
        if( AcDbBlockReference* block = readBlockFromSs( ssBlock, counter ) )
        {
            //On garde le point d'insertion
            vectRes.push_back( block->position() );
            
            //On ferme le bloc
            block->close();
            block = NULL;
        }
    }
    
    //Retourner le resultat
    return vectRes;
}

Acad::ErrorStatus getInsertPointOfSSBlock( const ads_name& ssBlock,
    std::vector<AcGePoint3d>& vecZ,
    std::vector<AcGePoint3d>& vec0 )
{
    //Declaration du resultat
    Acad::ErrorStatus es = Acad::eNotApplicable;
    
    //Recuperer la taille de la selection
    long taille = getSsLength( ssBlock );
    
    //Barre de progression
    ProgressBar prog = ProgressBar( _T( "Préparation des points" ), taille );
    
    //Boucle pour ajouter les sommets des blocs dans le vecteur
    for( long counter = 0; counter < taille; counter++ )
    {
        // Récuperer la poly3D
        if( AcDbBlockReference* block = readBlockFromSs( ssBlock, counter ) )
        {
            //Recuperer le point d'insertion du bloc
            AcGePoint3d ptIns = block->position();
            
            //Tester si le z du point
            if( ptIns.z == 0 )
                vec0.push_back( ptIns );
            else
                vecZ.push_back( ptIns );
                
            //On ferme le bloc
            block->close();
            block = NULL;
        }
        
        //Iterer la barre de progression
        prog.moveUp( counter );
    }
    
    //Retourner le resultat
    return Acad::eOk;
}


AcGePoint3dArray getInsertPointArrayOfBlockInLayer( const AcString& layerName )
{
    //Declaration du vecteur de sortie
    AcGePoint3dArray vectRes;
    
    //Declarer la selection sur les blocs
    ads_name ssBlock;
    
    //Recuperer les blocs dans la calque
    long nbrBlock = getSsAllBlock( ssBlock, layerName );
    
    //Boucle sur la selection des blocs
    for( int i = 0; i < nbrBlock; i++ )
    {
        //Declaration du reference de bloc
        AcDbBlockReference* obBlock3D = NULL;
        
        //Recuperer le bloc
        if( obBlock3D = readBlockFromSs( ssBlock, i ) )
        {
            //Recuperer le point d'insertion du bloc
            AcGePoint3d ptInBlock = obBlock3D->position();
            
            //Ajouter le position du bloc dans le vecteur
            vectRes.push_back( ptInBlock );
            
            //Fermer le bloc
            obBlock3D->close();
        }
    }
    
    //Liberer la selection sur les blocs
    acedSSFree( ssBlock );
    
    //Retourner le vecteur de point d'insertion du bloc 3d
    return vectRes;
}


AcGePoint3dArray getInsertPointArrayOfBlockInLayer( const AcString& layerName,
    vector<double>& vectX )
{
    //Declaration du vecteur de sortie
    AcGePoint3dArray vectRes;
    
    //Declarer la selection sur les blocs
    ads_name ssBlock;
    
    //Recuperer les blocs dans la calque
    long nbrBlock = getSsAllBlock( ssBlock, layerName );
    
    //Boucle sur la selection des blocs
    for( int i = 0; i < nbrBlock; i++ )
    {
        //Declaration du reference de bloc
        AcDbBlockReference* obBlock3D = NULL;
        
        //Recuperer le bloc
        if( obBlock3D = readBlockFromSs( ssBlock, i ) )
        {
            //Recuperer le point d'insertion du bloc
            AcGePoint3d ptInBlock = obBlock3D->position();
            
            //Ajouter la position du bloc dans le vecteur
            vectRes.push_back( ptInBlock );
            
            //Ajouter la position du bloc dans le vecteur
            vectX.push_back( ptInBlock.x );
            
            //Fermer le bloc
            obBlock3D->close();
        }
    }
    
    //Liberer la selection sur les blocs
    acedSSFree( ssBlock );
    
    //Retourner le vecteur de point d'insertion du bloc 3d
    return vectRes;
}


Acad::ErrorStatus getInsertPointArrayOfSSBlock( const ads_name& ssBlock,
    AcGePoint3dArray& arPt,
    vector< double >& vecX,
    const bool& is2D )
{
    Acad::ErrorStatus es = eNotApplicable;
    
    //Recuperer la taille de la selection
    long taille = getSsLength( ssBlock );
    
    if( taille == 0 )
        return es;
        
    //Boucle pour ajouter les sommets des blocs dans le vecteur
    for( long counter = 0; counter < taille; counter++ )
    {
        // Récuperer le bloc
        if( AcDbBlockReference* block = readBlockFromSs( ssBlock, counter ) )
        {
            //On garde le point d'insertion
            AcGePoint3d blkPos = block->position() ;
            
            if( is2D )
                arPt.append( AcGePoint3d( blkPos.x, blkPos.y, 0 ) );
            else
                arPt.append( blkPos );
                
            //On sauvearde les X
            vecX.push_back( blkPos.x );
            
            
            //On ferme le bloc
            block->close();
        }
        
        else
            return Acad::eNotThatKindOfClass;
    }
    
    //Retourner le resultat
    return eOk;
}


Acad::ErrorStatus prepareBlocks(
    ads_name& ssBlock,
    vector< double >& xPos,
    AcGePoint3dArray& blockPos,
    AcDbObjectIdArray& blockIds )
{
    //Explication
    //Ici on fait des tris dans le vecteur de double
    //pour diminuer le nombre d'iteration dans la suppression
    //des occurrences de point3d dans les AcArrays en paramètres
    
    //Longueur de la sélection
    long length = 0;
    acedSSLength( ssBlock,
        &length );
        
    //Si la selection est vide
    if( length == 0 )
        return Acad::eNotApplicable;
        
    //Boucle sur la selection de bloc
    AcDbBlockReference* blockTopo = NULL;
    AcGePoint3d blockP = AcGePoint3d::kOrigin;
    
    for( int i = 0; i < length; i++ )
    {
        //Recuperer le i-ème block
        blockTopo = getBlockFromSs( ssBlock, i, AcDb::kForRead );
        
        if( blockTopo )
        {
            //Recuperer la position du bloc
            blockP = blockTopo->position();
            
            //Ajouter le x du point dans le vecteur xPos
            xPos.push_back( blockP.x );
            
            //Ajouter le point d'insertion du block dans le blockPos
            blockPos.append( blockP );
            
            //Ajouter l'objectId du block dans le blockIds
            blockIds.append( blockTopo->id() );
            
            //Fermer le bloc
            blockTopo->close();
            blockTopo = NULL;
        }
    }
    
    //Iterateur
    int i, min, j;
    
    //Initialisation de la barre de progression
    int size = xPos.size();
    ProgressBar prog = ProgressBar( _T( "Préparation des blocs" ), size );
    
    //Boucle de tri
    for( i = 0; i < size - 1; i++ )
    {
        //Recuperer l'index du min
        min = i;
        
        //Boucle de test
        for( j = i + 1; j < size; j++ )
        {
            if( xPos[ j ] < xPos[ min ] )
                min = j;
        }
        
        //Swaper le vecteur et les AcArrays
        if( min != i )
        {
            std::swap( xPos[ i ], xPos[ min ] );
            blockPos.swap( i, min );
            blockIds.swap( i, min );
        }
        
        //Progresser la barre
        prog.moveUp( i );
    }
    
    //Retourner eOk
    return Acad::eOk;
}


Acad::ErrorStatus cleanDoublons( std::vector<double>& xPos,
    AcGePoint3dArray& blockPos,
    AcDbObjectIdArray& blockIds,
    const double& precision )
{
    if( xPos.size() != blockPos.size() || xPos.size() != blockIds.size() )
        return Acad::eNotApplicable;
        
    //Compteur
    int compt = 0;
    
    //Initialisation de la barre de progression
    int size = xPos.size();
    ProgressBar prog = ProgressBar( _T( "Progression" ), size );
    
    //Initialisation des itérateurs
    vector< double >::iterator xPosBegin = xPos.begin();
    AcGePoint3d* blockPosBegin = blockPos.begin();
    AcDbObjectId* blockIdsBegin = blockIds.begin();
    
    //Declarer une entité
    AcDbEntity* entity;
    
    //Boucle sur le vecteur de x
    int counter = 0;
    
    if( size == 0 )
        return eOk;
        
    for( int i = 0; i < size - 1; i++ )
    {
        double x1 = xPos[ i ];
        
        for( int o = i + 1; o < size; o++ )
        {
            double x2 = xPos[ o ];
            
            //Tester si le premier point et le second point sont égaux
            if( isTheSame( x1, x2, precision ) )
            {
                //Tester s'il on le même y et z
                if( isEqual3d( blockPos[ i ], blockPos[ o ], precision ) )
                {
                    //1. Suppresion de l'ntité
                    
                    //Ouvrir l'entité
                    if( Acad::eOk != acdbOpenAcDbEntity( entity, blockIds[o], AcDb::kForWrite ) )
                        return Acad::eNotApplicable;
                        
                    //Supprimer l'entité
                    entity->erase();
                    entity->close();
                    
                    //2. Arrangement des index
                    
                    //Supprimer le doublons dans xPos
                    xPos.erase( xPosBegin + o );
                    
                    //Supprimer le point dans blockPos
                    blockPos.erase( blockPosBegin + o );
                    
                    //Supprimer l'objectId dans blockIds
                    blockIds.erase( blockIdsBegin + o );
                    
                    //Increment du compteur
                    compt++;
                    
                    //Progresser la barre
                    prog.moveUp( i );
                    
                    i--;
                    size--;
                    
                    //On met à jours les itérateurs
                    xPosBegin = xPos.begin();
                    blockPosBegin = blockPos.begin();
                    blockIdsBegin = blockIds.begin();
                    continue;
                }
            }
            
            else
                continue;
        }
    }
    
    //Afficher une message
    acutPrintf( _T( "\nBlocs topos supprimés: %d" ), compt );
    
    //Retourner le resultat
    return eOk;
}


Acad::ErrorStatus cleanDoublonsWithZ( std::vector<double>& xPos,
    AcGePoint3dArray& blockPos,
    AcDbObjectIdArray& blockIds,
    const double& tolXY,
    const double& tolZ )
{
    //Compteur
    int compt = 0;
    
    //Initialisation de la barre de progression
    int size = xPos.size();
    ProgressBar prog = ProgressBar( _T( "Progression" ), size );
    
    //Initialisation des itérateurs
    vector< double >::iterator xPosBegin = xPos.begin();
    AcGePoint3d* blockPosBegin = blockPos.begin();
    AcDbObjectId* blockIdsBegin = blockIds.begin();
    
    //Declarer une entité
    AcDbEntity* entity;
    
    //Boucle sur le vecteur de x
    for( int i = 1; i < size; i++ )
    {
        //Tester si le premier point et le second point sont égaux
        if( isTheSame( xPos[ i - 1 ], xPos[ i ], tolXY ) )
        {
            //Tester s'il on le même y et z
            if( isEqual3d( blockPos[ i - 1 ], blockPos[ i ], tolZ ) )
            {
                //Supprimer le doublons dans xPos
                xPos.erase( xPosBegin + i );
                
                //Ouvrir l'entité
                if( Acad::eOk != acdbOpenAcDbEntity( entity, blockIds[i], AcDb::kForWrite ) )
                    return Acad::eNotApplicable;
                    
                //Supprimer l'entité
                entity->erase();
                entity->close();
                
                //Supprimer le point dans blockPos
                blockPos.erase( blockPosBegin + i );
                
                //Supprimer l'objectId dans blockIds
                blockIds.erase( blockIdsBegin + i );
                
                //Increment du compteur
                compt++;
                
                //Progresser la barre
                prog.moveUp( i );
                
                i--;
                size--;
            }
        }
    }
    
    //Afficher une message
    acutPrintf( _T( "\n%d Blocks Topos Supprimés" ), compt );
    
    //Retourner le resultat
    return eOk;
}


Acad::ErrorStatus
openBlock( const AcDbObjectId& idBlock,
    AcDbBlockReference*& block,
    const AcDb::OpenMode& mode )
{
    Acad::ErrorStatus es;
    AcDbEntity* pEnt;
    
    // On ouvre l'entité
    es = acdbOpenAcDbEntity(
            pEnt,
            idBlock,
            mode );
            
    // Retour s'il y a une erreur
    if( es != Acad::eOk )
        return es;
        
    // On cast en cercle
    block = AcDbBlockReference::cast( pEnt );
    
    return Acad::eOk;
}


Acad::ErrorStatus
openAttribute( const AcDbObjectId& attributeId,
    AcDbAttribute*& pAtt,
    const AcDb::OpenMode& mode )
{
    Acad::ErrorStatus es;
    
    AcDbEntity* pEnt;
    
    if( es = acdbOpenAcDbEntity( pEnt, attributeId, mode ) )
        return es;
        
    pAtt = AcDbAttribute::cast( pEnt );
    
    return es;
}

Acad::ErrorStatus
openAttributeDefinition( const AcDbObjectId& attributeDefId,
    AcDbAttributeDefinition*& pAttDef,
    const AcDb::OpenMode& mode )
{
    Acad::ErrorStatus es;
    
    AcDbEntity* pEnt;
    
    if( es = acdbOpenAcDbEntity( pEnt, attributeDefId, mode ) )
        return es;
        
    pAttDef = AcDbAttributeDefinition::cast( pEnt );
    
    return es;
}

Acad::ErrorStatus cleanSelectionFromDoublons(
    ads_name& ss )
{
    //Initialisation du vecteur de point en x et des AcArray
    std::vector<double> xPos;
    AcGePoint3dArray blockPos;
    AcDbObjectIdArray blockIds;
    
    Acad::ErrorStatus es = prepareBlocks( ss, xPos, blockPos, blockIds );
    
    if( es != Acad::eOk )
    {
        //Liberer la selection
        acedSSFree( ss );
        
        //Sortir
        return es;
    }
    
    if( Acad::eOk != es )
    {
        //Liberer la selection
        acedSSFree( ss );
        
        //Sortir
        return es;
    }
    
    es = cleanDoublons( xPos, blockPos, blockIds );
    
    if( Acad::eOk != es )
    {
        //Liberer la selection
        acedSSFree( ss );
        
        //Sortir
        return es;
    }
    
    //Sortir de la fonction
    return Acad::eOk;
}


Acad::ErrorStatus cleanSelectionFromDoublonsXY(
    ads_name& ssBlocToKeep,
    ads_name& ssotherbloc,
    int& deleted )
{
    //Initialisation du vecteur de point en x et des AcArray
    std::vector<double> xPosPonc;
    AcGePoint3dArray blockPosPonc;
    AcDbObjectIdArray blockIdsPonc;
    
    Acad::ErrorStatus es = prepareBlocks( ssBlocToKeep, xPosPonc, blockPosPonc, blockIdsPonc );
    
    if( Acad::eOk != es )
    {
        //Liberer la selection
        acedSSFree( ssBlocToKeep );
        
        //Sortir
        return es;
    }
    
    std::vector<double> xPosLev;
    AcGePoint3dArray blockPosLev;
    AcDbObjectIdArray blockIdsLev;
    es = prepareBlocks( ssotherbloc, xPosLev, blockPosLev, blockIdsLev );
    
    if( Acad::eOk != es )
    {
        //Liberer la selection
        acedSSFree( ssotherbloc );
        
        //Sortir
        return es;
    }
    
    //Supression
    int sizePonc = blockPosPonc.size();
    int count = 0;
    ProgressBar prog = ProgressBar( _T( "Progression" ), sizePonc );
    
    for( int counter = 0; counter < sizePonc; counter++ )
    {
        prog.moveUp( counter );
        AcGePoint3d pt = blockPosPonc[counter];
        
        es = cleanDoublons( pt, xPosLev, blockPosLev, blockIdsLev, 0.01 );
        
        //On incrémmente le compteur
        if( es == eOk )
            count++;
            
        else if( es == eNotApplicable )
            return es;
    }
    
    deleted = count;
    return eOk;
}

Acad::ErrorStatus cleanDoublons( const AcGePoint3d& ptSearch,
    vector<double>& xPos,
    AcGePoint3dArray& blockPos,
    AcDbObjectIdArray& blockIds,
    const double& precision )
{
    //Compteur
    int compt = 0;
    
    //Initialisation de la barre de progression
    int size = xPos.size();
    
    ErrorStatus es = eNotInBlock;
    //Initialisation des itérateurs
    AcGePoint3d* blockPosBegin = blockPos.begin();
    AcDbObjectId* blockIdsBegin = blockIds.begin();
    
    //Declarer une entité
    AcDbEntity* entity;
    
    //Boucle sur le vecteur de x
    for( int i = 0; i < size; i++ )
    {
        //Tester s'il on le même y
        if( isEqual2d( ptSearch, blockPos[i], precision ) )
        {
            //On supprime le bloc
            //Ouvrir l'entité
            if( Acad::eOk != acdbOpenAcDbEntity( entity, blockIds[i], AcDb::kForWrite ) )
                return Acad::eNotApplicable;
                
            //Supprimer l'entité
            entity->erase();
            entity->close();
            
            //Supprimer le point dans blockPos
            blockPos.erase( blockPosBegin + i );
            
            //Supprimer l'objectId dans blockIds
            blockIds.erase( blockIdsBegin + i );
            es = eOk;
            break;
        }
        
        continue;
    }
    
    return es;
}


AcDbBlockReference* getClosestBlock( const AcGePoint3d& circleCenter,
    const ads_name& ssBlock,
    const AcDb::OpenMode& opMode )
{

    long lengthBlock;
    
    if( acedSSLength( ssBlock, &lengthBlock ) != RTNORM || lengthBlock == 0 )
        return NULL;
        
    // Boucle sur les blocs
    AcDbBlockReference* block;
    AcGePoint3d blockPos;
    AcDbObjectId blockId;
    double distance = 10000;
    double dist;
    
    for( int i = 0; i < lengthBlock; i++ )
    {
    
        block = getBlockFromSs( ssBlock,
                i );
                
        blockPos = block->position();
        
        AcDbObjectId id = block->id();
        
        block->close();
        
        // Distance entre le bloc et le centre du cercle
        dist = getDistance2d( circleCenter,
                blockPos );
                
        if( dist < distance )
        {
            distance = dist;
            blockId = id;
        }
    }
    
    Acad::ErrorStatus es = openBlock( blockId,
            block,
            opMode );
            
    if( es != Acad::eOk )
        return NULL;
        
    else return block;
}


AcGePoint3d getProjectionOnBlock( AcDbBlockReference* block,
    const AcGePoint3d& pointToProject )
{
    //Récupérer le nom du bloc
    AcString name;
    getStaticBlockName( name, block );
    AcGePoint3d proj = AcGePoint3d::kOrigin;
    
    if( name ==  _T( "DELIM~CLOT~PIL~CAR" ) ||
        name == _T( "DELIM~CLOT~PIL~REC" ) )
    {
    
        AcGePoint3d pt1 = AcGePoint3d( -0.5, -0.5, 0 );
        AcGePoint3d pt2 = AcGePoint3d( 0.5, -0.5, 0 );
        AcGePoint3d pt3 = AcGePoint3d( 0.5, 0.5, 0 );
        AcGePoint3d pt4 = AcGePoint3d( -0.5, 0.5, 0 );
        
        //Matrice du bloc
        AcGeMatrix3d blockMatrix = block->blockTransform();
        
        //On calcule les 4 côtés du bloc
        pt1.transformBy( blockMatrix );
        pt2.transformBy( blockMatrix );
        pt3.transformBy( blockMatrix );
        pt4.transformBy( blockMatrix );
        
        AcGePoint3dArray ar;
        ar.append( pt1 );
        ar.append( pt2 );
        ar.append( pt3 );
        ar.append( pt4 );
        
        AcDb3dPolyline poly = AcDb3dPolyline( AcDb::Poly3dType::k3dSimplePoly,
                ar,
                true );
                
        poly.makeClosed();
        
        poly.getClosestPointTo( pointToProject,
            proj );
            
    }
    
    else if( name == _T( "DELIM~CLOT~PIL~CIR" ) )
    {
        //40 points de la polyligne
        AcGePoint3d pt01 = AcGePoint3d( 0.5, 0, 0 );
        AcGePoint3d pt02 = AcGePoint3d( 0.494, -0.078, 0 );
        AcGePoint3d pt03 = AcGePoint3d( 0.476, -0.155, 0 );
        AcGePoint3d pt04 = AcGePoint3d( 0.446, -0.227, 0 );
        AcGePoint3d pt05 = AcGePoint3d( 0.405, -0.294, 0 );
        AcGePoint3d pt06 = AcGePoint3d( 0.354, -0.354, 0 );
        AcGePoint3d pt07 = AcGePoint3d( 0.294, -0.405, 0 );
        AcGePoint3d pt08 = AcGePoint3d( 0.227, -0.446, 0 );
        AcGePoint3d pt09 = AcGePoint3d( 0.155, -0.476, 0 );
        AcGePoint3d pt10 = AcGePoint3d( 0.078, -0.494, 0 );
        AcGePoint3d pt11 = AcGePoint3d( 0, -0.5, 0 );
        AcGePoint3d pt12 = AcGePoint3d( -0.078, -0.494, 0 );
        AcGePoint3d pt13 = AcGePoint3d( -0.155, -0.476, 0 );
        AcGePoint3d pt14 = AcGePoint3d( -0.227, -0.446, 0 );
        AcGePoint3d pt15 = AcGePoint3d( -0.294, -0.405, 0 );
        AcGePoint3d pt16 = AcGePoint3d( -0.354, -0.354, 0 );
        AcGePoint3d pt17 = AcGePoint3d( -0.405, -0.294, 0 );
        AcGePoint3d pt18 = AcGePoint3d( -0.446, -0.227, 0 );
        AcGePoint3d pt19 = AcGePoint3d( -0.476, -0.155, 0 );
        AcGePoint3d pt20 = AcGePoint3d( -0.494, -0.078, 0 );
        AcGePoint3d pt21 = AcGePoint3d( -0.5, 0, 0 );
        AcGePoint3d pt22 = AcGePoint3d( -0.494, 0.078, 0 );
        AcGePoint3d pt23 = AcGePoint3d( -0.476, 0.155, 0 );
        AcGePoint3d pt24 = AcGePoint3d( -0.446, 0.227, 0 );
        AcGePoint3d pt25 = AcGePoint3d( -0.405, 0.294, 0 );
        AcGePoint3d pt26 = AcGePoint3d( -0.354, 0.354, 0 );
        AcGePoint3d pt27 = AcGePoint3d( -0.294, 0.405, 0 );
        AcGePoint3d pt28 = AcGePoint3d( -0.227, 0.446, 0 );
        AcGePoint3d pt29 = AcGePoint3d( -0.155, 0.476, 0 );
        AcGePoint3d pt30 = AcGePoint3d( -0.078, 0.494, 0 );
        AcGePoint3d pt31 = AcGePoint3d( 0, 0.5, 0 );
        AcGePoint3d pt32 = AcGePoint3d( 0.078, 0.494, 0 );
        AcGePoint3d pt33 = AcGePoint3d( 0.155, 0.476, 0 );
        AcGePoint3d pt34 = AcGePoint3d( 0.227, 0.446, 0 );
        AcGePoint3d pt35 = AcGePoint3d( 0.294, 0.405, 0 );
        AcGePoint3d pt36 = AcGePoint3d( 0.354, 0.354, 0 );
        AcGePoint3d pt37 = AcGePoint3d( 0.405, 0.294, 0 );
        AcGePoint3d pt38 = AcGePoint3d( 0.446, 0.227, 0 );
        AcGePoint3d pt39 = AcGePoint3d( 0.476, 0.155, 0 );
        AcGePoint3d pt40 = AcGePoint3d( 0.494, 0.078, 0 );
        
        //Matrice du bloc
        AcGeMatrix3d blockMatrix = block->blockTransform();
        
        //Ajouter dans le vecteur
        AcGePoint3dArray ar;
        
        //On calcule les 40 sommets du bloc
        ar.append( pt01.transformBy( blockMatrix ) );
        ar.append( pt02.transformBy( blockMatrix ) );
        ar.append( pt03.transformBy( blockMatrix ) );
        ar.append( pt04.transformBy( blockMatrix ) );
        ar.append( pt05.transformBy( blockMatrix ) );
        ar.append( pt06.transformBy( blockMatrix ) );
        ar.append( pt07.transformBy( blockMatrix ) );
        ar.append( pt08.transformBy( blockMatrix ) );
        ar.append( pt09.transformBy( blockMatrix ) );
        ar.append( pt10.transformBy( blockMatrix ) );
        ar.append( pt11.transformBy( blockMatrix ) );
        ar.append( pt12.transformBy( blockMatrix ) );
        ar.append( pt13.transformBy( blockMatrix ) );
        ar.append( pt14.transformBy( blockMatrix ) );
        ar.append( pt15.transformBy( blockMatrix ) );
        ar.append( pt16.transformBy( blockMatrix ) );
        ar.append( pt17.transformBy( blockMatrix ) );
        ar.append( pt18.transformBy( blockMatrix ) );
        ar.append( pt19.transformBy( blockMatrix ) );
        ar.append( pt20.transformBy( blockMatrix ) );
        ar.append( pt21.transformBy( blockMatrix ) );
        ar.append( pt22.transformBy( blockMatrix ) );
        ar.append( pt23.transformBy( blockMatrix ) );
        ar.append( pt24.transformBy( blockMatrix ) );
        ar.append( pt25.transformBy( blockMatrix ) );
        ar.append( pt26.transformBy( blockMatrix ) );
        ar.append( pt27.transformBy( blockMatrix ) );
        ar.append( pt28.transformBy( blockMatrix ) );
        ar.append( pt29.transformBy( blockMatrix ) );
        ar.append( pt30.transformBy( blockMatrix ) );
        ar.append( pt31.transformBy( blockMatrix ) );
        ar.append( pt32.transformBy( blockMatrix ) );
        ar.append( pt33.transformBy( blockMatrix ) );
        ar.append( pt34.transformBy( blockMatrix ) );
        ar.append( pt35.transformBy( blockMatrix ) );
        ar.append( pt36.transformBy( blockMatrix ) );
        ar.append( pt37.transformBy( blockMatrix ) );
        ar.append( pt38.transformBy( blockMatrix ) );
        ar.append( pt39.transformBy( blockMatrix ) );
        ar.append( pt40.transformBy( blockMatrix ) );
        
        //Creer la polyligne avec 40 sommets
        AcDb3dPolyline poly = AcDb3dPolyline( AcDb::Poly3dType::k3dSimplePoly,
                ar,
                true );
                
        //Recuperer le point le plus proche de la polyligne
        poly.getClosestPointTo( pointToProject,
            proj );
            
        //Fermer la polyligne
        poly.close();
        
        //Mettre sur la même z
        proj.z = pointToProject.z;
    }
    
    return proj;
}


Acad::ErrorStatus exportBlocks( const AcString& blockName,
    const AcString& newFile )
{
    //Récupérer tous les blocs de cette définition
    ads_name ssBlock;
    int length = getSsAllBlock( ssBlock,
            "",
            blockName );
            
    //Tester que la selection est valide
    if( !length )
        return Acad::eSelectionSetEmpty;
        
    //Créer une nouvelle base de données
    AcDbDatabase* pDb = new AcDbDatabase();
    
    //Créer la nouvelle table des blocs et la record table des blocs
    AcDbBlockTable* pTable;
    AcDbBlockTableRecord* pRecord;
    
    //ErrorStatus
    Acad::ErrorStatus es;
    
    // On va ajouter la définition du bloc dans la table des blocs du dessin à créer
    //es = pDb->insert( getBlockDefId( blockName ),
    //        blockName,
    //        blockName,
    //        acdbHostApplicationServices()->workingDatabase() );
    
    //if( es != Acad::eOk )
    //{
    //    pDb->close();
    //    acedSSFree( ssBlock );
    //    return es;
    //}
    
    //Récupérer un pointeur sur la définition du bloc à insérer dans le nouveau dessin
    AcDbObject* pObject;
    AcDbObjectId id = getBlockDefId( blockName );
    es = acdbOpenAcDbObject( pObject,
            id,
            AcDb::kForWrite );
            
    if( es != Acad::eOk )
    {
        pDb->close();
        acedSSFree( ssBlock );
        return es;
    }
    
    //Recuperer la table de la nouvelle base de données
    es = pDb->getBlockTable( pTable, AcDb::kForWrite );
    
    //Recuperer le table record sur la definition du bloc
    AcDbBlockTableRecord* record = AcDbBlockTableRecord::cast( pObject );
    
    //L'ajouter à la base de données du nouveau dessin
    es = pTable->add( record );
    id = record->id();
    
    if( es != Acad::eOk )
    {
        pDb->close();
        pObject->close();
        acedSSFree( ssBlock );
        return es;
    }
    
    //Fermer le record
    record->close();
    
    //Fermer l'objet
    pObject->close();
    
    //Récupérer le model space
    es = pTable->getAt( ACDB_MODEL_SPACE, pRecord, AcDb::kForWrite );
    
    if( es != Acad::eOk )
    {
        pTable->close();
        pDb->close();
        acedSSFree( ssBlock );
        return es;
    }
    
    //Fermer la table
    pTable->close();
    
    //// Récupérer la table des calques, on va créer les calques
    //AcDbLayerTable* layerTable;
    //es  = pDb->getLayerTable( layerTable, AcDb::kForWrite );
    //
    //if( es != Acad::eOk )
    //{
    //    acutPrintf( _T( "\nNe peut pas ouvrir la table des calques" ) );
    //    pRecord->close();
    //    pDb->close();
    //    acedSSFree( ssBlock );
    //    return es;
    //}
    
    AcDbBlockReference* block;
    AcString layer;
    
    // Boucler sur tous les blocs du dessin courant
    for( int i = 0; i < length; i++ )
    {
        // Récupérer le bloc i
        block = getBlockFromSs( ssBlock,
                i,
                AcDb::kForRead );
                
        //// Récupérer le calque du bloc
        //layer = block->layer();
        //
        //// Récupérer le calque du bloc
        //AcDbLayerTableRecord* layerRecord;
        //
        //es = layerTable->getAt( layer,
        //        layerRecord,
        //        AcDb::kForWrite );
        //
        //// Si le calque existe déjà dans le nouveau fichier
        //if( es == Acad::eOk )
        //{
        //
        //    // On ferme le calque
        //    layerRecord->close();
        
        // On copie les propriétés du bloc de notre dessin dans le nouveau
        AcDbBlockReference* block2 = new AcDbBlockReference( block->position(),
            id );
            
        block2->setRotation( block->rotation() );
        //es = block2->copyFrom( block );
        
        // On ajoute le bloc dans le MODEL_SPACE du nouveau dessin
        es = pRecord->appendAcDbEntity( block2 );
        
        // On ferme les blocs
        es = block->close();
        es = block2->close();
        
        continue;
        //}
        //
        //// Sinon le calque n'existe pas dans le nouveau fichier
        //
        //// On récupère l'objet calque de ce fichier courant
        //es = getLayer( layerRecord,
        //        layer,
        //        AcDb::kForWrite );
        //
        //if( es != Acad::eOk )
        //{
        //    block->close();
        //    acutPrintf( _T( "\nLes blocs du calque : " ) + layer + _T( "n'ont pas été pris en compte,\nProblème de calque" ) );
        //    continue;
        //}
        //
        //// Ajouter le calque à la table des calques de notre nouveau dessin
        //es = layerTable->add( layerRecord );
        //
        //if( es != Acad::eOk )
        //{
        //    block->close();
        //    layerRecord->close();
        //    acutPrintf( _T( "\nLes blocs du calque : " ) + layer + _T( "n'ont pas été pris en compte,\nProblème de calque" ) );
        //    continue;
        //}
        //
        //// Fermer le calque
        //layerRecord->close();
        //
        //// On copie les propriétés du bloc de notre dessin dans le nouveau
        //AcDbBlockReference* block2 = new AcDbBlockReference( block->position(),
        //    block->blockId() );
        //block2->setRotation( block->rotation() );
        //block2->copyFrom( block );
        //
        //// On ajoute le bloc dans le MODEL_SPACE du nouveau dessin
        //pRecord->appendAcDbEntity( block2 );
        //
        //block2->close();
        //block->close();
    }
    
    // Fermer la table des calques
    //layerTable->close();
    
    // Fermer le model space
    pRecord->close();
    
    // Sauvegarder le fichier
    pDb->saveAs( newFile );
    
    // Fermer la base de données
    pDb->close();
    
    // Libérer la sélection
    acedSSFree( ssBlock );
    
    return Acad::eOk;
}

ErrorStatus getDynamicBlockName( AcString& name,
    AcDbBlockReference* block )
{
    //Recuperer le bloc dynamique
    AcDbDynBlockReference* blockDyn = new AcDbDynBlockReference( block->id() );
    
    //Tester si c'est un bloc dynamique
    if( !blockDyn->isDynamicBlock() )
    {
        //Supprimer le bloc dynamique
        delete blockDyn;
        
        //Fermer le bloc
        block->close();
        
        //Sortir de la fonction
        return eNotApplicable;
    }
    
    //Recuperer l'id du bloc dynamique
    AcDbObjectId blockId =  blockDyn->dynamicBlockTableRecord();
    
    //Liberer la memoire
    delete blockDyn;
    
    //BlockTableRecordPointer
    AcDbBlockTableRecordPointer pBTR( blockId, AcDb::kForRead );
    
    Acad::ErrorStatus es;
    
    //Recuperer le nom du bloc
    if( es = pBTR.openStatus() )
        return es;
    else
        return pBTR->getName( name );
}

ErrorStatus getBlockName(
    AcString& name,
    AcDbBlockReference* block )
{
    if( isDynamicBlock( block ) )
        return getDynamicBlockName( name, block );
        
    else
        return  getStaticBlockName( name, block );
}

vector<AcString> getAttributesNamesListOfBlockRef( AcDbBlockReference* block3D )
{
    //On initialise le resultat
    vector<AcString> result;
    
    //On crée un itérateur pour itérer les attribut dans le bloc
    AcDbObjectIterator* attIt = block3D->attributeIterator();
    
    //On initialise l'attribut
    AcDbAttribute* obAttName   = NULL;
    
    //On boucle sur tous les attributs du bloc
    for( attIt; !attIt->done(); attIt->step() )
    {
        //On ouvre l'attribut
        if( Acad::eOk != block3D->openAttribute( obAttName, attIt->objectId(), AcDb::kForRead ) )
            continue;
            
        //Ajouter dans le vecteur
        result.push_back( obAttName->tag() );
        
        //on ferme l'attribut
        obAttName->close();
    }
    
    //on supprime l'itérateur
    delete attIt;
    
    //on renvoie le résultat
    return result;
}

vector<AcString> getAttributesNamesListOfBlockDef( AcDbBlockTableRecord* pBlkDef )
{
    //Initialiser l'iterateur
    AcDbBlockTableRecordIterator* pIterator;
    pBlkDef->newIterator( pIterator );
    pIterator->start();
    
    //Initialiser les objets
    AcDbEntity* pEnt = NULL;
    vector<AcString> result;
    int count = 0;
    
    //Iterer sur les objets
    while( !pIterator->done() )
    {
        //Recuperer l'entite
        pIterator->getEntity( pEnt, AcDb::kForRead );
        
        //Recuperer l'attribut
        AcDbAttributeDefinition* att = AcDbAttributeDefinition::cast( pEnt );
        
        if( att != NULL )
        {
            result.push_back( att->tag() );
            pEnt->close();
        }
        
        //Autre objet
        else
        {
            //Fermer l'objet
            pEnt->close();
        }
        
        //Iterer
        pIterator->step();
        
        count++;
    }
    
    //Effacer l'iterateur
    delete pIterator;
    
    return result;
}

vector<AcString> getAttributesNamesListOfBlockDef( AcDbBlockReference* pBlkRef )
{
    //Trouver le bloc définition
    AcDbBlockTableRecord* pBlkDef = getBlockDef( pBlkRef );
    
    //Récupérer le nom des attributs de la définition
    return getAttributesNamesListOfBlockDef( pBlkDef );
}

std::map<AcString, AcDbObjectId> getAttributesRef( AcDbBlockReference* pBlkRef )
{
    //On initialise le resultat
    std::map<AcString, AcDbObjectId> result;
    
    //On cree un iterateur pour iterer les attribut dans le bloc
    AcDbObjectIterator* attIt = pBlkRef->attributeIterator();
    
    //On initialise l'attribut
    AcDbAttribute* obAttName   = NULL;
    
    //On boucle sur tous les attributs du bloc
    for( attIt; !attIt->done(); attIt->step() )
    {
        //On ouvre l'attribut
        if( Acad::eOk != pBlkRef->openAttribute( obAttName, attIt->objectId(), AcDb::kForRead ) )
            continue;
            
        //Ajouter dans le vecteur
        result.insert( std::pair<AcString, AcDbObjectId>( obAttName->tag(), attIt->objectId() ) );
        
        //on ferme l'attribut
        obAttName->close();
    }
    
    //on supprime l'itérateur
    delete attIt;
    
    //on renvoie le résultat
    return result;
}

std::map<AcString, AcDbObjectId> getAttributesDef( AcDbBlockTableRecord* pBlkDef )
{
    //Initialiser l'iterateur
    AcDbBlockTableRecordIterator* pIterator;
    pBlkDef->newIterator( pIterator );
    pIterator->start();
    
    //Initialiser les objets
    AcDbEntity* pEnt = NULL;
    std::map<AcString, AcDbObjectId> result;
    
    //Iterer sur les objets
    while( !pIterator->done() )
    {
        //Recuperer l'entite
        pIterator->getEntity( pEnt, AcDb::kForRead );
        
        //Recuperer l'attribut
        AcDbAttributeDefinition* att = AcDbAttributeDefinition::cast( pEnt );
        
        if( att != NULL )
            result.insert( std::pair<AcString, AcDbObjectId>( att->tag(), att->objectId() ) );
            
        //Autre objet
        else
        {
            //Fermer l'objet
            pEnt->close();
        }
        
        //Iterer
        pIterator->step();
    }
    
    //Effacer l'iterateur
    delete pIterator;
    
    return result;
}

Acad::ErrorStatus getClosedPolyBlock( AcDb3dPolyline*& polyClosed, AcDbBlockReference* blockRef )
{
    Acad::ErrorStatus es;
    
    //ouvre la definition de bloc
    AcDbBlockTableRecord* blockRecord;
    es =  acdbOpenObject( ( AcDbObject*& )blockRecord, blockRef->blockTableRecord(), AcDb::kForRead ) ;
    
    if( es != Acad::eOk ) return es;
    
    // on set un iterateur qui va iterer sur les élements du bloc
    AcDbBlockTableRecordIterator* iter;
    es = blockRecord->newIterator( iter );
    
    if( es != Acad::eOk )
    {
        blockRecord->close();
        return es;
    }
    
    //Variable pour trouver la polyligne fermée
    bool found = false;
    
    // on itere sur les elements de la définition du bloc
    AcDbPolyline* polyBlock;
    
    for( iter->start(); !iter->done(); iter->step() )
    {
        AcDbEntity* entity = NULL;
        // on recupere l'entite pointee par l'it
        es = iter->getEntity( entity, AcDb::kForRead );
        
        if( es != Acad::eOk || entity == NULL ) continue;
        
        //On cast l'entité
        polyBlock = AcDbPolyline::cast( entity );
        
        if( !polyBlock )
            continue;
            
        //On verifie si la polyligne est fermée
        if( Adesk::kTrue == polyBlock->isClosed() )
        {
            //Setter la variable en true
            found = true;
            break;
        }
        
        polyBlock->close();
    }
    
    if( !found )
        return Acad::eNotInBlock;
        
    //Recuperer la matrice de transformation de la reference du bloc
    AcGeMatrix3d blockMatrix = blockRef->blockTransform();
    AcGePoint3d position = blockRef->position();
    
    //Boucler sur les sommets de la polyligne du bloc
    AcGePoint3dArray pointArray;
    int size = polyBlock->numVerts();
    
    for( int i = 0; i < size; i++ )
    {
        //Recuperer les coordonnées du ième sommet
        AcGePoint3d pt ;
        polyBlock->getPointAt( i, pt );
        
        //Transformer le point suivant la matrice de transformation
        pt = pt.transformBy( blockMatrix );
        
        //Ajouter le nouveau point dans un tableau
        pointArray.append( AcGePoint3d( pt.x, pt.y, position.z ) );
    }
    
    //Fermer les objets utilisés
    polyBlock->close();
    blockRecord->close();
    
    //Créer la nouvelle polyligne
    polyClosed = new AcDb3dPolyline( AcDb::k3dSimplePoly, pointArray, Adesk::kTrue );
    
    //renvoyer le resultat
    return Acad::eOk;
}

Acad::ErrorStatus getClosedPolyBlock( AcDb3dPolyline*& polyClosed, AcGePoint3dArray& ptarray, AcDbBlockReference* blockRef )
{
    Acad::ErrorStatus es;
    
    //ouvre la definition de bloc
    AcDbBlockTableRecord* blockRecord;
    es = acdbOpenObject( ( AcDbObject*& )blockRecord, blockRef->blockTableRecord(), AcDb::kForRead );
    
    if( es != Acad::eOk ) return es;
    
    // on set un iterateur qui va iterer sur les élements du bloc
    AcDbBlockTableRecordIterator* iter;
    es = blockRecord->newIterator( iter );
    
    if( es != Acad::eOk )
    {
        blockRecord->close();
        return es;
    }
    
    //Variable pour trouver la polyligne fermée
    bool found = false;
    
    // on itere sur les elements de la définition du bloc
    AcDbPolyline* polyBlock;
    
    for( iter->start(); !iter->done(); iter->step() )
    {
        AcDbEntity* entity = NULL;
        // on recupere l'entite pointee par l'it
        es = iter->getEntity( entity, AcDb::kForRead );
        
        if( es != Acad::eOk || entity == NULL ) continue;
        
        //On cast l'entité
        polyBlock = AcDbPolyline::cast( entity );
        
        if( !polyBlock )
            continue;
            
        //On verifie si la polyligne est fermée
        if( Adesk::kTrue == polyBlock->isClosed() )
        {
            //Setter la variable en true
            found = true;
            break;
        }
        
        polyBlock->close();
    }
    
    if( !found )
        return Acad::eNotInBlock;
        
    //Recuperer la matrice de transformation de la reference du bloc
    AcGeMatrix3d blockMatrix = blockRef->blockTransform();
    AcGePoint3d position = blockRef->position();
    
    //Boucler sur les sommets de la polyligne du bloc
    AcGePoint3dArray pointArray;
    int size = polyBlock->numVerts();
    
    for( int i = 0; i < size; i++ )
    {
        //Recuperer les coordonnées du ième sommet
        AcGePoint3d pt;
        polyBlock->getPointAt( i, pt );
        
        //Transformer le point suivant la matrice de transformation
        pt = pt.transformBy( blockMatrix );
        
        //Ajouter le nouveau point dans un tableau
        ptarray.append( AcGePoint3d( pt.x, pt.y, position.z ) );
    }
    
    //Fermer les objets utilisés
    polyBlock->close();
    blockRecord->close();
    
    //Créer la nouvelle polyligne
    polyClosed = new AcDb3dPolyline( AcDb::k3dSimplePoly, ptarray, Adesk::kTrue );
    
    //renvoyer le resultat
    return Acad::eOk;
}

AcDbBlockReference* insertBlockExternFile(
    AcDbBlockReference* pBlock,
    const AcString& sFilePath,
    AcDbDatabase* pDB )
{
    Acad::ErrorStatus es = Acad::eNotApplicable;
    
    /// VERIFICATION DE LA BASE DE DONNEES ET SON NOM
    // Si la base de données est NULL
    if( !pDB )
    {
        // Créer une nouvelle base de données
        pDB = new AcDbDatabase();
        
        // Lire la base de donnée du fichier en paramètre si le fichier n'est pas vide
        if( sFilePath.compare( "" ) != 0 )
        {
            es = pDB->readDwgFile( sFilePath );
            
            if( es != Acad::eOk )
            {
                delete pDB;
                pDB = new AcDbDatabase();
            }
        }
    }
    
    /// RECUPERATION DU NOM DU BLOC
    AcString blockName;
    
    if( eOk != getStaticBlockName( blockName, pBlock ) )
    {
        pDB->close();
        return NULL;
    }
    
    /// INSERTION DE LA DEFINITION DU BLOC DANS LE FICHIER EXTERNE
    // INSERTION DE LA DEFINITION DU BLOC DANS LE FICHIER EXTERNE
    // Verifier si le bloc est dans la base de données
    AcDbObjectId newId = getBlockDefId( blockName, pDB );
    
    if( newId == AcDbObjectId::kNull )
    {
        es = pDB->insert( getBlockDefId( blockName ),
                blockName,
                blockName,
                acdbHostApplicationServices()->workingDatabase() );
                
        if( es != eOk )
            return NULL;
    }
    
    /// RECUPERATION DE TABLE DE BLOCS ET DU MODEL SPACE DU FICHIER EXTERNE
    // Recuperer la table des blocs de base externe
    AcDbBlockTable* pBlockTable = NULL;
    pDB->getSymbolTable( pBlockTable, AcDb::kForWrite );
    
    // Recuperer le model space
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite );
    
    // Récupérer l'id de la définition du bloc
    if( newId == AcDbObjectId::kNull )
        pBlockTable->getAt( blockName,
            newId );
            
    // Fermer la table des blocs
    pBlockTable->close();
    
    
    // On crée une référence de bloc
    AcDbBlockReference* pBlockExtern = new AcDbBlockReference( pBlock->position(), newId );
    
    // On l'ajoute au model space
    AcDbObjectId objectId = AcDbObjectId::kNull;
    es = pBlockTableRecord->appendAcDbEntity( objectId, pBlockExtern );
    
    if( es != Acad::eOk )
    {
        pDB->close();
        return NULL;
    }
    
    // Transformer le nouveau bloc
    pBlockExtern->setBlockTransform( pBlock->blockTransform() );
    
    /// RECUPERER TOUS LES ATTRIBUTS DANS LA DEFINITION DU BLOC ET LES INSERER DANS LE NOUVEAU BLOC
    addAllAttributeToBlock( pBlockExtern );
    
    /// RECUPERER LES VALEURS DES ATTRIBUTS DU BLOC A COPIER ET SETTER LES ATTRIBUTS DU NOUVEAU BLOC PAR CES VALEURS
    // Créer un itérateur
    AcDbObjectIterator* iter = pBlock->attributeIterator();
    
    // Parcourir les attributs du bloc pBlock
    for( iter->start(); !iter->done(); iter->step() )
    {
        // Récuperer l'ième attribut
        AcDbAttribute* attribute =   NULL;
        
        //on ouvre l'attribut du bloc statique
        if( Acad::eOk != pBlock->openAttribute( attribute, iter->objectId(), AcDb::kForRead ) )
            continue;
            
        //on copie les valeurs des attributs du bloc statique dans l'attribut du bloc dynamique
        setAttributValue( pBlockExtern, attribute->tag(), attribute->textString() );
        
        // Fermer l'attribut
        attribute->close();
    }
    
    delete iter;
    iter = NULL;
    
    /// CREATION D'UN CALQUE
    //Recuperer le nom de calque du bloc à copier
    AcString layer = pBlock->layer();
    
    // Recuperer l'entité calque correspondant à ce nom dans la base de données courantes
    AcDbLayerTableRecord* layerEntity = NULL;
    es = getLayer( layerEntity, layer );
    
    // Verifier
    if( es != eOk )
        return NULL;
        
    // Recuperer la couleur de calque
    AcCmColor color = AcCmColor();
    
    // Verifier l'entité calque
    if( layerEntity != NULL )
    {
        //On recupere la couleur
        color = layerEntity->color();
        
        //Fermer l'entité calque
        layerEntity->close();
    }
    
    // Créer un nouveau calque dans la nouvelle base de données s'il n'y est pas encore
    es =  createLayer( layer, color, pDB );
    
    if( es != eOk )
        return NULL;
        
    /// COPIER LES PROPRIETES DU BLOC A COPIER DANS LE NOUVEAU BLOC
    // Visibilité
    pBlockExtern->setVisibility( pBlock->visibility() );
    
    // Line type scale
    pBlockExtern->setLinetypeScale( pBlock->linetypeScale() );
    
    // Type de ligne
    pBlockExtern->setLinetype( pBlock->linetype() );
    
    // Calque
    es = pBlockExtern->setLayer( layer );
    
    
    // Fermer le model space
    pBlockTableRecord->close();
    
    // Si le nom du fichier n'est pas vide, enregistrer le fichier
    if( sFilePath.compare( "" ) != 0 )
        pDB->saveAs( sFilePath, false, AcDb::kDHL_1800 );
        
    // Renvoyer le résultat
    return pBlockExtern;
}

AcDbBlockReference* insertBlockExternFile(
    const AcString& pBlockName,
    const AcGePoint3d& pPosition,
    const AcString& layer,
    const double angle,
    const AcString& sFilePath,
    AcDbDatabase* pDB )
{
    Acad::ErrorStatus es = Acad::eNotApplicable;
    
    /// VERIFICATION DE LA BASE DE DONNEES ET SON NOM
    // Si la base de données est NULL
    if( !pDB )
    {
        // Créer une nouvelle base de données
        pDB = new AcDbDatabase();
        
        // Lire la base de donnée du fichier en paramètre si le fichier n'est pas vide
        if( sFilePath.compare( "" ) != 0 )
        {
            es = pDB->readDwgFile( sFilePath );
            
            if( es != Acad::eOk )
            {
                delete pDB;
                pDB = new AcDbDatabase();
            }
        }
    }
    
    
    /// INSERTION DE LA DEFINITION DU BLOC DANS LE FICHIER EXTERNE
    // Verifier si le bloc est dans la base de données
    AcDbObjectId newId = getBlockDefId( pBlockName, pDB );
    
    if( newId == AcDbObjectId::kNull )
    {
        es = pDB->insert( getBlockDefId( pBlockName ),
                pBlockName,
                pBlockName,
                acdbHostApplicationServices()->workingDatabase() );
                
        if( es != eOk )
            return NULL;
    }
    
    
    /// RECUPERATION DE TABLE DE BLOCS ET DU MODEL SPACE DU FICHIER EXTERNE
    // Recuperer la table des blocs de base externe
    AcDbBlockTable* pBlockTable = NULL;
    pDB->getSymbolTable( pBlockTable, AcDb::kForWrite );
    
    // Recuperer le model space
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite );
    
    // Récupérer l'id de la définition du bloc
    if( newId == AcDbObjectId::kNull )
        pBlockTable->getAt( pBlockName,
            newId );
            
    // Fermer la table des blocs
    pBlockTable->close();
    
    
    // On crée une référence de bloc
    AcDbBlockReference* pBlockExtern = new AcDbBlockReference( pPosition, newId );
    pBlockExtern->setRotation( angle );
    
    // On l'ajoute au model space
    AcDbObjectId objectId = AcDbObjectId::kNull;
    es = pBlockTableRecord->appendAcDbEntity( objectId, pBlockExtern );
    
    if( es != Acad::eOk )
    {
        pDB->close();
        return NULL;
    }
    
    /// RECUPERER TOUS LES ATTRIBUTS DANS LA DEFINITION DU BLOC ET LES INSERER DANS LE NOUVEAU BLOC
    addAllAttributeToBlock( pBlockExtern );
    
    /// CREATION D'UN CALQUE
    // Recuperer l'entité calque correspondant à ce nom dans la base de données courantes
    AcDbLayerTableRecord* layerEntity = NULL;
    es = getLayer( layerEntity, layer );
    
    // Verifier
    if( es != eOk )
        return NULL;
        
    // Recuperer la couleur de calque
    AcCmColor color = AcCmColor();
    
    // Verifier l'entité calque
    if( layerEntity != NULL )
    {
        //On recupere la couleur
        color = layerEntity->color();
        
        //Fermer l'entité calque
        layerEntity->close();
    }
    
    // Créer un nouveau calque dans la nouvelle base de données s'il n'y est pas encore
    es =  createLayer( layer, color, pDB );
    
    if( es != eOk )
        return NULL;
        
    // Calque
    es = pBlockExtern->setLayer( layer );
    
    
    // Fermer le model space
    pBlockTableRecord->close();
    
    // Si le nom du fichier n'est pas vide, enregistrer le fichier
    if( sFilePath.compare( "" ) != 0 )
        pDB->saveAs( sFilePath, false, AcDb::kDHL_1800 );
        
    // Renvoyer le résultat
    return pBlockExtern;
}


Acad::ErrorStatus synchronizeAttribute(
    AcDbBlockReference* pBlkRef,
    const AcString& attName,
    const map< AcString, AcDbObjectId >& attDefMap )
{
    //Récupération des attributs de la référence
    std::map<AcString, AcDbObjectId> attRefMap = getAttributesRef( pBlkRef );
    
    //Pointeur sur l'attribut choisi s'il est présent dans la référence
    std::map<AcString, AcDbObjectId>::const_iterator itRef = attRefMap.find( attName );
    
    //Pointeur sur l'attribut choisi s'il est présent dans la définition
    std::map<AcString, AcDbObjectId>::const_iterator itDef = attDefMap.find( attName );
    
    //Update
    if( itRef != attRefMap.end() && itDef != attDefMap.end() )
        return updateAtt( pBlkRef, itRef->second, itDef->second );
        
    //Remove
    else if( itRef != attRefMap.end() && itDef == attDefMap.end() )
        return removeAtt( itRef->second );
        
    //Add
    else if( itRef == attRefMap.end() && itDef != attDefMap.end() )
        return createAtt( pBlkRef, itDef->second );
}

Acad::ErrorStatus updateAtt(
    AcDbBlockReference* pBlkRef,
    AcDbObjectId refAttId,
    AcDbObjectId defAttId )
{
    Acad::ErrorStatus es;
    
    //Initialisation des attributs de la référence et de la définition
    AcDbAttribute* pAttRef;
    AcDbAttributeDefinition* pAttDef;
    
    //Ouverture des attributs
    if( es =  openAttribute( refAttId, pAttRef, AcDb::kForWrite ) )
        return es;
        
    if( es =  openAttributeDefinition( defAttId, pAttDef ) )
        return es;
        
    //Récupération de la position du bloc référence
    AcGeMatrix3d transformation = pBlkRef->blockTransform();
    
    //Copie de l'attribut de la définition dans l'attribut de la référence
    if( es = pAttRef->setAttributeFromBlock( pAttDef, transformation ) )
        return es;
        
    if( es = pAttRef->close() )
        return es;
        
    return pAttDef->close();
}

Acad::ErrorStatus removeAtt(
    AcDbObjectId refAttId )
{
    AcDbEntity* pEnt;
    Acad::ErrorStatus es;
    
    if( es = acdbOpenAcDbEntity( pEnt, refAttId, AcDb::kForWrite ) )
        return es;
        
    if( es = pEnt->erase() )
        return es;
        
    return pEnt->close();
}

Acad::ErrorStatus createAtt(
    AcDbBlockReference* pBlkRef,
    AcDbObjectId defAttId )
{
    Acad::ErrorStatus es;
    
    //Initialisation des attributs de la référence et de la définition
    AcDbAttribute* pAttRef = new AcDbAttribute();
    AcDbAttributeDefinition* pAttDef;
    
    //Ouverture de l'attribut de la définition
    if( es =  openAttributeDefinition( defAttId, pAttDef ) )
        return es;
        
    //Récupération de la position du bloc référence
    AcGeMatrix3d transformation = pBlkRef->blockTransform();
    
    //Ajout du nouvel attribut à la référence
    if( es = pBlkRef->appendAttribute( pAttRef ) )
        return es;
        
    //Copie de l'attribut de la définition dans l'attribut de la référence
    if( es = pAttRef->setAttributeFromBlock( pAttDef, transformation ) )
        return es;
        
    if( es = pAttDef->close() )
        return es;
        
    return pAttRef->close();
}


Acad::ErrorStatus insertBlockWithJig(
    int& res,
    const AcString& blockName,
    const double& scaleX,
    const double& scaleY,
    const double& rotation )
{
    // On récupère la définition du bloc
    AcDbBlockTableRecord* blockDef = getBlockDef(
            blockName );
            
    AcString acharBlockName = blockName;
    
    if( !blockDef )
        return Acad::eNullBlockName;
        
    // On récupère le type d'échelle du bloc
    AcDbBlockTableRecord::BlockScaling scaling;
    scaling = blockDef->blockScaling();
    blockDef->close();
    
    if( scaling != AcDbBlockTableRecord::BlockScaling::kUniform )
    {
        // Si rotation = - 1 le dessinateur doit clicker la rotation du bloc
        if( rotation == -1 )
        {
            res = gcedCommand( RTSTR,
                    _T( "_.insert" ),
                    RTSTR,
                    acStrToAcharPointeur( acharBlockName ),
                    RTSTR,
                    _T( "X" ),
                    RTREAL,
                    scaleX,
                    RTSTR,
                    _T( "Y" ),
                    RTREAL,
                    scaleY,
                    RTSTR,
                    _T( "\\" ),
                    RTSTR,
                    _T( "\\" ),
                    RTNONE );
        }
        
        // Sinon la rotation du bloc inséré est celle passée en param de cette fonction
        else
        {
            res = gcedCommand( RTSTR,
                    _T( "_.insert" ),
                    RTSTR,
                    acStrToAcharPointeur( acharBlockName ),
                    RTSTR,
                    _T( "X" ),
                    RTREAL,
                    scaleX,
                    RTSTR,
                    _T( "Y" ),
                    RTREAL,
                    scaleY,
                    RTSTR,
                    _T( "R" ),
                    RTREAL,
                    rotation,
                    RTSTR,
                    _T( "\\" ),
                    RTNONE );
        }
        
        return Acad::eOk;
    }
    
    // On ne peut pas avoir une échelle uniforme et des valeurs d'échelles différentes
    else if( scaling == AcDbBlockTableRecord::BlockScaling::kUniform &&
        scaleX != scaleY )
        return Acad::eNotApplicable;
        
    else
    {
        // Si rotation = - 1 le dessinateur doit clicker la rotation du bloc
        if( rotation == -1 )
        {
            res = gcedCommand( RTSTR,
                    _T( "_.insert" ),
                    RTSTR,
                    acStrToAcharPointeur( acharBlockName ),
                    RTSTR,
                    _T( "E" ),
                    RTREAL,
                    scaleX,
                    RTSTR,
                    _T( "\\" ),
                    RTSTR,
                    _T( "\\" ),
                    RTNONE );
        }
        
        // Sinon la rotation du bloc inséré est celle passée en param de cette fonction
        else
        {
            res = gcedCommand( RTSTR,
                    _T( "_.insert" ),
                    RTSTR,
                    acStrToAcharPointeur( acharBlockName ),
                    RTSTR,
                    _T( "E" ),
                    RTREAL,
                    scaleX,
                    RTSTR,
                    _T( "R" ),
                    RTREAL,
                    rotation,
                    RTSTR,
                    _T( "\\" ),
                    RTNONE );
        }
        
        return Acad::eOk;
    }
}


Acad::ErrorStatus getLastBlockInserted( AcDbBlockReference*& pBlock,
    const AcDb::OpenMode& opMode )
{
    ads_name ads_block;
    int res = acdbEntLast( ads_block );
    
    if( res != RTNORM )
        return Acad::eNotApplicable;
        
    // Id du dernier bloc inséré
    
    Acad::ErrorStatus es = Acad::eOk;
    AcDbObjectId idLast;
    
    // On récupère l'id du dernier bloc inséré
    es = acdbGetObjectId( idLast, ads_block );
    
    if( !idLast.isValid() )
        return es;
        
    // On récupère un pointeur sur l'entity sélectionnée
    AcDbEntity* pEnt = NULL;
    es = acdbOpenAcDbEntity( pEnt, idLast, opMode );
    
    // Verifier qu'on a bien une entité
    if( pEnt )
    {
        //Verifier si c'est une polyligne 3d simple
        if( pEnt->isKindOf( AcDbBlockReference::desc() ) )
            pBlock = AcDbBlockReference::cast( pEnt );
            
        else
            print( "Impossible de recuperer le bloc" );
    }
    
    acedSSFree( ads_block );
    
    return es;
}

Acad::ErrorStatus eraseLastInseredBlock()
{
    ads_name ads_block;
    int res = acdbEntLast( ads_block );
    
    if( res != RTNORM )
        return Acad::eNotApplicable;
        
    // Id du dernier bloc inséré
    AcDbObjectId blockId = AcDbObjectId::kNull;
    
    Acad::ErrorStatus es = Acad::eOk;
    
    // On récupère l'id du dernier bloc inséré
    if( es = acdbGetObjectId(
                blockId,
                ads_block ) )
        return es;
        
    AcDbBlockReference* pBlock;
    
    // On ouvre le dernier bloc inséré
    if( es = openBlock( blockId,
                pBlock,
                AcDb::kForWrite ) )
        return es;
        
    if( pBlock )
    {
        pBlock->erase();
        pBlock->close();
    }
    
    return es;
}

bool isDynamicBlock( AcDbBlockReference* pBlock )
{
    if( !pBlock )
        return false;
        
    // On créer une référence de bloc dynamique
    AcDbDynBlockReference pDynRef( pBlock );
    
    // Vérification
    return pDynRef.isDynamicBlock();
}

Acad::ErrorStatus getBlockPropWithValueList(
    AcDbBlockReference* pBlock,
    AcStringArray& propName,
    vector<void*>& propValue )
{
    Acad::ErrorStatus er = Acad::eOk;
    
    //On crée le bloc dynamique
    AcDbDynBlockReference pDynRef( pBlock );
    
    //On récupère les propriétés du bloc dynamique
    AcDbDynBlockReferencePropertyArray propArr;
    pDynRef.getBlockProperties( propArr );
    
    //On boucle sur les propriétés du bloc
    int length = propArr.length();
    ACHAR buf[ 512 ];
    AcDbDynBlockReferenceProperty prop;
    
    for( int i = 0; i < length; i++ )
    {
        //On récupère la propriété courante
        prop = propArr[ i ];
        
        AcDbEvalVariant pValVar = prop.value();
        int iVar = 0;
        
        //Recuperer le type de la propriete
        short rtype = pValVar.restype;
        
        //Variable
        double val;
        OdInt32 valInt;
        AcString valAcStr;
        AcGePoint3d valPt = AcGePoint3d::kOrigin;
        
        //Si c'est un double
        if( rtype >= 40 && rtype <= 59 && rtype != 48 || rtype >= 140 && rtype <= 147 )
        {
            er = pValVar.getValue( val );
            
            double* valTemp = new double( val );
            
            propName.push_back( prop.propertyName() );
            
            propValue.push_back( valTemp );
        }
        
        //int
        else if( rtype >= 61 && rtype <= 79 && rtype != 62 && rtype != -7 ||
            rtype >= 170 && rtype <= 175 )
        {
            er = pValVar.getValue( valInt );
            
            OdInt32* valIntTemp = new OdInt32( valInt );
            
            propName.push_back( prop.propertyName() );
            
            propValue.push_back( valIntTemp );
        }
        
        //Si c'est une string
        else if( rtype >= 1 && rtype <= 4 ||
            rtype == 7 ||
            rtype == 9 ||
            rtype >= 300 && rtype <= 309 )
        {
            er = pValVar.getValue( valAcStr );
            
            AcString* valAcStrTemp = new AcString( valAcStr );
            
            propName.push_back( prop.propertyName() );
            
            propValue.push_back( valAcStrTemp );
        }
        
        //Si c'est une AcGePoint3d
        else if( rtype >= 10 && rtype <= 17 ||
            rtype == 5009 )
        {
            er = pValVar.getValue( valPt );
            
            AcGePoint3d* valPtTemp = new AcGePoint3d( valPt );
            
            propName.push_back( prop.propertyName() );
            
            propValue.push_back( valPtTemp );
        }
    }
    
    return Acad::eOk;
}


Acad::ErrorStatus getBlockPropWithValueList(
    AcDbBlockReference* pBlock,
    AcStringArray& propName,
    AcStringArray& propValue )
{
    //On crée le bloc dynamique
    AcDbDynBlockReference pDynRef( pBlock );
    
    //On récupère les propriétés du bloc dynamique
    AcDbDynBlockReferencePropertyArray propArr;
    pDynRef.getBlockProperties( propArr );
    
    //On boucle sur les propriétés du bloc
    int length = propArr.length();
    AcDbDynBlockReferenceProperty prop;
    
    //Boucle pour recuperer les proprietes
    for( int i = 0; i < length; i++ )
    {
        //On récupère la propriété courante
        prop = propArr[ i ];
        
        //On recupere le nom du propriete
        propName.push_back( prop.propertyName() );
        
        if( prop.propertyType() == 5 )
        {
            AcString ddd;
            prop.value().getValue( ddd );
            
            //Recuperer la valeur de la proprieté
            propValue.push_back( ddd );
        }
        
        else
        {
            double dd;
            prop.value().getValue( dd );
            
            //Recuperer la valeur de la proprieté
            propValue.push_back( numberToAcString( dd, 5 ) );
        }
        
    }
    
    return Acad::eOk;
}


Acad::ErrorStatus setLayerOfAttribute(
    AcDbBlockReference* pBlock,
    const AcString& attributeName,
    const AcString& layerName )
{
    AcDbAttribute* attribute = getAttributObject( pBlock, attributeName, AcDb::kForWrite );
    
    if( !attribute )
        return eNotApplicable;
        
    if( eOk != attribute->setLayer( layerName ) )
    {
        attribute->close();
        return eNotApplicable;
    }
    
    attribute->close();
    return eOk;
}