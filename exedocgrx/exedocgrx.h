#pragma once
#include <map>
#include <set>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

#define DOC_HTML_0 "<!DOCTYPE html><html><head><link rel=\"stylesheet\" href=\"style.css\"><script src=\"C:/Futurmap/Outils/GStarCAD2020/HTM/GRX/searchScript.js\"></script></head><body>"
#define DOC_HTML_1 "<p class=\"top\"> <font color=\"#f35221\">  "
#define DOC_HTML_2 "</font>  "
#define DOC_HTML_3 "</p>"
#define DOC_HTML_4 "<p>"
#define DOC_HTML_5 "</p>"
#define DOC_HTML_6 "<p>"
#define DOC_HTML_7 "</p>"
#define DOC_HTML_8 "<div class=\"row\"><div class=\"col-md-3\"><img src=\""
#define DOC_HTML_9 "\" width=\"400\" height=\"200\"><figcaption>"
#define DOC_HTML_9_ONE "\" width=\"400\" height=\"200\"><figcaption>"
#define DOC_HTML_10 "</figcaption></div>"
#define DOC_HTML_11 "<div class=\"col-md-3\"><img src=\""
#define DOC_HTML_12 "\" width=\"400\" height=\"200\"><figcaption>"
#define DOC_HTML_13 "</figcaption></div></div>"
#define DOC_HTML_BR "<br>"
#define DOC_HTML_BULLETOPEN "<ul type=\"circle\"> \n"
#define DOC_HTML_BULLETOPENLIST "<li>"
#define DOC_HTML_BULLETENDLIST "</li>"
#define DOC_HTML_BULLETEND "\n<ul>"
#define CMD_NAME_KEY "* \\cmdName"
#define DOC_KEY "* \\doc"
#define DESC_KEY "* \\desc"
#define BULLET_KEY "* \\bullet"
#define SECTION_KEY "* \\section"
#define SOUS_DESC_KEY "* \\sous_desc"
#define IMG_1_KEY "* \\img1"
#define CAPTION_1_KEY "* \\captionimg1"
#define IMG_2_KEY "* \\img2"
#define CAPTION_2_KEY "* \\captionimg2"
#define IMG_KEY "* \\img"
#define CAPTION_KEY "* \\captionimg"
#define END_KEY "* \\end"
#define TEMPLATE_IMG "\n<table width=\"100%\" border=\"0\">\n\t<tbody>\n\t<tr>\n\t\t<td width=\"50%\" align=\"center\">\n\t\t\t<img src=\"img1_path\" width=\"450\" height=\"450\"/>\n\t\t\t<p>img1_caption</p>\n\t\t</td>\n\t\t<td width=\"50%\" align=\"center\">\n\t\t\t<img src=\"img2_path\" width=\"450\" height=\"450\"/>\n\t\t\t<p>img2_caption</p>\n\t\t</td>\n\t</tr>\n\t</tbody>\n</table>\n"

#define NEW_TEMPLATE_IMG "<table width=\"100%\" border=\"0\">\n\t<tbody>\n\t<tr>\n\t\t<td width=\"50%\" align=\"center\">\n\t\t\t<img src=\"img_path\"/>\n\t\t</td>\n\t\t</tr>\n\t</tbody>\n</table>\n"

struct ImgAndCapt
{
	string imgPath;
	string imgCapt;
};
struct FmapDoc
{
	string section;
	string strCmdName;
	string strIcons;
	string strDesc;
	vector<string> vecSousDesc;
	string strDoc;
	vector<string> vecFree;
	vector<ImgAndCapt> imgCapt;
	vector<string> vecBullet{};
	string strBrief;
};

map<string, string> argparse(int argc, char* argv[]);
bool splitPath(
	const string& path,
	string& dir,
	string& name,
	string& ext);

void newWriteDoc(ifstream& fileCmd, ofstream& fileHtm);

bool replaceFirst(std::string& str, const std::string& from, const std::string& to);

void showHelp();