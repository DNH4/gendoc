/**
  * \file       circleEntity.cpp
  * \author     Marielle H.R
  * \brief      Fichier contenant des fonctions utiles pour travailler avec les cercles
  * \date       16 juillet 2019
 */


#pragma once
#define _USE_MATH_DEFINES
#include "circleEntity.h"
#include "layer.h"
#include "blockEntity.h"


long getSsCircle(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "", "CIRCLE", layerName );
}



long getSsOneCircle(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSingleSelection( ssName, "CIRCLE", layerName );
}



long getSsAllCircle(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "X", "CIRCLE", layerName ) ;
}




AcDbCircle* getCircleFromSs(
    const ads_name&        ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  pEntityCircle = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbCircle::cast( pEntityCircle );
}



AcDbCircle* readCircleFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getCircleFromSs( ssName, iObject, AcDb::kForRead );
}



AcDbCircle* writeCircleFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getCircleFromSs( ssName, iObject, AcDb::kForWrite );
}



AcGePoint3dArray discretize( AcDbCircle* circle, const double& fleche )
{
    //Declaration et initialisation des variables
    AcGePoint3d pt1, pt2, pt3, pt4;
    double centerX, centerY, centerZ, radius;
    
    centerX = circle->center().x;
    centerY = circle->center().y;
    centerZ = circle->center().z;
    radius = circle->radius();
    
    //On recupere les 4 points du cercle
    
    // On recupere l'Ouest
    pt1.x = centerX - radius;
    pt1.y = centerY;
    pt1.z = centerZ;
    
    // On recupere le Nord
    pt2.x = centerX;
    pt2.y = centerY + radius;
    pt2.z = centerZ;
    
    // On recupere l'Est
    pt3.x = centerX + radius;
    pt3.y = centerY;
    pt3.z = centerZ;
    
    // On recupere le Sud
    pt4.x = centerX;
    pt4.y = centerY - radius;
    pt4.z = centerZ;
    
    
    // Discretisation de l'arc superieur
    AcGePoint3dArray ptArray = discretize( AcGeCircArc3d( pt1, pt2, pt3 ), fleche );
    
    // Discretisation de l'arc inferieur
    AcGePoint3dArray ptArray1 = discretize( AcGeCircArc3d( pt3, pt4, pt1 ), fleche );
    
    // Discretisation du cercle
    ptArray.append( ptArray1 );
    
    //On retourne le resultat
    return ptArray;
    
}

AcDbCircle* insertCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcGeVector3d& normal )
{
    // Declarer le cercle
    AcDbCircle* circle = new AcDbCircle( ptCenter, normal, radius );
    
    // Ajouter le cercle � la base de donn�es
    addToModelSpace( circle );
    
    //Retourner l'objet circle
    return circle;
}


Acad::ErrorStatus insertCircle( AcDbObjectId& circleId,
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcGeVector3d& normal )
{
    // Declarer le cercle
    AcDbCircle* circle = new AcDbCircle( ptCenter, normal, radius );
    
    
    // Ajouter le cercle � la base de donn�es
    addToModelSpace( circle, circleId );
    
    if( circleId.isNull() )
        return Acad::eNullObjectId;
        
    circle->close();
    
    return Acad::eOk;
}


Acad::ErrorStatus drawCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcGeVector3d& normal,
    const bool& hasToZoom )
{
    //Declarer le cercle
    AcDbCircle* circle = new AcDbCircle( ptCenter, normal, radius ) ;
    
    //Ajouter le cercle au modelspace
    addToModelSpace( circle );
    
    if( hasToZoom == true )
        zoomOn( circle );
        
    //Fermer le cercle
    circle->close();
    
    return Acad::eOk;
}

Acad::ErrorStatus drawCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcString& layer,
    const AcGeVector3d& normal,
    const bool& hasToZoom )
{
    Acad::ErrorStatus res = createLayer( layer );
    
    if( res != Acad::eOk )
        return res;
        
    //Declarer le cercle
    AcDbCircle* circle = new AcDbCircle( ptCenter, normal, radius ) ;
    
    //Ajouter le cercle au modelspace
    addToModelSpace( circle );
    
    if( hasToZoom == true )
        zoomOn( circle );
        
    //Ajouter au calque
    AcCmColor color = AcCmColor();
    color.setRGB( 255, 0, 0 );
    createLayer( layer,
        color );
        
    circle->setLayer( layer );
    circle->setColorIndex( 256 );
    
    //Fermer le cercle
    circle->close();
    
    return Acad::eOk;
}


Acad::ErrorStatus drawCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcString& layer,
    const int& indexColor,
    const AcGeVector3d& normal,
    const bool& hasToZoom )
{
    Acad::ErrorStatus res = createLayer( layer );
    
    if( res != Acad::eOk )
        return res;
        
    //Declarer le cercle
    AcDbCircle* circle = new AcDbCircle( ptCenter, normal, radius );
    
    //Ajouter le cercle au modelspace
    addToModelSpace( circle );
    
    if( hasToZoom == true )
        zoomOn( circle );
        
    //Ajouter au calque
    AcCmColor color = AcCmColor();
    color.setRGB( 255, 0, 0 );
    createLayer( layer,
        color );
        
    circle->setLayer( layer );
    circle->setColorIndex( indexColor );
    
    //Fermer le cercle
    circle->close();
    
    return Acad::eOk;
}

Acad::ErrorStatus drawCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcString& layer,
    const AcString& message,
    const AcGeVector3d& normal,
    const bool& hasToZoom )
{
    Acad::ErrorStatus res = createLayer( layer );
    
    if( res != Acad::eOk )
        return res;
        
    //Declarer le cercle
    AcDbCircle* circle = new AcDbCircle( ptCenter, normal, radius ) ;
    
    //Ajouter le cercle au modelspace
    addToModelSpace( circle );
    
    if( hasToZoom == true )
        zoomOn( circle );
        
    //Ajouter au calque
    AcCmColor color = AcCmColor();
    color.setRGB( 255, 0, 0 );
    createLayer( layer,
        color );
        
    circle->setLayer( layer );
    circle->setColorIndex( 256 );
    
    //Fermer le cercle
    circle->close();
    
    //Ajout du message
    AcGePoint3d ptText = AcGePoint3d( ptCenter.x + radius, ptCenter.y + radius, ptCenter.z );
    AcDbText* text = new AcDbText( ptText, message );
    addToModelSpace( text );
    text->setLayer( layer );
    text->close();
    
    return Acad::eOk;
}

Acad::ErrorStatus zoomOn( AcDbCircle* circle,
    const double& height )
{
    return zoomOn( circle->center(), height ) ;
}


Acad::ErrorStatus
openCircle( const AcDbObjectId& idCircle,
    AcDbCircle*& circle,
    const AcDb::OpenMode& mode )
{
    Acad::ErrorStatus es;
    AcDbEntity* pEnt;
    
    // On ouvre l'entit�
    es = acdbOpenAcDbEntity(
            pEnt,
            idCircle,
            mode );
            
    // Retour s'il y a une erreur
    if( es != Acad::eOk )
        return es;
        
    // On cast en cercle
    circle = AcDbCircle::cast( pEnt );
    
    return Acad::eOk;
}


Acad::ErrorStatus importAcDbCircle( AcDbObjectIdArray& objectIdArray,
    const AcString& dwgFile )
{
    // R�cuperer la database
    AcDbDatabase* dbExtern = new AcDbDatabase( Adesk::kFalse );
    
    // Ouvrir la base de donn�es externe
    Acad::ErrorStatus es;
    es = dbExtern->readDwgFile( dwgFile );
    
    if( es != Acad::eOk )
    {
        //Sortir
        dbExtern->close();
        return es;
    }
    
    // R�cuperer la liste des blocks
    AcDbBlockTable* pBlockTable = NULL;
    dbExtern->getSymbolTable( pBlockTable, AcDb::kForRead );
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    
    // R�cuperer le model space, puis ferme la liste des blocks
    pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForRead );
    pBlockTable->close();
    
    // On cr�e un it�rateur sur les objets du model space
    AcDbBlockTableRecordIterator* it;
    pBlockTableRecord->newIterator( it );
    it->start();
    
    // On boucle jusqu'� ce qu'on est parcouru tous les objets du dessin
    AcDbEntity* ent;
    
    while( !it->done() )
    {
        // R�cup�rer l'entit� courante
        es = it->getEntity( ent, AcDb::kForRead );
        
        if( es != Acad::eOk )
            return es;
            
        // On v�rifie si l'entit� est bien un cercle
        if( ent->isKindOf( AcDbCircle::desc() ) )
            objectIdArray.append( ent->objectId() );
            
        // On oublie pas de fermer l'entit�
        es = ent->close();
        
        if( es != Acad::eOk )
            return es;
            
        // On incr�mente l'it�rateur
        it->step();
    }
    
    // Fermer le model space
    return pBlockTableRecord->close();
}



std::vector<pair<AcDbObjectId, AcGePoint3d>> getIdAndInsertPtOfSSCircle( const ads_name& ssCircle )
{
    //Declaration du resultat
    std::vector<pair<AcDbObjectId, AcGePoint3d>> vecRes;
    
    //Recuperer la taille de la selection
    long taille = getSsLength( ssCircle );
    
    //Boucle pour ajouter les sommets des blocs dans le vecteur
    for( long counter = 0; counter < taille; counter++ )
    {
        // R�cuperer la poly3D
        if( AcDbCircle* circle = readCircleFromSs( ssCircle, counter ) )
        {
        
            if( circle )
            {
                pair<AcDbObjectId, AcGePoint3d> data ;
                data.first = circle->id();
                data.second = circle->center();
                
                //On sauvegarde
                vecRes.push_back( data );
                
                //On ferme le bloc
                circle->close();
                circle = NULL;
            }
        }
    }
    
    //Retourner le resultat
    return vecRes;
}

AcDbCircle* getCircleAroundPoint( const ads_name& ssCircle,
    const AcGePoint3d& pt )
{
    //Recuperer la taille de la selection
    long taille = getSsLength( ssCircle );
    
    if( taille == 0 )
        return NULL;
        
    double buDist = 100000, dist = 0;
    AcDbCircle* res = NULL;
    AcGePoint3d ptCenter = AcGePoint3d::kOrigin;
    double radius = 0;
    
    for( long counter = 0; counter < taille; counter++ )
    {
        // R�cuperer la poly3D
        AcDbCircle* circle = readCircleFromSs( ssCircle, counter );
        
        if( circle )
        {
            // On prend le centre du cercle
            ptCenter = circle->center();
            radius = circle->radius();
            
            if( isPointInside2DCircle( ptCenter, radius, pt ) )
            {
                //On calcul la distance
                dist = getDistance2d( ptCenter, pt );
                
                //On sauvegarde la distance ainsi que le cercle
                if( dist < buDist )
                {
                    res = circle;
                    buDist = dist;
                }
            }
            
            //On ferme le bloc
            circle->close();
            circle = NULL;
        }
        
    }
    
    return res;
}


AcDbObjectIdArray getSsBlocksInCircle( AcDbObjectIdArray& arBlocksId,  AcGePoint3dArray& arBlocksPt,  vector<double>& xPos,  AcDbCircle*& circle )
{
    AcDbObjectIdArray res;
    
    int lengthBlock = arBlocksId.length();
    
    if( lengthBlock  == 0 )
        return res;
        
    if( !circle ) //Improbale met on met quand m�me
        return res;
        
    //On r�cup�re le centre et le rayon du cercle
    AcGePoint3d center = circle->center();
    double radius = circle->radius();
    AcDbBlockReference* block = NULL;
    vector<double> vecZ;
    vector<int> vecIdx;
    
    //Tri des blocs pour avoir seulements les blocs qui sont dans le
    if( isInList( getPoint2d( center ), xPos, arBlocksPt, vecZ, vecIdx, radius ) )
    {
        int size = vecIdx.size();
        
        for( int i = 0; i < size; i++ )
        {
        
            if( openBlock( arBlocksId[i], block ) == eOk )
            {
                res.append( block->id() );
                block->close();
                block = NULL;
                continue;
            }
            
        }
        
    }
    
    
    return res;
}


AcDbObjectIdArray getSsPolyInCircle( const ads_name& ssPoly,  AcDbCircle*& circle )
{

    AcDbObjectIdArray res;
    
    long lengthPoly = getSsLength( ssPoly );
    
    if( lengthPoly == 0 )
        return res;
        
    if( !circle )
        return res;
        
    //Plan de projection pour l'intersection
    AcGePlane projectionPlane = AcGePlane( AcGePoint3d::kOrigin, AcGeVector3d::kZAxis );
    AcDb3dPolyline* poly = NULL;
    AcDbExtents polyBox;
    AcGePoint3d polyLftBottomPt, polyRgtTopPt;
    
    //On r�cup�re la bounding box du cercle
    AcDbExtents circleBox;
    circle->getGeomExtents( circleBox );
    AcGePoint3d lftBottomPt = circleBox.minPoint();
    AcGePoint3d rgtTopPt = circleBox.maxPoint();
    
    //On boucle sur chaque polyligne
    for( long polyIdx = 0; polyIdx < lengthPoly; polyIdx++ )
    {
        //1. On prend la polyligne
        poly = getPoly3DFromSs( ssPoly, polyIdx );
        
        if( !poly )
        {
            //nettoyage
            res.removeAll();
            res.resize( 0 );
            break;//On arrete l'op�ration en cours
        }
        
        //1.1 R�cup�ration de la bounding box de la polyligne
        poly->getGeomExtents( polyBox );
        polyLftBottomPt = polyBox.minPoint();
        polyRgtTopPt = polyBox.maxPoint();
        
        //V�rification des bounding box
        if( are2RectIntersected( polyLftBottomPt, polyRgtTopPt, lftBottomPt, rgtTopPt ) )
        {
            AcGePoint3dArray arIntersection;
            // On cacul les intersections des 2 polylines sur le plan de projections
            ErrorStatus errorStatus = poly->intersectWith( circle,
                    AcDb::kOnBothOperands,
                    projectionPlane,
                    arIntersection );
                    
            //On sauvearde l'Id de la polyligne
            if( errorStatus == Acad::eOk )
                res.append( poly->id() );
        }
        
        poly->close();
        poly = NULL;
        
    }
    
    return res;
}


Acad::ErrorStatus compareCircles(
    bool& areEqual,
    const AcDbObjectId& id1,
    const AcDbObjectId& id2,
    const double& precision )
{
    AcDbCircle* circle1, *circle2;
    Acad::ErrorStatus es;
    
    // On r�cupre les cercles
    es = openCircle( id1,
            circle1,
            AcDb::kForRead );
            
    if( es != Acad::eOk )
        return es;
        
    if( !circle1 )
    {
        areEqual = false;
        return Acad::eOk;
    }
    
    es = openCircle( id2,
            circle2,
            AcDb::kForRead );
            
    if( es != Acad::eOk )
        return es;
        
    if( !circle2 )
    {
        areEqual = false;
        return Acad::eOk;
    }
    
    //On r�cup�re les calques des deux cercles
    AcString layer1, layer2;
    layer1 = circle1->layer();
    layer2 = circle2->layer();
    
    // On compare les rayons et les centres
    AcGePoint3d center1, center2;
    
    if( !isEqual2d( center1, center2, precision ) )
        areEqual = false;
        
    else
    {
        //On comapre les calques
        // Simplification des comparaisons
        int sizeLayer1 = layer1.length();
        //V�rification si le nom de calque layer2 se trouve � la fin de layer 1
        int sizeLayer2 = layer2.length();
        int toRem = sizeLayer2 - sizeLayer1;
        
        if( toRem >= 0 )
        {
            AcString tmpLayer2 = layer2.substrRev( sizeLayer1 );
            
            if( tmpLayer2.compare( layer1 ) == 0 )
                areEqual = true;
            else
                areEqual = false;
                
        }
        
    }
    
    // On n'oublie pas de fermer les cercles
    es = circle1->close();
    
    if( es != Acad::eOk )
        return es;
        
    es = circle2->close();
    
    if( es != Acad::eOk )
        return es;
        
    return Acad::eOk;
}


Acad::ErrorStatus changeCircleToOkLayer( AcDbCircle*& circle )
{
    AcString layer = circle->layer();
    
    AcString okLayer = _T( "OK_" ) + layer;
    AcCmColor green = AcCmColor();
    green.setColorIndex( 3 );
    Acad::ErrorStatus es = createLayer( okLayer, green );
    
    if( es != Acad::eOk )
        return es;
        
    circle->setColor( AcCmEntityColor::kByLayer );
    
    return circle->setLayer( okLayer );
}