/**
  * \file       faceEntity.h
  * \author     Dinahasina RALAIVAO
  * \brief      Fichier contenant des fonctions utiles pour travailler avec les faces
  * \date       16 Aought 2019
 */

#pragma once
#include "selectEntity.h"
#include "pointEntity.h"
#include "groupEntity.h"

/**
  * \brief Fait une selection quelconque sur les faces
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsFace(
    ads_name&  ssName,
    const AcString&  layerName = "" );

/**
  * \brief Selectionne tous les faces du dessin
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */

long getSsAllFace(
    ads_name  ssName,
    const AcString&  layerName = "" );

/**
  * \brief Renvoie un pointeur sur la face numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de la face a recuperer, par defaut 0
  * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \return Pointeur sur une face
  */
AcDbFace* getFaceFromSs(
    const ads_name& ssName,
    const long& iObject          = 0,
    const AcDb::OpenMode&    opMode = AcDb::kForRead );


/**
  * \brief Renvoie un pointeur en LECTURE sur la face numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de la  face recuperer, par defaut 0
  * \return Pointeur sur un face
  */
AcDbFace* readFaceFromSs(
    const ads_name&    ssName,
    const long&        iObject = 0 );


/**
  * \brief Renvoie un pointeur en ECRITURE sur la face numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du cercle a recuperer, par defaut 0
  * \return Pointeur sur un face
  */
AcDbFace* writeFaceFromSs(
    const ads_name&        ssName,
    const long&            iObject = 0 );

/**
  * \brief Creer une Cube � partir d'un point qui va �tre le centre du cube
  * \param point Le point central du cube
  * \param cote La longueur du c�t� du cube
  * \param intensity ???
  * \param blockTable ???
  * \return Vide
  */
void createCube(
    AcGePoint3d point,
    const double& cote,
    const double& intensity );


/**
  * \brief Creer une Cube � partir d'un point qui va �tre le centre du cube
  * \param point Le point central du cube
  * \param cote La longueur du c�t� du cube
  * \param hauteur La hauteur du cube
  * \param color La couleur du cube
  * \return Vide
  */
void createCube(
    AcGePoint3d point,
    const double& coteLarg,
    const double& coteLong,
    const double& hauteur,
    const double& rotation = 0,
    const AcString& layer = _T( "0" ),
    const AcCmColor& color = AcCmColor() );


/**
  * \brief Renvoie les sommets distincts d'une face
  * \param face Pointeur vers la face
  * \param vertices Vecteur contenant les sommets distincts
  * \return ErrorStatus
  */

Acad::ErrorStatus getDistinctVertices(
    const AcDbFace* face,
    vector<AcGePoint3d>& vertices );


/**
  * \brief V�rifie si un point est situ� � l'int�rieur d'une face en vue de dessus
  * \param face Pointeur sur la face
  * \param pt Point dont on veut v�rifier s'il apparatient � la face en 2d
  * \return True si le point est � l'int�rieur de la face, False sinon
  */

bool isInFace2D(
    const AcDbFace* face,
    const AcGePoint3d& pt );


/**
  * \brief Projete un AcGePoint3d sur une face suivant une ligne de projection donn�e
  * \param face (Input) Pointeur sur la face
  * \param ptToProject (Input) Point � projeter
  * \param projDir (Input) Vecteur de projection
  * \param projected (Output) True si le point a �t� projet�
  * \param projectedPt (Output) Point projet� (�gal � ptToProject s'il n'y a pas de projection)
  * \return ErrorStatus
  */
Acad::ErrorStatus projectPointOnFace( const AcDbFace* face,
    const AcGePoint3d& ptToProject,
    const AcGeVector3d& projDir,
    bool& projected,
    AcGePoint3d& projectedPt );