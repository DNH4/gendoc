#pragma once
#include <dbents.h>
#include "print.h"
#include "selectEntity.h"
#include "layer.h"

Acad::ErrorStatus getTexteStyleId( const AcString& styleName,
    AcDbObjectId& recordId );

Acad::ErrorStatus getStyleStringName( AcString& textStyle, const AcDbObjectId& obId );

Acad::ErrorStatus drawTextAcString(AcString text, AcGePoint3d,AcString layer = _T(""));


//AcDbText* drawTextAcString(AcString acTest, AcGePoint3d insertPoint);