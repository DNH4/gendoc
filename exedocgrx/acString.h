/**
 * @file acString.h
 * @author Romain LEMETTAIS
 * @brief Outils de conversion des textes
 */

#ifndef ACSTRING_H
#define ACSTRING_H
#pragma once
#include <fmapHeaders.h>

/**
 * \brief Convertit une AcString en ACHAR*
 * En g�n�ral on peut �crire ACHAR* var = _T( "myVar" )
 * Mais si on veut �crire var ACHAR* var = varAcString on doit passer par cette fonction
 * \param acStr AcString � convertir
 * \return ACHAR* correspondante
 */
ACHAR* acStrToAcharPointeur( AcString& acStr );


/**
 * \brief Convertit une string en AcString
 * \param s String � convertir
 * \return AcString correspondante
 */
AcString strToAcStr( const string& s );

/**
 * \brief Supprime tous les accents sur un AcString
 * \param s AcString � convertir
 * \return AcString correspondante
 */
AcString  acStrWithoutAccent( const AcString& s );


/**
 * \brief Convertit une AcString en string
 * \param AcString � convertir
 * \return String correspondante
 */
string acStrToStr( const AcString& acString );


/**
  * \brief Convertit un double en AcString
  * \param value Nombre a convertir
  * \param numberOfDigits Nombre de decimales a afficher
  * \return AcString contenant le double
  */
AcString numberToAcString(
    const double&  value,
    const int&     numberOfDigits = 0 );


/**
 * \brief Convertit un int en AcString pour pouvoir l'afficher dans GStarCAD
 * \param d int a convertir
 * \return AcString correspondante
 */
AcString numberToAcString( const int& d );



/**
 * \brief Convertit un float en AcString pour pouvoir l'afficher dans GStarCAD
 * \param d float a convertir
 * \return AcString correspondante
 */
AcString numberToAcString( const float& d );

/**
 * \brief
 * \param
 * \return
 */
AcString numberToAcString( const unsigned __int64& d );


/**
  * \brief Convertit un AcString en Double
  * \param str String a convertir
  * \param value Valuer qu'on veut lui donner
  * \return True si la conversion s'est bien passee, false sinon
  */
bool stringToDouble(
    const AcString    str,
    double* const     value );



/**
* \brief Convertit un AcString en Int
* \param str String a convertir
* \param value Valuer qu'on veut lui donner
* \return True si la conversion s'est bien passee, false sinon
*/
bool stringToInt(
    const AcString    str,
    int* const        value );



/**
 * \brief Compare deux textes sans prendre en compte les majuscules/minuscules
 * \param sText1 premier texte
 * \param sText2 second texte
 * \return true si les deux textes sont identiques
 */
bool areEqualNoCase(
    const AcString& sText1,
    const AcString& sText2 );


/**
  * \brief Compare deux textes en prenant en compte les majuscules/minuscules
  * \param sText1 premier texte
  * \param sText2 second texte
  * \return true si les deux textes sont identiques
  */
bool areEqualCase(
    const AcString& sText1,
    const AcString& sText2 );



/**
 * \brief Decoupe une string en vecteur de string
 * \param sentence String a decouper
 * \param sep Separateur recherche pour faire la d�coupe: espace, tabulation ...
 * \return Vecteur de sous-string, si la string en entree ne contient pas de separateur, la renvoie dans un vecteur
 */
vector< string > splitString(
    string sentence,
    const string& sep );


/**
 * \brief Verifie si une chaine de caractere de type string contient un nombre
 * \param word String a verifier
 * \return booleen : true si la chaine contient un nombre false sinon
 */
bool containsNumber(
    const string& word );


/**
 * \brief Convertir une chaine de caract�re en majuscule
 * \param word String a convertir
 * \param hasAccent booleen on garde ou pas les accents
 * \return string : chaine de caract�re en majuscule
 */
string toUpperString(
    const string& word,
    const bool& hasAccent = false );


/**
 * \brief Convertir une chaine de caract�re en majuscule
 * \param word AcString a convertir
 * \param hasAccent booleen on garde ou pas les accents
 * \return AcString : chaine de caract�re en majuscule
 */
AcString toUpperString(
    const AcString& word,
    const bool& hasAccent = false );

/**
 * \brief Effacer l'espace avant et apr�s la chaine de caract�re
 * \param str string a convertir
 * \return string : chaine de caract�re en majuscule
 */
string trimLeftRight(
    const string& str );

/**
 * \brief Effacer l'espace avant et apr�s la chaine de caract�re
 * \param input AcString a convertir
 * \return AcString : chaine de caract�re en majuscule
 */
AcString  trimLeftRight(
    const AcString& input );


/**
 * \brief Tester si le Keyword rentr� par l'utilisateur correspond � une option de la commande
 * \param oneString Keyword � tester
 * \param command Nom complet de la commande
 * \param shortcutCmd Raccourci de la commande
 * \return Bool : Si c'est bien le racourci de la commande
 */
bool isEqualToCommand( const AcString& oneString,
    const AcString& command,
    const AcString& shortcutCmd );


AcString numberReadable( const AcString& number, const AcString& sep = " " );
#endif


/**
 * \brief Tester si le Keyword rentr� par l'utilisateur correspond � une option de la commande
 * \param oneString Keyword � tester
 * \param command Nom complet de la commande
 * \param shortcutCmd Raccourci de la commande
 * \return Bool : Si c'est bien le racourci de la commande
 */
bool isAcstringNumber(const AcString& s);