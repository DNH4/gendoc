//-----------------------------------------------------------------------------------<arcEntity.cpp>
// Society: Futurmap
// Author : Romain LEMETTAIS
// Application: AutoCAD 2015
// Decription: Tools to work with arcs
// Decription (fr): Outil pour travailler avec les arcs


#define _USE_MATH_DEFINES
#include "arcEntity.h"
#define PI 3.14159265


long getSsArc(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "", "ARC", layerName );
}



long getSsOneArc(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSingleSelection( ssName, "ARC", layerName );
}



long getSsAllArc(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "X", "ARC", layerName ) ;
}




AcDbArc* getArcFromSs(
    const ads_name&        ssName,
    const long            iObject,
    const AcDb::OpenMode  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  pEntityArc = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbArc::cast( pEntityArc );
}



AcDbArc* readArcFromSs(
    const ads_name&        ssName,
    const long            iObject )
{
    //Recuperer l'entite
    return getArcFromSs( ssName, iObject, AcDb::kForRead );
}



AcDbArc* writeArcFromSs(
    const ads_name&        ssName,
    const long            iObject )
{
    //Recuperer l'entite
    return getArcFromSs( ssName, iObject, AcDb::kForWrite );
}



AcGeCircArc3d* getAcGeArc(
    AcDbArc* arcDb )
{
    //Recuperer les parametre de dbut et fin
    double param1, param2;
    arcDb->getStartParam( param1 );
    arcDb->getEndParam( param2 );
    
    //Recuperer les 3 points de l'arc
    AcGePoint3d pt1, pt2, pt3;
    arcDb->getStartPoint( pt1 );
    arcDb->getPointAtParam( 0.5 * ( param1 + param2 ), pt2 );
    arcDb->getEndPoint( pt3 );
    return new AcGeCircArc3d( pt1, pt2, pt3 );
}



AcGePoint3dArray discretize(
    const AcGeCircArc3d arc,
    const double&        tol )
{
    //Declarer les tableaux
    AcGePoint3dArray  ptArray;
    AcGeDoubleArray   dArray;
    
    //Discretiser la courbe
    arc.getSamplePoints(
        arc.paramOf( arc.startPoint() ),
        arc.paramOf( arc.endPoint() ),
        tol,
        ptArray,
        dArray );
        
    //Vider la liste
    dArray.setLogicalLength( 0 );

    //Sortir
    return ptArray;
}



AcGePoint3dArray discretize(
    AcDbArc*     arc,
    const double&       tol )
{
    return discretize( *getAcGeArc( arc ), tol );
}



double getTotalAngle(
    const AcDbArc*  arc )
{
    double diff = arc->endAngle() - arc->startAngle();
    
    if( diff > 0 )
        return diff;
    else
        return( diff + 2 * M_PI );
}


double getBulgeAt( AcDbArc* arc )
{
    return tan( getTotalAngle( arc ) / 4 );
}

double getTotalAngle( double startAngle, double endAngle )
{
    double diff = endAngle - startAngle;
    
    if( diff > 0 )
        return diff;
    else
        return( diff + 2 * M_PI );
}

double getBulgeAt( double startAngle, double endAngle )
{
    return tan( getTotalAngle( startAngle, endAngle ) / 4 );
}


AcDbArc* arcByThreePoints(
    const AcGePoint2d& pt1,
    const AcGePoint2d& pt2,
    const AcGePoint2d& pt3 )
{
    // On cr?e un AcGeCircArc3d
    AcGeCircArc3d geArc( getPoint3d( pt1 ),
        getPoint3d( pt2 ),
        getPoint3d( pt3 ) );
        
    // Pointeur sur l'AcDbArc
    AcDbArc* pArc = NULL;
    
    // On convertit l'AcGeCircArc en AcDbArc
    AcDbCurve::createFromOdGeCurve( geArc, ( AcDbCurve*& ) pArc );
    
    return pArc;
}



AcDbArc* arcByThreePoints(
    const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const AcGePoint3d& pt3 )
{
    // On cr?e un AcGeCircArc3d
    AcGeCircArc3d geArc( pt1, pt2, pt3 );
    
    // Pointeur sur l'AcDbArc
    AcDbArc* pArc = NULL;
    
    // On convertit l'AcGeCircArc en AcDbArc
    AcDbCurve::createFromOdGeCurve( geArc, ( AcDbCurve*& ) pArc );
    
    return pArc;
}


AcGePoint2d getMidPoint( const AcGePoint2d& pt1, const AcGePoint2d& pt2, const double& bulge, const double& tol )
{
	//Declarer le point resultat
	AcGePoint3d ptRes = AcGePoint3d::kOrigin;

	//Declarer une polyligne 2d
	AcDbPolyline* resPoly = new AcDbPolyline();

	//Ajouter le sommet dans la polyligne avec le bulge correspondant
	resPoly->addVertexAt( 0, pt1, bulge );

	//Ajouter le second point 
	resPoly->addVertexAt( 1, pt2 );

	//Declarer un arc
	AcGeCircArc3d arc;

	//Recuperer l'arc
	resPoly->getArcSegAt( 0, arc );

	//Discretiser l'arc
	AcGePoint3dArray ptArray = discretize( arc, tol );

	//Recuperer le point central
	ptRes = ptArray[ ptArray.length() / 2 ];
	
	//Supprimer la polyligne
	delete resPoly;

	//Retourner le point resultat
	return getPoint2d( ptRes );
}