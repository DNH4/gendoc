#pragma once
#include "selectEntity.h"
#include <math.h>


/**
  * \brief S�lectionner des rep�res multiple (MLEADER)
  * \param ssName S�lection de rep�res multiple
  * \param layerName Nom de calque des objets � selectionner
  * \return long Nombre d'objets s�lectionn�s
  */
long getSsMLeader(
    ads_name&        ssName,
    const AcString&  layerName = "" );


/**
  * \brief S�lectionner un rep�re multiple (MLEADER)
  * \param ssName S�lection de rep�re multiple
  * \param layerName Nom de calque de l'objet � selectionner
  * \return long Nombre d'objet s�lectionn�
  */
long getSsOneMLeader(
    ads_name&        ssName,
    const AcString&  layerName = "" );


/**
  * \brief S�lectionner tous les rep�res multiple (MLEADER) dans le dessin
  * \param ssName S�lection de rep�res multiple
  * \param layerName Nom de calque des objets � selectionner
  * \return long Nombre d'objets s�lectionn�
  */
long getSsAllMLeader(
    ads_name&        ssName,
    const AcString&  layerName = "" );


/**
  * \brief Recuperer un rep�re multiple dans la s�lection
  * \param ssName S�lection de rep�res multiple
  * \param iObject Rang de l'objet dans la s�lection
  * \param opMode Mode d'ouverture (par d�faut en lecture)
  * \return AcDbMLeader Pointeur qui pointe vers l'objet
  */
AcDbMLeader* getMLeaderFromSs(
    const ads_name&        ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode = AcDb::kForRead );


/**
  * \brief Recuperer un rep�re multiple dans la s�lection (en lecture)
  * \param ssName S�lection de rep�res multiple
  * \param iObject Rang de l'objet dans la s�lection
  * \return AcDbMLeader Pointeur qui pointe vers l'objet
  */
AcDbMLeader* readMLeaderFromSs(
    const ads_name&        ssName,
    const long&            iObject );


/**
  * \brief Recuperer un rep�re multiple dans la s�lection (en ecriture)
  * \param ssName S�lection de rep�res multiple
  * \param iObject Rang de l'objet dans la s�lection
  * \return AcDbMLeader Pointeur qui pointe vers l'objet
  */
AcDbMLeader* writeMLeaderFromSs(
    const ads_name&        ssName,
    const long&            iObject );



/**
  * \brief Dessiner un simple rep�re multiple associ� � un texte multiligne
  * \param mleader Pointeur qui recup�re l'objet rep�re multiple � dessiner
  * \param ptFirst Point cible du rep�re multiple
  * \param textLocation Point d'insertion du texte
  * \param textContent Contenu du texte
  * \param textHeight Hauteur du texte
  * \return Acad::ErrorStatus
  */
Acad::ErrorStatus drawSimpleMleader( AcDbMLeader*& mleader,
    const AcGePoint3d& ptFirst,
    const AcGePoint3d& textLocation,
    const AcString& textContent,
    const double& textHeight = 1.00 );
