/**
 * @file blockEntity.h
 * @author Romain LEMETTAIS
 * @brief Fonctions mathematiques utiles
 */


#pragma once
#include "fmapMath.h"

double round( const double& value, const int& numberOfDigits )
{

    if( numberOfDigits == 0 )
        return floor( value );
        
    if( numberOfDigits == 1 )
        return floor( value * 10 + 0.5 ) / 10;
        
    if( numberOfDigits == 2 )
        return floor( value * 100 + 0.5 ) / 100;
        
    if( numberOfDigits == 3 )
        return floor( value * 1000 + 0.5 ) / 1000;
        
    if( numberOfDigits == 4 )
        return floor( value * 10000 + 0.5 ) / 10000;
        
    if( numberOfDigits == 5 )
        return floor( value * 100000 + 0.5 ) / 100000;
        
    if( numberOfDigits == 6 )
        return floor( value * 1000000 + 0.5 ) / 1000000;
        
    if( numberOfDigits == 7 )
        return floor( value * 10000000 + 0.5 ) / 1000000;
        
    if( numberOfDigits == 8 )
        return floor( value * 100000000 + 0.5 ) / 100000000;
}


double round( const double& value,
    const long& factor )
{
    if( factor == 0 )
        return floor( value );
        
    long temp = ( long )( value * factor );
    double result = ( double )( temp / factor );
    
    return result;
}


double mean( const double& val1,
    const double& val2 )
{
    double halfDiff = ( val2 - val1 ) / 2;
    
    
    if( val2 > val1 )
        return ( val2 - halfDiff );
    else
        return ( val1 - halfDiff );
}

//Que fait cette fonction ?
long factor( const double& model )
{
    long result = 0;
    
    if( model == 0 )
        return result;
        
    //1. On prend la partie d�cimal
    double tempVal = model - ( long ) model;
    //2. On calcul la pr�cision
    
    while( tempVal < 0 )
    {
        tempVal * 10;
        result++;
    }
    
    return result;
}