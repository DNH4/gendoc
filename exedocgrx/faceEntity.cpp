#pragma once
#include "faceEntity.h"
#include "print.h"

long getSsFace(
    ads_name&  ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "", "3DFACE", layerName );
}

long getSsAllFace(
    ads_name  ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "X", "3DFACE", layerName ) ;
}

AcDbFace* getFaceFromSs(
    const ads_name& ssName,
    const long& iObject,
    const AcDb::OpenMode& opMode )
{
    //Recuperer l'entite
    AcDbEntity*  pEntity = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbFace::cast( pEntity );
}

AcDbFace* readFaceFromSs(
    const ads_name&    ssName,
    const long&        iObject )
{
    return getFaceFromSs( ssName, iObject, AcDb::kForRead );
}

AcDbFace* writeFaceFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    return getFaceFromSs( ssName, iObject, AcDb::kForWrite );
}

void createCube(
    AcGePoint3d point,
    const double& cote,
    const double& intensity )
{
    //D�claration de 8 points pour contenir les sommets du cube
    AcGePoint3d pt1, pt2, pt3, pt4, pt5, pt6, pt7, pt8;
    
    //D�claration du couleur
    AcCmColor color;
    
    //D�claration de la Face3D
    AcDbFace* face3DAvant = NULL;
    AcDbFace* face3DArr = NULL;
    AcDbFace* face3DHaut = NULL;
    AcDbFace* face3DBas = NULL;
    AcDbFace* face3DGauche = NULL;
    AcDbFace* face3DDroite = NULL;
    
    //Diviser la distance en deux
    double cote2 = cote / 2;
    
    //Vertex du Cube
    pt1.x = point.x - ( cote2 );
    pt1.y = point.y + ( cote2 );
    pt1.z = point.z + ( cote2 );
    pt2.x = point.x - ( cote2 );
    pt2.y = point.y - ( cote2 );
    pt2.z = point.z + ( cote2 );
    pt3.x = point.x + ( cote2 );
    pt3.y = point.y - ( cote2 );
    pt3.z = point.z + ( cote2 );
    pt4.x = point.x + ( cote2 );
    pt4.y = point.y + ( cote2 );
    pt4.z = point.z + ( cote2 );
    pt5.x = point.x - ( cote2 );
    pt5.y = point.y + ( cote2 );
    pt5.z = point.z - ( cote2 );
    pt6.x = point.x - ( cote2 );
    pt6.y = point.y - ( cote2 );
    pt6.z = point.z - ( cote2 );
    pt7.x = point.x + ( cote2 );
    pt7.y = point.y - ( cote2 );
    pt7.z = point.z - ( cote2 );
    pt8.x = point.x + ( cote2 );
    pt8.y = point.y + ( cote2 );
    pt8.z = point.z - ( cote2 );
    
    //Ajout Niveau de gris
    color.setRGB( intensity, intensity, intensity );
    
    //Cr�ation des faces 3D de chaque c�t� du point et ajout de la face dans le modeleSpace
    face3DAvant = new AcDbFace( pt1, pt2, pt3, pt4 );
    face3DAvant->setColor( color );
    addToModelSpace( face3DAvant );
    face3DAvant->close();
    
    face3DArr = new AcDbFace( pt5, pt6, pt7, pt8 );
    face3DArr->setColor( color );
    addToModelSpace( face3DArr );
    face3DArr->close();
    
    face3DHaut = new AcDbFace( pt1, pt4, pt8, pt5 );
    face3DHaut->setColor( color );
    addToModelSpace( face3DHaut );
    face3DHaut->close();
    
    face3DBas = new AcDbFace( pt2, pt3, pt7, pt6 );
    face3DBas->setColor( color );
    addToModelSpace( face3DBas );
    face3DBas->close();
    
    face3DGauche = new AcDbFace( pt4, pt3, pt7, pt8 );
    face3DGauche->setColor( color );
    addToModelSpace( face3DGauche );
    face3DGauche->close();
    
    face3DDroite = new AcDbFace( pt1, pt2, pt6, pt5 );
    face3DDroite->setColor( color );
    addToModelSpace( face3DDroite );
    face3DDroite->close();
}


void createCube(
    AcGePoint3d point,
    const double& coteLarg,
    const double& coteLong,
    const double& hauteur,
    const double& rotation,
    const AcString& layer,
    const AcCmColor& color )
{
    //D�claration de 8 points pour contenir les sommets du cube
    AcGePoint3d pt1, pt2, pt3, pt4, pt5, pt6, pt7, pt8;
    
    //D�claration de la Face3D
    AcDbFace* face3DAvant = NULL;
    AcDbFace* face3DArr = NULL;
    AcDbFace* face3DHaut = NULL;
    AcDbFace* face3DBas = NULL;
    AcDbFace* face3DGauche = NULL;
    AcDbFace* face3DDroite = NULL;
    
    //Diviser la distance en deux
    double cote1 = coteLarg / 2;
    double cote2 = coteLong / 2;
    
    //Vertex du Cube
    pt1.x = point.x - ( cote2 );
    pt1.y = point.y + ( cote1 );
    pt1.z = point.z + hauteur;
    pt2.x = point.x - ( cote2 );
    pt2.y = point.y - ( cote1 );
    pt2.z = point.z + hauteur;
    pt3.x = point.x + ( cote2 );
    pt3.y = point.y - ( cote1 );
    pt3.z = point.z + hauteur;
    pt4.x = point.x + ( cote2 );
    pt4.y = point.y + ( cote1 );
    pt4.z = point.z + hauteur;
    pt5.x = point.x - ( cote2 );
    pt5.y = point.y + ( cote1 );
    pt5.z = point.z;
    pt6.x = point.x - ( cote2 );
    pt6.y = point.y - ( cote1 );
    pt6.z = point.z;
    pt7.x = point.x + ( cote2 );
    pt7.y = point.y - ( cote1 );
    pt7.z = point.z;
    pt8.x = point.x + ( cote2 );
    pt8.y = point.y + ( cote1 );
    pt8.z = point.z;
    
    //Roter les points
    pt1 = pt1.rotateBy( rotation, AcGeVector3d::kZAxis, AcGePoint3d( point.x, point.y, pt1.z ) );
    pt2 = pt2.rotateBy( rotation, AcGeVector3d::kZAxis, AcGePoint3d( point.x, point.y, pt2.z ) );
    pt3 = pt3.rotateBy( rotation, AcGeVector3d::kZAxis, AcGePoint3d( point.x, point.y, pt3.z ) );
    pt4 = pt4.rotateBy( rotation, AcGeVector3d::kZAxis, AcGePoint3d( point.x, point.y, pt4.z ) );
    pt5 = pt5.rotateBy( rotation, AcGeVector3d::kZAxis, AcGePoint3d( point.x, point.y, pt5.z ) );
    pt6 = pt6.rotateBy( rotation, AcGeVector3d::kZAxis, AcGePoint3d( point.x, point.y, pt6.z ) );
    pt7 = pt7.rotateBy( rotation, AcGeVector3d::kZAxis, AcGePoint3d( point.x, point.y, pt7.z ) );
    pt8 = pt8.rotateBy( rotation, AcGeVector3d::kZAxis, AcGePoint3d( point.x, point.y, pt8.z ) );
    
    //Cr�ation des faces 3D de chaque c�t� du point et ajout de la face dans le modelSpace
    face3DAvant = new AcDbFace( pt1, pt2, pt3, pt4 );
    face3DAvant->setColor( color );
    addToModelSpace( face3DAvant );
    face3DAvant->setLayer( layer );
    face3DAvant->close();
    
    face3DArr = new AcDbFace( pt5, pt6, pt7, pt8 );
    face3DArr->setColor( color );
    addToModelSpace( face3DArr );
    face3DArr->setLayer( layer );
    face3DArr->close();
    
    face3DHaut = new AcDbFace( pt1, pt4, pt8, pt5 );
    face3DHaut->setColor( color );
    addToModelSpace( face3DHaut );
    face3DHaut->setLayer( layer );
    face3DHaut->close();
    
    face3DBas = new AcDbFace( pt2, pt3, pt7, pt6 );
    face3DBas->setColor( color );
    addToModelSpace( face3DBas );
    face3DBas->setLayer( layer );
    face3DBas->close();
    
    face3DGauche = new AcDbFace( pt4, pt3, pt7, pt8 );
    face3DGauche->setColor( color );
    addToModelSpace( face3DGauche );
    face3DGauche->setLayer( layer );
    face3DGauche->close();
    
    face3DDroite = new AcDbFace( pt1, pt2, pt6, pt5 );
    face3DDroite->setColor( color );
    addToModelSpace( face3DDroite );
    face3DDroite->setLayer( layer );
    face3DDroite->close();
}

Acad::ErrorStatus getDistinctVertices(
    const AcDbFace* face,
    vector<AcGePoint3d>& vertices )
{
    Acad::ErrorStatus es = Acad::eOk;
    AcGePoint3d pt;
    
    // R�cup�rer les sommets un par un
    for( int i = 0; i < 4; i++ )
    {
        if( es = face->getVertexAt( i, pt ) )
            return es;
            
        bool isDistinct = true;
        
        for( int j = 0; j < vertices.size(); j++ )
        {
            if( vertices[j] == pt )
                isDistinct = false;
        }
        
        if( isDistinct )
            vertices.push_back( pt );
    }
    
    return es;
}

bool isInFace2D(
    const AcDbFace* face,
    const AcGePoint3d& pt )
{
    bool res = false;
    
    // R�cup�rer les sommets de la face
    vector<AcGePoint3d> vertices;
    Acad::ErrorStatus es;
    
    if( es = getDistinctVertices( face, vertices ) )
        print( es );
        
    int len = vertices.size();
    
    // Cas 1 : la face a un seul sommet
    if( len == 1 )
    {
        if( pt == vertices[0] )
            res = true;
    }
    
    // Cas 2 : la face est une droite
    else if( len == 2 )
    {
        AcGeVector3d AB = AcGeVector3d( vertices[1].x - vertices[0].x, vertices[1].y - vertices[0].y, vertices[1].z - vertices[0].z );
        AcGeVector3d AM = AcGeVector3d( pt.x - vertices[0].x, pt.y - vertices[0].y, pt.z - vertices[0].z );
        
        if( AB.crossProduct( AM ).isZeroLength() )
        {
            // A, B et M sont align�s
            double kAB = AB.dotProduct( AB );
            double kAM = AB.dotProduct( AM );
            
            if( kAM > 0 && kAM < kAB )
            {
                // M est situ� sur le segment AB
                res = true;
            }
        }
    }
    
    else if( len >= 3 )
    {
        AcGeVector3d AB = AcGeVector3d( vertices[1].x - vertices[0].x, vertices[1].y - vertices[0].y, vertices[1].z - vertices[0].z );
        AcGeVector3d AC = AcGeVector3d( vertices[2].x - vertices[0].x, vertices[2].y - vertices[0].y, vertices[2].z - vertices[0].z );
        AcGeVector3d AM = AcGeVector3d( pt.x - vertices[0].x, pt.y - vertices[0].y, pt.z - vertices[0].z );
        
        double kMB = AM.dotProduct( AB );
        double kMC = AM.dotProduct( AC );
        
        // Cas 3 : la face est un triangle
        // Dans ce cas le point doit �tre
        if( len == 3 )
        {
            if( ( kMB > 0 ) && ( kMC > 0 ) && ( kMB + kMC <= 1 ) )
                res = true;
        }
        
        // Cas 4 : la face est un quadrilat�re
        else if( len == 4 )
        {
            if( ( kMB >= 0 ) && ( kMB <= 1 ) && ( kMC >= 0 ) && ( kMC <= 1 ) )
                res = true;
        }
    }
    
    return res;
}

Acad::ErrorStatus projectPointOnFace( const AcDbFace* face,
    const AcGePoint3d& ptToProject,
    const AcGeVector3d& projDir,
    bool& projected,
    AcGePoint3d& projectedPt )
{
    Acad::ErrorStatus es = Acad::eOk;
    projectedPt = ptToProject;
    AcGeLine3d projLine = AcGeLine3d( ptToProject, projDir );
    
    
    // V�rifier si le point est � l'int�rieur de la face
    if( isInFace2D( face, ptToProject ) )
    {
        // R�cup�rer le plan de la face
        AcGePlane plane;
        AcDb::Planarity planarity;
        
        if( es = face->getPlane( plane, planarity ) )
        {
            if( !plane.intersectWith( projLine, projectedPt ) )
                //Si le plan et la ligne de projection sont parall�les
                es = Acad::eNotApplicable;
            else
                projected = true;
        }
    }
    
    return es;
}