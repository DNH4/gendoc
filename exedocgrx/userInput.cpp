#pragma once
#include <userInput.h>
#include "print.h"

int askYesOrNo( bool& isYes,
    const AcString& message )
{
    GCHAR* ptr = ( GCHAR* )NULL;
    
    acedInitGet( RSG_OTHER, _T( "O N Y Oui Non Yes No _ O N O O N O N" ) );
    
    AcString defaultAnswer, question = message + _T( "[Oui/Non]" );
    
    if( isYes ) defaultAnswer = _T( "<Oui> " );
    else defaultAnswer = _T( "<Non> " );
    
    int RTTYPE = acedGetFullKword( question.concat( defaultAnswer ), ptr );
    
    if( RTTYPE == RTNORM )
    {
        AcString userInput = AcString( ptr ) ;
        
        // comparaison des inputs
        /// cas d'un oui
        if( ( userInput.compareNoCase( _T( "O" ) ) == 0 ) ||
            ( userInput.compareNoCase( _T( "Oui" ) ) == 0 ) ||
            ( userInput.compareNoCase( _T( "Y" ) ) == 0 ) ||
            ( userInput.compareNoCase( _T( "Yes" ) ) == 0 ) )
        {
            isYes = true;
            return RTNORM;
        }
        
        else if( ( userInput.compareNoCase( _T( "N" ) ) == 0 ) ||
            ( userInput.compareNoCase( _T( "Non" ) ) == 0 ) ||
            ( userInput.compareNoCase( _T( "N" ) ) == 0 ) ||
            ( userInput.compareNoCase( _T( "No" ) ) ) == 0 )
        {
            isYes = false;
            return RTNORM;
        }
        
        
    }
    
    else if( RTTYPE == RTNONE )
    {
        // On prend la valeur par defaut
        return RTNORM;
    }
    
    else
        return RTCAN;
}


int askForDouble( const ACHAR* message,
    ads_real& defaultValue,
    double& value )
{
    ads_real result;
    AcString pr;
    
    pr.format( L"%s <%g>: ", message, defaultValue );
    
    // On demande au dessinateur de rentrer une valeur
    int RTTYPE = acedGetReal( pr, &result );
    
    // Si le dessinateur fait entree, c'est qu'il veut la valeur par defaut
    // et s'il fait echap on doit garder la meme valeur par defaut
    
    if( RTTYPE == RTNONE || RTTYPE == RTCAN ) value = defaultValue;
    
    else value = result;
    
    defaultValue = value;
    
    return RTTYPE;
}


AcString askString( const AcString& textToPrint )
{
    //Demander a l'utilisateur de cliquer un point
    TCHAR l_tcharGet[10000];
    acedGetString( 0, textToPrint, l_tcharGet );
    return l_tcharGet;
}

AcString askString( const AcString& textToPrint, const bool& containSpaces )

{
    //Demander a l'utilisateur de cliquer un point
    GCHAR* l_tcharGet;
    int cronly = 0;
    
    if( containSpaces )
        cronly = 1;
        
    acedGetFullString( cronly, textToPrint, l_tcharGet );
    return l_tcharGet;
}
int askString( AcString& outputTxt,
    const AcString& textToPrint, const bool& containSpaces )
{
    //Demander a l'utilisateur de cliquer un point
    GCHAR* l_tcharGet ;
    int cronly = 0;
    
    if( containSpaces )
        cronly = 1;
        
    int es =  acedGetFullString( cronly, textToPrint, l_tcharGet );
    outputTxt = l_tcharGet;
    return es;
}

AcString askToChoose( const vector <AcString>& choix, const AcString& message )
{
    //Boucler sur le vecteur choix
    int iChoix = choix.size();
    AcString quest = _T( "[" );
    
    for( int i = 0; i < iChoix; i++ )
    {
    
        if( i != iChoix - 1 )
            quest += choix[i] + _T( "/" );
        else
            quest += choix[i] + _T( "]" );
    }
    
    AcString pr = message + quest;
    
    GCHAR* l_tcharGet;
    
    //Afficher le message
    acedInitGet( RSG_OTHER, NULL );
    
    int iError = acedGetFullKword( pr, l_tcharGet );
    
    AcString choice = AcString( l_tcharGet );
    
    return choice;
}

vector< unsigned char > askForColors()
{
    vector< unsigned char > rgb;
    rgb.resize( 3 );
    
    CHOOSECOLOR cc;                 // common dialog box structure
    static COLORREF acrCustClr[ 16 ]; // array of custom colors
    HWND hwnd = { 0 };                      // owner window
    HBRUSH hbrush;                  // brush handle
    static DWORD rgbCurrent;        // initial color selection
    
    // Initialize CHOOSECOLOR
    ZeroMemory( &cc, sizeof( cc ) );
    cc.lStructSize = sizeof( cc );
    cc.hwndOwner = hwnd;
    cc.lpCustColors = ( LPDWORD ) acrCustClr;
    cc.rgbResult = rgbCurrent;
    cc.Flags = CC_FULLOPEN | CC_RGBINIT;
    
    if( ChooseColor( &cc ) == TRUE )
    {
        hbrush = CreateSolidBrush( cc.rgbResult );
        rgbCurrent = cc.rgbResult;
    }
    
    rgb[ 0 ] = unsigned char( GetRValue( cc.rgbResult ) );
    rgb[ 1 ] = unsigned char( GetGValue( cc.rgbResult ) );
    rgb[ 2 ] = unsigned char( GetBValue( cc.rgbResult ) );
    
    return rgb;
}

int askString( const AcString& textToPrint,
    AcString& txt )
{
    //Demander a l'utilisateur de cliquer un point
    TCHAR l_tcharGet[10000];
    int res = acedGetString( 0, textToPrint, l_tcharGet );
    txt = l_tcharGet;
    return res;
}

int askString( const ACHAR* textToPrint,
    AcString& resultString,
    ACHAR* defaultValue )
{
    ACHAR l_tcharGet[255];
    l_tcharGet[ 0 ] = 0;
    
    ACHAR* txt;
    
    AcString pr;
    pr.format( L"%s<%s>:", textToPrint, defaultValue );
    
    int res = acedGetString( 0, pr, l_tcharGet );
    
    if( ( res == RTNORM && l_tcharGet[ 0 ] == 0 ) || res == RTCAN )
        txt = defaultValue;
        
    else
        txt = l_tcharGet;
        
    defaultValue = txt;
    
    resultString = txt;
    
    return res;
}

int askForInt( const ACHAR* message,
    int& defaultValue,
    int& value )
{
    int result;
    AcString pr;
    
    // On demande au dessinateur de rentrer une valeur
    pr.format( L"%s <%d>: ", message, defaultValue );
    int RTTYPE = acedGetInt( pr, &result );
    
    // Si le dessinateur fait entree, c'est qu'il veut la valeur par defaut
    // et s'il fait echap on doit garder la meme valeur par defaut
    
    if( RTTYPE == RTNONE || RTTYPE == RTCAN ) value = defaultValue;
    
    else if( RTTYPE != RTNORM )
        return RTTYPE;
        
    value = result;
    
    defaultValue = value;
    
    return RTTYPE;
}

int askToChoose( const AcString& message,
    const AcStringArray& choices,
    AcString& choice )
{
    // On v�rifie que les param�tres sont pass�s
    if( choices.isEmpty() )
        return RTCAN;
        
    // On r�cup�re les majuscules de tous les param�tres
    AcStringArray upperCases;
    int choicesLength = choices.length();
    
    for( int i = 0; i < choicesLength; i++ )
    {
    
        //string choiceI = acStrToStr( choices[ i ] );
        AcString choiceI = choices[ i ];
        int choiceILength = choiceI.length();
        
        for( int j = 0; j < choiceILength; j++ )
        {
            if( isupper( choiceI[ j ] ) )
            {
                upperCases.append( AcString( choiceI[ j ] ) );
                break;
            }
        }
    }
    
    // On v�rifie que les param�tres aient des majuscules
    if( upperCases.isEmpty() )
        return RTCAN;
        
    //acedInitGet( RSG_NONULL, _T( "A I C R Z AcadCouleurs Intensit� Classification RGB El�vation _ A I C R Z A I C R Z" ) );
    
    // On cr�e une string avec la liste des param�tres avec les standards d'AutoCAD
    AcString listOfParam = _T( "[" );
    
    for( int i = 0; i < choicesLength; i++ )
    {
        listOfParam.append( choices[ i ] );
        
        if( i != ( choicesLength - 1 ) )
            listOfParam.append( _T( "/" ) );
    }
    
    listOfParam += _T( "]" );
    
    AcString initGet = _T( "" );
    
    for( int i = 0; i < upperCases.length(); i++ )
        initGet += upperCases[ i ] + _T( " " );
        
    for( int i = 0; i < choices.length(); i++ )
        initGet += choices[ i ] + _T( " " );
        
    initGet += _T( " _ " );
    
    for( int i = 0; i < choices.length(); i++ )
        initGet += choices[ i ] + _T( " " );
        
    for( int i = 0; i < upperCases.length(); i++ )
    {
        initGet += upperCases[ i ];
        
        if( i != ( upperCases.length() - 1 ) )
            initGet += _T( " " );
    }
    
    
    acedInitGet( RSG_NONULL, initGet );
    
    GCHAR* gchar = new GCHAR();
    
    int res = acedGetFullKword( message + listOfParam + _T( " : " ), gchar );
    
    choice = AcString( gchar );
    
    return res;
}


Acad::ErrorStatus askToClickInVp( const ACHAR* message,
    AcDbObjectId& vpId )
{
    // Recuperer toutes les vues
    acedVports2VportTableRecords();
    
    // On ouvre la base de donn�es
    AcDbDatabase* db =  acdbHostApplicationServices()->workingDatabase();
    
    if( !db )
    {
        acutPrintf( _T( "\nImpossible de r�cup�rer la vue courante." ) );
        return eWrongDatabase;
    }
    
    // Demander � l'utilisateur de cliquer dans la fenetre
    ads_point point;
    int l_iError = acedGetPoint( NULL, message, point );
    
    // Cas ou l'utilisateur n'a pas click� dans la fen�tre
    if( l_iError == RTCAN || l_iError == RTERROR )
        return Acad::eNotApplicable;
        
    // On r�cup�re l'id du viewport actif
    vpId = acdbGetCurVportTableRecordId( db );
    
    // On sort de la fonction
    return Acad::eOk;
}


Acad::ErrorStatus askZoneDelim( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    AcGePoint3d& pt3,
    const AcString& messageP1,
    const AcString& messageP2,
    const AcString& messageP3,
    const AcGeVector3d& norm )
{
    ads_point ads_p1, ads_p2;
    
    // Empecher une entree nulle
    acedInitGet( RSG_NONULL, NULL );
    
    // Demander de saisir le premier point
    if( acedGetPoint( NULL, messageP1, ads_p1 ) == RTCAN )
    {
        acutPrintf( _T( "\nCommande annul�e" ) );
        return Acad::eInvalidInput;
    }
    
    acedInitGet( RSG_NONULL, NULL );
    
    // Demander de saisir le second point
    if( acedGetPoint( ads_p1, messageP2, ads_p2 ) == RTCAN )
    {
        acutPrintf( _T( "\nCommande annul�e" ) );
        return Acad::eInvalidInput;
    }
    
    // Conversion des deux premiers points
    pt1 = adsToAcGe( ads_p1 );
    pt2 = adsToAcGe( ads_p2 );
    
    getWcsPoint( &pt1 );
    getWcsPoint( &pt2 );
    
    // Demander le dernier point du rectangle
    pt3 = getRect( messageP3, pt1, pt2, norm );
    
    //Verifer si on annul�
    if( pt3 == AcGePoint3d( -0.123456, -0.123456, -0.123456 ) )
        return Acad::eNotApplicable;
        
    return Acad::eOk;
}



Acad::ErrorStatus askZoneDelim( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    AcGePoint3d& pt3,
    const AcDbObjectId& idVp,
    const bool& useVp,
    const AcString& messageP1,
    const AcString& messageP2,
    const AcString& messageP3,
    const AcGeVector3d& norm )
{
    ads_point ads_p1, ads_p2;
    
    //Recuperer la vue active
    AcDbObjectId activeVp = acedActiveViewportId();
    
    // Empecher une entree nulle
    acedInitGet( RSG_NONULL, NULL );
    
    // Demander de saisir le premier point
    if( acedGetPoint( NULL, messageP1, ads_p1 ) == RTCAN )
    {
        acutPrintf( _T( "\nCommande annul�e" ) );
        return Acad::eInvalidInput;
    }
    
    //Si l'id est nulle on ne fait rien
    if( idVp != AcDbObjectId::kNull )
    {
        //Checker si c'est dans le bon viewport
        if( useVp )
        {
            if( activeVp != idVp )
                return Acad::eNotApplicable;
        }
        
        else
        {
            if( activeVp == idVp )
                return Acad::eNotApplicable;
        }
    }
    
    acedInitGet( RSG_NONULL, NULL );
    
    // Demander de saisir le second point
    if( acedGetPoint( ads_p1, messageP2, ads_p2 ) == RTCAN )
    {
        acutPrintf( _T( "\nCommande annul�e" ) );
        return Acad::eInvalidInput;
    }
    
    //Verifier que l'utilisateur ne change pas de viewport
    if( activeVp != acedActiveViewportId() )
    {
        //Afficher message
        print( "Vous avez changer de vue. L'op�ration est annul�e." );
        return Acad::eNotApplicable;
    }
    
    // Conversion des deux premiers points
    pt1 = adsToAcGe( ads_p1 );
    pt2 = adsToAcGe( ads_p2 );
    
    getWcsPoint( &pt1 );
    getWcsPoint( &pt2 );
    
    // Demander le dernier point du rectangle
    pt3 = getRect( messageP3, pt1, pt2, norm );
    
    //Verifier que l'utilisateur ne change pas de viewport
    if( activeVp != acedActiveViewportId() )
    {
        //Afficher message
        print( "Vous avez changer de vue. L'op�ration est annul�e." );
        return Acad::eNotApplicable;
    }
    
    //Verifer si on annul�
    if( pt3 == AcGePoint3d( -0.123456, -0.123456, -0.123456 ) )
        return Acad::eNotApplicable;
        
    return Acad::eOk;
}


Acad::ErrorStatus askSlice( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    double& ep,
    const AcString& messageP1,
    const AcString& messageP2,
    const AcString& messageEp )
{
    //Premier point de la coupe
    ads_point ads_p1;
    
    //Second point de la coupe
    AcGePoint3d* temp = new AcGePoint3d();
    
    // Empecher une entree nulle
    acedInitGet( RSG_NONULL, NULL );
    
    // Demander de saisir le premier point
    if( acedGetPoint( NULL, messageP1, ads_p1 ) != RTNORM )
        return Acad::eInvalidInput;
        
    //Recuperer le premier point dans la bonne coordonn�es
    pt1 = adsToAcGe( ads_p1 );
    getWcsPoint( &pt1 );
    
    //Recuperer toutes les vues
    acedVports2VportTableRecords();
    
    //Recuperer le viewport de coupe
    AcDbViewportTableRecordPointer vp;
    
    //Variable de l'UCS de coupe
    AcGeVector3d vpX, vpY;
    AcGePoint3d center;
    
    //Ouvrir la vue
    if( Acad::eOk == vp.open( acedActiveViewportId(), AcDb::kForWrite ) )
    {
        //Recuperer l'ucs
        vp->getUcs( center, vpX, vpY );
        
        //Fermer le viewport
        vp->close();
    }
    
    //Recuperer le plan du scu
    AcGePlane plan = AcGePlane( center, vpX, vpY );
    
    //Recuperer le normal du plan
    AcGeVector3d vecNormal = plan.normal();
    
    //Empecher un entr�e null
    acedInitGet( RSG_NONULL, NULL );
    
    //Demander de saisir le second point
    if( getWcsPoint( messageP2, temp, pt1 ) == RTCAN )
        return Acad::eInvalidInput;
        
    //Recuperer le second point
    pt2 = *temp;
    
    //Liberer la m�moire sur le point
    delete temp;
    
    //Choix de la s�lection par l'utilisateur
    AcString prompt = _T( "\nChoisissez une �paisseur <" ) + numberToAcString( ep, 2 ) + _T( "> : " );
    
    // Demander l'epaisseur et dessiner un rectangle de la coupe
    double epTemp = getWidth( prompt, pt1, pt2, vecNormal );
    
    //Verifier
    if( epTemp == -1 )
    {
        //On v�rifie que l'�paisseur en m�moire est bien sup�rieure � 0
        acutPrintf( _T( "\nCommande annul�e" ) );
        return Acad::eInvalidInput;
    }
    
    //Sinon on affecte le nouveau choix de l'utilisateur
    else
        ep = epTemp;
        
    print( numberToAcString( ep, 2 ) + "\n" );
    
    return Acad::eOk;
}



Acad::ErrorStatus askSlice( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    double& ep,
    const AcDbObjectId& idVp,
    const bool& useIdVp,
    const AcString& messageP1,
    const AcString& messageP2,
    const AcString& messageEp )
{
    //Premier point de la coupe
    ads_point ads_p1;
    
    //Second point de la coupe
    AcGePoint3d* temp = new AcGePoint3d();
    
    //Viewport active
    AcDbObjectId activeVp = acedActiveViewportId();
    
    // Empecher une entree nulle
    acedInitGet( RSG_NONULL, NULL );
    
    // Demander de saisir le premier point
    if( acedGetPoint( NULL, messageP1, ads_p1 ) != RTNORM )
        return Acad::eInvalidInput;
        
    //Si l'id n'est pas nulle
    if( idVp != AcDbObjectId::kNull )
    {
        //Verifier le viewport cliquer
        if( useIdVp )
        {
            if( activeVp != idVp )
                return Acad::eNotApplicable;
        }
        
        //
        //else
        //{
        //    if( activeVp == idVp )
        //        return Acad::eNotApplicable;
        //}
    }
    
    //Recuperer le premier point dans la bonne coordonn�es
    pt1 = adsToAcGe( ads_p1 );
    getWcsPoint( &pt1 );
    
    //Recuperer toutes les vues
    acedVports2VportTableRecords();
    
    //Recuperer le viewport de coupe
    AcDbViewportTableRecordPointer vp;
    
    //Variable de l'UCS de coupe
    AcGeVector3d vpX, vpY;
    AcGePoint3d center;
    
    //Ouvrir la vue
    if( Acad::eOk == vp.open( activeVp, AcDb::kForWrite ) )
    {
        //Recuperer l'ucs
        vp->getUcs( center, vpX, vpY );
        
        //Fermer le viewport
        vp->close();
    }
    
    //Recuperer le plan du scu
    AcGePlane plan = AcGePlane( center, vpX, vpY );
    
    //Recuperer le normal du plan
    AcGeVector3d vecNormal = plan.normal();
    
    //Empecher un entr�e null
    acedInitGet( RSG_NONULL, NULL );
    
    //Demander de saisir le second point
    if( getWcsPoint( messageP2, temp, pt1 ) == RTCAN )
        return Acad::eInvalidInput;
        
    //Verifier que le dessinateur n'a pas changer de vue
    if( activeVp != acedActiveViewportId() )
    {
        print( "Vous avez changer de fenetre. L'op�ration est annul�e" );
        return Acad::eNotApplicable;
    }
    
    //Recuperer le second point
    pt2 = *temp;
    
    //Liberer la m�moire sur le point
    delete temp;
    
    //Choix de la s�lection par l'utilisateur
    AcString prompt = _T( "\nChoisissez une �paisseur <" ) + numberToAcString( ep, 2 ) + _T( "> : " );
    
    // Demander l'epaisseur et dessiner un rectangle de la coupe
    double epTemp = getWidth( prompt, pt1, pt2, vecNormal );
    
    //Verifier
    if( epTemp == -1 )
    {
        //On v�rifie que l'�paisseur en m�moire est bien sup�rieure � 0
        acutPrintf( _T( "\nCommande annul�e" ) );
        return Acad::eInvalidInput;
    }
    
    //Sinon on affecte le nouveau choix de l'utilisateur
    else
        ep = epTemp;
        
    print( numberToAcString( ep, 2 ) + "\n" );
    
    return Acad::eOk;
}


Acad::ErrorStatus askDelim( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    const AcString& messageP1,
    const AcString& messageP2,
    const AcDbObjectId& vpId )
{
    ads_point ads_p1, ads_p2;
    
    // Empecher une entree nulle
    acedInitGet( RSG_NONULL, NULL );
    
    AcDbDatabase* db = acdbHostApplicationServices()->workingDatabase();
    
    // Demander de saisir le premier
    int res = acedGetPoint( NULL, messageP1, ads_p1 );
    
    // Id du viewport actif
    AcDbObjectId currentVp = acdbGetCurVportTableRecordId( db );
    
    // Tant qu'on n'est pas dans le viewport de coupe il faut reclicker
    while( ( vpId != AcDbObjectId::kNull && vpId != currentVp )
        || res != RTNORM )
    {
        if( res == RTNORM )
        {
            res = acedGetPoint( NULL, messageP1, ads_p1 );
            currentVp = acdbGetCurVportTableRecordId( db );
        }
        
        else
            return Acad::eNotApplicable;
    }
    
    acedInitGet( RSG_NONULL, NULL );
    
    // Tant qu'on n'est pas dans le viewport de coupe il faut reclicker le second point
    res = acedGetCorner( ads_p1, messageP2, ads_p2 );
    
    currentVp = acdbGetCurVportTableRecordId( db );
    
    // v�rifier que l'on soit bien dans le viewport de coupe
    while( ( vpId != AcDbObjectId::kNull && vpId != currentVp )
        || res != RTNORM )
    {
        if( res == RTNORM )
        {
            res = acedGetCorner( ads_p1, messageP2, ads_p2 );
            currentVp = acdbGetCurVportTableRecordId( db );
        }
        
        else
            return Acad::eNotApplicable;
    }
    
    // On convertit les points dans le bon SCU
    
    AcGePoint3d* pt1Temp = &adsToAcGe( ads_p1 );
    getWcsPoint( pt1Temp );
    pt1 = *pt1Temp;
    
    AcGePoint3d* pt2Temp = &adsToAcGe( ads_p2 );
    getWcsPoint( pt2Temp );
    pt2 = *pt2Temp;
    
    return Acad::eOk;
}

int askUserOpt( const AcString& option,
    const AcString& msg,
    const AcGePoint3d& prevPt,
    AcGePoint3d& newPt,
    AcString& kwChoice )
{
    int usrInputType = -1;
    
    if( option.length() > 0 )
        acedInitGet( RSG_OTHER, option );
        
    //On v�rifie si on a un point pr�c�dent
    if( prevPt.isEqualTo( AcGePoint3d::kOrigin ) )
        usrInputType = getWcsPoint( msg, &newPt );
    else
        usrInputType = getWcsPoint( msg, &newPt, prevPt );
        
    //On v�rifie si l'utilisateur � entrer un text
    if( usrInputType == RTKWORD )
    {
        GCHAR keyWord[30];
        
        if( acedGetInput( keyWord ) == RTNORM )
            kwChoice = AcString( keyWord );
    }
    
    return usrInputType;
}


Acad::ErrorStatus askOrEnterDist( double& res,
    const AcString& message,
    const double& defValue )
{
    //Resultat par d�faut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Point
    AcGePoint3d point;
    
    //Demander � l'utilisateur
    AcString choice;
    AcGePoint3d prevPoint;
    int a = askUserOpt( _T( "[]" ), message, prevPoint, point, choice );
    
    //Verifier le resultat
    if( a == RTNORM )
    {
        //Le second point
        AcGePoint3d newPt;
        
        //On demande le point � ins�rer
        int es = getWcsPoint( _T( "\nSp�cifiez la distance :" ), &newPt, point );
        
        //Verifier le point
        if( es != RTNORM )
        {
            acutPrintf( _T( "\nOp�ration annul�e" ) );
            return Acad::eNotApplicable;
        }
        
        //Calculer la distance entre les deux points
        res = getDistance2d( point, newPt );
        
        //Retourner ok
        return Acad::eOk;
    }
    
    //Si echap
    else if( a == RTCAN )
        return er;
        
    //Si espace
    else if( a == 5000 )
    {
        res = defValue;
        return Acad::eOk;
    }
    
    //Sinon
    else
    {
        //Changer l'entr�e utilisateur en nombre
        string a = acStrToStr( choice );
        
        //Remplacer en double
        try
        {
            res = stod( a );
        }
        
        catch( const std::invalid_argument& )
        {
            acutPrintf( _T( "Valeur invalide" ) );
            throw;
            return er;
        }
        
        catch( const std::out_of_range& )
        {
            acutPrintf( _T( "Nombre en entr�e trop grand" ) );
            throw;
            return er;
        }
        
        //Retourner eOk
        return Acad::eOk;
    }
    
    return er;
}



Acad::ErrorStatus askPointOrEnterValue( double& res,
    AcGePoint3d& ptRes,
    bool& isDefault,
    const AcString& message,
    const double& defValue,
    const AcGePoint3d& prevPoint )
{
    //Resultat par d�faut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Demander � l'utilisateur
    AcString choice;
    int a = askUserOpt( _T( "[]" ), message, prevPoint, ptRes, choice );
    
    //Verifier le resultat
    if( a == RTNORM )
    {
        //Setter le booleen
        isDefault = true;
        
        //Retourner ok
        return Acad::eOk;
    }
    
    //Si echap
    else if( a == RTCAN )
        return er;
        
    //Si espace
    else if( a == 5000 )
    {
        res = defValue;
        isDefault = false;
        return Acad::eOk;
    }
    
    //Si l'utilisateur entre des valeurs
    else
    {
        //Setter le booeleen
        isDefault = false;
        
        //Changer l'entr�e utilisateur en nombre
        string a = acStrToStr( choice );
        
        //Remplacer en double
        try
        {
            res = stod( a );
        }
        
        catch( const std::invalid_argument& )
        {
            acutPrintf( _T( "Valeur invalide" ) );
            throw;
            return er;
        }
        
        catch( const std::out_of_range& )
        {
            acutPrintf( _T( "Nombre en entr�e trop grand" ) );
            throw;
            return er;
        }
        
        //Changer le booleen
        isDefault = false;
        
        //Retourner eOk
        return Acad::eOk;
    }
    
    return er;
}