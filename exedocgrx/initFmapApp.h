//-----------------------------------------------------------------------------------<initFmapApp.h>
// Society: Futurmap
// Author : Romain LEMETTAIS
// Application: AutoCAD 2015
// Decription: ObjectARX ENTRYPOINT for any Futurmap commands
// Decription (fr): ObjectARX ENTRYPOINT pour toutes les commandes Futurmap


//-----------------------------------------------------------------------------------<Include>
#pragma once
#include <aced.h>
#include <rxregsvc.h>
#include <tchar.h>


//-----------------------------------------------------------------------------------<Define>
#define TXT_LOAD            _T("\nChargement de %s")
#define TXT_UNLOAD          _T("\nDechargement de %s")
#define TXT_GLOBALNAME      _T("FMAP_")


//-----------------------------------------------------------------------------------<Initialisation>
void secuFmapApp();
void addFmapCmd( AcRxFunctionPtr,
    const AcString,
    const AcString );
void delFmapCmd( const AcString );