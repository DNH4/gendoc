﻿#include "poly3DEntity.h"
#include "pointEntity.h"
#include "geometry.h"
#include <vector>
#include <exception>
#include "layer.h"
#include "print.h"
#include "circleEntity.h"
#include "blockEntity.h"
#include "progressBar.h"


long getSsPoly3D(
    ads_name&        ssName,
    const AcString&  layerName,
    const bool&      isClosed )
{
    return getSelectionSet( ssName, "", "POLYLINE", layerName, "", "", isClosed, "8" );
}



long getSsOnePoly3D(
    ads_name&        ssName,
    const AcString&  layerName,
    const bool&      isClosed )
{
    return getSingleSelection( ssName, "POLYLINE", layerName, "", "", isClosed, "8" );
}



long getSsAllPoly3D(
    ads_name&        ssName,
    const AcString&  layerName,
    const bool&      isClosed )
{
    return getSelectionSet( ssName, "X", "POLYLINE", layerName, "", "", isClosed, "8" );
}


long getSsAllFmPoly3D(
    ads_name& ssName,
    const AcString layerName,
    const bool& isClosed )
{
    return getSelectionSet( ssName, "X", "FmapPolyline3d", layerName, "", "", isClosed );
}


AcDb3dPolyline* getPoly3DFromSs(
    const ads_name&               ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Récuperer l'entite
    AcDbEntity*  l_pEntityLine = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDb3dPolyline::cast( l_pEntityLine );
}



AcDb3dPolyline* readPoly3DFromSs(
    const ads_name&               ssName,
    const long&            iObject )
{
    //Récuperer l'entite
    return getPoly3DFromSs( ssName, iObject, AcDb::kForRead );
}



AcDb3dPolyline* writePoly3DFromSs(
    ads_name&               ssName,
    const long&            iObject )
{
    //Récuperer l'entite
    return getPoly3DFromSs( ssName, iObject, AcDb::kForWrite );
}



bool isClosed( const AcDb3dPolyline* const poly )
{
    return poly->isClosed() == Adesk::kTrue;
}



double getLengthPoly( const AcDb3dPolyline*  poly )
{
    //Calculer le parametre de fin
    double l_dParam;
    
    if( Acad::eOk != poly->getEndParam( l_dParam ) )
        return -1.0;
        
    //Calculer la longueur
    double l_dLength;
    
    if( Acad::eOk != poly->getDistAtParam( l_dParam, l_dLength ) )
        return -1.0;
        
    //Sortir
    return l_dLength;
}



double get2DLengthPoly(
    AcDb3dPolyline*& poly3D )
{
    double lenght = 0;
    
    if( !poly3D )
        return lenght;
        
    Acad::ErrorStatus es = Acad::eInvalidIndex  ;
    
    if( poly3D->objectId() == AcDbObjectId::kNull ) return lenght;
    
    AcGePoint3dArray arPt;
    AcGePoint3d pt ;
    
    //Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Recuperer le vertex puis le sommet
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
        {
            //Recupérer le sommet
            pt = vertex->position() ;
            // On stock le point en X,y 0 seulement
            arPt.append( AcGePoint3d( pt.x, pt.y, 0 ) );
            vertex->close();
        }
    }
    
    delete iterPoly3D;
    
    if( arPt.size() < 2 )
        return 0;
        
    // Construction d'une polyligne
    AcGePolyline3d poly3dGe = AcGePolyline3d( arPt );
    
    // calcul de la longueur
    return poly3dGe.length();
    
}

int getNumberOfVertex( AcDb3dPolyline* poly )
{
    int numVerts = 0;
    
    AcDbObjectIterator* iterPoly = poly->vertexIterator();
    
    for( iterPoly->start(); !iterPoly->done(); iterPoly->step() )
        numVerts++;
        
    delete iterPoly;
    
    return numVerts;
}



bool isBeginEqualToEnd( AcDb3dPolyline* poly3D, const double& tol, bool hasToErase )
{
    //Déclaration du variable qui va contenir le nombre de sommet dans la polyligne 3D
    int nbrVertex;
    nbrVertex = getNumberOfVertex( poly3D );
    
    //Déclaration du premier et du dernier sommet
    AcGePoint3d pt0, pt1;
    AcDbObjectId id0, id1;
    
    //Déclarer les objets et creer un iterateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Initialiser l'itérateur
    iterPoly3D->start();
    
    //Ouvrir le vertex et Récuperer le premier sommet
    if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) ) return -1;
    
    pt0 = vertex->position();
    id0 = iterPoly3D->objectId();
    
    vertex->close();
    iterPoly3D->step();
    
    //Récuperer le dernier sommet de la polyligne
    for( ; !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Ouvrir le vertex et Récuperer le point N°3 ainsi que son Id
        if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) ) return -1;
        
        pt1 = vertex->position();
        id1 = iterPoly3D->objectId();
        vertex->close();
    }
    
    //Si on veut supprimer le dernier sommet
    if( hasToErase )
    {
        if( isEqual3d( pt0, pt1, tol ) && ( Acad::eOk == poly3D->openVertex( vertex, id1, AcDb::kForWrite ) ) )
        {
            vertex->erase();
            vertex->close();
        }
        
        return true;
    }
    
    //Si on ne veut pas supprimer le dernier sommet, mais seulement connaitre si pt0 et pt1 sont superposés
    else if( ( hasToErase == false ) && ( isEqual3d( pt0, pt1, tol ) ) )
        return true;
        
    //Retourne false si pt1 et pt2 ne sont pas superposés
    else
        return false;
}



bool isBeginEqualToEnd( AcDb3dPolyline* poly3D, AcDb3dPolylineVertex* vertex, AcGePoint3d& pt0, AcGePoint3d& pt1, AcDbObjectId& id1, const double& tol, bool hasToErase )
{
    //Si le dernier et le premier sommet de la polyligne sont superposés et qu'on veut supprimer le dernier sommet
    if( isEqual3d( pt0, pt1, tol ) && hasToErase == true )
    {
        if( Acad::eOk == poly3D->openVertex( vertex, id1, AcDb::kForWrite ) )
        {
            vertex->erase();
            vertex->close();
        }
        
        poly3D->makeClosed();
        
        return true;
    }
    
    //Si on veut seulement savoir si le dernier et le premier sommet de la polyligne sont superposés
    else if( isEqual3d( pt0, pt1, tol ) && ( hasToErase == false ) )
        return true;
        
    //Sinon false
    else
        return false;
}



bool isOneAndTwoColiToEnd( AcDb3dPolyline* poly3D, AcDb3dPolylineVertex* vertex, AcGePoint3d& pt0, AcDbObjectId& id0, AcGePoint3d& pt1, AcGePoint3d& pt2, const double& tol, bool hasToErase )
{
    //Si le dernier, le premier, et le deuxieme sommet de la polyligne sont colinéaires et qu'on veut supprimer le premier sommet
    if( isPointOnLine( pt2, pt1, pt0, tol ) && hasToErase == true )
    {
        if( Acad::eOk == poly3D->openVertex( vertex, id0, AcDb::kForWrite ) )
        {
            vertex->erase();
            vertex->close();
        }
        
        poly3D->makeClosed();
        
        return true;
    }
    
    //Si on veut seulement savoir si le dernier, le premier, et le deuxieme sommet de la polyligne sont superposés
    else if( isPointOnLine( pt2, pt1, pt0, tol ) && hasToErase == false )
        return true;
        
    //Sinon false
    else
        return false;
}

void mergePoly3dFromVector( AcDb3dPolyline* poly3D, AcDb3dPolylineVertex* vertex, vector< AcDbObjectId >& coliVec )
{
    int size = coliVec.size();
    
    for( int i = 0; i < size; i++ )
    {
        //Ouverture du vertex ou l'Id est stocker dans le vecteur coliVec, fusion des vertex colineaires, et fermeture du vertex
        if( Acad::eOk == poly3D->openVertex( vertex, coliVec[ i ], AcDb::kForWrite ) )
        {
            vertex->erase();
            vertex->close();
        }
    }
}


int netPoly( AcDb3dPolyline* poly3D, const double& tol )
{
    //Retourne le nombre de vertex nettoye dans la polyligne
    int ver = 0;
    
    //Creer un iterateur sur les sommets de la polyligne 3D
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Declarer les objets
    AcDb3dPolylineVertex* vertex = NULL;
    
    //Initialiser l'itérateur, Déclarer le second point et récupérer le premier sommet
    iterPoly3D->start();
    AcGePoint3d pt2;
    AcDbObjectId id1, id2;
    
    if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
    
    if( !vertex )
        return -1;
        
    AcGePoint3d pt1 = vertex->position();
    id1 = iterPoly3D->objectId();
    vertex->close();
    iterPoly3D->step();
    
    //Copier le premier sommet dans pt0
    AcGePoint3d pt0 = pt1;
    
    //Boucle de lecture du vertex
    for( ; !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Récupérer le point N°2 et son id
        if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
        
        pt2 = vertex->position();
        id2 = iterPoly3D->objectId();
        vertex->close();
        
        //Tester si les deux points se superposés et supprimer le second point si oui
        if( isEqual3d( pt1, pt2, tol ) )
        {
            if( Acad::eOk == poly3D->openVertex( vertex, id1, AcDb::kForWrite ) )
            {
                vertex->erase();
                vertex->close();
                ver++;
            }
        }
        
        //Remplacer la valeur des points N°1 et N°2
        pt1 = pt2;
        id1 = id2;
    }
    
    //Test de superpostition entre le premier et le dernier sommet de la polyligne 3D, s'il sont superposés on supprime le dernier sommet et on ferme la polyligne 3D
    if( isBeginEqualToEnd( poly3D, vertex, pt0, pt1, id1, tol, true ) )
        ver++;
        
    //Retourne le nombre de vertex superposé enlevé dans la selection
    return ver;
}


int coliPoly( AcDb3dPolyline* poly3D, const double& tol )
{
    //Déclaration d'un vecteur pour stocker les Id des sommets colinéaires à fusionner
    vector< AcDbObjectId > coliVec;
    coliVec.resize( 0 );
    
    //Déclarer les objets et créer un itérateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Déclarer trois points pour calculer la colinearite et leur Id
    AcGePoint3d pt1, pt2, pt3, pt, pt0, pt_new_last;
    AcDbObjectId id0, id1, id2, id3;
    
    //Initialiser l'itérateur
    iterPoly3D->start();
    
    //Ouvrir le vertex et Récupérer le point et l'Id du point N°1 et du point N°2
    if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
    
    pt0 = vertex->position();
    pt1 = pt0;
    id0 = iterPoly3D->objectId();
    id1 = id0;
    vertex->close();
    iterPoly3D->step();
    
    if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
    
    pt2 = vertex->position();
    pt = pt2;
    id2 = iterPoly3D->objectId();
    vertex->close();
    iterPoly3D->step();
    
    //Boucle de lecture du vertex de la polyligne 3D
    for( ; !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Ouvrir le vertex et Récupérer le point N°3 ainsi que son Id
        if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
        
        pt3 = vertex->position();
        id3 = iterPoly3D->objectId();
        vertex->close();
        
        //Test si les 3 points sont colineaires, Si colineaire stocker l'Id du point N°2, si non stocker le dernier point non supprimé
        if( isPointOnLine( pt1, pt3, pt2, tol ) )
            coliVec.push_back( id2 );
        else
            pt_new_last = pt2;
            
        //Remplacer les valeurs des points 3D et les Ids
        pt1 = pt2;
        pt2 = pt3;
        id1 = id2;
        id2 = id3;
    }
    
    delete iterPoly3D;
    
    //Fusionner les sommets colinéaires de la polyligne 3D
    mergePoly3dFromVector( poly3D, vertex, coliVec );
    
    // Tester si le premier et le dernier sommet sont superposés, supprimer le dernier sommet si oui
    if( isBeginEqualToEnd( poly3D, vertex, pt0, pt3, id3, tol, true ) )
        coliVec.push_back( id3 );
        
    //Tester si le dernier, le premier et le second sommet de la polyligne sont superposés
    if( isOneAndTwoColiToEnd( poly3D, vertex, pt0, id0, pt, pt_new_last, tol, true ) )
        coliVec.push_back( id0 );
        
    //Retourner le nombre de vertex fusionnes
    return coliVec.size();
}


int netColiPoly( AcDb3dPolyline* poly3D, const double& tol )
{
    //Variable qui contient le nombre de vertex superposé supprime et/ou le nombre de vertex colineaire fusionne dans une polyligne 3D
    int ver = 0;
    
    //Declaration d'un booleen qui va contenir l'etat de fermeture de la polyligne, par defaut la polyligne est ouvert
    bool closeOrOpen = false;
    
    //Declaration d'un vecteur pour stocker les Id des sommets colineaires et superposés fusionner et supprimés
    vector< AcDbObjectId > coliVec;
    coliVec.resize( 0 );
    
    //Declarer les objets et creer un iterateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Declarer trois points pour calculer la colinearite et declaration de leurs Id
    AcGePoint3d pt1, pt2, pt3, pt, pt0, pt_new_last;
    AcDbObjectId id0, id1, id2, id3;
    
    //Initialiser l'itérateur, et récupérer le sommet N°1 et N°2
    iterPoly3D->start();
    
    if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
    
    pt1 = vertex->position();
    id1 = vertex->objectId();
    vertex->close();
    pt0 = pt1;
    id0 = id1;
    
    iterPoly3D->step();
    
    if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
    
    pt2 = vertex->position();
    id2 = vertex->objectId();
    vertex->close();
    pt = pt2;
    
    iterPoly3D->step();
    
    //Boucle pour supprimer les sommets superposés et fusionner les sommets colineaires
    for( ; !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Recuperer le point N°3
        if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
        
        pt3 = vertex->position();
        id3 = vertex->objectId();
        vertex->close();
        
        //Tester l'égalité entre le premier et le second point, si superposé on supprime le point N°1
        if( isEqual3d( pt1, pt2, tol ) )
        {
            if( Acad::eOk == poly3D->openVertex( vertex, id1, AcDb::kForWrite ) )
            {
                vertex->erase();
                vertex->close();
            }
            
            //Remplacer les points et leurs id
            pt1 = pt2;
            id1 = id2;
            
            iterPoly3D->step();
            
            //Recuperer le nouveau point N°2
            if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return -1;
            
            pt2 = vertex->position();
            id2 = vertex->objectId();
            vertex->close();
        }
        
        //Sinon on test si le point N°2 et N°3 sont superposés, si oui on supprime le point N°2
        else
        {
            if( isEqual3d( pt2, pt3, tol ) )
            {
                if( Acad::eOk == poly3D->openVertex( vertex, id2, AcDb::kForWrite ) )
                {
                    vertex->erase();
                    vertex->close();
                }
                
                //Remplacer les points et leurs id
                pt2 = pt3;
                id2 = id3;
            }
            
            //Sinon on test la colinearite entre les trois sommets, si colinéaire on stock l'id du point N°2 dans coliVec, sinon on le stock
            else if( isPointOnLine( pt1, pt3, pt2, tol ) )
            {
                coliVec.push_back( id2 );
                
                //Remplacer les points et leurs id
                pt1 = pt2;
                id1 = id2;
                pt2 = pt3;
                id2 = id3;
            }
            
            else
            {
                pt_new_last = pt2;
                
                //Remplacer les points et leurs id
                pt1 = pt2;
                id1 = id2;
                pt2 = pt3;
                id2 = id3;
            }
        }
    }
    
    delete iterPoly3D;
    
    //Boucle pour fusionner les sommets colineaires de la polyligne 3D
    mergePoly3dFromVector( poly3D, vertex, coliVec );
    
    // Tester si le premier et le dernier sommet sont superposes, supprimer le dernier sommet si oui
    if( isBeginEqualToEnd( poly3D, vertex, pt0, pt3, id3, tol, true ) )
        coliVec.push_back( id3 );
        
    //Tester si le dernier, le premier et le second sommet de la polyligne sont superposes, supprimer le premier sommet si oui
    if( isOneAndTwoColiToEnd( poly3D, vertex, pt0, id0, pt, pt_new_last, tol, true ) )
        coliVec.push_back( id0 );
        
    //Retourne le nombre de sommets superposés fusionnés et de sommets colineaires supprimés
    return coliVec.size();
}


bool isPointInPoly( AcDb3dPolyline* poly3D, const AcGePoint3d& point )
{
    bool isPoint = false;
    
    // on teste si la polyligne est ouverte
    if( !isClosed( poly3D ) )
    {
        acutPrintf( _T( "la polyligne n'est pas fermee\n" ) );
        return isPoint;
    }
    
    AcGeVector3d vecX = AcGeVector3d( 1, 0, 0 );
    
    int intersectionCounter = 0;
    
    // on crée une ligne horizontale semi infinie dans le sens des x croissants, avec pour depart le point
    // en question
    AcGeRay3d rayon = AcGeRay3d( point, vecX );
    int numVerts = getNumberOfVertex( poly3D );
    
    for( int i = 0; i < numVerts; i++ )
    {
    
        int varI = i + 1;
        
        if( i == numVerts - 1 )
            varI = 0;
            
        // on va créer un segment entre chaque sommet de la polyligne
        AcGePoint3d ptI, ptVarI;
        Acad::ErrorStatus  es1 = getPointAt( poly3D, i, ptI );
        
        if( es1 == Acad::eInvalidIndex )
            return false;
            
        Acad::ErrorStatus  es2 = getPointAt( poly3D, varI, ptVarI );
        
        if( es2 == Acad::eInvalidIndex )
            return false;
            
        AcGeLineSeg3d segment = AcGeLineSeg3d( ptI, ptVarI );
        
        AcGePoint3d ptOut;
        
        if( segment.intersectWith( rayon, ptOut ) )
            intersectionCounter++;
    }
    
    
    // si la ligne horizontale qu'on a definit precedement coupe la polyligne 0, 2, 4 .. etc fois, alors
    // le point est dans la polyligne, si le nombre est impair, il est à l'exterieur de la polyligne
    if( intersectionCounter % 2 != 0 )
        isPoint = true;
        
        
    return isPoint;
    
}


bool isPointInPoly3dOnPlane( AcDb3dPolyline* poly3D,
    const AcGePoint3d& point )
{
    bool isPoint = false;
    
    //Onteste que la polylignes est fermer
    if( !poly3D->isClosed() )
    {
        print( "Polyligne n'est pas fermer" );
        return isPoint;
    }
    
    AcGeVector2d vecX = AcGeVector2d( 1, 0 );
    
    int intersectionCounter = 0;
    
    // on cree une ligne horizontale semi infinie dans le sens des x croissants, avec pour depart le point
    // en question
    AcGeRay2d rayon = AcGeRay2d( AcGePoint2d( point.x, point.y ), vecX );
    int numVerts = getNumberOfVertex( poly3D );
    
    for( int i = 0; i < numVerts; i++ )
    {
        int varI = i + 1;
        
        if( i == numVerts - 1 )
            varI = 0;
            
        // on va creer un segment entre chaque sommet de la polyligne
        AcGePoint2d ptI, ptVarI;
        AcGePoint3d ptI3d, ptVarI3d;
        getPointAt( poly3D, i, ptI3d );
        ptI = AcGePoint2d( ptI3d.x, ptI3d.y );
        getPointAt( poly3D, varI, ptVarI3d );
        ptVarI = AcGePoint2d( ptVarI3d.x, ptVarI3d.y );
        
        AcGeLineSeg2d segment = AcGeLineSeg2d( ptI, ptVarI );
        
        AcGePoint2d ptOut;
        
        if( segment.intersectWith( rayon, ptOut ) )
            intersectionCounter++;
    }
    
    // si la ligne horizontale qu'on a definit precedement coupe la polyligne 0, 2, 4 .. etc fois, alors
    // le point est dans la polyligne, si le nombre est impair, il est à l'exterieur de la polyligne
    if( intersectionCounter % 2 != 0 )
        isPoint = true;
        
    return isPoint;
}

bool isPointInPolyByAngle( AcDb3dPolyline* poly3D, const AcGePoint3d& point )
{
    long double pi = 3.14159265358979323846;
    
    bool isPoint = false;
    
    // on teste si la polyligne est ouverte
    if( !isClosed( poly3D ) )
    {
        acutPrintf( _T( "la polyligne n'est pas fermee\n" ) );
        return isPoint;
    }
    
    int numVerts = getNumberOfVertex( poly3D );
    double angle = 0.0;
    
    /*for (int i = 0; i < numVerts; i++)
    {
        //On recupere les sommes des angles que fait le polyligne et le point
        AcGePoint3d ptI, ptVarI;
        Acad::ErrorStatus es1 = getPointAt(poly3D, i, ptI);
    
        int varI = i + 1;
        if (varI == numVerts - 1)
            varI = 0;
        Acad::ErrorStatus  es2 = getPointAt(poly3D, varI, ptVarI);
    
        if (es1 == Acad::eInvalidIndex)
            return false;
    
        if (es2 == Acad::eInvalidIndex)
            break;
    
        //Creer les deux vecteurs
        AcGeVector3d vec1 = getVector3d(point, ptI);
        AcGeVector3d vec2 = getVector3d(point, ptVarI);
        vec1.z = 0;
        vec2.z = 0;
    
        //Sommes des angles
        double pangle = vec1.angleTo(vec2);
        //Recuperer le sens du vecteur
        int sens = 1;
        if (vec1.dotProduct(vec2) < 0)
            sens = -1;
        angle = pangle * sens;
    
        print(angle * 180 / pi);
    }
    */
    
    //Boucler sur les sommets
    AcDbObjectIterator* iterPoly = poly3D->vertexIterator();
    AcDb3dPolylineVertex* vertex;
    AcGeVector3d vec1;
    AcGeVector3d vec2;
    int i = 0;
    
    AcGePoint3d pt1, pt2;
    
    for( iterPoly->start(); !iterPoly->done(); iterPoly->step() )
    {
        if( !poly3D->openVertex( vertex, iterPoly->objectId(), AcDb::kForRead ) )
        {
            if( i = 0 )
                pt1 = vertex->position();
                
            else
            {
                pt2 = vertex->position();
                
                vec1 = getVector3d( point, pt1 );
                vec2 = getVector3d( point, pt2 );
                
                vec1.z = 0;
                vec2.z = 0;
                
                //Recuperer les angles
                angle += vec1.angleTo( vec2 );
                
                pt1 = pt2;
            }
        }
        
        ++i;
    }
    
    vertex->close();
    poly3D->getStartPoint( pt2 );
    vec1 = getVector3d( point, pt1 );
    vec2 = getVector3d( point, pt2 );
    
    vec1.z = 0;
    vec2.z = 0;
    
    //Recuperer les angles
    angle += vec1.angleTo( vec2 );
    
    if( angle >= ( pi * 2 ) - 0.01745329251994329576922 || angle <= ( pi * 2 ) + 0.01745329251994329576922 )
        isPoint = true;
        
    return isPoint;
}


Acad::ErrorStatus getPointAt( AcDb3dPolyline* poly3D, const int& index, AcGePoint3d& ptOutput )
{
    Acad::ErrorStatus es = Acad::eInvalidIndex  ;
    
    if( poly3D->objectId() == AcDbObjectId::kNull ) return es;
    
    int i = 0;
    
    AcGePoint3d pt ;
    
    //Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Recuperer le vertex puis le sommet
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
        {
            //Recupérer le sommet
            pt = vertex->position() ;
            vertex->close();
            
            //On test si l'index est egal au compteur
            if( index == i )
            {
                ptOutput = pt;
                es = Acad::eOk;
                break;
            }
            
            //Incrémenter i
            i++;
        }
    }
    
    delete iterPoly3D;
    
    //On retourne le resultat
    return es;
}


Acad::ErrorStatus setPointAt( AcDb3dPolyline* poly3D, const int& index, AcGePoint3d& ptInput )
{
    //Declaration par défaut de la valeur de retour
    Acad::ErrorStatus es =  Acad::eInvalidIndex  ;
    
    //Si l'objet n'est pas une polyligne
    if( poly3D->objectId() == AcDbObjectId::kNull ) return es;
    
    //Compteur utilisé dans la boucle de lecture de la polyligne 3D
    int i = 0;
    
    //Creer un itérateur pour les sommets de la polyligne
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Boucle de lecture des sommets de la polyligne 3D, si index = i changer les valeurs du point 3D
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        if( index == i )
        {
            if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
            {
                vertex->setPosition( ptInput );
                vertex->close();
            }
            
            //C'est OK
            es = Acad::eOk;
        }
        
        //Incrémenter i
        i++;
    }
    
    //Supprimer l'itérateur
    delete iterPoly3D;
    
    //Retourne le resultat
    return es;
}



Acad::ErrorStatus insertPointAt( AcDb3dPolyline* poly3D, const int& index, AcGePoint3d& ptInput )
{
    //Déclaration par défaut de la valeur de retour
    Acad::ErrorStatus es =  Acad::eInvalidIndex;
    
    //Si l'objet n'est pas une polyligne 3D
    if( poly3D->objectId() == AcDbObjectId::kNull ) return es;
    
    //Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Création du vertex à partir d'un point 3d et déclaration de son id
    AcDb3dPolylineVertex* vertexPt3d = new AcDb3dPolylineVertex( ptInput );
    AcDbObjectId idPt3d;
    
    //Déclaration d'un compteur utiliser dans la boucle
    int i = 0;
    
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Si on veut inserer un point sur l'index 0
        if( index == 0 )
        {
            if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
                poly3D->insertVertexAt( idPt3d, AcDbObjectId::kNull, vertexPt3d );
                
            vertexPt3d->close();
            vertex->close();
            break;
        }
        
        //Sinon
        else
        {
            if( ( index - 1 ) == i )
            {
                if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
                    poly3D->insertVertexAt( idPt3d, iterPoly3D->objectId(), vertexPt3d );
                    
                vertexPt3d->close();
                vertex->close();
                break;
            }
        }
        
        //Incrément de i
        i++;
    }
    
    delete iterPoly3D;
    
    //C'est OK
    es = Acad::eOk;
    
    //Retour du resultat
    return es;
}



Acad::ErrorStatus removePointAt( AcDb3dPolyline* poly3D, const int& index )
{
    //Déclaration par défaut de la valeur de retour
    Acad::ErrorStatus es =  Acad::eInvalidIndex  ;
    
    //Si l'objet n'est pas une polyligne 3D
    if( poly3D->objectId() == AcDbObjectId::kNull ) return es;
    
    //Déclaration d'un compteur utiliser dans la boucle de lecture de la polyligne 3D
    int i = 0;
    
    //Créer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Boucle de lecture des sommets de la polyligne 3D
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        if( index == i )
        {
            if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
            {
                vertex->erase();
                vertex->close();
                break;
            }
        }
        
        //Incrémentation de i
        i++;
    }
    
    delete iterPoly3D;
    
    //C'est OK
    es = Acad::eOk;
    
    return es;
}


AcGePoint3dArray intersectPoly3D( AcDb3dPolyline*& firstPoly3D,  AcDb3dPolyline*& secondPoly3D )
{
    AcGePoint3dArray resultPointArray;
    
    if( ( firstPoly3D == NULL ) || ( secondPoly3D == NULL ) )
        return resultPointArray;
        
    // On crée un plan de projection
    AcGePlane projectionPlane = AcGePlane( AcGePoint3d::kOrigin, AcGeVector3d::kZAxis );
    
    // On calcul les intérsections des 2 polylines sur le plan de projections
    Acad::ErrorStatus errorStatus =  firstPoly3D->intersectWith( secondPoly3D,
            AcDb::kOnBothOperands,
            projectionPlane,
            resultPointArray );
            
    return  resultPointArray;
}

AcGePoint3dArray  intersectPoly3D( const AcDbObjectId& firstPoly3DId, const AcDbObjectId&  secondPoly3DId )
{
    AcDb3dPolyline* firstPoly3D = NULL;
    AcDb3dPolyline* secondPoly3D = NULL;
    AcDbEntity* firstEntity = NULL;
    AcDbEntity* secondEntity = NULL;
    
    // Récupérer le premier entité
    if( Acad::eOk != acdbOpenAcDbEntity( firstEntity, firstPoly3DId, AcDb::kForRead ) )
        return NULL;
        
    firstPoly3D = AcDb3dPolyline::cast( firstEntity );
    
    
    // Récupérer le second entitee
    if( Acad::eOk != acdbOpenAcDbEntity( secondEntity, secondPoly3DId, AcDb::kForRead ) )
        return NULL;
        
    secondPoly3D = AcDb3dPolyline::cast( secondEntity );
    
    
    // On vérifie si le bounding box se coupe sur z = 0 avant tout
    AcGePoint3dArray resultPointArray;
    
    AcDbExtents firstPolyBox;
    AcDbExtents secondPolyBox;
    AcDbExtents result;
    
    firstPoly3D->getGeomExtents( firstPolyBox );
    AcGePoint3d firstLeftBottomPoint = firstPolyBox.minPoint();
    AcGePoint3d firstRightTopPoint = firstPolyBox.maxPoint();
    
    secondPoly3D->getGeomExtents( secondPolyBox );
    AcGePoint3d secondLeftBottomPoint = secondPolyBox.minPoint();
    AcGePoint3d secondRightTopPoint = secondPolyBox.maxPoint();
    
    if( are2RectIntersected( firstLeftBottomPoint, firstRightTopPoint, secondLeftBottomPoint, secondRightTopPoint ) )
    {
        resultPointArray = intersectPoly3D( firstPoly3D, secondPoly3D );
        
        firstPoly3D->close();
        firstEntity = NULL;
        
        secondPoly3D->close();
        secondEntity = NULL;
        
        return  resultPointArray;
    }
    
    else
    {
        // Pas d'intersection, on ne fait rien
        firstPoly3D->close();
        firstEntity = NULL;
        
        secondPoly3D->close();
        secondEntity = NULL;
        return resultPointArray;
    }
    
}


bool hasToTranslate( AcDb3dPolyline* poly3D,
    const double& distRef,
    AcGeVector3d& vecOfTranslationRef )
{
    bool bResult = false;
    
    if( poly3D == NULL )
        return false;
        
    AcGePoint3d nearestPoint;
    
    // Récupérer le point le plus proche de l'origine
    if( poly3D->getClosestPointTo( AcGePoint3d::kOrigin, nearestPoint ) == Acad::eOk )
    {
        // Calculer la distance du pointpar rapport de l'origine
        double fDistance = nearestPoint.distanceTo( AcGePoint3d::kOrigin );
        
        // Comparer avec la distance de référence si plus grand dans ce cas retourner un vecteur de translation
        if( fDistance > distRef )
        {
            bResult = true;
            vecOfTranslationRef = AcGeVector3d( -nearestPoint.x,
                    -nearestPoint.y,
                    -nearestPoint.z );
        }
    }
    
    return bResult;
}


Acad::ErrorStatus insertVertexesToPoly3D( AcDb3dPolyline*& poly3D,
    AcGePoint3dArray& intersectPoint3DArray,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    if( !poly3D )
        return Acad::eNullHandle;
        
    if( intersectPoint3DArray.empty() )
        return errorStatus;
        
    // Valeur pour savoir si la polyline3D est fermée
    bool isPolylineclosed = poly3D->isClosed();
    // Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDb3dPolylineVertex* vertex2 = NULL;
    AcDb3dPolylineVertex* indexVertex = NULL;
    AcDbObjectId newVertexId;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectIterator* iterPoly3DNext = poly3D->vertexIterator();
    AcGePoint3d ptCurrentVertexCoord;
    AcGePoint3d ptNextVertexCoord;
    
    // Se baser sur les points d'intersection et non les sommets des polylines
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        // Récupérer le sommet courrant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex->close();
        }
        
        else
            break;
            
        if( !iterPoly3DNext->done() )
            iterPoly3DNext->step();
        else
            break;
            
        // Récupérer le sommet suivant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
        {
            if( vertex )
            {
                ptNextVertexCoord = vertex->position();
                vertex->close();
            }
            
            else
            {
                if( isPolylineclosed )
                    break;
                else
                    return errorStatus;
            }
            
        }
        
        // Vérifier si une des intersections se trouve entre les deux sommets
        
        int size = intersectPoint3DArray.size();
        
        for( int counter = 0; counter < size; counter ++ )
        {
            AcGePoint3d ptOneIntersection = intersectPoint3DArray[ counter ];
            
            
            if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                    ptNextVertexCoord,
                    ptOneIntersection,
                    tol ) )
            {
                // Ajout du sommet
                AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( ptOneIntersection );
                
                //Acad::ErrorStatus err = poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite );//TEST OH
                
                if( Acad::eOk == poly3D->openVertex( vertex2, iterPoly3D->objectId(), AcDb::kForRead ) ) // iterator
                {
                    if( vertex2 )
                    {
                        if( newVertexId.isNull() || newVertexId.isErased() )
                        {
                            poly3D->insertVertexAt( newVertexId, vertex2->objectId(), newVertex );
                            newVertex->close();
                            vertex2->close();
                        }
                        
                        else
                        {
                            poly3D->insertVertexAt( newVertexId, newVertexId, newVertex );
                            newVertex->close();
                            vertex2->close();
                        }
                        
                    }
                    
                    else
                        delete newVertex;
                        
                }
                
                else
                    delete newVertex;
                    
                //une fois un point inséré on passe au suivant
                continue;
            }
            
        }
        
    }
    
    // Si la polyline est fermé alors on véerifie une intersection se trouve entre le dernie et le premier sommet
    if( isPolylineclosed )
    {
    
        AcDbObjectId secondObjId;
        
        // Pointer le second itérator sur le début
        iterPoly3DNext->start();
        
        // Récupérer les coordonnées des vertex associés
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex->close();
        }
        
        // Pointer le premier itérator sur la fin
        for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
        {
        
            if( !iterPoly3D->done() )
            {
                secondObjId = iterPoly3D->objectId();
                iterPoly3D->step();
            }
            
            else
                break;
                
        }
        
        if( Acad::eOk == poly3D->openVertex( vertex, secondObjId, AcDb::kForRead ) )
        {
            ptNextVertexCoord = vertex->position();
            vertex->close();
        }
        
        // Vérifier si une des intersections se trouve entre les deux sommets
        int size = intersectPoint3DArray.size();
        
        for( int nCounter = 0; nCounter < size; nCounter ++ )
        {
            AcGePoint3d ptOneIntersection = intersectPoint3DArray.getAt( nCounter );
            
            if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                    ptNextVertexCoord,
                    ptOneIntersection,
                    tol ) )
            {
                // Ajout du sommet
                AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( ptOneIntersection );
                
                if( Acad::eOk == poly3D->openVertex( vertex, secondObjId, AcDb::kForWrite ) )
                {
                    if( vertex )
                    {
                        poly3D->insertVertexAt( newVertexId, secondObjId, newVertex );
                        newVertex->close();
                        vertex->close();
                    }
                    
                    else
                        delete newVertex;
                }
                
                else
                    delete newVertex;
                    
                //une fois un point inséré on passe au suivant
                continue;
            }
        }
        
    }
    
    poly3D->close();
    return errorStatus;
}


Acad::ErrorStatus insertVertexesToPoly3D( AcDb3dPolyline*& poly3D,
    long& count,
    AcGePoint3dArray& intersectPoint3DArray,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    count = 0;
    
    if( !poly3D )
        return Acad::eNullHandle;
        
    if( intersectPoint3DArray.empty() )
        return errorStatus;
        
    // Valeur pour savoir si la polyline3D est fermée
    bool isPolylineclosed = poly3D->isClosed();
    // Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDb3dPolylineVertex* vertex2 = NULL;
    AcDb3dPolylineVertex* indexVertex = NULL;
    AcDbObjectId newVertexId;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectIterator* iterPoly3DNext = poly3D->vertexIterator();
    AcGePoint3d ptCurrentVertexCoord;
    AcGePoint3d ptNextVertexCoord;
    
    // Se baser sur les points d'intersection et non les sommets des polylines
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        // Récupérer le sommet courrant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex->close();
        }
        
        else
            break;
            
        if( !iterPoly3DNext->done() )
            iterPoly3DNext->step();
        else
            break;
            
        // Récupérer le sommet suivant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
        {
            if( vertex )
            {
                ptNextVertexCoord = vertex->position();
                vertex->close();
            }
            
            else
            {
                if( isPolylineclosed )
                    break;
                else
                    return errorStatus;
            }
            
        }
        
        // Vérifier si une des intersections se trouve entre les deux sommets
        
        int size = intersectPoint3DArray.size();
        
        for( int counter = 0; counter < size; counter ++ )
        {
            AcGePoint3d ptOneIntersection = intersectPoint3DArray[ counter ];
            
            
            if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                    ptNextVertexCoord,
                    ptOneIntersection,
                    tol ) )
            {
                // Ajout du sommet
                AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( ptOneIntersection );
                
                //Acad::ErrorStatus err = poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite );//TEST OH
                
                if( Acad::eOk == poly3D->openVertex( vertex2, iterPoly3D->objectId(), AcDb::kForRead ) ) // iterator
                {
                    if( vertex2 )
                    {
                        if( newVertexId.isNull() || newVertexId.isErased() )
                        {
                            poly3D->insertVertexAt( newVertexId, vertex2->objectId(), newVertex );
                            newVertex->close();
                            vertex2->close();
                            count++;
                        }
                        
                        else
                        {
                            poly3D->insertVertexAt( newVertexId, newVertexId, newVertex );
                            newVertex->close();
                            vertex2->close();
                            count++;
                        }
                        
                    }
                    
                    else
                        delete newVertex;
                        
                }
                
                else
                
                    delete newVertex;
                    
                //une fois un point inséré on passe au suivant
                continue;
            }
            
        }
        
    }
    
    // Si la polyline est fermé alors on véerifie une intersection se trouve entre le dernie et le premier sommet
    if( isPolylineclosed )
    {
    
        AcDbObjectId secondObjId;
        
        // Pointer le second itérator sur le début
        iterPoly3DNext->start();
        
        // Récupérer les coordonnées des vertex associés
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex->close();
        }
        
        // Pointer le premier itérator sur la fin
        for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
        {
        
            if( !iterPoly3D->done() )
            {
                secondObjId = iterPoly3D->objectId();
                iterPoly3D->step();
            }
            
            else
                break;
                
        }
        
        if( Acad::eOk == poly3D->openVertex( vertex, secondObjId, AcDb::kForRead ) )
        {
            ptNextVertexCoord = vertex->position();
            vertex->close();
        }
        
        // Vérifier si une des intersections se trouve entre les deux sommets
        int size = intersectPoint3DArray.size();
        
        for( int nCounter = 0; nCounter < size; nCounter ++ )
        {
            AcGePoint3d ptOneIntersection = intersectPoint3DArray.getAt( nCounter );
            
            if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                    ptNextVertexCoord,
                    ptOneIntersection,
                    tol ) )
            {
                // Ajout du sommet
                AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( ptOneIntersection );
                
                if( Acad::eOk == poly3D->openVertex( vertex, secondObjId, AcDb::kForWrite ) )
                {
                    if( vertex )
                    {
                        poly3D->insertVertexAt( newVertexId, secondObjId, newVertex );
                        newVertex->close();
                        vertex->close();
                        count++;
                    }
                    
                    else
                        delete newVertex;
                }
                
                else
                    delete newVertex;
                    
                //une fois un point inséré on passe au suivant
                continue;
            }
        }
        
    }
    
    poly3D->close();
    return errorStatus;
}


Acad::ErrorStatus projectAndInsertPointsToPoly3D( AcDb3dPolyline*& poly3D,
    AcGePoint3dArray& intersectPoint3DArray,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    
    if( poly3D == NULL )
        return Acad::eNullHandle;
        
    if( intersectPoint3DArray.empty() )
        return errorStatus;
        
    // Création d'un plan de projection
    AcGePlane projectionPlane = AcGePlane( AcGePoint3d::kOrigin, AcGeVector3d::kZAxis ); //Plan de projection par défaut
    // AcGeVector3d normalVectorOfthePlan = projectionPlane.normal();
    AcGeVector3d normalVectorOfthePlan = AcGeVector3d::kZAxis;
    
    AcGePoint3dArray projIntersectionsPt3DArray;
    
    AcGePlane polyPlane;
    AcDb::Planarity plnrty;
    poly3D->getPlane( polyPlane, plnrty );
    
    // On boucle sur les sommets
    for( int nCounter = 0; nCounter < intersectPoint3DArray.size(); nCounter ++ )
    {
        AcGePoint3d ptOneIntersection = intersectPoint3DArray.getAt( nCounter );
        AcGePoint3d ptOneIntersectionProjectedOnPoly;
        AcGePoint3dArray intersectionPointsProjectedArray;
        
        AcGePoint3dArray ptProjectionArray;
        AcGePoint3d ptOneIntersectionWithZValue = AcGePoint3d( ptOneIntersection.x, ptOneIntersection.y, 5 ); //To changes to constant
        ptProjectionArray.append( ptOneIntersection );
        ptProjectionArray.append( ptOneIntersectionWithZValue );
        
        // Projeter le point d'intersection avec Z = 0.0 sur la poluline3D
        // 1. On calcul l'intersection d'un polyline passant par le point d'interction et un autre point avec un Z > 0 (pour avoir un vecteur directeur)
        AcDb3dPolyline* projectionLine3D = new AcDb3dPolyline( AcDb::k3dSimplePoly, ptProjectionArray );
        poly3D->intersectWith( projectionLine3D, AcDb::kExtendArg, intersectionPointsProjectedArray );
        
        
        if( intersectionPointsProjectedArray.length() > 0 )
        {
            int lenght = intersectionPointsProjectedArray.length();
            AcGePoint3d oneIntersectionProjected = intersectionPointsProjectedArray.at( 0 );
            
            // 2. On cherche le point le plus proche
            poly3D->getClosestPointTo( oneIntersectionProjected, ptOneIntersectionProjectedOnPoly );
            
            projIntersectionsPt3DArray.append( ptOneIntersectionProjectedOnPoly );
        }
        
        delete projectionLine3D;
    }
    
    insertVertexesToPoly3D( poly3D, projIntersectionsPt3DArray, tol );
    
    return errorStatus;
}


Acad::ErrorStatus intersectTwoPoly3D( AcDb3dPolyline*& poly3D,
    AcDb3dPolyline*& otherPoly3D,
    long& count,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    if( !poly3D )
        return Acad::eNullHandle;
        
    if( !otherPoly3D )
        return Acad::eNullHandle;
        
    AcGePoint3dArray intersectPoint3DArray = intersectPoly3D( poly3D, otherPoly3D );
    
    int size = intersectPoint3DArray.length();
    
    for( int counter = 0; counter  < size ; counter ++ )
    {
        AcGePoint3d one = intersectPoint3DArray[counter];
        counter = counter;
    }
    
    // Ajout des sommets sur le premier poly3d
    if( size > 0 )
    {
        //cumule le nombre d'intersection
        count += size;
        insertVertexesToPoly3D( poly3D, intersectPoint3DArray, tol );
        
        // Ajout des sommets sur le premier poly3d
        projectAndInsertPointsToPoly3D( otherPoly3D, intersectPoint3DArray, tol );
    }
    
    return errorStatus;
}


Acad::ErrorStatus insertVertexToPoly3D( const AcDbObjectId& idPoly3D,
    AcGePoint3d onePt3D,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    // Vérification de la polyline
    if( idPoly3D.isNull() || !idPoly3D.isValid() )
        return Acad::eNullHandle;
        
        
    AcDb3dPolyline* poly3D = NULL;
    AcDbEntity* entity = NULL;
    
    // Récupérer le premier entitee
    if( Acad::eOk != acdbOpenAcDbEntity( entity, idPoly3D, AcDb::kForWrite ) )
        return Acad::eNullHandle;
        
    poly3D = AcDb3dPolyline::cast( entity );
    
    
    // Test si le poly3D est fermé
    bool isClosed = poly3D->isClosed();
    
    
    // Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDb3dPolylineVertex* indexVertex = NULL;
    AcDbObjectId newVertexId;
    AcDbObjectId PrevVertexId;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectIterator* iterPoly3DNext = poly3D->vertexIterator();
    AcGePoint3d ptCurrentVertexCoord;
    AcGePoint3d ptNextVertexCoord;
    
    
    // Recherche le sommet précédent le nouveau vertex
    if( ( searchPreviewVertextIdToPoint( poly3D, onePt3D, PrevVertexId, tol ) == Acad::eOk ) && ( !PrevVertexId.isNull() ) )
    {
        // Ajout du sommet
        AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( onePt3D );
        
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
        {
            if( vertex )
            {
                poly3D->insertVertexAt( newVertexId, PrevVertexId, newVertex );
                newVertex->close();
                vertex->close();
            }
            
            else
                delete newVertex;
                
        }
        
        else
            delete newVertex;
    }
    
    
    // Libérer la mémoire
    poly3D->close();
    
    return errorStatus;
}


Acad::ErrorStatus insertVertexToPoly3D( AcDb3dPolyline*& poly3D,
    AcGePoint3d& intersectPoint3D,
    const double& tol )
{
    // Vérification de la polyline
    if( !poly3D )
        return Acad::eNullHandle;
        
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    // Test si le poly3D est fermé
    bool isClosed = poly3D->isClosed();
    
    
    // Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDb3dPolylineVertex* indexVertex = NULL;
    AcDbObjectId newVertexId;
    AcDbObjectId PrevVertexId;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectIterator* iterPoly3DNext = poly3D->vertexIterator();
    AcGePoint3d ptCurrentVertexCoord;
    AcGePoint3d ptNextVertexCoord;
    
    
    // Recherche le sommet précédent le nouveau vertex
    if( ( searchPreviewVertextIdToPoint( poly3D, intersectPoint3D, PrevVertexId, tol ) == Acad::eOk ) && ( !PrevVertexId.isNull() ) )
    {
        // Ajout du sommet
        AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( intersectPoint3D );
        
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
        {
            if( vertex )
            {
                poly3D->insertVertexAt( newVertexId, PrevVertexId, newVertex );
                newVertex->close();
                vertex->close();
            }
            
            else
                delete newVertex;
                
        }
        
        else
            delete newVertex;
    }
    
    return errorStatus;
}



Acad::ErrorStatus insertVertexToPoly3DFilter( const AcDbObjectId& idPoly3D,
    AcGePoint3d onePt3D,
    const double& tol, const double& filter )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    // Vérification de la polyline
    if( idPoly3D.isNull() || !idPoly3D.isValid() )
        return Acad::eNullHandle;
        
        
    AcDb3dPolyline* poly3D = NULL;
    AcDbEntity* entity = NULL;
    
    // Recuperer le premier entitee
    if( Acad::eOk != acdbOpenAcDbEntity( entity, idPoly3D, AcDb::kForWrite ) )
        return Acad::eNullHandle;
        
    poly3D = AcDb3dPolyline::cast( entity );
    
    
    // Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectId newVertexId;
    AcDbObjectId PrevVertexId;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    // Recherche le sommet précédent le nouveau vertex
    if( ( searchPreviewVertextIdToPointFilter( poly3D, onePt3D, PrevVertexId, tol, filter ) == Acad::eOk ) && ( !PrevVertexId.isNull() ) )
    {
        // Ajout du sommet
        AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( onePt3D );
        
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
        {
            if( vertex )
            {
                poly3D->insertVertexAt( newVertexId, PrevVertexId, newVertex );
                newVertex->close();
                vertex->close();
            }
            
            else
                delete newVertex;
                
        }
        
        else
            delete newVertex;
    }
    
    
    // Libérer la mémoire
    poly3D->close();
    
    return errorStatus;
}


Acad::ErrorStatus insertVertexesToPoly3DId( const AcDbObjectId& idPoly3D,
    AcGePoint3dArray& pointsToInsertArray,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    // Vérification de la polyline
    if( idPoly3D.isNull() || !idPoly3D.isValid() )
        return Acad::eNullHandle;
        
    //On vérifie qu'il y a au moin un point à insérer
    long pointsCount = pointsToInsertArray.length();
    
    if( pointsCount == 0 )
        return errorStatus;
        
    // On ouvre la polyligne pour écriture
    AcDb3dPolyline* poly3D = NULL;
    AcDbEntity* entity = NULL;
    
    if( Acad::eOk != acdbOpenAcDbEntity( entity, idPoly3D, AcDb::kForWrite ) )
        return Acad::eNullHandle;
        
    poly3D = AcDb3dPolyline::cast( entity );
    
    
    
    for( long counter = 0; counter < pointsCount; counter ++ )
    {
        AcGePoint3d onePoint = pointsToInsertArray[counter];
        errorStatus = insertVertexToPoly3D( poly3D, onePoint, tol );
    }
    
    poly3D->close();
    //On ferme la polyline
    return errorStatus;
    
}


Acad::ErrorStatus insertVertexesToPoly3DId( const AcDbObjectId& idPoly3D,
    AcGePoint3dArray& pointsToInsertArray,
    const double& tol, const double& filter )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    // Vérification de la polyline
    if( idPoly3D.isNull() || !idPoly3D.isValid() )
        return Acad::eNullHandle;
        
    //On vérifie qu'il y a au moin un point à insérer
    long pointsCount = pointsToInsertArray.length();
    
    if( pointsCount == 0 )
        return errorStatus;
        
        
        
    for( long counter = 0; counter < pointsCount; counter ++ )
    {
        AcGePoint3d onePoint = pointsToInsertArray[counter];
        errorStatus = insertVertexToPoly3DFilter( idPoly3D, onePoint, tol, filter );
    }
    
    
    return errorStatus;
}


Acad::ErrorStatus intersectTwoPoly3D( const AcDbObjectId& idFirstPoly3D,
    const AcDbObjectId& idSecondPoly3D,
    AcGePoint3dArray& pointsOfInsectionArray,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    if( idFirstPoly3D.isNull() || !idFirstPoly3D.isValid() )
        return Acad::eNullHandle;
        
    if( idSecondPoly3D.isNull() || !idSecondPoly3D.isValid() )
        return Acad::eNullHandle;
        
    AcGePoint3dArray intersectPoint3DArray = intersectPoly3D( idFirstPoly3D, idSecondPoly3D );
    
    pointsOfInsectionArray.append( intersectPoint3DArray );
    
    intersectPoint3DArray.removeAll();
    
    return errorStatus;
}


Acad::ErrorStatus projectToPoly3D( const AcDbObjectId& idPoly3D,
    const AcGePoint3dArray& pointsToProject3DArray,
    AcGePoint3dArray& pointsProjected3DArray,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    if( idPoly3D.isNull() || !idPoly3D.isValid() )
        return Acad::eNullHandle;
        
    if( pointsToProject3DArray.empty() )
        return errorStatus;
        
    // Récupérer la poly3D
    AcDbEntity* poly3DEntity = NULL;
    
    if( Acad::eOk != acdbOpenAcDbEntity( poly3DEntity, idPoly3D, AcDb::kForRead ) )
        return Acad::eNullHandle;
        
    AcDb3dPolyline* poly3D = AcDb3dPolyline::cast( poly3DEntity );
    
    
    // Création d'un plan de projection
    /// Plan de projection par défaut
    AcGePlane projectionPlane = AcGePlane( AcGePoint3d::kOrigin, AcGeVector3d::kZAxis );
    
    // On boucle sur les sommets
    for( long nCounter = 0; nCounter < pointsToProject3DArray.size(); nCounter ++ )
    {
        AcGePoint3d ptOneIntersection = pointsToProject3DArray[ nCounter ];
        AcGePoint3d ptOneIntersectionProjectedOnPoly;
        AcGePoint3dArray intersectionPointsProjectedArray;
        
        AcGePoint3dArray ptProjectionArray;
        AcGePoint3d ptOneIntersectionWithZValue = AcGePoint3d( ptOneIntersection.x, ptOneIntersection.y, 5 ); //To changes to constant
        ptProjectionArray.append( ptOneIntersection );
        ptProjectionArray.append( ptOneIntersectionWithZValue );
        
        // Projeter le point d'intersection avec Z = 0.0 sur la poluline3D
        // 1. On calcul l'intersection d'un polyline passant par le point d'interction et un autre point avec un Z > 0 (pour avoir un vecteur directeur)
        AcDb3dPolyline* projectionLine3D = new AcDb3dPolyline( AcDb::k3dSimplePoly, ptProjectionArray );
        
        AcGePlane projectionPlane = AcGePlane( AcGePoint3d::kOrigin, AcGeVector3d::kZAxis );
        poly3D->intersectWith( projectionLine3D, AcDb::kExtendArg, projectionPlane, intersectionPointsProjectedArray );
        
        if( intersectionPointsProjectedArray.length() > 0 )
        {
            AcGePoint3d oneIntersectionProjected = intersectionPointsProjectedArray.at( 0 );
            
            // 2. On cherche le point le plus proche
            poly3D->getClosestPointTo( oneIntersectionProjected, ptOneIntersectionProjectedOnPoly );
            
            pointsProjected3DArray.append( ptOneIntersectionProjectedOnPoly );
        }
        
        delete projectionLine3D;
    }
    
    poly3D->close();
    poly3DEntity = NULL;
    
    
    return errorStatus;
}

Acad::ErrorStatus projectToPoly3D( AcDb3dPolyline*& poly3D,
    const AcGePoint3dArray& pointsToProject3DArray,
    AcGePoint3dArray& pointsProjected3DArray,
    const double& tol )
{
    if( !poly3D )
        return eNotApplicable;
        
    Acad::ErrorStatus errorStatus = Acad::eOk;
    // Création d'un plan de projection
    AcGePlane projectionPlane = AcGePlane( AcGePoint3d::kOrigin, AcGeVector3d::kZAxis );
    AcGeVector3d normalVectorOfthePlan = AcGeVector3d::kZAxis;
    
    int size = pointsToProject3DArray.size();
    
    // On boucle sur les sommets
    for( long nCounter = 0; nCounter < size ; nCounter ++ )
    {
        AcGePoint3d ptOneIntersection = pointsToProject3DArray[ nCounter ];
        AcGePoint3d ptOneIntersectionProjectedOnPoly;
        AcGePoint3dArray intersectionPointsProjectedArray;
        
        AcGePoint3dArray ptProjectionArray;
        AcGePoint3d ptOneIntersectionWithZValue = AcGePoint3d( ptOneIntersection.x, ptOneIntersection.y, ptOneIntersection.z + 5 );
        ptProjectionArray.append( ptOneIntersection );
        ptProjectionArray.append( ptOneIntersectionWithZValue );
        
        // Projeter le point d'intersection avec Z = 0.0 sur la poluline3D
        // 1. On calcul l'intersection d'un polyline passant par le point d'interction et un autre point avec un Z > 0 (pour avoir un vecteur directeur)
        AcDb3dPolyline* projectionLine3D = new AcDb3dPolyline( AcDb::k3dSimplePoly, ptProjectionArray );
        errorStatus = poly3D->intersectWith( projectionLine3D, AcDb::kExtendArg, intersectionPointsProjectedArray );
        
        if( ( intersectionPointsProjectedArray.length() > 0 ) && ( errorStatus == eOk ) )
        {
            AcGePoint3d oneIntersectionProjected = intersectionPointsProjectedArray.at( 0 );
            
            // 2. On cherche le point le plus proche
            errorStatus = poly3D->getClosestPointTo( oneIntersectionProjected, ptOneIntersectionProjectedOnPoly );
            
            if( errorStatus == eOk )
                pointsProjected3DArray.append( ptOneIntersectionProjectedOnPoly );
        }
        
        delete projectionLine3D;
    }
    
    poly3D->close();
    return errorStatus;
}

Acad::ErrorStatus searchPreviewVertextIdToPoint( AcDb3dPolyline* poly3D,
    AcGePoint3d onePt3D,
    AcDbObjectId& indexVertId,
    const double& tol )
{

    Acad::ErrorStatus errorStatus = Acad::eKeyNotFound;
    
    // Vérification de la polyline
    if( !poly3D )
        return Acad::eNullHandle;
        
    // Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDb3dPolylineVertex* indexVertex = NULL;
    AcDbObjectId newVertexId;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectIterator* iterPoly3DNext = poly3D->vertexIterator();
    AcGePoint3d ptCurrentVertexCoord;
    AcGePoint3d ptNextVertexCoord;
    
    // Variable pour stocker les ids du premier et dernier sommet de la poly si celle la est fermée
    AcDbObjectId firstVertexId;
    AcDbObjectId lastVertexId;
    
    bool isClosed = poly3D->isClosed();
    
    if( isClosed )
    {
        // On sauvegarde le premier ID
        iterPoly3D->start();
        firstVertexId = iterPoly3D->objectId();
        
        // On sauvegarde le dernier ID
        iterPoly3D->start( true );
        lastVertexId = iterPoly3D->objectId();
    }
    
    // On remet l'itérator dans le bon sens par précaution
    iterPoly3D->start();
    
    
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        // Récupérer le sommet courrant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex ->close();
            vertex = NULL;
        }
        
        else
        {
            vertex = NULL;
            return eNullHandle;
        }
        
        if( !iterPoly3DNext->done() )
            iterPoly3DNext->step();
        else
            break;
            
        // Récupérer le sommet suivant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
        {
            if( vertex )
            {
                ptNextVertexCoord = vertex->position();
                vertex->close();
                vertex = NULL;
            }
            
            else
            {
                vertex = NULL;
                
                if( !isClosed )
                    return Acad::eOk;
                else
                    break;
            }
            
        }
        
        // Vérifier si le point d'intersection se trouve entre les deux sommets
        if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                ptNextVertexCoord,
                onePt3D,
                tol ) )
        {
            indexVertId = iterPoly3D->objectId();
            errorStatus =  Acad::eOk;
            break;
        }
        
        // Vérifier si le point d'intersection se trouve entre les deux sommets
    }
    
    // Si la polyline est fermé alors on vérifie une intersection se trouve entre le dernie et le premier sommet
    if( isClosed && ( errorStatus !=  Acad::eOk ) )
    {
    
        AcDbObjectId secondObjId;
        
        // Pointer le second itérator sur le début
        iterPoly3DNext->start();
        
        // Récupérer les coordonnées des vertex associés
        if( Acad::eOk == poly3D->openVertex( vertex, firstVertexId, AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex->close();
            vertex = NULL;
        }
        
        if( Acad::eOk == poly3D->openVertex( vertex, lastVertexId, AcDb::kForRead ) )
        {
            ptNextVertexCoord = vertex->position();
            vertex->close();
            vertex = NULL;
        }
        
        // Vérifier si une des intersections se trouve entre les deux sommets
        if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                ptNextVertexCoord,
                onePt3D,
                tol ) )
        {
        
        
        
            // Ajout du sommet
            AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( onePt3D );
            
            if( Acad::eOk == poly3D->openVertex( vertex, lastVertexId, AcDb::kForWrite ) )
            {
                if( vertex )
                {
                    poly3D->insertVertexAt( newVertexId, lastVertexId, newVertex );
                    newVertex->close();
                    vertex->close();
                    vertex = NULL;
                }
                
                else
                    delete newVertex;
            }
            
            else
                delete newVertex;
                
        }
        
        
    }
    
    
    // Si la polyline est fermé alors on vérifie une intersection se trouve entre le dernie et le premier sommet
    
    return errorStatus;
}


Acad::ErrorStatus searchPreviewIndexToPoint( AcDb3dPolyline* poly3D,
    AcGePoint3d onePt3D,
    int& indexVert,
    const double& tol )
{
    Acad::ErrorStatus es = Acad::eOk;
    
    AcGePoint3d ptClose;
    
    //Recuperer le point le plus proche
    es = poly3D->getClosestPointTo( onePt3D, ptClose );
    
    if( es != Acad::eOk )
    {
        print( "Erreur dans le getClosestPointTo" );
        indexVert = -1;
        return Acad::eNotApplicable;
    }
    
    //Recuperer les distances curvilignes de chaque point de la polyligne
    vector<double> distVec;
    es = getDistVec( poly3D, distVec );
    
    if( es != Acad::eOk )
    {
        print( "Erreur lors de la recuperation des distances curvilignes." );
        indexVert = -1;
        return es;
    }
    
    double d = 0.0;
    
    //Recuperer la distance curviligne correspondant au point
    es = poly3D->getDistAtPoint( ptClose, d );
    
    if( es != Acad::eOk )
    {
        print( es );
        indexVert = -1;
        return es;
    }
    
    // Dichotomie pour chercher l'index
    std::vector<double>::const_iterator low;
    
    //Chercher l'index d'avant du distance curviligne la plus proche
    low = std::lower_bound( distVec.begin(), distVec.end(), d );
    
    //Recuperer l'index
    int ix = ( low - distVec.begin() );
    
    // Le point pass dans la fonction est un sommet de la polyligne, on retourne son indice
    if( abs( distVec[ ix ] - d ) < tol ||
        abs( distVec[ ix + 1 ] - d ) < tol ||
        abs( distVec[ ix - 1 ] - d ) < tol )
    {
        indexVert = -1;
        return Acad::eOk;
    }
    
    // Le point pass dans la fonction est sur la polyligne, MAIS n'est pas un sommet
    // Dans ce cas on retourne l'indice du sommet avec la plus courte distance curviligne juste avant le point
    else
        indexVert = ix;
        
    return es;
}


Acad::ErrorStatus searchPreviewVertextIdToPointFilter( AcDb3dPolyline* poly3D,
    AcGePoint3d onePt3D,
    AcDbObjectId& indexVertId,
    const double& tol, const double& filter )
{
    Acad::ErrorStatus errorStatus = Acad::eKeyNotFound;
    
    // Vérification de la polyline
    if( !poly3D )
        return Acad::eNullHandle;
        
    // Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDb3dPolylineVertex* indexVertex = NULL;
    AcDbObjectId newVertexId;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectIterator* iterPoly3DNext = poly3D->vertexIterator();
    AcGePoint3d ptCurrentVertexCoord;
    AcGePoint3d ptNextVertexCoord;
    
    // Variable pour stocker les ids du premier et dernier sommet de la poly si celle la est fermée
    AcDbObjectId firstVertexId;
    AcDbObjectId lastVertexId;
    
    bool isClosed = poly3D->isClosed();
    
    if( isClosed )
    {
        // On sauvegarde le premier ID
        iterPoly3D->start();
        firstVertexId = iterPoly3D->objectId();
        
        // On sauvegarde le dernier ID
        iterPoly3D->start( true );
        lastVertexId = iterPoly3D->objectId();
    }
    
    // On remet l'itérator dans le bon sens par précaution
    iterPoly3D->start();
    
    
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        // Récupérer le sommet courrant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex ->close();
            vertex = NULL;
        }
        
        else
        {
            vertex = NULL;
            return eNullHandle;
        }
        
        if( !iterPoly3DNext->done() )
            iterPoly3DNext->step();
        else
            break;
            
        // Récupérer le sommet suivant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
        {
            if( vertex )
            {
                ptNextVertexCoord = vertex->position();
                vertex->close();
                vertex = NULL;
            }
            
            else
            {
                vertex = NULL;
                
                if( !isClosed )
                    return Acad::eOk;
                else
                    break;
            }
            
        }
        
        
        // Vérifier si le point d'intersection se trouve entre les deux sommets
        if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord, ptNextVertexCoord, onePt3D, tol, 10000, false, true, filter ) )
        {
            indexVertId = iterPoly3D->objectId();
            errorStatus =  Acad::eOk;
            break;
        }
        
        // Vérifier si le point d'intersection se trouve entre les deux sommets
    }
    
    // Si la polyline est fermé alors on vérifie une intersection se trouve entre le dernie et le premier sommet
    if( isClosed && ( errorStatus !=  Acad::eOk ) )
    {
    
        AcDbObjectId secondObjId;
        
        // Pointer le second itérator sur le début
        iterPoly3DNext->start();
        
        // Récupérer les coordonnées des vertex associés
        // if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
        if( Acad::eOk == poly3D->openVertex( vertex, firstVertexId, AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex->close();
            vertex = NULL;
        }
        
        //if( Acad::eOk == poly3D->openVertex( vertex, secondObjId, AcDb::kForRead ) )
        if( Acad::eOk == poly3D->openVertex( vertex, lastVertexId, AcDb::kForRead ) )
        {
            ptNextVertexCoord = vertex->position();
            vertex->close();
            vertex = NULL;
        }
        
        // Vérifier si une des intersections se trouve entre les deux sommets
        
        if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                ptNextVertexCoord,
                onePt3D,
                tol ) )
        {
        
        
        
            // Ajout du sommet
            AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( onePt3D );
            
            if( Acad::eOk == poly3D->openVertex( vertex, lastVertexId, AcDb::kForWrite ) )
            {
                if( vertex )
                {
                    poly3D->insertVertexAt( newVertexId, lastVertexId, newVertex );
                    newVertex->close();
                    vertex->close();
                    vertex = NULL;
                }
                
                else
                    delete newVertex;
            }
            
            else
                delete newVertex;
                
        }
        
        
    }
    
    
    // Si la polyline est fermé alors on vérifie une intersection se trouve entre le dernie et le premier sommet
    
    return errorStatus;
}

Acad::ErrorStatus getMilieu( AcDbCurve* poly,
    AcGePoint3d& milieu )
{

    // On récupère le dernier param
    double endParam = 0;
    double dist = 0;
    Acad::ErrorStatus es = poly->getEndParam( endParam );
    
    if( es != Acad::eOk )
        return es;
        
    es = poly->getDistAtParam( endParam, dist );
    
    if( es != Acad::eOk )
        return es;
        
    // On récupère le milieu
    dist /= 2;
    
    es = poly->getPointAtDist( dist, milieu );
    
    return es;
}


Acad::ErrorStatus getMilieu( AcDbCurve* poly,
    AcGePoint2d& milieu )
{

    // On récupère le dernier param
    double endParam = 0;
    double dist = 0;
    Acad::ErrorStatus es = poly->getEndParam( endParam );
    
    if( es != Acad::eOk )
        return es;
        
    es = poly->getDistAtParam( endParam, dist );
    
    if( es != Acad::eOk )
        return es;
        
    // On récupère le milieu
    dist /= 2;
    
    AcGePoint3d pointAtDist = AcGePoint3d::kOrigin;
    es = poly->getPointAtDist( dist, pointAtDist );
    milieu = getPoint2d( pointAtDist );
    
    return es;
}


Acad::ErrorStatus getCentroid( AcDb3dPolyline* poly3D,
    AcGePoint3d& centroid )
{
    if( !poly3D->isClosed() )
        return Acad::eInvalidInput;
        
    //Declaration par défaut de la valeur de retour
    Acad::ErrorStatus es = Acad::eOk;
    
    //Si l'objet n'est pas une polyligne 3D
    if( poly3D->objectId() == AcDbObjectId::kNull ) return eNullObjectId;
    
    //Creer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    centroid = AcGePoint3d::kOrigin;
    double numVerts = 0;
    
    //Boucle de lecture des sommets de la polyligne 3D
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        // On incrémente le nombre de sommets
        numVerts++;
        
        // On ouvre le sommet courant
        es = poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite );
        
        if( es != Acad::eOk )
            return es;
            
        // On additionne tous les points de la polyligne
        centroid = addPoints( centroid,
                vertex->position() );
                
        // On ferme le sommet
        vertex->close();
    }
    
    delete iterPoly3D;
    
    // On moyenne le résultat, en n'oubliant pas de retirer le vertex que l'on a sauté au début
    centroid = AcGePoint3d(
            centroid.x / ( numVerts ),
            centroid.y / ( numVerts ),
            centroid.z / ( numVerts ) );
            
    return Acad::eOk;
}


bool addSomeVertexToPoly( AcDb3dPolyline* poly,
    const int& nbr,
    const int& dist )
{
    //Déclarer les objets et creer un iterateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly->vertexIterator();
    
    //Declarer trois points
    AcGePoint3d pt, pt1, pt2;
    
    //Déclarer un vecteur de point
    std::vector<AcGePoint3d> vectPoint;
    
    //Boucle sur les vertex de la polyligne
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Ouvrir le vertex
        if( Acad::eOk != poly->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return false;
        
        //Recuperer le vertex
        pt = vertex->position();
        
        //Fermer le vertex
        vertex->close();
        
        //Ajouter le vertex dans le vecteur
        vectPoint.push_back( pt );
    }
    
    //Si le vecteur contient plusieurs sommet
    if( vectPoint.size() > 1 )
    {
        //Récupérer le premier point dans le vecteur
        pt1 = vectPoint[0];
        
        //Boucle sur le vecteur
        for( int i = 1; i < vectPoint.size(); i++ )
        {
            //Récupérer le second point dans le vecteur
            pt2 = vectPoint[i];
            
            //Discretisation 0 à 1
            double t = 0.0;
            double k = nbr;
            t = ( 1 / k );
            
            //Tester la distance entre les deux sommets si superieur on ajoute les sommets
            if( getDistance2d( pt1, pt2 ) > dist )
            {
                //Le point à insérer dans la polyligne
                AcGePoint3d ptInsert;
                
                //Insérer les nouveaux vertex dans la polyligne
                for( int j = 0; j < nbr; j++ )
                {
                    //Calcul du point
                    ptInsert.x = pt1.x + ( pt2.x - pt1.x ) * t;
                    ptInsert.y = pt1.y + ( pt2.y - pt1.y ) * t;
                    ptInsert.z = pt1.z + ( pt2.z - pt1.z ) * t;
                    
                    //Calcul de l'index pour inserer le vertex
                    int index = getNumberOfVertex( poly ) - vectPoint.size() + i;
                    
                    //Insérer le point dans la polyligne
                    if( !insertPointAt( poly, index, ptInsert ) == Acad::eOk )
                    {
                        acutPrintf( _T( "Erreur lors de l'ajout du vertex" ) );
                        return false;
                    }
                    
                    //Changer le t
                    t = t + ( 1 / k );
                }
            }
            
            //Remplacer le point
            pt1 = pt2;
        }
    }
    
    else
        return false;
        
    //Supprimer l'iterateur
    delete iterPoly3D;
    
    //Nettoyer la polyligne
    netPoly( poly, 0.1 );
    
    //Fermer la polyligne
    poly->close();
    
    return true;
}


std::vector<AcGePoint3d> addSomeVertexToPoly( std::vector<AcGePoint3d>& vectPoint,
    const int& nbr,
    const int& dist )
{
    //Declarer trois points
    AcGePoint3d pt, pt1, pt2;
    
    //Déclarer un vecteur de point pour le resultat
    std::vector<AcGePoint3d> vectRes;
    
    //Si le vecteur contient plusieurs sommet
    if( vectPoint.size() > 1 )
    {
        //Récupérer le premier point dans le vecteur
        pt1 = vectPoint[0];
        
        //Ajouter le premier point dans vectRes
        vectRes.push_back( pt1 );
        
        //Boucle sur le vecteur
        for( int i = 1; i < vectPoint.size(); i++ )
        {
            //Récupérer le second point dans le vecteur
            pt2 = vectPoint[i];
            
            //Discretisation 0 à 1
            double t = 0.0;
            double k = nbr;
            t = ( 1 / k );
            
            //Tester la distance entre les deux sommets si superieur on ajoute les sommets
            if( getDistance2d( pt1, pt2 ) > dist )
            {
                //Le point à insérer dans la polyligne
                AcGePoint3d ptInsert;
                
                //Insérer les nouveaux vertex dans la polyligne
                for( int j = 0; j < nbr; j++ )
                {
                    //Calcul du point
                    ptInsert.x = pt1.x + ( pt2.x - pt1.x ) * t;
                    ptInsert.y = pt1.y + ( pt2.y - pt1.y ) * t;
                    ptInsert.z = pt1.z + ( pt2.z - pt1.z ) * t;
                    
                    //Ajouter le point dans le vectRes
                    vectRes.push_back( ptInsert );
                    
                    //Changer le t
                    t = t + ( 1 / k );
                }
            }
            
            //Remplacer le point
            pt1 = pt2;
            
            //Ajouter le dernier point dans le vectRes
            vectRes.push_back( pt1 );
        }
        
        //Ajouter le dernier point dans le vectRes
        vectRes.push_back( pt2 );
    }
    
    else
        return vectPoint;
        
    return vectRes;
}


std::vector<AcGePoint3d> poly3dToVector( AcDb3dPolyline* poly3D )
{
    //Declarer le resultat
    std::vector<AcGePoint3d> vectPoint;
    
    //Déclarer les vertex et créer un itérateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Déclarer le point3d du vertex en question
    AcGePoint3d pt3d;
    int nbVtx = 0;
    
    //Boucle sur les sommets de la polyligne en question
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Ouvrir le vertex
        if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) ) return vectPoint;
        
        //Recuperer les coordonnées du vertex
        pt3d  = vertex->position();
        
        //Fermer le vertex
        vertex->close();
        
        //Ajouter le point dans le vecteur
        vectPoint.push_back( pt3d );
        nbVtx++;
    }
    
    if( vectPoint.size() == 0 )
        return vectPoint;
        
    //Tester si la polyligne est fermee, on ajoute le premier point dans le vecteur de polyligne
    if( poly3D->isClosed() )
        vectPoint.push_back( vectPoint[0] );
        
    //Retourner le vecteur de point 3d
    return vectPoint;
}


void drawPolyline3D( std::vector<AcGePoint3d>& tab,
    AcCmColor& colorPoly,
    AcString layer,
    const bool& coli,
    const bool& hasToZoom,
    const bool& closePoly )
{
    //Créer une entité
    AcDbEntity* ent = NULL;
    AcDbObjectId polyId;
    
    //Déclarer une polyligne 3D
    AcDb3dPolyline* poly3D = new AcDb3dPolyline();
    
    //Ajouter la polyligne au modelspace
    addToModelSpace( poly3D, polyId );
    
    //Ouvrir l'entité
    Acad::ErrorStatus er = acdbOpenAcDbEntity( ent, polyId, AcDb::kForWrite );
    
    if( er != eWasOpenForWrite )
        return;
        
    //Créer un vertex et un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectId newVertexId;
    
    //Taille de la polyligne
    int size = tab.size();
    
    //Ouvrir la polyligne 3D en ecriture
    if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
    {
        //Boucle sur les sommets de la polyligne
        for( long counter = 0; counter < size; counter ++ )
        {
            // Vertex de la polyligne à ajouter
            AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( tab[counter] );
            
            //Insertion des sommets
            if( newVertex )
            {
                poly3D->insertVertexAt( newVertexId, newVertex );
                newVertex->close();
            }
            
            //Le cas echeant on supprime le vertex
            else
                delete newVertex;
        }
    }
    
    //Fermer le vertex
    if( vertex )
        vertex->close();
        
    //Nettoyer la polyligne si c'est demandé
    if( coli )
        int k = coliPoly( poly3D, 0.1 );
        
    //Fermer la polyligne si c'est demandé
    if( closePoly )
        poly3D->makeClosed();
        
    //Changer la couleur
    poly3D->setColor( colorPoly );
    
    //Créer le layer
    createLayer( layer );
    
    //Ajouter la polyligne dans la calque trajectoire
    poly3D->setLayer( layer );
    
    AcDbEntity* entity = AcDbEntity::cast( poly3D );
    
    redrawEntity( entity );
    
    //Zoomer sur l'objet
    if( hasToZoom )
        zoomOn( poly3D );
        
    //Fermer la polyligne
    poly3D->close();
}



void drawPolyline3D( AcGePoint3dArray& tab,
    AcCmColor& colorPoly,
    AcString layer,
    const bool& coli,
    const bool& hasToZoom,
    const bool& closePoly )
{
    //Créer une entité
    AcDbEntity* ent = NULL;
    AcDbObjectId polyId;
    
    //Déclarer une polyligne 3D
    AcDb3dPolyline* poly3D = new AcDb3dPolyline( AcDb::Poly3dType::k3dSimplePoly, tab );
    
    //Ajouter la polyligne au modelspace
    addToModelSpace( poly3D, polyId );
    
    //Nettoyer la polyligne si c'est demandé
    if( coli )
        int k = coliPoly( poly3D, 0.1 );
        
    //Fermer la polyligne si c'est demandé
    if( closePoly )
        poly3D->makeClosed();
        
    //Changer la couleur
    poly3D->setColor( colorPoly );
    
    //Créer le layer
    createLayer( layer );
    
    //Ajouter la polyligne dans la calque trajectoire
    poly3D->setLayer( layer );
    
    AcDbEntity* entity = AcDbEntity::cast( poly3D );
    
    redrawEntity( entity );
    
    //Zoomer sur l'objet
    if( hasToZoom )
        zoomOn( poly3D );
        
    //Fermer la polyligne
    poly3D->close();
}


AcDb3dPolyline* drawGetPolyline3D( std::vector<AcGePoint3d>& tab, AcCmColor& colorPoly, AcString layer, const bool& coli, const bool& hasToZoom )
{
    //Créer une entité
    AcDbEntity* ent = NULL;
    AcDbObjectId polyId;
    
    //Déclarer une polyligne 3D
    AcDb3dPolyline* poly3D = new AcDb3dPolyline();
    
    //Ajouter la polyligne au modelspace
    addToModelSpace( poly3D, polyId );
    
    //Ouvrir l'entité
    Acad::ErrorStatus er = acdbOpenAcDbEntity( ent, polyId, AcDb::kForWrite );
    
    if( er != eWasOpenForWrite )
        return poly3D;
        
    //Créer un vertex et un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectId newVertexId;
    
    //Taille de la polyligne
    int size = tab.size();
    
    //Ouvrir la polyligne 3D en ecriture
    if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
    {
        //Boucle sur les sommets de la polyligne
        for( long counter = 0; counter < size; counter ++ )
        {
            // Vertex de la polyligne à ajouter
            AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( tab[counter] );
            
            //Insertion des sommets
            if( newVertex )
            {
                poly3D->insertVertexAt( newVertexId, newVertex );
                newVertex->close();
            }
            
            //Le cas echeant on supprime le vertex
            else
                delete newVertex;
        }
    }
    
    //Fermer le vertex
    if( vertex )
        vertex->close();
        
    //Nettoyer la polyligne si c'est demandé
    if( coli )
        int k = coliPoly( poly3D, 0.1 );
        
    //Changer la couleur
    poly3D->setColor( colorPoly );
    
    //Creer le layer
    createLayer( layer );
    
    //Ajouter la polyligne dans la calque trajectoire
    poly3D->setLayer( layer );
    
    AcDbEntity* entity = AcDbEntity::cast( poly3D );
    
    redrawEntity( entity );
    
    //Zoomer sur l'objet
    if( hasToZoom )
        zoomOn( poly3D );
        
    //Fermer la polyligne
    //poly3D->close();
    
    //Retourner la polyligne
    return poly3D;
}



AcDb3dPolyline* drawGetPolyline3D( AcGePoint3dArray& tab,
    const AcCmColor& colorPoly,
    const AcString& layer,
    const bool& coli,
    const bool& hasToZoom )
{
    //Créer une entité
    AcDbEntity* ent = NULL;
    AcDbObjectId polyId;
    
    //Déclarer une polyligne 3D
    AcDb3dPolyline* poly3D = new AcDb3dPolyline();
    
    //Ajouter la polyligne au modelspace
    addToModelSpace( poly3D, polyId );
    
    //Ouvrir l'entité
    Acad::ErrorStatus er = acdbOpenAcDbEntity( ent, polyId, AcDb::kForWrite );
    
    if( er != eWasOpenForWrite )
        return poly3D;
        
    //Créer un vertex et un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectId newVertexId;
    
    //Taille de la polyligne
    int size = tab.size();
    
    //Ouvrir la polyligne 3D en ecriture
    if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) )
    {
        //Boucle sur les sommets de la polyligne
        for( long counter = 0; counter < size; counter ++ )
        {
            // Vertex de la polyligne à ajouter
            AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( tab[counter] );
            
            //Insertion des sommets
            if( newVertex )
            {
                poly3D->insertVertexAt( newVertexId, newVertex );
                newVertex->close();
            }
            
            //Le cas echeant on supprime le vertex
            else
                delete newVertex;
        }
    }
    
    //Fermer le vertex
    if( vertex )
        vertex->close();
        
    //Nettoyer la polyligne si c'est demandé
    if( coli )
        int k = coliPoly( poly3D, 0.1 );
        
    //Changer la couleur
    poly3D->setColor( colorPoly );
    
    //Creer le layer
    createLayer( layer );
    
    //Ajouter la polyligne dans la calque trajectoire
    poly3D->setLayer( layer );
    
    AcDbEntity* entity = AcDbEntity::cast( poly3D );
    
    redrawEntity( entity );
    
    //Zoomer sur l'objet
    if( hasToZoom )
        zoomOn( poly3D );
        
    //Fermer la polyligne
    poly3D->close();
    
    //Retourner la polyligne
    return poly3D;
}



Acad::ErrorStatus zoomOn( AcDb3dPolyline* polyline3D,
    const double& height )
{
    return zoomOnEntity( polyline3D, height );
}


Acad::ErrorStatus projectPolyOnZBlock( AcDb3dPolyline* poly3d, std::vector<AcGePoint3d>& vectPoint, const double& tol )
{
    //Initialisation du resultat
    Acad::ErrorStatus result = Acad::eNotApplicable;
    
    //Tester si le vecteur en entrer est valide
    if( vectPoint.size() == 0 )
    {
        //Afficher un message
        print( "Vecteur non valide" );
        
        //Retourner le resultat
        return result;
    }
    
    //Déclarer les vertex et creer un iterateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3d->vertexIterator();
    
    //Déclarer le point3d du vertex
    AcGePoint3d pt3d = AcGePoint3d::kOrigin;
    
    //Réinitialiser l'itérateur
    iterPoly3D->start();
    
    //Ouvrir le premier sommet
    if( Acad::eOk != poly3d->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) ) return result;
    
    //Récupérer le coordonnés du premier vertex
    AcGePoint3d firstPoint = vertex->position();
    
    //Tester si le premier vertex est le même qu'un point d'insertion du bloc
    for( int i = 0; i < vectPoint.size(); i++ )
    {
        if( isEqual2d( getPoint2d( vectPoint[i] ), getPoint2d( firstPoint ), tol ) )
        {
            //Changer le z du vertex
            vertex->setPosition( AcGePoint3d( firstPoint.x, firstPoint.y, vectPoint[i].z ) );
        }
    }
    
    //Fermer le vertex
    vertex->close();
    
    //Initialiser un index
    int index = 0;
    
    //Initialiser un compteur
    int compt = 0;
    
    //Boucle pour ajouter des sommets à une polyligne qui coupe un bloc
    for( ; !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Iterer l'index
        index++;
        
        //Ouvrir le vertex
        if( Acad::eOk != poly3d->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) ) return result;
        
        //Récupérer les coordonnées du vertex
        AcGePoint3d nextPoint = vertex->position();
        
        //Boucle sur le vecteur de point d'insertion
        for( int j = 0; j < vectPoint.size(); j++ )
        {
            //Tester si les trois points sont colinéaires avec une certaine tolerance
            if( isPointOnLine( getPoint2d( firstPoint ), getPoint2d( nextPoint ), getPoint2d( vectPoint[j] ), tol ) )
            {
                //Tester le compteur
                if( compt > 0 )
                    index++;
                    
                //Iterer le compteur
                compt++;
                
                //Insérer le point dans la polyligne
                if( !insertPointAt( poly3d, index - 1, vectPoint[j] ) == Acad::eOk )
                {
                    //Afficher un message
                    acutPrintf( _T( "\nErreur lors de l'ajout du vertex" ) );
                }
            }
        }
        
        //Boucle sur le vecteur de point d'insertion
        for( int k = 0; k < vectPoint.size(); k++ )
        {
            //Tester si un vertex est sur le meme point qu'un point d'insertion
            if( isEqual2d( nextPoint, vectPoint[k], tol ) )
                vertex->setPosition( AcGePoint3d( nextPoint.x, nextPoint.y, vectPoint[k].z ) );
        }
        
        //Fermer le vertex
        vertex->close();
        
        //Echanger les points
        firstPoint = nextPoint;
    }
    
    //Retourner le resultat
    return Acad::eOk;
}


int projectPolyOnBlock( AcDb3dPolyline*& poly3d, std::vector<AcGePoint3d>& vectPoint, const double& tol )
{
    //Initialisation du resultat
    int result = 0;
    
    //Tester si le vecteur en entrer est valide
    if( vectPoint.size() == 0 )
    {
        //Afficher un message
        print( "Vecteur non valide" );
        
        //Retourner le resultat
        return result;
    }
    
    //Recuperer la taille du vecteur
    long taille = vectPoint.size();
    
    //Boucle sur le vecteur de point
    for( long j = 0; j < taille ; j++ )
    {
        //Tester si le point est sur la polyligne
        if( isPointInsidePolyBox( poly3d, vectPoint[j] ) )
        {
            //Déclarer les vertex et creer deux itérateurs sur les sommets de la polyligne 3D
            AcDb3dPolylineVertex* vertex1 = NULL;
            AcDb3dPolylineVertex* vertex2 = NULL;
            AcDbObjectIterator* iter1 = poly3d->vertexIterator();
            AcDbObjectIterator* iter2 = poly3d->vertexIterator();
            
            //Declaration d'un vertex
            AcDb3dPolylineVertex* ver;
            
            //Declaration des deux points à tester
            AcGePoint3d lastPoint = AcGePoint3d::kOrigin;
            AcGePoint3d nextPoint = AcGePoint3d::kOrigin;
            
            //Initialisation des iterateurs
            iter2->start();
            iter2->step();
            iter1->start();
            
            //Boucle sur la polyligne
            for( ; !iter2->done(); iter2->step() )
            {
                //Ouvrir le premier sommet en lecture
                if( Acad::eOk != poly3d->openVertex( vertex1, iter1->objectId(), AcDb::kForRead ) ) return result;
                
                //Tester que le vertex est valide
                if( vertex1 == NULL )
                    return 0;
                    
                //Recuperer le dernier point
                lastPoint = vertex1->position();
                
                //Ouvrir le second sommet en lecture
                if( Acad::eOk != poly3d->openVertex( vertex2, iter2->objectId(), AcDb::kForRead ) ) return result;
                
                //Recuperer le ppoint suivant
                nextPoint = vertex2->position();
                
                //Recuperer la taille du vecteur
                long taille = vectPoint.size();
                
                //Boucle sur le vecteur de point d'insertion
                for( long j = 0; j < taille; j++ )
                {
                    if( isPointOnLine( getPoint2d( lastPoint ), getPoint2d( nextPoint ), getPoint2d( vectPoint[j] ), tol ) )
                    {
                        //Récupérer la distance entre les deux points et le point central
                        double d1 = getDistance2d( lastPoint, vectPoint[j] );
                        double d2 = getDistance2d( nextPoint, vectPoint[j] );
                        
                        //Recuperer le z moyen
                        double moy = ( nextPoint.z * d1 + lastPoint.z * d2 ) / ( d1 + d2 );
                        
                        //Tester si le z du bloc est sur le z moyen des deux points
                        if( isTheSame( vectPoint[j].z, moy, tol ) )
                        {
                            //Créer un vertex avec le point
                            ver = new AcDb3dPolylineVertex( vectPoint[j] );
                            
                            //Récupérer l'objectId du vertex d'avant
                            AcDbObjectId idLast = vertex1->id();
                            AcDbObjectId idNext = vertex2->id();
                            
                            //Ajouter le vertex
                            poly3d->insertVertexAt( idNext, idLast, ver );
                            
                            //Fermer le pointeur
                            ver->close();
                            
                            //Compter le sommet ajouté
                            result++;
                            
                            //Stepper l'iterateur 1
                            iter1->step();
                        }
                    }
                }
                
                //Fermer les deux vertex
                vertex1->close();
                vertex2->close();
                
                //Changer les deux points
                lastPoint = nextPoint;
                
                //Stepper l'iterateur 1
                iter1->step();
            }
        }
    }
    
    //Retourner le resultat
    return result;
}



int projectPolyOnBlock( AcDb3dPolyline* poly3d, const AcGePoint3d& point3d, const double& tol )
{
    //Initialisation du resultat
    int result = 0;
    
    //Déclarer les vertex et creer deux itérateurs sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex1 = NULL;
    AcDb3dPolylineVertex* vertex2 = NULL;
    AcDbObjectIterator* iter1 = poly3d->vertexIterator();
    AcDbObjectIterator* iter2 = poly3d->vertexIterator();
    
    //Declaration d'un vertex
    AcDb3dPolylineVertex* ver;
    
    //Declaration des deux points à tester
    AcGePoint3d lastPoint = AcGePoint3d::kOrigin;
    AcGePoint3d nextPoint = AcGePoint3d::kOrigin;
    
    //Initialisation des iterateurs
    iter2->start();
    iter2->step();
    iter1->start();
    
    //Boucle sur la polyligne
    for( ; !iter2->done(); iter2->step() )
    {
        //Ouvrir le premier sommet en lecture
        if( Acad::eOk != poly3d->openVertex( vertex1, iter1->objectId(), AcDb::kForRead ) ) return result;
        
        //Tester que le vertex est valide
        if( vertex1 == NULL )
            return 0;
            
        //Recuperer le dernier point
        lastPoint = vertex1->position();
        
        //Ouvrir le second sommet en lecture
        if( Acad::eOk != poly3d->openVertex( vertex2, iter2->objectId(), AcDb::kForRead ) ) return result;
        
        //Recuperer le ppoint suivant
        nextPoint = vertex2->position();
        
        if( isPointOnLine( getPoint2d( lastPoint ), getPoint2d( nextPoint ), getPoint2d( point3d ), tol ) )
        {
            //Récupérer la distance entre les deux points et le point central
            double d1 = getDistance2d( lastPoint, point3d );
            double d2 = getDistance2d( nextPoint, point3d );
            
            //Recuperer le z moyen
            double moy = ( nextPoint.z * d1 + lastPoint.z * d2 ) / ( d1 + d2 );
            
            //Tester si le z du bloc est sur le z moyen des deux points
            if( isTheSame( point3d.z, moy, tol ) )
            {
                //Créer un vertex avec le point
                ver = new AcDb3dPolylineVertex( point3d );
                
                //Récupérer l'objectId du vertex d'avant
                AcDbObjectId idLast = vertex1->id();
                AcDbObjectId idNext = vertex2->id();
                
                //Ajouter le vertex
                poly3d->insertVertexAt( idNext, idLast, ver );
                
                //Fermer le pointeur
                ver->close();
                
                //Compter le sommet ajouté
                result++;
                
                //Stepper l'iterateur 1
                iter1->step();
            }
            
        }
        
        //Fermer les deux vertex
        vertex1->close();
        vertex2->close();
        
        //Changer les deux points
        lastPoint = nextPoint;
        
        //Stepper l'iterateur 1
        iter1->step();
        
        //Retourner le resultat
        return result;
    }
}


Acad::ErrorStatus setPolyToTrigonomicSense( AcDb3dPolyline*& poly )
{
    bool isTrigo = false;
    Acad::ErrorStatus err = isPolyTrigonomicSense( poly, isTrigo );
    
    if( err != Acad::eOk )
        return err;
        
    // Si A est a gauche de B la poly est dans le sens horaire
    // On doit inverser son sens
    // Sinon, ekke est déjà dans le bon sens, il n'y a rien à faire
    if( !isTrigo )
        poly->reverseCurve();
        
    return Acad::eOk;
}


Acad::ErrorStatus isPolyTrigonomicSense( AcDb3dPolyline*& poly, bool& res )
{
    // Retourne une erreur si la poly est ouverte
    if( !poly->isClosed() )
    {
        print( "Polyligne ouverte" );
        return Acad::eNotApplicable;
    }
    
    // On récupère le nombre de sommets de la polyligne
    int size = getNumberOfVertex( poly );
    
    AcGePoint3d ptA, ptB, ptC, pt;
    int indexA, indexB, indexC;
    
    // on initialise B avec le 1er point de la poly
    if( getPointAt( poly, 0, ptB ) != Acad::eOk )
    {
        print( "Problème de récupération de vertex de polyligne 3d" );
        return Acad::eNotApplicable;
    }
    
    indexB = 0;
    
    // on recherche le point le plus haut
    for( int i = 0; i < size; i++ )
    {
        // On récupère le sommet i
        if( getPointAt( poly, i, pt ) != Acad::eOk )
        {
            print( "Problème de récupération de vertex de polyligne 3d" );
            return Acad::eNotApplicable;
        }
        
        if( pt.y > ptB.y )
        {
            ptB = pt;
            indexB = i;
        }
    }
    
    // On gère les extrêmités
    indexA = indexB - 1;
    indexC = indexB + 1;
    
    if( indexB == 0 )
    {
        indexA = size - 1;
        indexC = 1;
    }
    
    if( indexB == size - 1 )
    {
        indexA = size - 2;
        indexC = 0;
    }
    
    // on definit les vecteurs unitaires BA et BC
    if( getPointAt( poly, indexA, ptA ) != Acad::eOk ||
        getPointAt( poly, indexC, ptC ) != Acad::eOk )
    {
        print( "Problème de récupération de vertex de polyligne 3d" );
        return Acad::eNotApplicable;
    }
    
    AcGeVector2d vecBA = AcGeVector2d( ptA.x - ptB.x, ptA.y - ptB.y ).normal();
    AcGeVector2d vecBC = AcGeVector2d( ptC.x - ptB.x, ptC.y - ptB.y ).normal();
    
    // Si A est a gauche de B la poly est dans le sens horaire
    // On doit inverser son sens
    // Sinon, ekke est déjà dans le bon sens, il n'y a rien à faire
    if( vecBA.x < vecBC.x )
        res = false;
    else
        res = true;
        
    return Acad::eOk;
}


bool isPointInsidePolyBox( AcDb3dPolyline*& poly3D, const AcGePoint3d& ptSearch )
{
    //Declarer un AcDbExtents pour recuperer le bounding box de la polyligne
    AcDbExtents polyBox;
    
    //Recuperer le bounding box de la polyligne
    poly3D->getGeomExtents( polyBox );
    AcGePoint3d leftBottomPoint = polyBox.minPoint();
    AcGePoint3d rightTopPoint = polyBox.maxPoint();
    
    //Tester si le point est dans le bounding box de la polyligne
    if( ptSearch.x >= leftBottomPoint.x && ptSearch.x <= rightTopPoint.x
        && ptSearch.y >= leftBottomPoint.y && ptSearch.y <= rightTopPoint.y )
        //&& ptSearch.z >= leftBottomPoint.z && ptSearch.z <= rightTopPoint.z )
        return true;
    else
        return false;
}


Acad::ErrorStatus isPointOnPoly( AcDb3dPolyline*& poly,
    const AcGePoint3d& pt,
    bool& isOnPoly,
    const double& precision )
{

    AcGePoint3d pointOnCurve = AcGePoint3d::kOrigin;
    
    Acad::ErrorStatus es = poly->getClosestPointTo( pt,
            pointOnCurve );
            
    if( es != Acad::eOk )
    {
        isOnPoly = false;
        return es;
    }
    
    if( pt.distanceTo( pointOnCurve ) < precision )
        isOnPoly = true;
    else
        isOnPoly = false;
        
    return Acad::eOk;
}


Acad::ErrorStatus isPointOnPoly2D( AcDb3dPolyline*& poly,
    const AcGePoint3d& pt,
    bool& isOnPoly,
    const double& precision )
{
    AcGePoint3d pointOnCurve = AcGePoint3d::kOrigin;
    
    Acad::ErrorStatus es = poly->getClosestPointTo( pt,
            pointOnCurve );
            
    if( es != Acad::eOk )
    {
        isOnPoly = false;
        return es;
    }
    
    //conversion en 2D
    AcGePoint3d pt2D = AcGePoint3d( pt.x, pt.y, 0 );
    
    AcGePoint3d pointOnCurve2D = AcGePoint3d( pointOnCurve.x, pointOnCurve.y, 0 );
    
    if( pt2D.distanceTo( pointOnCurve2D ) < precision )
        isOnPoly = true;
    else
        isOnPoly = false;
        
    return Acad::eOk;
}


std::vector<int> getSuperposedIndex( AcDb3dPolyline* poly3d,
    const double& precision )
{
    //Vecteur resultat
    std::vector<int> result;
    
    std::vector<AcGePoint3d> sommet;
    
    //Déclarer les vertex et creer un iterateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3d->vertexIterator();
    
    //Reéinitialiser l'itérateur
    iterPoly3D->start();
    
    //Boucle sur la polyligne
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Ouvrir le premier sommet de la polyligne
        if( Acad::eOk != poly3d->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) ) return result;
        
        //Récupérer les coordonnées du vertex en cours
        AcGePoint3d currentVertex = vertex->position();
        
        //Ajouter le point dans le sommet
        sommet.push_back( currentVertex );
        
        //Fermer le vertex
        vertex->close();
    }
    
    //Taille du vecteur
    long taille = sommet.size();
    
    //Boucle pour tester le nombre d'occurence des points dans le vecteur
    for( long i = 0; i < taille - 1; i++ )
    {
        for( long j = i + 1; j < taille; j ++ )
        {
            if( isEqual3d( sommet[i], sommet[j], precision ) )
            {
                result.push_back( i );
                result.push_back( j );
            }
            
        }
    }
    
    //Retourner le resutlat
    return result;
}


Acad::ErrorStatus getVertexesPoly(
    AcDb3dPolyline*&  poly,
    AcGePoint3dArray& arPtVtx,
    vector< double >& xPosOther )
{
    Acad::ErrorStatus es = eNotApplicable;
    
    if( !poly )
        return es;
        
    //Rénitialisation
    arPtVtx.removeAll();
    arPtVtx.setPhysicalLength( 0 );
    
    xPosOther.clear();
    
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly->vertexIterator();
    
    //Initialiser l'itérateur
    iterPoly3D->start();
    
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Ouvrir le premier sommet de la polyligne
        if( Acad::eOk != poly->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) ) return es;
        
        //Récupérer les coordonnées du vertex en cours
        AcGePoint3d curVtxPt = vertex->position();
        
        //Ajouter le point dans le tableau de sommets
        arPtVtx.append( curVtxPt );
        
        //Ajouter le point.x dans le vecteur
        xPosOther.push_back( curVtxPt.x );
        
        //Fermer le vertex
        vertex->close();
        vertex = NULL;
    }
    
    delete iterPoly3D;
    
    es = eOk;
    return es;
}


Acad::ErrorStatus
openPoly3d( const AcDbObjectId& idPoly,
    AcDb3dPolyline*& poly3d,
    const AcDb::OpenMode& mode )
{
    Acad::ErrorStatus es;
    AcDbEntity* pEnt;
    
    // On ouvre l'entité
    es = acdbOpenAcDbEntity(
            pEnt,
            idPoly,
            mode );
            
            
    //Si la poly
    // Retour s'il y a une erreur
    if( es != Acad::eOk )
        return es;
        
    // On cast en cercle
    poly3d = AcDb3dPolyline::cast( pEnt );
    
    return Acad::eOk;
}


bool isColiPoly( AcDb3dPolyline* poly3D,
    AcGePoint3d& ptPoly,
    const double& tol )
{
    //Resultat
    bool res = true;
    
    //Recuperer le premier point de la polyligne
    AcGePoint3d ptFirst = AcGePoint3d::kOrigin;
    poly3D->getStartPoint( ptFirst );
    
    //Recuperer le second point de la polyligne
    AcGePoint3d ptLast = AcGePoint3d::kOrigin;
    poly3D->getEndPoint( ptLast );
    
    //Declarer les vertex et creer un iterateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Initialiser l'iterateur sur la polyligne
    iterPoly3D->start();
    iterPoly3D->step();
    
    //Boucle sur les polylignes
    for( ; !iterPoly3D->done() - 1; iterPoly3D->step() )
    {
        //Ouvrir le sommet de la polyligne
        if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return false;
        
        //Recuperer le premier sommet
        AcGePoint3d pt = vertex->position();
        
        //Fermer le vertex
        vertex->close();
        
        //Tester si les 3 points sont colineaires
        if( !isPointOnLine( getPoint2d( ptFirst ), getPoint2d( ptLast ), getPoint2d( pt ), 0.01 ) )
        {
            //Supprimer l'iterateur sur la polyligne
            delete iterPoly3D;
            
            //Recuperer le point
            ptPoly = pt;
            
            return false;
        }
    }
    
    //Supprimer l'iterateur sur la polyligne
    delete iterPoly3D;
    
    //Sinon retourner true
    return true;
}


Acad::ErrorStatus getDistVec( AcDb3dPolyline*& poly3d,
    vector<double>& distVec )
{
    distVec.clear();
    
    //Declaration du resultat
    Acad::ErrorStatus es = Acad::eNotApplicable;
    
    //Déclarer les vertex et creer deux itérateurs sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex;
    
    //Boucle sur la polyligne
    AcDbObjectIterator* iter = poly3d->vertexIterator();
    
    iter->start();
    iter->step();
    
    for( ; !iter->done(); iter->step() )
    {
        //Ouvrir le sommet courant en lecture
        es = poly3d->openVertex( vertex,
                iter->objectId(),
                AcDb::kForRead );
                
        if( Acad::eOk != es )
            return es;
            
        //Tester que le vertex est valide
        if( vertex == NULL )
            return es;
            
        double dist = 0;
        
        // Récupérer la distance curviligne entre le sommet courant
        // et le début de la polyligne
        es = poly3d->getDistAtPoint( vertex->position(),
                dist );
                
        if( Acad::eOk != es )
            return es;
            
        // Stocker la distance curviligne
        distVec.push_back( dist );
        
        // On n'oublie pas de fermer le vertex
        vertex->close();
    }
    
    delete iter;
    
    return Acad::eOk;
}



Acad::ErrorStatus getCurvDistPoly(
    const AcDbObjectIdArray&   arPolyIds, vector< vector< double >>& allVectDir )
{
    int size = arPolyIds.size();
    
    if( size == 0 )
        return eOk;
        
    Acad::ErrorStatus es = eNotApplicable;
    
    //On boulce sur les polylignes
    for( int counter = 0; counter < size; counter++ )
    {
        //Récupérer la polyligne
        AcDbObjectId id = arPolyIds[counter];
        
        AcDb3dPolyline* poly3d;
        vector< double > vectDir;
        
        ErrorStatus es = openPoly3d( id, poly3d ) ;
        
        if( es == eOk )
        {
            //Recuperer le vecteur de distance curviligne du -ieme polyligne
            es = getDistVec( poly3d, vectDir );
            
            poly3d->close();
            poly3d = NULL;
            
            
            //Tester le resultat
            if( es != Acad::eOk )
            {
                print( "Problme lors de la cration du vecteur de distances curvilignes de la polyligne" );
                return es;
            }
            
            // Ajouter le vecteur de distance curviligne au vecteur global de distances curviligne
            allVectDir.push_back( vectDir );
        }
        
    }
    
    return eOk;
}


Acad::ErrorStatus appendVertexToList( AcDb3dPolyline*& poly,
    AcGePoint3dArray& pos,
    vector< double >& xPos )
{

    // Itérateur sur les vertex
    AcDbObjectIterator* iterPoly = poly->vertexIterator();
    
    // On boucle sur les sommets de la polyligne
    AcDb3dPolylineVertex* vertex;
    Acad::ErrorStatus es;
    
    for( iterPoly->start(); !iterPoly->done(); iterPoly->step() )
    {
    
        es = poly->openVertex( vertex,
                iterPoly->objectId(),
                AcDb::kForRead );
                
        if( es != Acad::eOk )
        {
            print( es );
            return es;
        }
        
        // On regarde si le vertex est dans la liste en 2d
        AcGePoint3d vertexPos = vertex->position();
        pos.append( vertexPos );
        xPos.push_back( vertexPos.x );
        
        vertex->close();
    }
    
    return es;
}


Acad::ErrorStatus getSurroundingVertexes( AcDb3dPolyline*& poly3D,
    const AcGePoint3d& onePt3D,
    long& prevIdxRes,
    long& nextIdxRes,
    const double& tol )
{

    Acad::ErrorStatus errorStatus = Acad::eKeyNotFound;
    
    // Vérification de la polyline
    if( !poly3D )
        return Acad::eNullHandle;
        
    // Créer un itérateur pour les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDb3dPolylineVertex* indexVertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDbObjectIterator* iterPoly3DNext = poly3D->vertexIterator();
    AcGePoint3d ptCurrentVertexCoord;
    AcGePoint3d ptNextVertexCoord;
    
    long counter;
    
    bool Found = false;
    // Variable pour stocker les ids du premier et dernier sommet de la poly si celle la est fermée
    AcDbObjectId firstVertexId;
    AcDbObjectId lastVertexId;
    
    bool isClosed = poly3D->isClosed();
    
    if( isClosed )
    {
        // On sauvegarde le premier ID
        iterPoly3D->start();
        firstVertexId = iterPoly3D->objectId();
        
        // On sauvegarde le dernier ID
        iterPoly3D->start( true );
        lastVertexId = iterPoly3D->objectId();
    }
    
    // On remet l'itérator dans le bon sens par précaution
    iterPoly3D->start();
    
    // initialisation du compteur
    counter = 0;
    
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        // Récupérer le sommet courant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex ->close();
            vertex = NULL;
        }
        
        else
        {
            vertex = NULL;
            return eNullHandle;
        }
        
        if( !iterPoly3DNext->done() )
            iterPoly3DNext->step();
        else
            break;
            
        // Récupérer le sommet suivant
        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
        {
            if( vertex )
            {
                ptNextVertexCoord = vertex->position();
                vertex->close();
                vertex = NULL;
            }
            
            else
            {
                vertex = NULL;
                
                if( !isClosed )
                    return Acad::eOk;
                else
                    break;
            }
            
        }
        
        // Vérifier si le point d'intersection se trouve entre les deux sommets
        if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                ptNextVertexCoord,
                onePt3D,
                tol,
                10000.0,
                true ) )
        {
            errorStatus =  Acad::eOk;
            Found = true;
            prevIdxRes = counter;
            nextIdxRes = prevIdxRes + 1;
            break;
        }
        
        // Vérifier si le point d'intersection se trouve entre les deux sommets
        
        //si pas trouvé On incrémente
        counter ++;
    }
    
    // Si la polyline est fermé alors on vérifie une intersection se trouve entre le dernie et le premier sommet
    if( isClosed && ( errorStatus !=  Acad::eOk ) && ( !Found ) )
    {
        // Pointer le second itérator sur le début
        iterPoly3DNext->start();
        
        // Récupérer les coordonnées des vertex associés
        if( Acad::eOk == poly3D->openVertex( vertex, firstVertexId, AcDb::kForRead ) )
        {
            ptCurrentVertexCoord = vertex->position();
            vertex->close();
            vertex = NULL;
        }
        
        if( Acad::eOk == poly3D->openVertex( vertex, lastVertexId, AcDb::kForRead ) )
        {
            ptNextVertexCoord = vertex->position();
            vertex->close();
            vertex = NULL;
        }
        
        // Vérifier si une des intersections se trouve entre les deux sommets
        if( isPointOnLineAndInsidesTwoPoints( ptCurrentVertexCoord,
                ptNextVertexCoord,
                onePt3D,
                tol,
                10000.0,
                true ) )
        {
            prevIdxRes = -3;
            nextIdxRes = -2;
            errorStatus =  Acad::eOk;
            
        }
        
    }
    
    
    // Si la polyline est fermé alors on vérifie une intersection se trouve entre le dernie et le premier sommet
    
    return errorStatus;
}


//Acad::ErrorStatus getSurroundingVertexes( AcDb3dPolyline*& poly3D,
//    const AcGePoint3d& onePt3D,
//    AcDbObjectId& indexVertId,
//    const double& tol )
//{
//    Acad::ErrorStatus errorStatus = Acad::eKeyNotFound;
//
//    // Vérification de la polyline
//    if( !poly3D )
//        return Acad::eNullHandle;
//
//    // Creer un itérateur pour les sommets de la polyligne 3D
//    AcDb3dPolylineVertex* vertex = NULL;
//    AcDb3dPolylineVertex* indexVertex = NULL;
//    AcDbObjectId newVertexId;
//    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
//    AcDbObjectIterator* iterPoly3DNext = poly3D->vertexIterator();
//    AcGePoint3d ptCurrentVertexCoord;
//    AcGePoint3d ptNextVertexCoord;
//
//    // Variable pour stocker les ids du premier et dernier sommet de la poly si celle la est fermée
//    AcDbObjectId firstVertexId;
//    AcDbObjectId lastVertexId;
//
//    bool isClosed = poly3D->isClosed();
//
//    if( isClosed )
//    {
//        // On sauvegarde le premier ID
//        iterPoly3D->start();
//        firstVertexId = iterPoly3D->objectId();
//
//        // On sauvegarde le dernier ID
//        iterPoly3D->start( true );
//        lastVertexId = iterPoly3D->objectId();
//    }
//
//    // On remet l'itérator dans le bon sens par précaution
//    iterPoly3D->start();
//
//
//    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
//    {
//        // Récupérer le sommet courrant
//        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
//        {
//            ptCurrentVertexCoord = vertex->position();
//            vertex ->close();
//            vertex = NULL;
//        }
//
//        else
//        {
//            vertex = NULL;
//            return eNullHandle;
//        }
//
//        if( !iterPoly3DNext->done() )
//            iterPoly3DNext->step();
//        else
//            break;
//
//        // Récupérer le sommet suivant
//        if( Acad::eOk == poly3D->openVertex( vertex, iterPoly3DNext->objectId(), AcDb::kForRead ) )
//        {
//            if( vertex )
//            {
//                ptNextVertexCoord = vertex->position();
//                vertex->close();
//                vertex = NULL;
//            }
//
//            else
//            {
//                vertex = NULL;
//
//                if( !isClosed )
//                    return Acad::eOk;
//                else
//                    break;
//            }
//
//        }
//
//        // Vérifier si le point d'intersection se trouve entre les deux sommets
//        if( isPointOnLineAndInside( ptCurrentVertexCoord,
//                ptNextVertexCoord,
//                onePt3D,
//                tol ) )
//        {
//            indexVertId = iterPoly3D->objectId();
//            errorStatus =  Acad::eOk;
//            break;
//        }
//
//        // Vérifier si le point d'intersection se trouve entre les deux sommets
//    }
//
//    // Si la polyline est fermé alors on vérifie une intersection se trouve entre le dernie et le premier sommet
//    if( isClosed && ( errorStatus !=  Acad::eOk ) )
//    {
//
//        AcDbObjectId secondObjId;
//
//        // Pointer le second itérator sur le début
//        iterPoly3DNext->start();
//
//        // Récupérer les coordonnées des vertex associés
//        if( Acad::eOk == poly3D->openVertex( vertex, firstVertexId, AcDb::kForRead ) )
//        {
//            ptCurrentVertexCoord = vertex->position();
//            vertex->close();
//            vertex = NULL;
//        }
//
//        if( Acad::eOk == poly3D->openVertex( vertex, lastVertexId, AcDb::kForRead ) )
//        {
//            ptNextVertexCoord = vertex->position();
//            vertex->close();
//            vertex = NULL;
//        }
//
//        // Vérifier si une des intersections se trouve entre les deux sommets
//        if( isPointOnLineAndInside( ptCurrentVertexCoord,
//                ptNextVertexCoord,
//                onePt3D,
//                tol ) )
//        {
//
//
//
//            // Ajout du sommet
//            AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( onePt3D );
//
//            if( Acad::eOk == poly3D->openVertex( vertex, lastVertexId, AcDb::kForWrite ) )
//            {
//                if( vertex )
//                {
//                    poly3D->insertVertexAt( newVertexId, lastVertexId, newVertex );
//                    newVertex->close();
//                    vertex->close();
//                    vertex = NULL;
//                }
//
//                else
//                    delete newVertex;
//            }
//
//            else
//                delete newVertex;
//
//        }
//
//
//    }
//
//
//    // Si la polyline est fermé alors on vérifie une intersection se trouve entre le dernie et le premier sommet
//
//    return errorStatus;
//}



AcGePoint3d getClosestVert( const AcGePoint3d& point3d,
    AcGePoint3dArray& vectPoint,
    vector<double>& vectX,
    const double& toleranceXY,
    const double& toleranceZ )
{
    //Initialisation du resultat
    AcGePoint3d ptRes = AcGePoint3d( point3d );
    
    //Si le vecteur ne contient qu'un sommet
    if( vectPoint.size() == 1 )
    {
        //Retourner le resultat
        return ptRes;
    }
    
    //Recuperer les indexs des x semblables
    vector<int> indexs = dichotomToVec( vectX, ptRes.x, toleranceXY );
    
    //Taille de l'index
    int maxim = indexs[ indexs.size() - 1 ];
    int minim = indexs[0];
    
    //Boucle sur les elements du vecteur
    for( long i = minim; i <= maxim; i++ )
    {
        if( isEqual3d( point3d, vectPoint[i], 0.0000001 ) )
            continue;
            
        //Tester si le i-eme point du vecteur est un point voisin en X
        if( isTheSame( point3d.x, vectPoint[i].x, toleranceXY ) )
        {
            //Tester si le i-eme point du vecteur est un point voisin en Y
            if( isEqual2d( getPoint2d( point3d ), getPoint2d( vectPoint[i] ), toleranceXY ) )
            {
                //Tester si le i-eme point du vecteur est un point voisin en Z avec 3cm de tolerance
                if( isTheSame( point3d.z, vectPoint[i].z, toleranceZ ) )
                    return vectPoint[i];
                else
                    return AcGePoint3d( vectPoint[i].x, vectPoint[i].y, point3d.z );
            }
        }
    }
    
    //Retour
    return ptRes;
}



Acad::ErrorStatus getVertexesOfAllPoly( const ads_name& ssPoly,
    AcGePoint3dArray& arrayOfVertex,
    vector<double>& vectX,
    const bool& hasToUseExtremities,
    const bool& sort )
{
    //Declaration d'une polyligne
    AcDb3dPolyline* poly3D;
    
    //Recuperer le nombre dans la selection
    long lengthPoly = 0;
    acedSSLength( ssPoly, &lengthPoly );
    
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Recuperation des vertexs" ), lengthPoly );
    
    //Récupérer tous les sommets de polylignes dans un vecteur de AcGePoint3d
    for( int i = 0; i < lengthPoly; i++ )
    {
        //Récupérer le i-eme polyligne
        poly3D = getPoly3DFromSs( ssPoly, i, AcDb::kForRead );
        
        //Déclarer les vertex et creer un itérateur sur les sommets de la polyligne 3D
        AcDb3dPolylineVertex* vertex;
        AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
        
        //Boucle sur la polyligne
        int c = 0;
        int length = getNumberOfVertex( poly3D ) - 1;
        
        for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
        {
            if( !hasToUseExtremities )
                if( c == 0 || c == length )
                {
                    c++;
                    continue;
                }
                
            //Ouvrir le premier sommet de la polyligne
            if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return Acad::eNotApplicable;
            
            //Récupérer les coordonnes du vertex en cours
            AcGePoint3d currentVertex = vertex->position();
            
            //Fermer le vertex
            vertex->close();
            
            //Ajouter le point dans le vertex
            arrayOfVertex.push_back( currentVertex );
            
            //Ajouter le x du point dans le vecteur de double
            vectX.push_back( currentVertex.x );
            
            c++;
        }
        
        delete iterPoly3D;
        
        //Fermer la polyligne
        poly3D->close();
        
        //Progression de la barre
        prog.moveUp( i );
    }
    
    //Tester si l'utilisateur veut trier la liste
    if( sort )
    {
        //Recuperer la taille des vecteurs
        if( arrayOfVertex.size() == 0 || arrayOfVertex.size() != vectX.size() )
        {
            //Afficher message
            print( "ERREUR, Impossible de trier la liste, Sortie" );
            
            return Acad::eNotApplicable;
        }
        
        //Trier la liste
        sortList( arrayOfVertex, vectX );
    }
    
    //Retourner eOk
    return Acad::eOk;
}


Acad::ErrorStatus getVertexesOfAllPoly( const AcString& layerPoly,
    AcGePoint3dArray& arrayOfVertex,
    vector<double>& vectX,
    const bool& hasToUseExtremities,
    const bool& sort )
{
    //Declaration d'une polyligne
    AcDb3dPolyline* poly3D;
    
    //Recuperer tous les polylignes dans le calque
    ads_name ssAllPoly;
    long lengthPoly = getSsAllPoly3D( ssAllPoly, layerPoly );
    
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Recuperation des vertexs" ), lengthPoly );
    
    //Récupérer tous les sommets de polylignes dans un vecteur de AcGePoint3d
    for( int i = 0; i < lengthPoly; i++ )
    {
        //Récupérer le i-eme polyligne
        poly3D = getPoly3DFromSs( ssAllPoly, i, AcDb::kForRead );
        
        //Déclarer les vertex et creer un itérateur sur les sommets de la polyligne 3D
        AcDb3dPolylineVertex* vertex;
        AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
        
        //Boucle sur la polyligne
        int c = 0;
        int length = getNumberOfVertex( poly3D ) - 1;
        
        for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
        {
            if( !hasToUseExtremities )
                if( c == 0 || c == length )
                {
                    c++;
                    continue;
                }
                
            //Ouvrir le premier sommet de la polyligne
            if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) ) return Acad::eNotApplicable;
            
            //Récupérer les coordonnes du vertex en cours
            AcGePoint3d currentVertex = vertex->position();
            
            //Fermer le vertex
            vertex->close();
            
            //Ajouter le point dans le vertex
            arrayOfVertex.push_back( currentVertex );
            
            //Ajouter le x du point dans le vecteur
            vectX.push_back( currentVertex.x );
            
            c++;
        }
        
        delete iterPoly3D;
        
        //Fermer la polyligne
        poly3D->close();
        
        //Progression de la barre
        prog.moveUp( i );
    }
    
    //Tester si l'utilisateur veut trier la liste
    if( sort )
    {
        //Recuperer la taille des vecteurs
        if( arrayOfVertex.size() == 0 || arrayOfVertex.size() != vectX.size() )
        {
            //Afficher message
            print( "ERREUR, Impossible de trier la liste, Sortie" );
            
            return Acad::eNotApplicable;
        }
        
        //Trier la liste
        sortList( arrayOfVertex, vectX );
    }
    
    
    //Retourner eOk
    return Acad::eOk;
}



Acad::ErrorStatus getExtremitiesOfPoly( const ads_name& ssPoly,
    AcGePoint3dArray& arrayVertex,
    const bool& onlyBegin,
    const bool& onlyEnd )
{
    // On ne peut pas appeler la fonction avec les 2 booléens à true
    // Soit on veut que le début de la poly, soit on veut que la fin
    // Si on veut le début ET la fin, les 2 booléens doivent être à false
    if( onlyBegin && onlyEnd )
        return Acad::eNotApplicable;
        
    long size = 0;
    
    if( acedSSLength( ssPoly,
            &size ) != RTNORM || size == 0 )
        return Acad::eNotApplicable;
        
    // On boucle sur les poly3D ouvertes
    AcDb3dPolyline* poly;
    AcGePoint3d pt;
    
    for( long i = 0; i <  size; i++ )
    {
        poly = getPoly3DFromSs( ssPoly, i );
        
        if( !poly )
            continue;
            
        AcDbObjectIterator* iterPoly3D = poly->vertexIterator();
        AcDb3dPolylineVertex* vtx;
        
        // On veut que le début des polylignes
        if( onlyBegin ||
            ( !onlyBegin && !onlyEnd ) )
        {
        
            //On prend le premier vertex
            iterPoly3D->start();
            
            if( poly->openVertex( vtx, iterPoly3D->objectId(), AcDb::kForRead ) == eOk )
            {
                if( vtx )
                {
                    //On stoque le point aini que la valeur x du point
                    pt = vtx->position();
                    arrayVertex.append( pt );
                    vtx->close();
                }
                
                else
                {
                    poly->close();
                    delete iterPoly3D;
                    continue;
                }
            }
        }
        
        // On veut que la fin des polylignes
        if( onlyEnd ||
            ( !onlyBegin && !onlyEnd ) )
        {
        
            //On prend le dernier vertex
            iterPoly3D->start( true );
            
            if( poly->openVertex( vtx, iterPoly3D->objectId(), AcDb::kForRead ) == eOk )
            {
                if( vtx )
                {
                    //On stoque le point aini que la valeur x du point
                    pt = vtx->position();
                    arrayVertex.append( pt );
                    vtx->close();
                }
                
                else
                {
                    poly->close();
                    delete iterPoly3D;
                    continue;
                }
            }
        }
        
        poly->close();
        delete iterPoly3D;
    }
    
    return Acad::eOk;
}

Acad::ErrorStatus  getClosestVtx( const AcGePoint3d& point3d, AcDb3dPolyline*& poly3D, AcGePoint3d& ptRes, AcDbObjectIterator*& iterVtxPoly3d )
{
    //Pas besoin de check de la polygne car c'est passé par référence met on le fait juste pour rappel des bonnes pratiques
    if( !poly3D )
        return eNullEntityPointer;
        
    /// 1 On récupère un itérateur sur les vertew
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    AcDb3dPolylineVertex* vtx;
    
    double distBu = 1000000000;
    
    /// 2 On parcours les vertex
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Ouvrir le premier sommet de la polyligne
        if( Acad::eOk != poly3D->openVertex( vtx, iterPoly3D->objectId(), AcDb::kForRead ) ) return Acad::eNotApplicable;
        
        AcGePoint3d curPt = vtx->position();
        double dist = point3d.distanceTo( curPt );
        
        ///On compare la distance du point et le sommet courant; si plus petite que la sauvegarde on garde si supérieur on arrete
        if( dist < distBu )
        {
            distBu = dist;
            ptRes = curPt;
            iterVtxPoly3d = iterPoly3D;
            vtx->close();
        }
        
        else
        {
            vtx->close();
            break;
        }
        
    }
    
    return eOk;
    
}


void closePoly3dPtr( vector< AcDb3dPolyline* >& vectPoly )
{
    int size = vectPoly.size();
    
    sort( vectPoly.begin(),
        vectPoly.end() );
        
    vectPoly.erase( unique( vectPoly.begin(),
            vectPoly.end() ) );
            
    for( int i = 0; i < size; i++ )
    {
        if( vectPoly[ i ] )
            vectPoly[ i ]->close();
    }
}


Acad::ErrorStatus addVertexOnClickedPoint( AcDb3dPolyline* poly3D,
    const AcGePoint3d& ptPoly )
{
    //Resultat
    Acad::ErrorStatus res = Acad::eNotApplicable;
    
    //Declarer les vertex et creer un iterateur sur les sommets de la polyligne 3D
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
    
    //Initialiser l'iterateur
    iterPoly3D->start();
    
    //Ouvrir le premier sommet de la polyligne
    if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) );
    
    //Recuperer le point
    AcGePoint3d firstPoint = vertex->position();
    
    //Recuperer l'objectId du vertex 1
    AcDbObjectId vertId1 = vertex->id();
    
    //Fermer le vertex
    vertex->close();
    
    //Stepper l'iterateur
    iterPoly3D->step();
    
    //Boucle sur les sommets de la polyligne
    for( ; !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Ouvrir le sommet de la polyligne
        if( Acad::eOk != poly3D->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForWrite ) ) return res;
        
        //Recuperer les coordonnées du vertex en cours
        AcGePoint3d secondPoint = vertex->position();
        
        //Recuperer l'objectId du vertex 2
        AcDbObjectId vertId2 = vertex->id();
        
        //Tester si les trois points sont colineaire
        if( isPointOnLine( getPoint2d( firstPoint ), getPoint2d( secondPoint ), getPoint2d( ptPoly ), 0.01 ) )
        {
            //AcDbObjectId du nouveau vertex
            AcDbObjectId idNewVert;
            
            //Nouveau vertex
            AcDb3dPolylineVertex* newVertex = new AcDb3dPolylineVertex( ptPoly );
            
            //Ajouter le nouveau sommet sur la polyligne
            poly3D->insertVertexAt( idNewVert, vertId1, newVertex );
            
            //Fermer le vertex
            vertex->close();
            
            //Fermer le nouveau vertex
            newVertex->close();
            
            //Retourner eOk
            return Acad::eOk;
        }
        
        //Changer les vertex et leurs ids
        firstPoint = AcGePoint3d( secondPoint );
        vertId1 = vertId2;
        
        //Fermer le vertex
        vertex->close();
    }
    
    return res;
}


Acad::ErrorStatus preparePoly3d( std::vector<double>& vectX,
    AcGePoint3dArray& vectPoint3d,
    std::vector<AcDbObjectId>& vectObjId,
    std::vector<AcDb3dPolyline*>& vectPolyPointer )
{
    //Taille du vecteur d'abscisse
    int taille = vectX.size();
    
    //TEST
    int taille2 = vectPoint3d.size();
    
    //Barre de progression
    ProgressBar prog = ProgressBar( _T( "Préparation des polylignes" ), taille );
    
    //Declaration de min
    int min = 0;
    
    //Boucle sur le vecteur d'abscisse
    for( int i = 0; i < taille - 1; i++ )
    {
        //Recuperer l'index du min
        min = i;
        
        //Boucle de test
        for( int j = i + 1; j < taille; j++ )
        {
            if( vectX[j] < vectX[min] )
                min = j;
        }
        
        //Swapper le vecteur, le AcArray et le map
        if( min != i )
        {
            std::swap( vectX[ i ], vectX[ min ] );
            vectPoint3d.swap( i, min );
            std::swap( vectObjId[i], vectObjId[min] );
            std::swap( vectPolyPointer[i], vectPolyPointer[min] );
        }
        
        //Progresser la barre de progression
        prog.moveUp( i );
    }
    
    //Retourner le resultat
    return Acad::eOk;
}


bool isVertexOfPoly( AcDb3dPolyline* poly3D,
    const AcGePoint3d& pt,
    const double& tol )
{
    //Array de point 3d
    AcGePoint3dArray ptVert;
    
    //Vecteur de double
    vector<double> xPos;
    
    //Recuperer les sommets de la polyligne
    Acad::ErrorStatus er = getVertexesPoly( poly3D, ptVert, xPos );
    
    //Test que le acArray est valide
    if( ptVert.size() == 0 )
        return false;
        
    //Trier le acArray
    er = sortList( ptVert, xPos );
    
    //Tester que le tri est ok
    if( er != Acad::eOk )
    {
        //Afficher message
        print( "Impossible de trier le tableau de point 3d" );
        
        return false;
    }
    
    //Chercher si le point est un vertex
    AcGePoint3dArray ptNeigh;
    
    //Chercher si c'est un vertex
    if( isInList( pt, xPos, ptVert, ptNeigh, tol ) )
        return true;
    else
        return false;
}


Acad::ErrorStatus getClosestPolyToPoint( const AcGePoint3d& pt,
    vector<AcDb3dPolyline*>& poly3DVec,
    AcGePoint3dArray& vecPoint,
    const double& tol )
{
    //Resultat
    Acad::ErrorStatus er = eNotApplicable;
    
    //Selection sur les polylignes 3d du dessin
    ads_name ssPoly;
    
    //Recuperer tous les polylignes du dessin
    long lenPoly = getSsAllPoly3D( ssPoly );
    
    //Verfier qu'on a recuperer les polylignes
    if( lenPoly ==  0 )
    {
        //Afficher message
        print( "Selection ivalide, Sortie" );
        
        return er;
    }
    
    //Polyligne
    AcDb3dPolyline* polyTemp;
    
    //Boucle sur les polylignes de la selection
    for( int i = 0; i < lenPoly; i++ )
    {
        //Recuperer la i-eme polyligne
        polyTemp = getPoly3DFromSs( ssPoly, i, AcDb::kForWrite );
        
        if( polyTemp )
        {
            //Point projeté
            AcGePoint3d ptOnCurve;
            
            //Projeter le point sur la polyligne
            er = polyTemp->getClosestPointTo( pt, ptOnCurve );
            
            //Tester que le point est bien trouvé
            if( Acad::eOk != er )
            {
                //Afficher message
                print( "Impossible de recuperer le point, Sortie" );
                
                return er;
            }
            
            //Tester la distance entre le point trouvé et le point cliqué
            if( isEqual2d( pt, ptOnCurve, tol ) )
            {
                //Ajouter la polyligne dans le vecteur
                poly3DVec.push_back( polyTemp );
                
                //Ajouter le point trouvé dans le vecteur
                vecPoint.push_back( ptOnCurve );
            }
            
            //Sinon fermer la polyligne
            else
                polyTemp->close();
        }
    }
    
    //Verifier qu'on a quelque chose
    if( poly3DVec.size() == 0 )
        return er;
        
    //Retourner ok
    return Acad::eOk;
}

Acad::ErrorStatus intersectTwoPoly3D( AcDb3dPolyline*& poly3D,
    AcDb3dPolyline*& otherPoly3D,
    AcGePoint3dArray& intersectPoint3DArray,
    long& count,
    const double& tol )
{
    Acad::ErrorStatus errorStatus = Acad::eOk;
    
    if( !poly3D )
        return Acad::eNullHandle;
        
    if( !otherPoly3D )
        return Acad::eNullHandle;
        
    intersectPoint3DArray = intersectPoly3D( poly3D, otherPoly3D );
    
    int size = intersectPoint3DArray.length();
    
    for( int counter = 0; counter  < size ; counter ++ )
    {
        AcGePoint3d one = intersectPoint3DArray[counter];
        counter = counter;
    }
    
    // Ajout des sommets sur le premier poly3d
    if( size > 0 )
    {
        //Cumule le nombre d'intersection
        count += size;
        insertVertexesToPoly3D( poly3D, intersectPoint3DArray, tol );
    }
    
    return errorStatus;
}


Acad::ErrorStatus addArVertexOnPoly( const AcGePoint3dArray& arNewPoint,
    AcDb3dPolyline*& poly3D,
    const bool& delEntity )
{
    //Resultat par défaut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Recuperer la taille du tableau
    int taille = arNewPoint.size();
    
    //Verifier
    if( taille == 0 )
        return Acad::eOk;
        
    //Recuperer les sommets de la polyligne
    AcGePoint3dArray vecPoly;
    vector<double> xPoly;
    er = getVertexesPoly( poly3D,
            vecPoly,
            xPoly );
            
    //Verifier
    if( er != Acad::eOk )
        return er;
        
    //Fusionner les sommets des vecteurs et des nouveaux points dans un seul vecteur
    vecPoly.insert( vecPoly.end(), arNewPoint.begin(), arNewPoint.end() );
    
    //Premier point de la polyligne comme reference
    AcGePoint3d ptRef = vecPoly[0];
    
    //Trier le vecteur
    er = sortListUsingCurveDistance( poly3D,
            vecPoly );
            
    //Verifier
    if( Acad::eOk != er )
        return er;
        
    //Creer la nouvelle polyligne
    AcDb3dPolyline* newPoly3d = new AcDb3dPolyline( AcDb::k3dSimplePoly, vecPoly );
    
    //Ajouter dans le modelSpace
    addToModelSpace( newPoly3d );
    
    //Copier les propriétés
    newPoly3d->setPropertiesFrom( poly3D );
    
    //Fermer la polyligne
    newPoly3d->close();
    
    //Si l'utilisateur veut supprimer la polyligne
    if( delEntity )
    {
        //Verifier si la polyligne est en lecture
        if( poly3D->isWriteEnabled() )
            poly3D->erase( true );
        else
            return Acad::eWasOpenForRead;
    }
    
    //Retourner ok
    return Acad::eOk;
}



Acad::ErrorStatus optionPolyline3D( AcDb3dPolyline*& usrPoly,
    AcGePoint3d& retPt2 )
{
    //Resultat par defaut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Selection sur une polyligne
    ads_name ssPoly3D;
    ads_point point;
    
    //Selectionner la polyligne
    long lenPoly = getSsPoly3D( ssPoly3D );
    
    //Verifier
    if( lenPoly == 0 )
        return er;
        
    //La polyligne selectionnée
    AcDb3dPolyline* poly = getPoly3DFromSs( ssPoly3D, 0 );
    
    //Tester qu'on a bien recuperer la polyligne
    if( !poly )
        return er;
        
    //Verifier si l'utilisateur a selectionné la meme polyligne
    if( poly->objectId() == usrPoly->objectId() )
    {
        //Afficher une message
        print( "\nVous avez selectionner la même polyligne, Sortie" );
        
        return er;
    }
    
    //Point 3d
    AcGePoint3d pt1, pt2;
    
    //Demander à l'utilisateur de cliquer le point début
    if( RTNORM != getWcsPoint( _T( "\nCliquer sur le premier point" ), &pt1 ) )
    {
        //Afficher le message
        print( "\nCommande annulée" );
        
        //Fermer la polyligne
        poly->close();
        acedSSFree( ssPoly3D );
        
        //Sortir
        return er;
    }
    
    //Demander à l'utilisateur de cliquer le second point
    if( RTNORM != getWcsPoint( _T( "\nCliquer sur le second point" ), &pt2 ) )
    {
        //Afficher un message
        print( "\nCommande annulée" );
        
        //Fermer la polyligne
        poly->close();
        acedSSFree( ssPoly3D );
        
        //Sortir
        return er;
    }
    
    //Point le plus proche sur la polyligne
    AcGePoint3d ptClose1;
    er = poly->getClosestPointTo( pt1, ptClose1 );
    
    //Verifier
    if( er != Acad::eOk )
    {
        //Afficher un message
        print( "\nImpossible de recuperer le point 1 sur la polyligne" );
        
        //Fermer la polyligne
        poly->close();
        acedSSFree( ssPoly3D );
        
        //Sortir
        return er;
    }
    
    //Recuperer le dernier point
    retPt2 = pt2;
    
    //Point le plus proche sur la polyligne
    AcGePoint3d ptClose2;
    er = poly->getClosestPointTo( pt2, ptClose2 );
    
    //Verifier
    if( er != Acad::eOk )
    {
        //Afficher un message
        print( "\nImpossible de recuperer le point 2 sur la polyligne" );
        
        //Fermer la polyligne
        poly->close();
        acedSSFree( ssPoly3D );
        
        //Sortir
        return er;
    }
    
    //Recuperer les nouvelles sommets à ajouter à la polyligne
    AcGePoint3dArray newVec;
    er = getSubVertexOfPoly3D( poly, pt1, pt2, newVec );
    
    //Recuperer la taille du vecteur
    int taille = newVec.size();
    
    //Verifier
    if( taille == 0 )
    {
        poly->close();
        acedSSFree( ssPoly3D );
        
        return er;
    }
    
    //Boucle pour ajouter les nouvelles sommets
    for( int i = 0; i < taille; i++ )
    {
        AcDb3dPolylineVertex* nVert = new AcDb3dPolylineVertex( newVec[i] );
        
        //Ajouter le nouveau sommet de la polyligne
        usrPoly->appendVertex( nVert );
        
        nVert->close();
        
        //Verifier
        if( er != Acad::eOk )
            continue;
    }
    
    //Fermer la polyligne
    poly->close();
    
    //Liberer la selection
    acedSSFree( ssPoly3D );
    
    //Retourner eOk
    return Acad::eOk;
}


Acad::ErrorStatus getSubVertexOfPoly3D( AcDb3dPolyline* poly,
    AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    AcGePoint3dArray& newVec )
{
    //Resultat par defaut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Vider le vecteur avant de faire les calculs
    newVec.clear();
    newVec.resize( 0 );
    
    //Verifier que l'utilisateur n'a pas cliquer le même point
    if( isEqual3d( pt1, pt2, 0.01 ) )
    {
        //Afficher message
        print( "Vous avez cliquer le même point, Sortie" );
        
        //Sortir
        return er;
    }
    
    //Recuperer les sommets de la polyligne selectionnée
    AcGePoint3dArray polyVert;
    vector<double> xPolyVert;
    er = getVertexesPoly( poly, polyVert, xPolyVert );
    
    //Verifier
    if( er != Acad::eOk )
        return er;
        
    //Booleen de reverse
    bool reverse = false;
    
    //Recuperer la distance curviligne des deux points
    double c1 = 0;
    double c2 = 0;
    poly->getDistAtPoint( pt1, c1 );
    poly->getDistAtPoint( pt2, c2 );
    
    //Tester les distances
    if( c1 > c2 )
        reverse = true;
        
    //Recuperer les segments des deux points
    vector<int> seg1;
    getSegmentOfPointInPoly( poly, pt1, seg1 );
    vector<int> seg2;
    getSegmentOfPointInPoly( poly, pt2, seg2 );
    
    //Verifier si le point 1 n'est pas un vertex
    if( !isVertexOfPoly( poly, pt1 ) )
    {
        //Ajouter le premier point dans le vecteur
        newVec.push_back( pt1 );
        
        //Tester le reverse
        if( reverse )
            pt1 = polyVert[seg1[0]];
        else
            pt1 = polyVert[seg1[1]];
    }
    
    //Booleen du dernier point
    bool pt2IsAVert = true;
    AcGePoint3d pt2Temp = pt2;
    
    //Verifier si le point 2 n'est pas un vertex
    if( !isVertexOfPoly( poly, pt2 ) )
    {
        //Ajouter le premier point dans le vecteur
        pt2IsAVert = false;
        
        //Tester le reverse
        if( reverse )
            pt2 = polyVert[seg2[1]];
        else
            pt2 = polyVert[seg2[0]];
    }
    
    //Si pt1 et pt2 sont les mêmes
    if( isEqual3d( pt1, pt2, 0.01 ) )
    {
        newVec.push_back( pt1 );
        newVec.push_back( pt2Temp );
        
        //Verifier
        if( newVec.size() == 0 )
            return er;
            
        //Sortir de la fonction
        return Acad::eOk;
    }
    
    //Recuperer la taille du vecteur
    int taille = polyVert.size();
    
    if( reverse )
        std::reverse( polyVert.begin(), polyVert.end() );
        
    //Booleen
    bool find = false;
    
    //Boucle sur le vecteur
    for( int i = 0; i < taille; i++ )
    {
        if( isEqual3d( pt1, polyVert[i], 0.01 && !find ) )
        {
            //Changer le booleen en true
            find = true;
            
            //Inserer au debut du vecteur le vertex
            newVec.push_back( polyVert[i] );
        }
        
        else if( find )
        {
            //Tester si on arrive au second point
            if( !isEqual3d( pt2, polyVert[i], 0.01 ) )
                newVec.push_back( polyVert[i] );
            else
            {
                //Ajouter le vertex dans le vecteur
                newVec.push_back( polyVert[i] );
                
                //Sortir du boucle
                break;
            }
        }
    }
    
    //Ajouter le dernier vertex dans le vecteur si ce n'est pas un vertex
    if( !pt2IsAVert )
        newVec.push_back( pt2Temp );
        
    //Verifier
    if( newVec.size() == 0 )
        return er;
        
    //Sortir de la fonction
    return Acad::eOk;
}


Acad::ErrorStatus getSegmentOfPointInPoly( AcDb3dPolyline* poly3D,
    const AcGePoint3d& pt,
    vector<int>& vectRes )
{
    //Resultat par defaut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Nettoyer d'abord le vecteur avant le calcul
    vectRes.clear();
    vectRes.resize( 0 );
    
    //Recuperer les sommets de la polyligne selectionnée
    AcGePoint3dArray polyVert;
    vector<double> xPolyVert;
    er = getVertexesPoly( poly3D, polyVert, xPolyVert );
    
    //Taille du vecteur
    int taille = polyVert.size();
    
    //Verifier
    if( polyVert.size() == 0 )
        return er;
        
    //Boucle sur le vecteur de sommet
    for( int i = 1; i < taille ; i++ )
    {
        //Tester si les trois points sont colinaire
        if( isPointOnLine( polyVert[i - 1], polyVert[i], pt ) )
        {
            //Recuperer les index
            vectRes.push_back( i - 1 );
            vectRes.push_back( i );
            
            return Acad::eOk;
        }
    }
    
    //Retourner la valeur par défaut
    return er;
}


Acad::ErrorStatus drawNewPoly3D( const AcGePoint3d& fPoint )
{
    //Resultat par defaut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Polyligne3d
    AcDb3dPolyline* poly3D = new AcDb3dPolyline();
    
    //Ajouter la polyligne au modelSpace
    addToModelSpace( poly3D );
    
    //Point 3d
    AcGePoint3d pt1, pt2;
    
    //Tester si le point en entrée est l'origine
    if( fPoint == AcGePoint3d::kOrigin )
    {
        //Demander à l'utilisateur de cliquer le point début
        if( getWcsPoint( _T( "\nCliquer sur le premier point de la polyligne" ), &pt1 ) != RTNORM )
        {
            //Afficher le message
            print( "Commande annulée" );
            
            //Sortir
            return er;
        }
    }
    
    //Sinon recuperer le point en entrée
    else
        pt1 = fPoint;
        
    //Ajouter le vertex
    AcDb3dPolylineVertex* vert = new AcDb3dPolylineVertex( pt1 );
    poly3D->appendVertex( vert );
    vert->close();
    
    //Booleen de sortie de boucle
    bool exit = false;
    
    //Option POlyligne
    AcString option = _T( "Polyligne" );
    
    //Boucle pour recuperer les nouvelles entrée
    while( !exit )
    {
        //Initialiser l'userInput
        int usrInput = -1;
        
        AcString kw;
        
        usrInput = askUserOpt( option, _T( "\nCliquer sur le point suivant ou [Polyligne]" ), pt1, pt2, kw );
        
        //Demander à l'utilisateur de cliquer le nouveau point du sommet
        if( RTNORM != usrInput )
        {
            if( usrInput != -5005 )
            {
                //Afficher le message
                print( "Commande termimée" );
                
                //Verifier la taille du vecteur
                if( getNumberOfVertex( poly3D ) < 2 )
                {
                    //Supprimer la polyligne
                    poly3D->erase();
                    
                    //Fermer la polyligne
                    poly3D->close();
                    
                    //Sortir
                    return er;
                }
                
                //Sinon sortir de la boucle
                break;
            }
            
            //Si c'est un keyword
            else
            {
                //Appeler la fonction option polyligne
                er = optionPolyline3D( poly3D, pt1 );
                
                //Redessiner la polyligne
                AcDbEntity* entity = AcDbEntity::cast( poly3D );
                redrawEntity( entity );
                
                //Continuer
                continue;
            }
        }
        
        //Ajouter le sommet
        AcDb3dPolylineVertex* vertN = new AcDb3dPolylineVertex( pt2 );
        poly3D->appendVertex( vertN );
        vertN->close();
        
        //Redessiner la polyligne
        AcDbEntity* entity = AcDbEntity::cast( poly3D );
        redrawEntity( entity );
        
        //Remplacer les points
        pt1 = pt2;
    }
    
    //Fermer la polyligne
    poly3D->close();
    
    return Acad::eOk;
}



Acad::ErrorStatus continuePoly3D( AcDb3dPolyline* poly3D )
{
    //Resultat par defaut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Recuperer le dernier point de la polyligne
    AcGePoint3d pt1, pt2;
    er = poly3D->getEndPoint( pt1 );
    
    if( er != Acad::eOk )
        return er;
        
    //Booleen de sortie de boucle
    bool exit = false;
    
    //Option POlyligne
    AcString option = _T( "Polyligne" );
    
    //Boucle pour recuperer les nouvelles entrée
    while( !exit )
    {
        //Initialiser l'userInput
        int usrInput = -1;
        
        AcString kw;
        
        usrInput = askUserOpt( option, _T( "\nCliquer sur le point suivant ou [Polyligne]" ), pt1, pt2, kw );
        
        //Demander à l'utilisateur de cliquer le nouveau point du sommet
        if( RTNORM != usrInput )
        {
            if( usrInput != -5005 )
            {
                //Afficher le message
                print( "Commande termimée" );
                
                //Verifier la taille du vecteur
                if( getNumberOfVertex( poly3D ) < 2 )
                {
                    //Supprimer la polyligne
                    poly3D->erase();
                    
                    //Fermer la polyligne
                    poly3D->close();
                    
                    //Sortir
                    return er;
                }
                
                //Sinon sortir de la boucle
                break;
            }
            
            //Si c'est un keyword
            else
            {
                //Appeler la fonction option polyligne
                er = optionPolyline3D( poly3D, pt1 );
                
                //Redessiner la polyligne
                AcDbEntity* entity = AcDbEntity::cast( poly3D );
                redrawEntity( entity );
                
                //Continuer
                continue;
            }
        }
        
        //Ajouter le sommet
        AcDb3dPolylineVertex* vertN = new AcDb3dPolylineVertex( pt2 );
        poly3D->appendVertex( vertN );
        vertN->close();
        
        //Redessiner la polyligne
        AcDbEntity* entity = AcDbEntity::cast( poly3D );
        redrawEntity( entity );
        
        //Remplacer les points
        pt1 = pt2;
    }
    
    return Acad::eOk;
}

AcGePoint3dArray discretizeSeg( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const double& dist )
{
    //Longueur du segment
    double distPt = pt1.distanceTo( pt2 );
    
    //Initialiser le tableau de point
    AcGePoint3dArray ptArray = AcGePoint3dArray();
    ptArray.append( pt1 );
    
    // Le segment est grand que la tolérance
    if( distPt > dist )
    {
        //Recuperer le nouveau nombre de sommet
        int nbSeg = int ( ( distPt / dist ) ) + 1;
        
        AcGeVector3d vect = getVector3d( pt1, pt2 ) / nbSeg;
        
        //Initialiser le point temporaire
        AcGePoint3d ptTemp = pt1;
        
        //Boucler sur les nouveaux sommets
        nbSeg--;
        
        for( int numSommet = 0; numSommet < nbSeg; numSommet++ )
        {
            ptTemp += vect;
            ptArray.append( ptTemp );
        }
    }
    
    // On retourne le tableau de points
    return ptArray;
}

Acad::ErrorStatus discretizePoly( AcGePoint3dArray& ptAr,
    AcDb3dPolyline* poly,
    const double& dist )
{
    // Vider le tableau
    ptAr.clear();
    
    // Itérateur sur les sommets de la polyligne
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* it = poly->vertexIterator();
    
    // Initialisation de l'itérateur
    it->start();
    
    //Recuperer le premier point de la polyligne
    Acad::ErrorStatus es = poly->openVertex( vertex, it->objectId(), AcDb::kForRead );
    
    if( es )
    {
        delete it;
        return es;
    }
    
    //Initialiser les points du segment et le dernier point de la polyligne
    AcGePoint3d ptA = vertex->position();
    vertex->close();
    AcGePoint3d ptFirst = ptA;
    AcGePoint3d ptB = AcGePoint3d::kOrigin;
    it->step();
    
    //Boucler sur le sommet
    while( !it->done() )
    {
        // On ouvre le vertex courant
        es = poly->openVertex( vertex, it->objectId(), AcDb::kForRead ) ;
        
        if( es )
        {
            delete it;
            return es;
        }
        
        // On discrétise le segment AB
        ptB = vertex->position();
        ptAr.append( discretizeSeg( ptA, ptB, dist ) );
        
        // On ferme le vertex courant
        vertex->close();
        
        ptA = ptB;
        it->step();
    }
    
    //Tester si le polyligne est ferme
    if( poly->isClosed() )
        ptAr.append( discretizeSeg( ptB, ptFirst, dist ) );
        
    else
        ptAr.append( ptB );
        
    //Retourner la valeur du polyligne discretisé
    return Acad::eOk;
}


Acad::ErrorStatus modifyPolyline3d( AcDb3dPolyline* poly,
    const AcGePoint3dArray& pointArray )
{
    //Iterateur sur le polyligne
    AcDb3dPolylineVertex* vertex = NULL;
    AcDbObjectIterator* it = poly->vertexIterator();
    it->start();
    int i = 0;
    
    //Taille de la listes des points
    long ptArSize = pointArray.size();
    
    //Boucler sur l'iterateur
    while( !it->done() )
    {
        //Ouvrir le vertex
        poly->openVertex( vertex, it->objectId(), AcDb::kForWrite ) ;
        
        // Cas où il ya plus de sommet dans la polyligne que dans la liste de points
        if( ptArSize <= i )
            vertex->erase();
            
        // Sinon modifier le sommet
        else
            vertex->setPosition( pointArray[i] );
            
        //fermer le vertex
        vertex->close();
        
        //Compter le nombre de sommet
        i++;
        
        //Icrementer l'iterateur
        it->step();
    }
    
    //Ajouter les autre points du tableau de point
    while( i < ptArSize )
    {
        //Creer le vertex
        vertex = new AcDb3dPolylineVertex( pointArray[i] );
        
        //Ajouter le sommet dans la polyligne
        poly->appendVertex( vertex );
        
        //fermer le vertex
        vertex->close();
        
        // Incrémentation du compteur
        i++;
    }
    
    return Acad::eOk;
}


AcDb3dPolyline* getProjectedPoly3DOnPlane( AcDb3dPolyline* poly3D,
    const AcGePlane& plane,
    const AcGeVector3d& projectDir )
{
    //Resultat par défaut
    AcDb3dPolyline* polyRes = NULL;
    
    //Verifier la polyligne
    if( poly3D == NULL )
        return polyRes;
        
    //Projeter la polyligne sur le plan
    AcDbCurve* curve = getProjCurveOnPlane( poly3D, plane, projectDir );
    
    if( !curve )
        return NULL;
        
    //Caster en polyligne 3d
    if( curve->isKindOf( AcDb3dPolyline::desc() ) )
        polyRes = AcDb3dPolyline::cast( curve );
        
    //Retourner NULL
    return polyRes;
}

AcDb3dPolyline* getPoly3DFromEntSel(
    const ads_name&             ssName,
    const AcDb::OpenMode& opMode )
{
    AcDbObjectId id = AcDbObjectId::kNull;
    
    //On récupère l'Id de la pièce sélectionnée
    acdbGetObjectId( id, ssName );
    
    //On récupère un pointeur sur l'entity sélectionnée
    AcDbEntity* pEnt = NULL;
    acdbOpenAcDbEntity( pEnt, id, opMode );
    
    if( pEnt )
    {
        // Vérification du type de class
        if( pEnt->isKindOf( AcDb3dPolyline::desc() ) )
            return AcDb3dPolyline::cast( pEnt );
        else
            pEnt->close();
            
    }
    
    return NULL;
}


AcGePoint3dArray getPoint3dArray(
    AcDb3dPolyline* poly )
{
    //Listes des points du polylignes
    AcGePoint3dArray pointPolyList;
    
    //Déclarer un vertex
    AcDb3dPolylineVertex* vertex = NULL;
    
    //Déclarer un iterateur sur les vertexes
    AcDbObjectIterator* iterPoly3D = poly->vertexIterator();
    
    //Recuperer les sommets du polylignes
    for( iterPoly3D->start(); !iterPoly3D->done(); iterPoly3D->step() )
    {
        //Recuperer le vertex puis le sommet
        if( Acad::eOk == poly->openVertex( vertex, iterPoly3D->objectId(), AcDb::kForRead ) )
        {
            //Inserer le sommet dans la liste
            pointPolyList.push_back( vertex->position() );
            
            //Liberer la mémoire
            vertex->close();
        }
    }
    
    return pointPolyList;
}