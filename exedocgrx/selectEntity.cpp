#pragma once
#include "selectEntity.h"
#include "tchar.h"
#include "print.h"


long getSelectionSet(
    ads_name         ssName,
    const AcString&  ssType,
    const AcString&  objectType,
    const AcString&  layerName,
    const AcString&  blockName,
    const AcString&  textValue,
    const bool&      isClosed,
    const bool&      is3d )
{
    //Declarer la valeur de sortie
    long l_lGetSelectionSet = 0;
    
    //Creer les resbuf
    struct resbuf* l_rbObjectType = new struct resbuf;
    struct resbuf* l_rbLayerName = new struct resbuf;
    struct resbuf* l_rbBlockName = new struct resbuf;
    struct resbuf* l_rbTextValue = new struct resbuf;
    struct resbuf* l_rbIsClosed  = new struct resbuf;
    struct resbuf* l_rbIs3D      = new struct resbuf;
    struct resbuf* l_rbOr1       = new struct resbuf;
    struct resbuf* l_rbOr2       = new struct resbuf;
    
    //Definir le type de Resbuf
    l_rbObjectType->restype = 0;
    l_rbLayerName ->restype = 8;
    l_rbBlockName ->restype = 2;
    l_rbTextValue ->restype = 1;
    l_rbIsClosed  ->restype = 70;
    l_rbIs3D      ->restype = 70;
    l_rbOr1       ->restype = -4;
    l_rbOr2       ->restype = -4;
    
    //Declarer les valeurs
    TCHAR l_sObjectType[65535];// 16379 2exp14 old ->65535 2exp17
    TCHAR l_sLayerName [65535];
    TCHAR l_sBlockName [65535];
    TCHAR l_sTextValue [65535];
    TCHAR l_sTextOr1   [65535];
    TCHAR l_sTextOr2   [65535];
    int   l_iIsClosed  = 1 ;
    int   l_iIs3D      = 8 ;
    
    //Copier les valeurs
    _tcscpy_s( l_sObjectType, objectType );
    _tcscpy_s( l_sLayerName, layerName );
    _tcscpy_s( l_sBlockName, blockName );
    _tcscpy_s( l_sTextValue, textValue );
    _tcscpy_s( l_sTextOr1, _T( "<OR" ) );
    _tcscpy_s( l_sTextOr2, _T( "OR>" ) );
    
    if( is3d ) l_iIsClosed = 9;
    
    
    //Completer la valeur des resbuf
    l_rbObjectType->resval.rstring = l_sObjectType;
    l_rbLayerName ->resval.rstring = l_sLayerName ;
    l_rbBlockName ->resval.rstring = l_sBlockName ;
    l_rbTextValue ->resval.rstring = l_sTextValue ;
    l_rbIsClosed  ->resval.rint    = l_iIsClosed  ;
    l_rbIs3D      ->resval.rint    = l_iIs3D      ;
    
    //Initialiser un pointeur sur un resbuf
    struct resbuf* l_rbNext = NULL;
    
    //Creer la chaine
    if( !objectType.isEmpty() ) {l_rbObjectType->rbnext = l_rbNext; l_rbNext = l_rbObjectType;}
    
    if( !layerName .isEmpty() ) {l_rbLayerName ->rbnext = l_rbNext; l_rbNext = l_rbLayerName ;}
    
    if( !blockName .isEmpty() ) {l_rbBlockName ->rbnext = l_rbNext; l_rbNext = l_rbBlockName ;}
    
    if( !textValue .isEmpty() ) {l_rbTextValue ->rbnext = l_rbNext; l_rbNext = l_rbTextValue ;}
    
    //Cas 2D ferme
    if( isClosed && !is3d )
    {
        l_rbIsClosed->rbnext = l_rbNext;
        l_rbNext = l_rbIsClosed;
    }
    
    //Cas 3D ferme
    else if( isClosed && is3d )
    {
        l_rbIsClosed->rbnext = l_rbNext;
        l_rbNext = l_rbIsClosed;
    }
    
    //Cas 3D ouvert
    else if( isClosed && is3d )
    {
        //Fermer le OR
        l_rbOr2->rbnext = l_rbNext;
        l_rbNext = l_rbOr2;
        
        //Ajouer les deux conditions
        l_rbIsClosed->rbnext = l_rbNext;
        l_rbNext = l_rbIsClosed;
        l_rbIs3D->rbnext = l_rbNext;
        l_rbNext = l_rbIs3D;
        
        //Ouvrir le OR
        l_rbOr1->rbnext = l_rbNext;
        l_rbNext = l_rbOr1;
    }
    
    //Lancer la selection et verifie rla taille
    if(
        RTNORM == acedSSGet( ssType, NULL, NULL, l_rbNext, ssName )
        && RTNORM != acedSSLength( ssName, &l_lGetSelectionSet ) )
        l_lGetSelectionSet = 0;
        
    //Liberer la memoire
    delete( l_rbObjectType );
    delete( l_rbLayerName );
    delete( l_rbBlockName );
    delete( l_rbTextValue );
    delete( l_rbOr2 );
    delete( l_rbIsClosed );
    delete( l_rbIs3D );
    delete( l_rbOr1 );
    
    //delete(l_rbNext);
    
    //Sortir
    return l_lGetSelectionSet;
}



long getSingleSelection(
    ads_name        ssName,
    const AcString&  objectType,
    const AcString&  layerName,
    const AcString&  blockName,
    const AcString&  textValue,
    const bool&      isClosed,
    const bool&      is3d )
{
    //Lancer la selection
    long l_lAskSingleSelection = getSelectionSet( ssName, "", objectType, layerName, blockName, textValue, isClosed, is3d );
    
    //Boucler tant que la selection n'est pas valide
    while( 1 < l_lAskSingleSelection )
    {
        //Afficher un message d'erreur
        acutPrintf( TXT_SELECTERROR1, l_lAskSingleSelection );
        
        //Relancer la selecion
        l_lAskSingleSelection = getSelectionSet( ssName, "", objectType, layerName, blockName, textValue, isClosed, is3d );
    }
    
    return l_lAskSingleSelection;
}




long getSsLength( const ads_name ssToTest )
{
    //Verifieer la selection
    if( !ssToTest )
        return 0;
        
    //Recuperer la taille de la selection
    long l_iObject = 0;
    acedSSLength( ssToTest, &l_iObject );
    
    //Sortir
    return l_iObject;
}



bool isSsValid( const ads_name ssToTest )
{
    return getSsLength( ssToTest ) > 0;
}



AcDbObjectId getObIdFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    //Recuperer le ssName
    ads_name l_ssObject;
    
    int res = acedSSName( ssName, iObject, l_ssObject );
    
    if( RTNORM != res )
    {
        //Vider la selection et sortie
        acedSSFree( l_ssObject );
        return AcDbObjectId::kNull;
    }
    
    //Recuperer l'object ID
    AcDbObjectId l_idGetObIdFromSs = AcDbObjectId::kNull;
    
    if( Acad::eOk != acdbGetObjectId( l_idGetObIdFromSs, l_ssObject ) )
    {
        //Vider la selection et sortie
        acedSSFree( l_ssObject );
        return AcDbObjectId::kNull;
    }
    
    //Vider la selection et sortie
    acedSSFree( l_ssObject );
    return l_idGetObIdFromSs;
}



AcDbEntity* getEntityFromSs(
    const ads_name&          ssName,
    const long&              iObject,
    const AcDb::OpenMode&    opMode )
{
    //Recuperer l'object ID
    AcDbObjectId l_idObject = getObIdFromSs( ssName, iObject );
    
    //Verifier l'objectId
    if( l_idObject.isNull() )
        return NULL;
        
    //Declarer l'entite a renvoyer
    AcDbEntity* l_pEntityFromSs = NULL;
    
    // Recuperer l'entite
    if( Acad::eOk != acdbOpenAcDbEntity( l_pEntityFromSs, l_idObject, opMode ) )
        return NULL;
        
    //Sotir
    return l_pEntityFromSs;
    
}



AcDbEntity* readEntityFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    return getEntityFromSs( ssName, iObject, AcDb::kForRead );
}



AcDbEntity* writeEntityFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    return getEntityFromSs( ssName, iObject, AcDb::kForWrite );
}



long getSsObject(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSelectionSet( ssName, "", "", layerName );
}




long getSsOneObject(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSingleSelection( ssName, "", layerName );
}




long getSsAllObject(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSelectionSet( ssName, "X", "", layerName );
}




long getSsText(
    ads_name        ssName,
    const AcString  layerName,
    const AcString  textValue )
{
    return getSelectionSet( ssName, "", "TEXT", layerName, "", textValue );
}

long getSsMText(
    ads_name        ssName,
    const AcString  layerName,
    const AcString  textValue )
{
    return getSelectionSet( ssName, "", "MTEXT", layerName, "", textValue );
}


long getSsOneText(
    ads_name        ssName,
    const AcString  layerName,
    const AcString  textValue )
{
    return getSingleSelection( ssName, "TEXT", layerName, "", textValue );
}

long getSsOneMText(
    ads_name        ssName,
    const AcString  layerName,
    const AcString  textValue )
{
    return getSingleSelection( ssName, "MTEXT", layerName, "", textValue );
}

long getSsAllMText(
    ads_name        ssName,
    const AcString  layerName,
    const AcString  textValue )
{
    return getSelectionSet( ssName, "X", "MTEXT", layerName, "", textValue );
}

long getSsAllText(
    ads_name        ssName,
    const AcString  layerName,
    const AcString  textValue )
{
    return getSelectionSet( ssName, "X", "TEXT", layerName, "", textValue );
}


long getSsFmPoly3D(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSelectionSet( ssName, "", "FmapPolyline3d", layerName );
}

long getSsOneFmPoly3D(
    ads_name        ssName,
    const AcString  layerName )
{

    return getSingleSelection( ssName, "", "FmapPolyline3d", layerName );
    
}

Acad::ErrorStatus zoomOnEntity( AcDbEntity* entity,
    const double& height )
{
    Acad::ErrorStatus es = Acad::eOk;
    //On recup�re le bounding box de l'entit�
    AcDbExtents boundingBox ;
    es = entity->getGeomExtents( boundingBox );
    
    if( es != Acad::eOk )
        return es;
        
    //On calcule le milieu du bounding box
    AcGePoint3d minPoint = boundingBox.minPoint();
    AcGePoint3d maxPoint = boundingBox.maxPoint();
    
    AcGePoint3d target = midPoint3d( minPoint, maxPoint );
    
    //Zoomer sur l'entit�
    return zoomOn( target, height );
}


AcDbText* getTextFromSs( const ads_name& ssName,
    const long& iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  l_pEntityLine = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en texte
    return AcDbText::cast( l_pEntityLine );
}

AcDbMText* getMTextFromSs( const ads_name& ssName,
    const long& iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  l_pEntityLine = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en texte
    return AcDbMText::cast( l_pEntityLine );
}

AcDbXline* getXlineFromSs( const ads_name& ssName,
    const long& iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  l_pEntityLine = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en texte
    return AcDbXline::cast( l_pEntityLine );
}


long getSsXline(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSelectionSet( ssName, "", "XLINE", layerName );
}


long getSsOneXline(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSingleSelection( ssName, "XLINE", layerName );
}



long getSsAllXLine(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSelectionSet( ssName, "X", "XLINE", layerName );
}



Acad::ErrorStatus createPickFirstSelection( const AcDbEntity* pEnt )
{

    // On r�cup�re l'id de l'entit�
    AcDbObjectId id = pEnt->id();
    
    // On cr�e un ads_name � partir de l'id
    ads_name ssEnt;
    acdbGetAdsName( ssEnt, id );
    
    // On cr�e la s�lection
    ads_name ssFirst;
    acedSSAdd( ssEnt, ssFirst, ssFirst );
    acedSSSetFirst( ssFirst,
        ssFirst );
        
    return Acad::eOk;
}


Acad::ErrorStatus getAllTextFromSs(
    const ads_name& sstext,
    std::vector<AcDbText*>& vecttextpointer,
    std::vector<double>& vectX,
    const AcDb::OpenMode& op )
{
    //Recuperer la selection
    long ln = 0;
    
    if( acedSSLength( sstext, &ln ) != RTNORM )
        return Acad::eNotApplicable;
        
    //Boucler sur la selection
    for( int i = 0; i < ln; i++ )
    {
        AcDbText* text = getTextFromSs( sstext, i, op );
        
        if( !text )
            continue;
            
        vecttextpointer.push_back( text );
        vectX.push_back( text->position().x );
    }
    
    //Verifier la taille du vecteur
    if( vecttextpointer.size() == 0 )
        return eNotApplicable;
        
        
    //Renvoyer le r�sultat
    return eOk;
}


Acad::ErrorStatus getLastEntityInserted(
    AcDbEntity*& entity,
    const AcDb::OpenMode& op )
{

    ads_name ads_ent;
    int res = acdbEntLast( ads_ent );
    
    if( res != RTNORM )
        return Acad::eNotApplicable;
        
    // Id du dernier bloc ins�r�
    
    Acad::ErrorStatus es = Acad::eOk;
    AcDbObjectId idLast;
    
    // On r�cup�re l'id du dernier bloc ins�r�
    es = acdbGetObjectId( idLast, ads_ent );
    
    if( !idLast.isValid() )
        return es;
        
    // On r�cup�re un pointeur sur l'entity s�lectionn�e
    AcDbEntity* pEnt = NULL;
    es = acdbOpenAcDbEntity( entity, idLast, op );
    
    if( !entity )
    {
        acedSSFree( ads_ent );
        print( "Impossible de recuperer l'entit�" );
        return eNotApplicable;
    }
    
    acedSSFree( ads_ent );
    
    return es;
}