/**
 * @file getPoints.h
 * @author Romain LEMETTAIS
 * @brief Fonctions pratiques pour recuperer des points dans differents systemes de coordonnees
 */

#pragma once
#include <dbents.h>
#include <dbjig.h>
#include <geassign.h>
#include <adscodes.h>
#include "textSelcet.h"
#include "pointEntity.h"
#include "acString.h"

using namespace std;


/** \brief Convertit un point depuis le WCS ( World Coordinate System ) en UCS ( User Coordinate System )
  * \param Point a convertir
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  */
int wcsToUcs(
    const AcGePoint3d* pt );


/** \brief Demande a l'utilisateur de saisir un point, le renvoie dans le SCU
  * \param message Texte a afficher
  * \param pt Point 3D dans le mauvais SCU
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entre
  */
int getUcsPoint(
    const AcString    message,
    const AcGePoint3d*    pt );



/**  \brief Demande a l'utilisateur de cliquer un premier point, dessine des pointilles en partant de ce point jusqu'a ce que le dessinateur en clique un second
  * \param message Texte a afficher
  * \param ptGet Second Point clique, c'est celui que l'on recupere en UCS
  * \param ptFrom Premier point clique, celui a partir duquel les pointilles vont etre traces
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  * Pour etre clair, le premier point et les pointilles ne servent a rien, ils sont utiles pour le confort utilisateur lorsque l'on veut cliquer un point en fonction d'un autre point
  * \image html "C:\Futurmap\Dev-Outils\GStarCAD\GRX\FmapLib\documentation\images\getUcsPoint.png"
  */
int getUcsPoint(
    const AcString        message,
    const AcGePoint3d*    ptGet,
    const AcGePoint3d     ptFrom );


/** \brief Convertit un point depuis le UCS ( User Coordinate System ) en WCS ( World Coordinate System )
 * \param Point a transformer
 * \return RTNORM si OK, RTCAN si annule, RTNONE si entre
 */
int getWcsPoint(
    const AcGePoint3d* pt );


/** \brief Convertit un point depuis le WCS ( World Coordinate System )  en UCS ( User Coordinate System )
 * \param Point a transformer
 * \return RTNORM si OK, RTCAN si annule, RTNONE si entre
 */
int getUcsPoint(
	const AcGePoint3d* pt);



/** \brief Demande a l'utilisateur de saisir un point, le renvoie dans le WCS
  * \param message Texte a afficher
  * \param pt Point 3D dans le mauvais SCU
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entre
  */
int getWcsPoint(
    const AcString        message,
    const AcGePoint3d*        pt );


/**  \brief Demande a l'utilisateur de cliquer un premier point, dessine des pointilles en partant de ce point jusqu'a ce que le dessinateur en clique un second.
  *
  *  Pour etre clair, le premier point et les Spointilles ne servent a rien, ils sont utiles pour le confort utilisateur lorsque l'on veut cliquer un point en fonction d'un autre point.
  * Voir l'image de @getUcsPoint
  *
  * \param message Texte a afficher
  * \param ptGet Second Point clique, c'est celui que l'on recupere en WCS
  * \param ptFrom Premier point clique, celui a partir duquel les pointilles vont etre traces
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  */
int getWcsPoint(
    const AcString        message,
    const AcGePoint3d*    ptGet,
    const AcGePoint3d     ptFrom );


/**  \brief Demande a l'utilisateur de cliquer un premier point, dessine un rectangle en partant de ce point jusqu'a ce que le dessinateur en clique un second
  *
  * Pour etre clair, le premier point et le rectangle ne servent a rien, ils sont utiles pour le confort utilisateur lorsque l'on veut cliquer un point en fonction d'un autre point
  *
  * \param message Texte a afficher
  * \param ptGet Second Point clique, c'est celui que l'on recupere en UCS
  * \param ptFrom Premier point clique, celui a partir duquel le rectangle va etre trace
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  * \image html "C:\Futurmap\Dev-Outils\GStarCAD\GRX\FmapLib\documentation\images\getUcsBox.png"
  */
int getUcsBox(
    const AcString        message,
    const AcGePoint3d*      ptGet,
    const AcGePoint3d     ptFrom );


/**  \brief Demande a l'utilisateur de cliquer un premier point, dessine un rectangle en partant de ce point jusqu'a ce que le dessinateur en clique un second
  *
  * Pour etre clair, le premier point et le rectangle ne servent a rien, ils sont utiles pour le confort utilisateur lorsque l'on veut cliquer un point en fonction d'un autre point
  *
  * \param message1 Texte a afficher pour le premier point
  * \param message2 Texte a afficher pour le second point
  * \param ptGet Second Point clique, c'est celui que l'on recupere en UCS
  * \param ptFrom Premier point clique, celui a partir duquel le rectangle va etre trace
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  */
int getUcsBox(
    const AcString     message1,
    const AcString     message2,
    const AcGePoint3d* p_ptBox1,
    const AcGePoint3d* p_ptBox2 );

/**  \brief Demande a l'utilisateur de cliquer un premier point, dessine un rectangle en partant de ce point jusqu'a ce que le dessinateur en clique un second
  *
  * Pour etre clair, le premier point et le rectangle ne servent a rien, ils sont utiles pour le confort utilisateur lorsque l'on veut cliquer un point en fonction d'un autre point
  * Voir l'image de getUcsBox()
  *
  * \param message Texte a afficher
  * \param ptGet Second Point clique, c'est celui que l'on recupere en WCS
  * \param ptFrom Premier point clique, celui a partir duquel le rectangle va etre trace
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  */
int getWcsBox(
    const AcString        message,
    const AcGePoint3d*      ptGet,
    const AcGePoint3d     ptFrom );


/** \brief Demande a l'utilisateur de cliquer un premier point, dessine un rectangle en partant de ce point jusqu'a ce que le dessinateur en clique un second
  *
  * Pour etre clair, le premier point et le rectangle ne servent a rien, ils sont utiles pour le confort utilisateur lorsque l'on veut cliquer un point en fonction d'un autre point
  * Voir l'image de getUcsBox()
  *
  * \param message1 Texte a afficher pour le premier point
  * \param message2 Texte a afficher pour le second point
  * \param ptGet Second Point clique, c'est celui que l'on recupere en WCS
  * \param ptFrom Premier point clique, celui a partir duquel le rectangle va etre trace
  * \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  */
int getWcsBox(
    const AcString     message1,
    const AcString     message2,
    const AcGePoint3d* p_ptBox1,
    const AcGePoint3d* p_ptBox2 );


/** \brief  Demande a l'utilisateur de saisir un point
  *  \param message Texte a afficher
  *  \param ptGet Point 3d dans le SCU
  *  \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  * \image html "C:\Futurmap\Dev-Outils\GStarCAD\GRX\FmapLib\documentation\images\getJigUcsPoint.jpg"
  */
int getJigUcsPoint(
    const AcString        message,
    AcGePoint3d*    ptGet );



/** \brief  Demande a l'utilisateur de saisir un point
  *  \param message Texte a afficher
  *  \param ptGet Point 3d dans le WCU
  *  \return RTNORM si OK, RTCAN si annule, RTNONE si entree
  */
int getJigWcsPoint(
    const AcString        message,
    AcGePoint3d*    ptGet );



/** \brief  Demande a l'utilisateur de saisir plusieurs points, affiche la polyligne correspondante en Jig
  *  \param message1 Message affiche lorsque le 1er point est clique
  *  \param message2 Message affiche jusqu'a ce que l'utilisateur fasse entree
  *  \return Pointeur sur le tableau de points cliques
  */
AcGePoint3dArray* getListPoint(
    const AcString        message1,
    const AcString        message2 );



/** \brief  Fonction pour effectuer une delimitation dans le plan orhtogonal au vecteur fourni avec un Jig
  * \param message Message affiche lorsque le 1er point est clique
  * \param ptStart Message affiche jusqu'a ce que l'utilisateur fasse entree
  * \param vecNormal Vecteur normal au plan de delimitation, par defaut c'est le vecteur ( 0, 0, 1 )
  * \return Pointeur sur le tableau de points cliques
  * \image html "C:\Futurmap\Dev-Outils\GStarCAD\GRX\FmapLib\documentation\images\getListPoint.png"
*/
AcGePoint3dArray* getListPoint(
    const AcString      message,
    const AcGePoint3d   ptStart,
    const AcGeVector3d  vecNormal = AcGeVector3d( 0.0, 0.0, 1.0 ) );



/** \brief  Fonction qui retourne l'epaissuer d'une coupe
  * \param message Message affiche lorsque le 1er point est clique
  * \param ptStart Premier point de la coupe
  * \param ptEnd Second point de la coupe
  * \param vecNormal Vecteur normal au plan de delimitation, par defaut c'est le vecteur ( 0, 0, 1 )
  * \return Epaisseur de la coupe
*/
double getWidth(
    const AcString        message,
    const AcGePoint3d     ptStart,
    const AcGePoint3d     ptEnd,
    const AcGeVector3d    vecNormal = AcGeVector3d( 0.0, 0.0, 1.0 ) );

/** \brief
  * \param
  * \param
  * \param
  * \return
*/
AcGePoint3d getRect(
    const AcString        message,
    const AcGePoint3d     ptStart,
    const AcGePoint3d     ptEnd,
    const AcGeVector3d    vecNormal = AcGeVector3d( 0.0, 0.0, 1.0 ) );

//-----------------------------------------------------------------------------------<jigListPoint>
class jigPoint : public AcEdJig {

        //---------------------------------------------------------------------------------<Public : Fonction>
    public:
    
        //Constructeur
        jigPoint( const AcGeVector3d = AcGeVector3d( 0.0, 0.0, 1.0 ) );
        ~jigPoint();
        
        //Virtual
        virtual DragStatus      sampler();
        virtual Adesk::Boolean  update() ;
        virtual AcDbEntity*     entity() const;
        
        //Actions
        int askPoint( const AcString );
        AcGePoint3d getPoint();
        
    private:
        AcDbPoint*   m_ptCurent;
        DragStatus  m_iDragStatus;
};


//-----------------------------------------------------------------------------------<jigListPoint>
class jigListPoint : public AcEdJig {

        //---------------------------------------------------------------------------------<Public : Fonction>
    public:
    
        //Constructeur
        jigListPoint(
            const AcGePoint3d,
            const AcGeVector3d = AcGeVector3d( 0.0, 0.0, 1.0 ) );
        ~jigListPoint();
        
        //Virtual
        virtual DragStatus      sampler();
        virtual Adesk::Boolean  update() ;
        virtual AcDbEntity*     entity() const;
        
        //Actions
        AcGePoint3dArray* getListPoint( const AcString );
        
    private:
        AcDbPolyline* m_poly2D;
        AcGePlane    m_planPoly;
        AcGePoint3d  m_ptCurent;
};



//-----------------------------------------------------------------------------------<jigListPoint>
class jigWidthPoint : public AcEdJig {

        //---------------------------------------------------------------------------------<Public : Fonction>
    public:
    
        //Constructeur
        jigWidthPoint(
            const AcGePoint3d,
            const AcGePoint3d,
            const AcGeVector3d = AcGeVector3d( 0.0, 0.0, 1.0 ) );
        ~jigWidthPoint();
        
        //Virtual
        virtual DragStatus      sampler();
        virtual Adesk::Boolean  update() ;
        virtual AcDbEntity*     entity() const;
        
        //Actions
        double getWidth( const AcString );
        
    private:
        AcDbPolyline* m_poly2D;
        AcGePlane    m_planPoly;
        AcGePoint3d  m_ptCurent;
        AcGePoint2d  m_ptStart;
        AcGePoint2d  m_ptEnd;
        AcGeVector2d m_vectAxe;
        AcGeVector2d m_vectPerp;
        double       m_dXFactor;
        double       m_dYFactor;
        double       m_dWidth;
};



class jigRectPoint : public AcEdJig {

        //---------------------------------------------------------------------------------<Public : Fonction>
    public:
    
        //Constructeur
        jigRectPoint(
            const AcGePoint3d,
            const AcGePoint3d,
            const AcGeVector3d = AcGeVector3d( 0.0, 0.0, 1.0 ) );
        ~jigRectPoint();
        
        //Virtual
        virtual DragStatus      sampler();
        virtual Adesk::Boolean  update() ;
        virtual AcDbEntity*     entity() const;
        
        //Actions
        AcGePoint3d getRect( const AcString );
        
    private:
        AcDbPolyline* m_poly2D;
        AcGePlane    m_planPoly;
        AcGePoint3d  m_ptCurent;
        AcGePoint2d  m_ptStart;
        AcGePoint2d  m_ptEnd;
        AcGeVector2d m_vectAxe;
        AcGeVector2d m_vectPerp;
        double       m_dXFactor;
        double       m_dYFactor;
        double       m_dWidth;
};


