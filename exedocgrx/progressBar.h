/**
 * @file progressBar.h
 * @author Romain LEMETTAIS
 * @brief Fichier qui implemente un objet de barre de progression
 */

#pragma once
#include <aced.h>
#include <core_rxmfcapi.h>

/**
  * \class progressBar 1 2 Objet qui permet d'implementer faiclement une barre de progressiit gon. \n
  * Utile pour les operations qui prennent du temps comme la conversion d'une grosse selection d'objets, etc...
  * Le nom de la barre ci-dessous est "Progression".
  * Cette barre de progression est actualisee a l'aide de la methode moveUp
  * \brief ProgressBar
  * \image html C:\Futurmap\Dev-Outils\GStarCAD\GRX\FmapLib\documentation\Images\ProgressBar.png
*/
class ProgressBar {

    public :
        /**
          * \fn ProgressBar( const ACHAR* textLabel, const long size)
          * \brief Constructeur progressBar
          * \param textLabel Nom de la barre de progression
          * \param size La taille de la barre de progression (valeur par defaut 100)
        */
        ProgressBar( const ACHAR* textLabel, const long& size = 100 );
        
        /**
          * Destructeur de l'objet
          */
        ~ProgressBar();
        
        /**
          * \brief Progression de la gauche vers la droite
          * \param long Nouvelle position
          */
        int moveUp( const long& position );
        
    private :
    
        /**
          * \param La barre de progression � toujours une longueur de 100 ( la progression se fait de 0 � 100 % ).
          * De sorte que si on a 1000 �l�ments, la barre de progression va �tre incr�menter de
          * 100 / 1000 = 0,1 % par 0,1 % � chaque it�ration, dans ce cas m_factor = 0.1
          */
        double m_factor;
};