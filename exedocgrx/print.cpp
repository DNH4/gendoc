#include "print.h"
#include "gcestext.h"
#include "dataBase.h"

void print( const AcGePoint3d& point )
{
    AcString value = _T( "" );
    
    value += _T( "\n( " );
    value += numberToAcString( point[ 0 ], 3 );
    value += _T( ", " );
    value += numberToAcString( point[ 1 ], 3 );
    value += _T( ", " );
    value += numberToAcString( point[ 2 ], 3 );
    value += _T( " )" );
    
    acutPrintf( value );
}

void print( const AcGePoint2d& point )
{
    AcString value = _T( "" );
    
    value += _T( "\n( " );
    value += numberToAcString( point[ 0 ], 3 );
    value += _T( ", " );
    value += numberToAcString( point[ 1 ], 3 );
    value += _T( " )" );
    
    acutPrintf( value );
}


void print( const int& number )
{

    acutPrintf( _T( "\n" ) + numberToAcString( number ) );
}


void print( const unsigned __int64& number )
{

    acutPrintf( _T( "\n" ) + numberToAcString( number ) );
}


void print( const float& number )
{

    acutPrintf( _T( "\n" ) + numberToAcString( number, 4 ) );
}


void print( const double& number )
{

    acutPrintf( _T( "\n" ) + numberToAcString( number, 4 ) );
}


void print( const AcString& acStr )
{
    acutPrintf( _T( "\n" ) + acStr );
}


void print( const string& str, const int& val )
{
    size_t  i = str.find( "%i" );
    
    if( i == string::npos )
        i = str.find( "%d" );
        
    print( str.substr( 0, i ) );
    acutPrintf( numberToAcString( val ) );
    acutPrintf( strToAcStr( str.substr( i + 2, str.length() ) ) );
}


void print( const string& str, const double& val )
{
    int i = str.find( "%f" );
    
    print( str.substr( 0, i ) );
    acutPrintf( numberToAcString( val ) );
    acutPrintf( strToAcStr( str.substr( i + 2, str.length() ) ) );
}


void print( const string& str, const string& val )
{
    if( str.empty() )
        return;
        
    int i = str.find( "%s" );
    int len = str.length();
    print( str.substr( 0, i ) );
    acutPrintf( strToAcStr( val ) );
    
    if( len > 0 )
        acutPrintf( strToAcStr( str.substr( i + 2, len ) ) );
}


void print( const AcString& str, const AcString& val )
{
    if( str == _T( "" ) )
        return;
        
    int i = str.find( "%s" );
    int len = str.length();
    print( str.substr( 0, i ) );
    acutPrintf( val );
    
    if( len > 0 )
        acutPrintf( str.substr( i + 2, len ) );
}


void print( const Acad::ErrorStatus& es )
{
    print( acadErrorStatusText( es ) );
}


void print( const string& str )
{
    acutPrintf( _T( "\n" ) + strToAcStr( str ) );
}


void print( const char str[] )
{
    std::string s( str );
    
    acutPrintf( _T( "\n" ) + strToAcStr( s ) );
}


void print( const AcGeVector2d& vec2d )
{
    acutPrintf( _T( "\nmyVec = ( %f, %f )" ), vec2d.x, vec2d.y );
}


void print( const AcGeVector3d& vec3d )
{
    acutPrintf( _T( "\nmyVec = ( %f, %f, %f )" ), vec3d.x, vec3d.y, vec3d.z );
}


void print( const vector< AcString >& vecAc )
{
    int size = vecAc.size();
    
    for( int i = 0; i < size; i++ )
        acutPrintf( _T( "\nEl�ment %i = %s" ), i, vecAc[ i ] );
}

void drawVectorDirection( const AcGeVector3d& vec,
    const AcGePoint3d& org,
    AcCmColor& color )
{
    AcGePoint3d end = org;
    end = end.transformBy( AcGeMatrix3d::translation( vec ) );
    
    AcDbLine* line = new AcDbLine( org, end );
    line->setColor( color );
    addToModelSpace( line );
    line->close();
}