#include "viewport.h"

int getNumberOfViewports()
{

    // Compteur de viewports
    int c = 0;
    
    if( acedViewportIdFromNumber( 2 ) != AcDbObjectId::kNull )
        c++;
        
    if( acedViewportIdFromNumber( 3 ) != AcDbObjectId::kNull )
        c++;
        
    if( acedViewportIdFromNumber( 4 ) != AcDbObjectId::kNull )
        c++;
        
    return c;
}

Acad::ErrorStatus getTargetOfActiveVp( AcGePoint3d& target,
    double& height,
    double& width )
{
    Acad::ErrorStatus es;
    
    //Recuperer toutes les vues
    acedVports2VportTableRecords();
    
    //Declarer un pointeur sur un ViewPort
    AcDbViewportTableRecordPointer currentView;
    
    //Ouvrir la vue actuelle
    if( es = currentView.open( acedActiveViewportId(), AcDb::kForWrite ) )
        acutPrintf( _T( "Probl�me d'ouverture de la fen�tre active" ) );
        
    //Recuperer le target de la vue active
    target = currentView->target();
    height = currentView->height();
    width = currentView->width();
    
    //Fermer la vue active
    currentView->close();
    acedVportTableRecords2Vports();
    
    return es;
}


AcDbObjectIdArray getAllViewportId( AcDbObjectIdArray& ucsNames )
{
    //Resultat
    AcDbObjectIdArray res;
    
    //Vider le tableau d'ucs
    ucsNames.clear();
    ucsNames.resize( 0 );
    
    //Recuperer toutes les vues
    acedVports2VportTableRecords();
    
    //Declarer un pointeur sur un viewport
    AcDbViewportTableRecordPointer currentView;
    
    //Id null
    AcDbObjectId id = AcDbObjectId::kNull;
    
    //Boucle sur les viewports
    for( int i = 1; i < 5; i++ )
    {
        //Recuperer et verifier l'id
        id = acedViewportIdFromNumber( i );
        
        if( id != AcDbObjectId::kNull )
        {
            //Ajouter l'id dans le tableau
            res.push_back( id );
            
            //Ouvrir la vue actuelle
            if( Acad::eOk == currentView.open( id, AcDb::kForRead ) )
            {
                //Ajouter dans le tableau le nom de l'ucs
                ucsNames.push_back( currentView->ucsName() );
                
                //Fermer le viewPort
                currentView->close();
            }
        }
    }
    
    //Retourner le resultat
    return res;
}