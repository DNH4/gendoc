/**
 * @file lineEntity.h
 * @author Romain LEMETTAIS
 * @brief Fichier contenant des fonctions utiles pour travailler avec les lignes
 */

#pragma once
#include "selectEntity.h"


//-----------------------------------------------------------------------------------<SelectLine>
/**
  * \brief Fait une selection quelconque sur les lignes
  * \param  ssName      Pointeur sur la selection
  * \param  slayerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsLine(
	ads_name&         ssName,
	const AcString&   layerName = "" );

/**
  * \brief Selectionne un seul line dans le dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsOneLine(
	ads_name&         ssName,
	const AcString&   sLayerName = "" );

/**
  * \brief  Fait une selection de toutes lignes
  * \param  ssName      Pointeur sur la selection
  * \param  slayerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsAllLine(
	ads_name&       ssName,
	const AcString& sLayer = "" );


//---------------------------------------------------------------------------------<Select>
/**
  * \brief  Recuperer la ligne numero N dans la Selection
  * \param  ssName      Pointeur sur la selection
  * \param  iObject     Numero de l'objet a recuperer
  * \param  opMode      Mode de lecture de l'objet
  * \return Pointeur sur un objet autocad
  */
AcDbLine* getLineFromSs(
	const ads_name&             ssName,
	const long&           iObject     =   0,
	const AcDb::OpenMode& opMode      =   AcDb::kForRead );


/**
  * \brief  Renvoie la ligne numero N dans une selection, ouverture en lecture
  * \param  ssName  Pointeur sur la selection
  * \param  iObject Numero de l'objet a recuperer
  * \return Pointeur sur un objet autocad
  */
AcDbLine* readLineFromSs(
	const ads_name&         ssName,
	const long&       iObject     = 0 );

/**
  * \brief Renvoie la ligne numero N dans une selection, ouverture en ecriture
  * \param ssName   Pointeur sur la selection
  * \param iObject  Numero de l'objet a recuperer
  * \return Pointeur sur un objet autocad
  */
AcDbLine* writeLineFromSs(
	const ads_name&         ssName,
	const long        iObject     = 0 );


//---------------------------------------------------------------------------------<Draw>
/**
  * \brief  Dessiner deux Points en ligne
  * \param  ptStart Point de depart de la ligne
  * \param  ptEnd   Point d'arrive de la ligne
  * \return Void car la fonction ne fait que dessiner
  */
void drawLine(
	const AcGePoint3d& ptStart,
	const AcGePoint3d& ptEnd );


//---------------------------------------------------------------------------------<Draw>
/**
  * \brief  Dessiner deux Points en ligne
  * \param  ptStart Point de depart de la ligne
  * \param  ptEnd   Point d'arrive de la ligne
  * \param  blockTable  Pointeur sur la base de donnees
  * \param  entity  Pointeur sur l'entite dont on doit copier les proprietes
  * \return Void car la fonction ne fait que dessiner
  */
void drawLine(
	const AcGePoint3d&    ptStart,
	const AcGePoint3d&    ptEnd,
	AcDbBlockTableRecord* blocktable,
	AcDbEntity*           entity = NULL );


//---------------------------------------------------------------------------------<Draw>
/**
  * \brief  Dessiner deux Points en ligne
  * \param  ptStart Point de depart de la ligne
  * \param  ptEnd   Point d'arrive de la ligne
  * \param  blockTable  Pointeur sur la base de donnees
  * \param  entity  Pointeur sur l'entite dont on doit copier les proprietes
  * \return Void car la fonction ne fait que dessiner
  */
void drawLine(
	const AcGePoint2d&    ptStart,
	const AcGePoint2d&    ptEnd,
	AcDbBlockTableRecord* blocktable,
	AcDbEntity*           entity = NULL );

/**
  * \brief	Recuperer la ligne former par deux point
  * \param  ptStart Point de depart de la ligne
  * \param  ptEnd   Point d'arrive de la ligne
  * \return le pointeur de la ligne
  */
AcDbLine* insertLine(
	const AcGePoint3d& ptStart,
	const AcGePoint3d& ptEnd
);