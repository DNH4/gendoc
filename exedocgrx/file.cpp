#pragma once
#include <Windows.h>
#include "file.h"
#include "acString.h"
#include "userInput.h"
#include "print.h"

int main()
{
    return 0;
}

AcString getCurrentFileName()
{
    return curDoc()->fileName();
}

AcString getCurrentFileFolder()
{
    return getFileDir( getCurrentFileName() );
}


AcString askForFilePath(
    const bool&     bToReadOnly,
    const AcString& sExtension,
    const AcString& sTitle,
    const AcString& sDefaulFolder )
{
    //Si Filedia = 0
    if( !getBoolVariable( "FILEDIA" ) )
    {
        //Demander a l'utilisateur de saisir le texte en ligne de commande
        return askString( "\n" + sTitle + " :" );
    }
    
    //Declarer le resbuf
    struct resbuf* rbResult;
    rbResult = acutNewRb( RTSTR );
    
    //Ouvrir la boite de dialog
    int iResult = acedGetFileD(
            sTitle,
            sDefaulFolder.isEmpty() ? getCurrentFileFolder() + "\\" : sDefaulFolder,
            sExtension,
            bToReadOnly ? 0 : 1,
            rbResult );
            
    //Verifier la valeur
    if( RTNORM != iResult )
    {
        //Sortir
        acutRelRb( rbResult );
        return "";
    }
    
    //Declarer le string a recuperer
    TCHAR* tcResult;
    
    //Recuperer la valeur
    if( Acad::eOk != acutNewString( rbResult->resval.rstring, tcResult ) )
    {
        //Sortir
        acutRelRb( rbResult );
        return "";
    }
    
    //Effacer le resbuf
    acutRelRb( rbResult );
    
    //Declarer le string a recuperer
    AcString sResult = tcResult;
    
    //Effacer le TCHAR
    acutDelString( tcResult );
    
    //Sortir
    return sResult;
}


AcString askToReadFilePath(
    const AcString& sExtension,
    const AcString& sTitle,
    const AcString& sDefaulFolder )
{
    return askForFilePath(
            true,
            sExtension,
            sTitle.isEmpty() ? "Selectionner un fichier " + sExtension : sTitle,
            sDefaulFolder );
}


AcString askToWriteFilePath(
    const AcString& sExtension,
    const AcString& sTitle,
    const AcString& sDefaulFolder )
{
    return askForFilePath(
            false,
            sExtension,
            sTitle.isEmpty() ? "Selectionner un fichier " + sExtension : sTitle,
            sDefaulFolder );
}


AcString getFileDir(
    const AcString path )
{
    AcString l_sDir = "\\";
    l_sDir = path.substr( path.findOneOfRev( l_sDir ) );
    return l_sDir;
}




AcString getFileName(
    const AcString path )
{
    AcString l_sName = "\\";
    int l_iDir = path.findOneOfRev( l_sName ) + 1;
    l_sName = ".";
    int l_iExt = path.findOneOfRev( l_sName );
    l_sName = path.substr( l_iDir, l_iExt - l_iDir );
    return l_sName;
}




AcString getFileExt(
    const AcString path )
{
    AcString l_sExt = ".";
    l_sExt = path.substrRev( path.length() - path.findOneOfRev( l_sExt ) - 1 );
    return l_sExt;
}


bool splitPath(
    const AcString& path,
    AcString& dir,
    AcString& name,
    AcString& ext )
{
    dir = _T( "\\" );
    int l_iDir = path.findOneOfRev( dir );
    ext = _T( "." );
    int l_iExt = path.findOneOfRev( ext );
    dir  = path.substr( l_iDir );
    name = path.substr( l_iDir + 1, l_iExt - l_iDir - 1 );
    ext  = path.substrRev( path.length() - l_iExt - 1 );
    return !dir.isEmpty() && !name.isEmpty() && !ext.isEmpty();
}



bool isFileExisting( const string& path )
{
    //Ouvrir le fichier
    ifstream file( path );
    
    //Tester s'il y a des erreurs � l'ouverture du fichier
    if( file.fail() )
    {
        file.close();
        return false;
    }
    
    return true;
}


bool isFileExisting( const AcString& path )
{
    //Ouvrir le fichier
    ifstream file( acStrToStr( path ) );
    
    //Tester s'il y a des erreurs à l'ouverture du fichier
    if( file.fail() )
    {
        file.close();
        return false;
    }
    
    return true;
}

bool createFolder( string newFolder )
{
    std::wstring stemp = std::wstring( newFolder.begin(), newFolder.end() );
    return CreateDirectory( stemp.c_str(), NULL );
}

bool copyFile( string oldPath, string newPath )
{
    std::wstring oldTemp = std::wstring( oldPath.begin(), oldPath.end() );
    std::wstring newTemp = std::wstring( newPath.begin(), newPath.end() );
    return CopyFile( oldTemp.c_str(), newTemp.c_str(), true );
}

int getNumberOfLines(string path) {
	// Ouverture du fichier
	std::ifstream input(path);
	std::string line;

	// Si l'ouverture s'est mal passée
	if (input.fail())
	{
		input.close();
		return 0;
	}

	// Comptage du nombre de lignes
	int nbOfLines = 0;

	while (getline(input, line)) {
		nbOfLines += 1;
	}

	input.close();
	return nbOfLines;
}



// ------------------------------------------ M�thodes de la classe FileReader

// Constructeur par d�faut
FileReader::FileReader()
{
    //separateur
    m_separateur = "\t";
    
    //initialiser le variable activeline number a 0
    m_activeLineNumber = 0;
    
    // On initialise le nombre de lignes du fichier � 0
    m_lineNumber = 0;
    
}


// Constructeur
FileReader::FileReader(
    const string& filePath,
    const string& separateur )
{
    // Initialiser la ligne active � 0
    m_activeLineNumber = 0;
    
    // On initialise le nombre de lignes du fichier � 0
    m_lineNumber = 0;
    
    // Initialiser le chemin d'acc�s du fichier et le s�parateur
    m_filePath = filePath;
    m_separateur = separateur;
    string row;
    
    // Ouvrir le fichier
    m_file.open( m_filePath );
    
    // S'il y a pas d'erreur d'ouverture de fichier
    if( isFileExisting( m_filePath ) )
    {
        // On va � la premi�re ligne
        getline( m_file, row );
        
        // On a lu la premi�re ligne
        m_activeLineNumber++;
        
        // On r�cup�re l'ent�te
        m_entete = splitString( row, separateur );
        
        // On initialise la ligne courante
        m_activeLine = m_entete;
        
        // On initialise le nombre de colonnes
        m_columnNumber = m_entete.size();
    }
}

//Surcharge type AcString
FileReader::FileReader(
    const AcString& filePath,
    const AcString& separateur )
{
    // Initialiser la ligne active � 0
    m_activeLineNumber = 0;
    
    // On initialise le nombre de lignes du fichier � 0
    m_lineNumber = 0;
    
    // Initialiser le chemin d'acc�s du fichier et le s�parateur
    m_filePath = acStrToStr( filePath );
    m_separateur = acStrToStr( separateur );
    string row;
    
    // Ouvrir le fichier
    m_file.open( m_filePath );
    
    // S'il y a pas d'erreur d'ouverture de fichier
    if( isFileExisting( m_filePath ) )
    {
        // On va � la premi�re ligne
        getline( m_file, row );
        
        // On a lu la premi�re ligne
        m_activeLineNumber++;
        
        // On r�cup�re l'ent�te
        m_entete = splitString( row, acStrToStr( separateur ) );
        
        // On initialise la ligne courante
        m_activeLine = m_entete;
        
        // On initialise le nombre de colonnes
        m_columnNumber = m_entete.size();
    }
}


//Destructeur
FileReader::~FileReader()
{
    m_file.close();
}


bool FileReader::init(
    string filepath,
    string separateur )
{
    // Appeler la constructeur
    FileReader( filepath, separateur );
    
    // Si il y avait une erreur pendant l'ouverture du fichier
    if( m_file.fail() )
        return false;
    else
        return true;
}

/*//Surcharge type AcString
bool FileReader::init(
    AcString filepath,
    AcString separateur )
{
    // Appeler la constructeur
    FileReader( acStrToStr(filepath), acStrToStr( separateur ) );

    // Si il y avait une erreur pendant l'ouverture du fichier
    if( m_file.fail() )
        return false;
    else
        return true;
}*/


// Connaitre si c'est possible de lire la ligne d'apr�s
bool FileReader::readNextLine()
{
    string row;
    
    // Si on peut ouvrir le fichier et qu'il existe une ligne d'apr�s
    if( m_activeLineNumber != m_lineNumber && getline( m_file, row ) )
    {
        m_activeLine = splitString( row, m_separateur );
        m_activeLineNumber++;
        return true;
    }
    
    return false;
}


//Recuperer la ligne courant
vector <string> FileReader::getLine()
{
    return m_activeLine;
}


//Recuperer la ligne courant
vector < AcString > FileReader::getLineAc()
{
    vector< AcString > vec;
    int sizeVec = m_activeLine.size();
    
    for( int i = 0; i < sizeVec; i++ )
        vec.push_back( strToAcStr( m_activeLine[ i ] ) );
        
    return vec;
}


vector <string> FileReader::getLine( const int& lineNumber )
{
    string row;
    
    // Tester la ligne active et le nombre de ligne
    if( m_activeLineNumber <= lineNumber )
    {
        //Boucler jusqu'a la ligne
        for( int i = m_activeLineNumber; i <= lineNumber; i++ )
        {
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;
            
            if( lineNumber == m_activeLineNumber )
                return m_activeLine;
        }
    }
    
    else
    {
        //Fermer le fichier et le reouvrir
        m_file.close();
        ifstream m_file( m_filePath );
        m_activeLineNumber = 0;
        
        for( int i = 0 ; i <= lineNumber; i++ )
        {
            getline( m_file, row );
            
            if( lineNumber == m_activeLineNumber )
                return splitString( row, m_separateur );
                
            m_activeLineNumber++;
        }
    }
}

bool FileReader::getLine( string head, vector<string>& line )
{
    // Fermer le fichier et le reouvrir
    m_file.close();
    m_file.open( m_filePath );
    m_activeLineNumber = 0;
    
    string row;
    
    // On parcourt le fichier depuis le d�but et tant qu'on n'a pas atteint la fin
    while( !m_file.eof() )
    {
        // On r�cup�re la ligne suivante
        getline( m_file, row );
        m_activeLineNumber++;
        m_activeLine = splitString( row, m_separateur );
        
        // On v�rifie qu'elle n'est pas vide
        if( m_activeLine.size() > 0 )
        {
            // Si on trouve l'ent�te de la ligne, on renvoie true
            if( m_activeLine[0] == head )
            {
                line = m_activeLine;
                return true;
            }
        }
    }
    
    return false;
}


bool isStringInLine( const string& val )
{
    // A DEVELOPPER
    return true;
}



//Recuperer la valeur de la case d'un cellule de la ligne active
bool FileReader::getValue(
    const int& colomnNumber,
    string& valueResult )
{
    if( colomnNumber <= m_columnNumber + 1 )
    {
        valueResult = m_activeLine[colomnNumber + 1];
        return true;
    }
    
    return false;
}


bool FileReader::getValue(
    const string& colomnName,
    string& valueResult )
{
    // Creer une iterateur it pour le vecteur de string
    vector< string >::iterator it;
    
    // Creer un iterateur sur l'entete
    it = find( m_entete.begin(), m_entete.end(), colomnName );
    
    bool result = true;
    
    // Si la valeur est bien pr�sente ans l'ent�te
    if( it != m_entete.end() )
        valueResult = m_activeLine[ distance( m_entete.begin(), it ) ];
        
    // Si la valeur n'appartient pas � l'ent�te
    else
        result = false;
        
    // Renvoyer True si tout s'est bien pass�, false sinon
    return result;
}

void FileReader::setActiveLineNumber( int value )
{
    m_activeLineNumber = value;
}


bool FileReader::getValue(
    const int& lineNumber,
    const int& columnNumber,
    string& valueResult )
{
    //initialise la ligne
    string row;
    
    //si le nombre le numero du colone est inferieur au nombre de colonne
    if( columnNumber > m_columnNumber )
        return false;
        
    // Si la ligne active est �gale � la ligne demand�e
    if( lineNumber == m_activeLineNumber )
    {
        valueResult = m_activeLine[columnNumber];
        return true;
    }
    
    //et que le numero de la ligne est inferieur au numero de la ligne active
    if( m_activeLineNumber <= lineNumber )
    {
    
        // Boucler sur la ligne
        for( int i = m_activeLineNumber; i < lineNumber; i++ )
        {
            getline( m_file, row );
            m_activeLineNumber++;
        }
        
        // On est arriv� sur la ligne demand�e
        m_activeLine = splitString( row, m_separateur );
        valueResult = m_activeLine[columnNumber];
        return true;
    }
    
    else if( lineNumber < m_activeLineNumber )
    {
        //Fermer le fichier et le reouvrir
        m_file.close();
        m_file.open( m_filePath );
        
        // On enl�ve la ligne d'ent�te
        getline( m_file, row );
        m_activeLineNumber = 1;
        
        // Boucler sur toute la ligne
        for( int i = 1; i < lineNumber; i++ )
        {
            getline( m_file, row );
            m_activeLineNumber++;
        }
        
        // On est arriv� sur la ligne demand�e
        m_activeLine = splitString( row, m_separateur );
        valueResult = m_activeLine[columnNumber];
        
        return true;
    }
    
    // Si non returner false et ne faire rien
    return false;
}


bool FileReader::getValue(
    const int& lineNumber,
    const string& columnName,
    string& valueResult )
{
    // Creer une iterateur it pour le vecteur de string
    vector< string >::iterator it;
    
    // Creer un iterateur sur l'entete
    it = find( m_entete.begin(), m_entete.end(), columnName );
    
    string row;
    
    // Tester la ligne active et le nombre de ligne et le nom de colone existe
    if( m_activeLineNumber <= lineNumber && it != m_entete.end() )
    {
        //boucler jusqu'a la ligne
        for( int i = m_activeLineNumber; i < lineNumber; i++ )
        {
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;
            
            if( lineNumber == m_activeLineNumber )
            {
                valueResult = m_activeLine[distance( m_entete.begin(), it )];
                return true;
            }
        }
    }
    
    else if( it != m_entete.end() && m_activeLineNumber > lineNumber )
    {
    
        //boucler jusqu'a la ligne
        for( int i = m_activeLineNumber; i < lineNumber; i++ )
        {
        
            //Fermer le fichier et le reouvrir
            m_file.close();
            ifstream m_file( m_filePath );
            m_activeLineNumber = 0;
            
            //Recuperer les lignes
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;
            
            if( lineNumber == m_activeLineNumber )
            {
                valueResult = m_activeLine[distance( m_entete.begin(), it )];
                return true;
            }
        }
        
    }
    
    // Sinon retourner false
    return false;
}


bool FileReader::getValue(
    const int& columnNumber,
    double& valueResult )
{
    if( columnNumber <= m_columnNumber + 1 )
    {
        valueResult = stod( m_activeLine[columnNumber + 1] );
        return true;
    }
    
    return false;
}


bool FileReader::getValue(
    const string colomnName,
    double& valueResult )
{

    // Creer une iterateur it pour le vecteur de string
    vector< string >::iterator it;
    
    // Creer un iterateur sur l'entete
    it = find( m_entete.begin(), m_entete.end(), colomnName );
    
    bool result = true;
    
    // Si la valeur est bien pr�sente ans l'ent�te
    if( it != m_entete.end() )
        valueResult = stod( m_activeLine[ distance( m_entete.begin(), it ) ] );
        
    // Si la valeur n'appartient pas � l'ent�te
    else
        result = false;
        
    // Renvoyer True si tout s'est bien pass�, false sinon
    return result;
}

/*//Surcharge
bool FileReader::getValue(
    const AcString colomnName,
    double& valueResult )
{

    // Creer une iterateur it pour le vecteur de string
    vector< string >::iterator it;

    // Creer un iterateur sur l'entete
    it = find( m_entete.begin(), m_entete.end(), acStrToStr( colomnName ) );

    bool result = true;

    // Si la valeur est bien pr�sente ans l'ent�te
    if( it != m_entete.end() )
        valueResult = stod( m_activeLine[ distance( m_entete.begin(), it ) ] );

    // Si la valeur n'appartient pas � l'ent�te
    else
        result = false;

    // Renvoyer True si tout s'est bien pass�, false sinon
    return result;
}*/



bool FileReader::getValue(
    const int& lineNumber,
    const string& columnName,
    double& valueResult )
{
    // Creer une iterateur it pour le vecteur de string
    vector< string >::iterator it;
    
    // Creer un iterateur sur l'entete
    it = find( m_entete.begin(), m_entete.end(), columnName );
    
    string row;
    
    // Tester la ligne active et le nombre de ligne
    if( m_activeLineNumber <= lineNumber )
    {
        //boucler jusqu'a la ligne
        for( int i = m_activeLineNumber; i < lineNumber; i++ )
        {
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;
            
            if( lineNumber == m_activeLineNumber )
            {
                valueResult = stod( m_activeLine[distance( m_entete.begin(), it )] );
                return true;
            }
        }
    }
    
    else if( it != m_entete.end() && m_activeLineNumber > lineNumber )
    {
    
        //boucler jusqu'a la ligne
        for( int i = m_activeLineNumber; i < lineNumber; i++ )
        {
        
            //Fermer le fichier et le reouvrir
            m_file.close();
            ifstream m_file( m_filePath );
            m_activeLineNumber = 0;
            
            //Recuperer les lignes
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;
            
            if( lineNumber == m_activeLineNumber )
            {
                valueResult = stod( m_activeLine[distance( m_entete.begin(), it )] );
                return true;
            }
        }
        
    }
    
    // Sinon retourner false
    return false;
}


/*//Surcharge
bool FileReader::getValue(
    const int& lineNumber,
    const AcString& columnName,
    double& valueResult )
{

    // Creer une iterateur it pour le vecteur de string
    vector< string >::iterator it;

    // Creer un iterateur sur l'entete
    it = find( m_entete.begin(), m_entete.end(), acStrToStr( columnName ) );

    string row;

    // Tester la ligne active et le nombre de ligne
    if( m_activeLineNumber <= lineNumber )
    {
        //boucler jusqu'a la ligne
        for( int i = m_activeLineNumber; i < lineNumber; i++ )
        {
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;

            if( lineNumber == m_activeLineNumber )
            {
                valueResult = stod( m_activeLine[distance( m_entete.begin(), it )] );
                return true;
            }
        }
    }

    else if( it != m_entete.end() && m_activeLineNumber > lineNumber )
    {

        //boucler jusqu'a la ligne
        for( int i = m_activeLineNumber; i < lineNumber; i++ )
        {

            //Fermer le fichier et le reouvrir
            m_file.close();
            ifstream m_file( m_filePath );
            m_activeLineNumber = 0;

            //Recuperer les lignes
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;

            if( lineNumber == m_activeLineNumber )
            {
                valueResult = stod( m_activeLine[distance( m_entete.begin(), it )] );
                return true;
            }
        }

    }
    // Sinon retourner false
    return false;
}*/



bool FileReader::getValue(
    const int& lineNumber,
    const int& columnNumber,
    double& valueResult )
{
    //initialise la ligne
    string row;
    
    //si le nombre le numero du colone est inferieur au nombre de colonne
    //et que le numero de la ligne est inferieur au numero de la ligne active
    if( columnNumber <= m_columnNumber && lineNumber <= m_activeLineNumber )
    {
        // Boucler sur la ligne
        for( int i = m_activeLineNumber; i <= lineNumber; i++ )
        {
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;
            
            if( lineNumber == m_activeLineNumber )
                valueResult = stod( m_activeLine[columnNumber] );
        }
        
        return true;
    }
    
    else if( columnNumber <= m_columnNumber && lineNumber > m_activeLineNumber )
    {
        // Boucler sur toute la ligne
        for( int i = 0; i < lineNumber; i++ )
        {
            //Fermer le fichier et le reouvrir
            m_file.close();
            ifstream m_file( m_filePath );
            m_activeLineNumber = 0;
            
            getline( m_file, row );
            m_activeLine = splitString( row, m_separateur );
            m_activeLineNumber++;
            
            if( lineNumber == m_activeLineNumber )
                valueResult = stod( m_activeLine[columnNumber] );
        }
        
        return true;
    }
    
    // Si non returner false et ne faire rien
    return false;
}


//Recuperer la colone enti�re
bool FileReader::getColonne(
    const int& colomnNumber,
    vector<string>& valueResult )
{
    //Rouvrir le fichier pour le reparcourir
    m_file.close();
    m_file.open( m_filePath );
    
    valueResult.resize( 0 );
    
    string row;
    
    //Faire un getline pour exclure l'entete
    m_activeLineNumber = 0;
    getline( m_file, row );
    
    //Parcourir la ligne entiere
    while( getline( m_file, row ) )
    {
        m_activeLineNumber ++;
        //Recuperer la ligne courante
        m_activeLine = splitString( row, m_separateur );
        
        //Recuperer tout les donne de cette colone sauf l'entete
        valueResult.push_back( m_activeLine[colomnNumber - 1] );
    }
    
    m_lineNumber = m_activeLineNumber;
    return true;
}


bool FileReader::getColonne(
    const string& columnName,
    vector <string>& valueResult )
{
    //rouvrir le fichier pour le reparcourir
    m_file.close();
    m_file.open( m_filePath );
    
    valueResult.resize( 0 );
    
    string row;
    
    //FAire un getline pour exclure l'entete
    m_activeLineNumber = 0;
    getline( m_file, row );
    
    if( row == "" )
        return false;
        
    // Creer une iterateur it pour le vecteur de string
    vector< string >::iterator it;
    
    // Creer un iterateur sur l'entete
    it = find( m_entete.begin(), m_entete.end(), columnName );
    
    if( it == m_entete.end() )
        return false;
        
    int index = distance( m_entete.begin(), it );
    
    //Parcourir le fichier entiere
    while( getline( m_file, row ) )
    {
        m_activeLineNumber ++;
        
        //Recuperer la ligne courante
        m_activeLine = splitString( row, m_separateur );
        
        //recuperer tout les donne de cette colone sauf l'entete
        valueResult.push_back( m_activeLine[ index ] );
    }
    
    m_lineNumber = m_activeLineNumber;
    return true;
}


bool FileReader::getColonne(
    const int& colomnNumber,
    vector<double>& valueResult )
{
    //rouvrir le fichier pour le reparcourir
    m_file.close();
    m_file.open( m_filePath );
    
    string row;
    
    valueResult.resize( 0 );
    
    //FAire un getline pour exclure l'entete
    m_activeLineNumber = 0;
    getline( m_file, row );
    
    //Parcourir la ligne entiere
    while( getline( m_file, row ) )
    {
        m_activeLineNumber ++;
        //Recuperer la ligne courante
        m_activeLine = splitString( row, m_separateur );
        
        //recuperer tout les donne de cette colone sauf l'entete
        valueResult.push_back( stod( m_activeLine[colomnNumber - 1] ) );
    }
    
    m_lineNumber = m_activeLineNumber;
    return true;
}


bool FileReader::getColonne(
    const string& columnName,
    vector <double>& valueResult )
{
    //Rouvrir le fichier pour le reparcourir
    m_file.close();
    m_file.open( m_filePath );
    
    string row;
    
    valueResult.resize( 0 );
    
    //Faire un getline pour exclure l'entete
    m_activeLineNumber = 0;
    getline( m_file, row );
    
    // Creer une iterateur it pour le vecteur de string
    vector< string >::iterator it;
    
    // Creer un iterateur sur l'entete
    it = find( m_entete.begin(), m_entete.end(), columnName );
    
    //Parcourir le fichier entiere
    while( getline( m_file, row ) )
    {
        m_activeLineNumber ++;
        //Recuperer la ligne courante
        m_activeLine = splitString( row, m_separateur );
        
        //recuperer tout les donne de cette colone sauf l'entete
        valueResult.push_back( stod( m_activeLine[distance( m_entete.begin(), it )] ) );
    }
    
    m_lineNumber = m_activeLineNumber;
    return true;
}


int FileReader::getLineNumber()
{
    //Si nombre de ligne est non vide
    if( m_lineNumber )
        return m_lineNumber;
        
    string row;
    
    //Parcourir la ligne enti�re et incr�menter le nombre de ligne
    while( getline( m_file, row ) )
        m_lineNumber++;
        
    //Fermer et ouvrir le fichier pour le re-parcourir
    m_file.close();
    m_file.open( m_filePath );
    
    //Boucle pour arriver au num�ro de la ligne courante
    for( int i = 0; i < m_activeLineNumber; i++ )
        getline( m_file, row );
        
    //Retourne le nombre de ligne dans le fichier
    return m_lineNumber;
}


//Get-Setter pour le chemin du fichier
AcString FileReader::getFilePath()
{
    //Retourne le chemin du fichier en cours
    return strToAcStr( m_filePath );
}

void FileReader::setFilePath( const AcString& filePath )
{
    //Setter le chemin vers le fichier
    m_filePath = filePath;
}

//Get-Setter pour le separateur;
AcString FileReader::getSeparateur()
{
    //Retourne le separateur en cours
    return strToAcStr( m_separateur );
}

void FileReader::setSeparateur( const AcString& sep )
{
    //Setter le separateur
    m_separateur = acStrToStr( sep );
}

int FileReader::getActiveLineNumber()
{
    //Retourne la ligne active
    return m_activeLineNumber;
}

int FileReader::getColumnNumber()
{
    //Retourne le numéro de colonne
    return m_columnNumber;
}

void FileReader::setColumnNumber( const int& value )
{
    //Setter le numero de la colonne
    m_columnNumber = value;
}

void FileReader::setLineNumber( const int& value )
{
    //Setter le numero de la ligne
    m_lineNumber = value;
}

vector<string> getFileNamesInFolder(const AcString& folderFiles)
{
	//Resultat
	vector<string> res;

	//Structure dirent
	struct dirent* entry;
	DIR* dir = opendir(acStrToStr(folderFiles).c_str());

	//Verifier
	if (dir == NULL)
		return res;

	//Boucle pour recuperer les noms des fichiers dans le dossier
	while ((entry = readdir(dir)) != NULL)
		res.push_back(entry->d_name);

	//Fermer le dossier
	closedir(dir);

	//Retourner le resultat
	return res;
}