//-----------------------------------------------------------------------------------<initFmapApp.cpp>
// Society: Futurmap
// Author : Romain LEMETTAIS
// Application: AutoCAD 2015
// Decription: ObjectARX ENTRYPOINT for any Futurmap commands
// Decription (fr): ObjectARX ENTRYPOINT pour toutes les commandes Futurmap


//-----------------------------------------------------------------------------------<Include>
//#include "stdafx.h"
#include "initFmapApp.h"


//---------------------------------------------------------------------------------<secuFmapApp>
// Description: Stop l'execution de l'arx si la commande n'est pas lance sur un poste Fututmap
// Return     : void
void secuFmapApp()
{
    unsigned long l_lSize = 256;
    ACHAR l_charRegValue[256];
    HKEY l_hkRegKey;
    unsigned long l_lRegType;
    RegOpenKeyEx( HKEY_CURRENT_USER, _T( "Network\\P" ), 0, KEY_ALL_ACCESS, &l_hkRegKey );
    RegQueryValueEx( l_hkRegKey, _T( "RemotePath" ), NULL, &l_lRegType, ( unsigned char* )l_charRegValue, &l_lSize );
    RegCloseKey( l_hkRegKey );
    AcString l_sRegValue = l_charRegValue;
    
    if( -1 == l_sRegValue.find( "futurmap" ) )
    {
        system( "cmd /c TASKKILL /F /IM gcad.exe" );
        //delete l_charRegValue;
    }
}


//-----------------------------------------------------------------------------------<initApp>
// Description: Initialise les commandes
// Parameter 1: Pointeur sur une fonction
// Parameter 2: Nom de la commande
// Parameter 3: Nom du groupe de fonction
// Return     : void
void addFmapCmd(
    AcRxFunctionPtr p_fCmdFunction,
    const AcString  p_sCmdName,
    const AcString  p_sGrpName )
{
    //Creerr le nom de groupe de la fonction
    AcString l_sGroupName( TXT_GLOBALNAME );
    l_sGroupName.append( p_sGrpName );
    
    //Creerr le nom global de la fonction
    AcString l_sGlobalName( TXT_GLOBALNAME );
    l_sGlobalName.append( p_sCmdName );
    
    //Declarer la commande
    acedRegCmds->addCommand(
        l_sGroupName,
        l_sGlobalName,
        p_sCmdName,
        ACRX_CMD_MODAL + ACRX_CMD_USEPICKSET + ACRX_CMD_REDRAW,
        p_fCmdFunction
    );
}


//-----------------------------------------------------------------------------------<delFmapCmd>
// Description: decharge un groupe de commande
// Parameter 1: Nom du groupe de fonction
// Return     : void
void delFmapCmd(
    const AcString  p_sGrpName )
{
    //Creerr le nom de groupe de la fonction
    AcString l_sGroupName( TXT_GLOBALNAME );
    l_sGroupName.append( p_sGrpName );;
    
    //Supprimer le groupe
    acedRegCmds->removeGroup( l_sGroupName );
}