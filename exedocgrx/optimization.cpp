#include "optimization.h"

bool generateFmapMat( ads_name& ssEntity,
    FmapMat& matrix,
    const int& step )
{
    //entitySize : nombre d'entit�s dans la selection
    long entitySize = getSsLength( ssEntity );
    
    //tester entitySize
    if( entitySize < 1 )
        return false;
        
    //affecter le pas � la matrice
    matrix.step = step;
    
    
    //AcdDbExtents : pour recuperer l'extents de l'entit� courante
    AcDbExtents tExtents;
    
    //tMin : coordonn�e min de la matrice
    double tMinx = DBL_MAX;
    double tMiny = DBL_MAX;
    
    //tMax : coordonn�e max de la matrice
    double tMaxx = .0;
    double tMaxy = .0;
    
    
    // parcourir les entiti�s
    for( int n{ 0 }; n < entitySize; n++ )
    {
        //d�clarer une entit�
        AcDbEntity* tEntity;
        
        
        //recuperer la n-eme entit�
        tEntity = getEntityFromSs( ssEntity, n, AcDb::kForRead );
        
        //verifier tEntity
        if( !tEntity )
            continue;
            
        //recuperer l'extents de l'entit�
        tEntity->getGeomExtents( tExtents );
        
        //recuperer les points max et min de l'entit�
        AcGePoint3d pExtMin = tExtents.minPoint();
        AcGePoint3d pExtMax = tExtents.maxPoint();
        
        //recuperer l'abscisse min
        if( pExtMin.x < tMinx )
            tMinx = pExtMin.x;
            
        //recuperer l'ordonn�e min
        if( pExtMin.y < tMiny )
            tMiny = pExtMin.y;
            
        //recuperer l'abscisse max
        if( pExtMax.x > tMaxx )
            tMaxx = pExtMax.x;
            
        //recuperer l'ordonn�e max
        if( pExtMax.y > tMaxy )
            tMaxy = pExtMax.y;
            
        //liberer la m�moire
        tEntity->close();
    }
    
    double stepd = ( double )step;
    
    //nombre de cellules en x et y
    int cellnbx = 0;
    int cellnby = 0;
    bool changeTmaxx = false;
    bool changeTmaxy = false;
    
    //recuperer le nombre de cellules suivant chaque axe x et y
    if( std::fmod( tMaxx - tMinx, stepd ) != 0 )
    {
        //recuperer le nombre provisoire de cellules sur x
        cellnbx = ( int )( ( tMaxx - tMinx ) / stepd );
        
        //recuperer le nombre multiple de step le plus proche
        double rest = ( tMaxx - tMinx ) - ( double )( cellnbx * step );
        
        //calculer la reste a ajouter pour cette derni�re cellule
        double toAdd = stepd - rest;
        
        //ajouter cette rest
        tMaxx += toAdd;
        
        //mettre a jour le booleen
        changeTmaxx = true;
        
    }
    
    //recuperer le nombre de cellules suivant chaque axe x et y
    if( std::fmod( tMaxy - tMiny, stepd ) != 0 )
    {
        //recuperer le nombre provisoire de cellules sur x
        cellnby = ( int )( ( tMaxy - tMiny ) / stepd );
        
        //recuperer le nombre multiple de step le plus proche
        double rest = ( tMaxy - tMiny ) - ( double )( cellnby * step );
        
        //calculer la reste a ajouter pour cette derni�re cellule
        double toAdd = stepd - rest;
        
        //ajouter cette rest
        tMaxy += toAdd;
        
        //mettre a jour le booleen
        changeTmaxy = true;
    }
    
    //former le point max et min de la structure
    matrix.min.x = tMinx;
    matrix.min.y = tMiny;
    matrix.max.x = tMaxx;
    matrix.max.y = tMaxy;
    
    //mettre a jour les nombre de cellules dans chaque axe
    if( changeTmaxx )
        cellnbx++;
        
    if( changeTmaxy )
        cellnby++;
        
    //cr�er le vecteur de cellule
    AcGeVector3d vecCell = AcGeVector3d( step, step, 0 );
    
    //les points min et max de la matrice
    AcGePoint3d minMtx = matrix.min;
    AcGePoint3d maxMtx = matrix.max;
    
    //un vecteur temporaire
    vector<vector<FmapCell>> vecTemp;
    
    //boucler sur le step
    for( int y = 0; y < cellnby; y++ )
    {
        //cr�er un vecteur de fmapcell
        vector<FmapCell> celluleY;
        AcGePoint3d ptMin;
        AcGePoint3d ptMax;
        
        for( int x = 0; x < cellnbx; x++ )
        {
            //recuperer l'extents de la cellule
            ptMin = minMtx;
            ptMax = ptMin + vecCell;
            
            //cr�er un fmapcell
            FmapCell fm;
            
            //affecter les indices
            fm.y = y;
            fm.x = x;
            
            //recuperer les extents
            fm.min = ptMin;
            fm.max = ptMax;
            
            //inserer les informations
            celluleY.emplace_back( fm );
            
            //deplacer minMtx au minPoint de la cellule suivante
            minMtx.x = ptMax.x;
        }
        
        //inserer les informations
        vecTemp.emplace_back( celluleY );
        
        //retourner au minPoint de d�part
        minMtx = matrix.min;
        minMtx.y = ptMax.y;
    }
    
    //recuperer le vecteur
    matrix.objects = vecTemp;
    
    
    // parcourir les entiti�s
    for( int n{ 0 }; n < entitySize; n++ )
    {
        //AcDbEntity : pour recuperer l'entit� num�ro n
        AcDbEntity* tEntity;
        
        //recuperer la n-eme entit�
        tEntity = getEntityFromSs( ssEntity, n, AcDb::kForRead );
        
        //verifier tEntity
        if( !tEntity )
            continue;
            
        //recuperer l'extents de l'entit�
        tEntity->getGeomExtents( tExtents );
        
        //recuperer les indices des cellules dans lesquelles on inserera l'entit�
        int is = 0;
        int js = 0;
        int ie = 0;
        int je = 0;
        
        
        //calculer is et js
        is = ( int )( ( tExtents.minPoint().y - matrix.min.y ) / step ) - 1;
        js = ( int )( ( tExtents.minPoint().x - matrix.min.x ) / step ) - 1;
        
        //safeguard
        if( is < 0 )
            is = 0;
            
        //safeguard
        if( js < 0 )
            js = 0;
            
        //calculer ie et je
        ie = ( int )( ( tExtents.maxPoint().y - matrix.min.y ) / step ) - 1;
        je = ( int )( ( tExtents.maxPoint().x - matrix.min.x ) / step ) - 1;
        
        for( int v = is; v <= ie; v++ )
        {
            for( int u = js; u <= je; u++ )
            {
                //inserer l'object id de l'entit�
                matrix.objects[v][u].objects.push_back( tEntity->id() );
            }
        }
        
        //liberer la m�moire
        tEntity->close();
    }
    
}

bool getObjectsFromMatrix( FmapMat& matrix,
    vector<AcDbObjectId>& objects,
    AcDbExtents& tExtents )
{

    //verifier les donn�es
    if( matrix.objects.size() == 0 )
        return false;
        
        
    //recuperer les indices des cellules dans lesquelles on inserera l'entit�
    int is = 0;
    int js = 0;
    int ie = 0;
    int je = 0;
    
    
    //calculer is et js
    is = ( int )( ( tExtents.minPoint().y - matrix.min.y ) / matrix.step ) - 1;
    js = ( int )( ( tExtents.minPoint().x - matrix.min.x ) / matrix.step ) - 1;
    
    //calculer ie et je
    ie = ( int )( ( tExtents.maxPoint().y - matrix.min.y ) / matrix.step ) - 1;
    je = ( int )( ( tExtents.maxPoint().x - matrix.min.x ) / matrix.step ) - 1;
    
    //safeguard
    if( is < 0 )
        is = 0;
        
    //safeguard
    if( js < 0 )
        js = 0;
        
    //chercher les indices de la cellule
    int i = 0;
    int j = 0;
    
    //calcul du pas
    double pasx = ( matrix.max.x - matrix.min.x ) / matrix.step;
    double pasy = ( matrix.max.y - matrix.min.y ) / matrix.step;
    
    //recuperer les donn�es
    for( int v = is; v <= ie; v++ )
    {
        for( int u = js; u <= je; u++ )
        {
            //inserer les
            auto objTemp = matrix.objects[v][u].objects;
            
            //inserer l'object id de l'entit�
            objects.insert( objects.end(), objTemp.begin(), objTemp.end() );
        }
    }
    
    //supprimer les doublons dans le vecteur
    uniqueObjectId( objects );
    
    return true;
}

//fonction qui supprime les doublons
bool uniqueObjectId( vector<AcDbObjectId>& objects )
{
    //safeguard
    if( objects.size() == 0 )
        return false;
        
    for( int i = 0; i < objects.size(); i++ )
    {
        //reiterer sur le vecteur
        for( int j = i + 1; j < objects.size(); j++ )
        {
            //comparer les deux idsloa
            if( objects[i] == objects[j] )
            {
                //supprimer le donn�es dans le vecteur
                objects.erase( objects.begin() + j );
                j--;
            }
        }
    }
    
    return true;
}





