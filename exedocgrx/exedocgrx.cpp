#include <iostream>
#include"exedocgrx.h"

int main(int argc, char* argv[])
{
	//Recuperer la source entrer par l'utilisateur
	map<string, string> arglist = argparse(argc, argv);

	if (arglist.find("-help") != arglist.end())
	{
		showHelp();
		return 0;
	}

	//Si le qrglist n'est pas vide
	if (arglist.find("-src") == arglist.end())
	{
		cout << "Pas de chemin specifier" << endl;
		return 0;
	}
	
	string path = arglist["-src"];

	
	if (path.find("cmd") != string::npos && path.find(".h") != string::npos)
	{
		string folder, name, ext;
		splitPath(path,
			folder,
			name,
			ext);
		string docPath;
		if(arglist.find("-dst") == arglist.end())
			docPath = "C:\\Futurmap\\Outils\\GStarCAD2020\\HTM\\GRX\\";

		else docPath = arglist["-dst"];

		docPath.append(name.substr(3, name.length() - 1) + ".htm");

		//Fichier de commande o� on va lire les aides
		ifstream cmdFile(path);

		//On g�n�re la stream
		ofstream docFile(docPath);

		//On �crit le fichier de doc
		newWriteDoc(cmdFile, docFile);
	}
}

map<string, string> argparse(int argc, char* argv[]) 
{
	map<string, string> mp;
	for (int i = 0; i < argc - 1; i++)
	{
		std::string arg = argv[i + 1];
		string strKey, strVal;

		int equalPos = arg.find("=");
		if (arg == "-help" || arg == "-h")
		{
			mp.insert({ "-help", "Help" });
		}
		if (equalPos != std::string::npos)
		{
			std::string argName = arg.substr(0, equalPos);
			std::string argValue = arg.substr(equalPos + 1, arg.size());
			strKey = argName;
			strVal = argValue;
			mp.insert({ strKey, strVal });
		}
	}
	return mp;
}

bool splitPath(
	const string& path,
	string& dir,
	string& name,
	string& ext)
{
	dir = ("\\");
	int l_iDir = path.rfind(dir);
	ext = (".");
	int l_iExt = path.rfind(ext);
	dir = path.substr(l_iDir);
	name = path.substr(l_iDir + 1, l_iExt - l_iDir - 1);
	ext = path.substr(path.length() - l_iExt - 1);
	return !dir.empty() && !name.empty() && !ext.empty();
}


void newWriteDoc(ifstream& fileCmd, ofstream& fileHtm)
{
	//Listes des sections
	vector<string> sectionList;

	//Variable
	string line, aide = "";
	string cmdTemp, cmdName, desc, bullet;
	map<string, FmapDoc> mpDoc;

	fileHtm << DOC_HTML_0;

	//Mettre le top bar
	fileHtm << "<div class = \"topnav\"><a class = \"active\" href=\"C:/Futurmap/Outils/GStarCAD2020/HTM/GRX/index.html\">Accueil</a><input id = \"myInput\" onkeyup = \"searchCmd()\" type = \"text\" name = \"search\" placeholder = \"Rechercher..\"></div>";

	//Vecteur de string qui va contenir tous les commandes
	vector<string> allCmdName;
	bool bNewCmd = true;

	FmapDoc emptyDoc;
	emptyDoc.strCmdName = "";
	emptyDoc.strIcons = "";
	emptyDoc.strDesc = "";
	emptyDoc.vecSousDesc.clear();
	emptyDoc.strDoc = "";
	emptyDoc.vecFree.clear();
	emptyDoc.imgCapt.clear();
	emptyDoc.strBrief = "";

	//Verifions si le commentaire a une section
	string section = "";

	//Boucle sur les lignes du fichier .h
	while (getline(fileCmd, line))
	{
		//Creer un ImgAndCapt
		ImgAndCapt temp;

		if (bNewCmd)
		{
			emptyDoc.strCmdName = "";
			emptyDoc.strIcons = "";
			emptyDoc.strDesc = "";
			emptyDoc.vecSousDesc.clear();
			emptyDoc.strDoc = "";
			emptyDoc.vecFree.clear();
			emptyDoc.imgCapt.clear();
			emptyDoc.strBrief = "";
			emptyDoc.vecBullet.clear();
			emptyDoc.section = "";
			bNewCmd = false;
		}

		// On r�cup�re le nom de commande
		if (line.find(CMD_NAME_KEY) != string::npos)
		{
			cmdName = line.substr(13, line.length());
			int c = cmdName.find(" ");
			cmdName = cmdName.substr(0, c);

			//Ajouter le nom de la commande dans le fichier
			if (cmdName != "")
				emptyDoc.strCmdName = cmdName;

			continue;
		}

		if (line.find(DOC_KEY) != string::npos)
		{

			line = line.substr(9, line.length());
			int c = line.find(" ");

			emptyDoc.strDoc = line;
			string strCmdName = line.substr(0, c);
			emptyDoc.strCmdName = strCmdName;

			pair<string, FmapDoc> onePair;

			onePair.first = emptyDoc.strCmdName;
			onePair.second = emptyDoc;
			mpDoc.insert(onePair);

			bNewCmd = true;

			continue;
		}

		// On r�cup�re la description principale de la commande
		else  if (line.find(DESC_KEY) != string::npos)
		{
			desc = line.substr(10, line.length());

			//Ajouter le nom de la commande dans le fichier
			if (desc != "")
				emptyDoc.strDesc = desc;

			continue;
		}

		// On r�cup�re l'autre description de la commande
		else if (line.find(SOUS_DESC_KEY) != string::npos)
		{
			string temp = line.substr(15, line.length());

			//Ajouter le nom de la commande dans le fichier
			if (temp != "")
				emptyDoc.vecSousDesc.push_back(temp);

			continue;
		}

		// On r�cup�re les remarques
		else if (line.find(BULLET_KEY) != string::npos)
		{
			string temp = line.substr(12, line.length());

			//Ajouter le nom de la commande dans le fichier
			if (temp != "")
				emptyDoc.vecBullet.push_back(temp);

			continue;
		}
		// On r�cup�re les remarques
		else if (line.find(SECTION_KEY) != string::npos)
		{
			string temp = line.substr(13, line.length());

			//Ajouter le nom de la commande dans le fichier
			if (temp != "")
				section = (temp);//emptyDoc.vecBullet.push_back(trimLeftRight(temp));

			continue;
		}
		else if (line.find("icons") != string::npos)
		{
			string temp = line.substr(11, line.length());

			//Ajouter l'image dans le fichier
			if (temp != "")
				emptyDoc.strIcons = temp;
		}

		// On r�cup�re l'image avant la commande
		else if (line.find("imgPath") != string::npos)
		{
			//Ajouter le nom de la commande dans le fichier
			if (line.substr(13, line.length()) != "")
			{
				//Recuperer le chemin correspondant � l'image
				temp.imgPath = line.substr(13, line.length());

				//Passer � la ligne suivante
				getline(fileCmd, line);

				//On recuperer le titre de l'image avant la commande
				if (line.find("caption") != string::npos)
				{
					//Recuperer le titre de l'image
					temp.imgCapt = line.substr(13, line.length());
				}

				//Ajouter dans la doc l'image et son caption
				emptyDoc.imgCapt.push_back(temp);
			}


			continue;
		}

		else if (line.find("free") != string::npos)
		{
			//Ajouter le nom de la commande dans le fichier
			if (line.substr(10, line.length()) != "")
				emptyDoc.vecFree.push_back(line.substr(10, line.length()));

			continue;
		}

		// On d�tecte la fin de commentaire par la balise brief
		else if (line.find(END_KEY) != string::npos)
		{
			pair<string, FmapDoc> onePair;

			if (section == "")section = "section0";

			emptyDoc.section = section;
			onePair.first = emptyDoc.strCmdName;
			onePair.second = emptyDoc;
			mpDoc.insert(onePair);

			bNewCmd = true;
			sectionList.push_back(section);
			section = "";
			continue;
		}
	}


	// Verification et trie
	int size = mpDoc.size();

	if (size > 0)
	{
		map<string, FmapDoc>::iterator itDoc;

		//Supprimer les sections qui se dupplique
		std::set<string>sectionset(sectionList.begin(), sectionList.end());
		sectionList.assign(sectionset.begin(), sectionset.end());

		//Boucle sur les sections
		for (string sections : sectionList)
		{
			//Mertter les memes sections dans un divs
			fileHtm << "\n<div class = \"section\"> \n <br>";
			fileHtm << "\n <p = class = sectiontitle>" << sections << "\n</p>";

			for (itDoc = mpDoc.begin(); itDoc != mpDoc.end(); ++itDoc)
			{
				//Si c'est le meme section
				if (sections == itDoc->second.section)
				{
					string strCmd = itDoc->first;
					FmapDoc oneDoc = itDoc->second;

					// On r�cup�re le nom de commande
					if (strCmd != "")
					{
						fileHtm << "<div class = \"docCom\"";
						//Ajouter l'id
						fileHtm << " id = \"" << strCmd << "\">";

						//Ajouter les infos
						fileHtm << DOC_HTML_1;
						fileHtm << strCmd;
						fileHtm << DOC_HTML_2;
						fileHtm << DOC_HTML_3;

						//Ajouter le cmdName dans le vecteur
						allCmdName.push_back(strCmd);
					}

					if (oneDoc.strDoc != "")
					{
						fileHtm << "<p class=\"doc\"><font color=\"#f35221\"> " << oneDoc.strDoc << "</font>";

						fileHtm << "<br>";
						fileHtm << "</p>" << endl;
					}

					// On ajoute la description principale de la commande
					if (oneDoc.strDesc != "")
						fileHtm << "<p class = \"desc\"> Description: " << oneDoc.strDesc << "</p>";

					// On r�cup�re l'autre description de la commande
					int sizeSDesc = oneDoc.vecSousDesc.size();

					if (sizeSDesc > 0)
					{
						for (int counter = 0; counter < sizeSDesc; counter++)
						{
							string temp = oneDoc.vecSousDesc[counter];

							//Ajouter le nom de la commande dans le fichier
							if (temp != "")
								fileHtm << "<p class = \"sousdesc\">" << temp << "</p>";
						}

					}

					if (oneDoc.strIcons != "")
						fileHtm << "<p class = \"ico\" align = \"center\"><img src =\"" << oneDoc.strIcons << "\"></p>";


					//Recuperer la taille du vecteur d'image
					int sizeIm = oneDoc.imgCapt.size();

					// On r�cup�re l'image avant la commande
					if (sizeIm != 0)
					{
						//Boucle sur les images
						for (int counter = 0; counter < sizeIm; counter++)
						{
							fileHtm << DOC_HTML_1;
							fileHtm << DOC_HTML_2;
							fileHtm << DOC_HTML_3;

							string htmlImg = NEW_TEMPLATE_IMG;

							ImgAndCapt temp = oneDoc.imgCapt[counter];

							replaceFirst(htmlImg, "img_path", temp.imgPath);

							//Ajouter dans le html
							fileHtm << htmlImg;

							//Ajouter le nom de la commande dans le fichier
							fileHtm << DOC_HTML_1;
							fileHtm << DOC_HTML_2;
							fileHtm << DOC_HTML_3;

							//Boucle sur le vecteur de

							fileHtm << "<p class = \"imgdesc\">" << temp.imgCapt << "</p>";

							fileHtm << DOC_HTML_BR;
						}
					}

					// On r�cup�re l'autre description de la commande
					int sizeFree = oneDoc.vecFree.size();

					if (sizeFree > 0)
					{
						for (int counter = 0; counter < sizeFree; counter++)
						{
							string temp = oneDoc.vecFree[counter];

							//Ajouter le nom de la commande dans le fichier
							if (temp != "")
								fileHtm << "<p class = \"free\">" << temp << "</p>";
						}

					}

					//On rajoutes les remarques
					if (oneDoc.vecBullet.size() > 0)
					{
						fileHtm << DOC_HTML_BULLETOPEN;
						for (string temp : oneDoc.vecBullet)
						{
							fileHtm << DOC_HTML_BULLETOPENLIST << temp << DOC_HTML_BULLETENDLIST;
						}
						fileHtm << DOC_HTML_BULLETEND;
					}

					//On ajoute dans le fichier
					fileHtm << "</div>";
					fileHtm << "</br>" << endl;
				}
			}


			fileHtm << "</div> </br>";
		}
	}

	//Ajouter le div
	fileHtm << "<div class = \"vertical-menu\"><a href=\"#\" class=\"active\">Liste commandes</a>";

	//Recuperer la taille du vecteur cmdName
	int taille = allCmdName.size();

	//Boucle sur le vecteur de cmdName
	for (int i = 0; i < taille; i++)
	{
		vector<string>::iterator vecstrit = find(sectionList.begin(), sectionList.end(), mpDoc[allCmdName[i]].section);
		int dist = vecstrit - sectionList.begin();
		if (dist % 2 == 0)
			dist = 0;
		else dist = 1;
		fileHtm << "<a href = \"#" << allCmdName[i] << "\" class=\"section" << to_string(dist) << "\">" << allCmdName[i] << "</a>";
	}
	//Fermer le div
	fileHtm << "</div>";

	//Ajouter le reste
	fileHtm << "</body></html>";

	fileCmd.close();
	fileHtm.close();
}


bool replaceFirst(std::string& str,
	const std::string& from,
	const std::string& to)
{
	size_t start_pos = str.find(from);

	if (start_pos == std::string::npos)
		return false;

	str.replace(start_pos, from.length(), to);
	return true;
}

void showHelp()
{
	cout << "----------------------------------------------------------------------------------------" << endl;
	cout << "\t\t\t\t HELP \n\t\t\t\t------ "<< endl;
	std::cout << "-src   Type : String		|	Chemin vers le fichier contenant les commandes." << std::endl;
	std::cout << "-dst   Type : String		|	Chemin du fichier de sortie html." << std::endl;
	cout << "________________________________________________________________________________________" << endl;
}
