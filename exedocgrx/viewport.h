#include "fmapHeaders.h"


int getNumberOfViewports();

Acad::ErrorStatus getTargetOfActiveVp( AcGePoint3d& target,
    double& height,
    double& width );

AcDbObjectIdArray getAllViewportId( AcDbObjectIdArray& ucsNames );