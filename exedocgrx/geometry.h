/**
 * @file geometry.h
 * @author Lambert DURAN
 * @brief Operations geometriques: calculs de projections, de distance ..
 */

#pragma once
#include <fmapHeaders.h>
#include "pointEntity.h"
#include <gemat3d.h>
#include <dbspfilt.h>


/**
 * \brief Re-indexe la polyligne de sorte a ce que ses sommets soient ordonnes dans le sens trigonometrique
 * \brief M�thode: on prend le sommet avec le + haut y de la polyligne, puis on re-indexe en partant de celui-ci
 * \param poly Objet polyligne
 * \return vecteur de points 3d correspondant aux coord des sommets de la polyligne, ordonnes trigonometriquement
 */
vector< AcGePoint3d > getVertexTrigonometricSense( AcDbPolyline* poly );

/**
 * \brief Re-indexe la polyligne de sorte a ce que ses sommets soient ordonnes dans le sens trigonometrique
 * \brief M�thode: on prend le sommet avec le + haut y de la polyligne, puis on re-indexe en partant de celui-ci
 * \param poly Objet polyligne
 * \return vecteur de points 3d correspondant aux coord des sommets de la polyligne, ordonnes trigonometriquement
 */
vector< AcGePoint3d > getVertexTrigonometricSense( AcDbPolyline* poly, bool& trigo );

/**
 * \brief Verifier si les points sont  dans le sens trigonometrique ou pas
 * \brief M�thode: Arranger dans la sens trigo si int b = 1, arranger dans le sens inverse du sens trigo si i = 2 lasse comme il est si i = 0 aou vide
 * \param AcGePoint3dArray
 * \param int i ;
 * \return vecteur de points 3d correspondant aux coord des sommets de la polyligne, ordonnes trigonometriquement
 */
bool isVertexInTrigoSens( AcGePoint3dArray& ptA, const int& choise = 0 );


/**
 * \brief Renvoie true si le point I est sur le segmentAB, false sinon
 * \param ptA Premier point du segment
 * \param ptB Second point du segment
 * \param ptI Point a tester pour savoir s'il appartient au segment [ AB ]
 * \param precision On verifie que IA ^ IB < precision
 */
bool isPointOnLine( const AcGePoint2d& ptA,
    const AcGePoint2d& ptB,
    const AcGePoint2d& ptI,
    const double& precision = 0.0001 );


/**
 * \brief Renvoie true si le point I est sur le segmentAB, false sinon
 * \param ptA Premier point du segment
 * \param ptB Second point du segment
 * \param ptI Point a tester pour savoir s'il appartient au segment [ AB ]
 * \param precision On verifie que IA ^ IB < precision
 */
bool isPointOnLine( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& precision = 0.0001 );


/**
 * \brief Renvoie true si le point I est sur le segmentAB, false sinon
 * \param ptA Premier point du segment
 * \param ptB Second point du segment
 * \param ptI Point a tester pour savoir s'il appartient au segment [ AB ]
 * \param tolXY La tolerance sur XY
 * \param tolZ La tolerance sur Z
 */
bool isPointOnLine3D( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& tolXY = 0.0001,
    const double& tolZ = 0.0001 );


/**
 * \brief Calcule la plus courte distance entre le point C et le segment AB
 * \image html C:\Futurmap\Dev-Outils\GStarCAD\GRX\FmapLib\documentation\images\distanceTo.png
 */
double distanceTo( AcGePoint2d ptA, AcGePoint2d ptB, AcGePoint2d ptC );


/**
 * \brief Calcule la plus courte distance entre le point C et le segment AB dans le plan XY
 */
double distance2DTo( const AcGePoint3d& ptA, const AcGePoint3d& ptB, const AcGePoint3d& ptC );


/**
  * \brief  Renvoie le projete orthogonal du point C sur le segment AB
  * \param  ptA Le point 2d d�but du segment
  * \param  ptB Le point 2d fin du segment
  * \param  ptC Le point 2d � projeter sur le segment AB
  * \param  Renvoie le point proj�t� du point 2d sur le segment AB ou renvoie ( 0, 0, 0 ) s'il n'y pas de projection
  */
AcGePoint3d projectionOn( AcGePoint2d ptA, AcGePoint2d ptB, AcGePoint2d ptC );


/**
  * \brief  Renvoie le projete orthogonal du point C sur le segment AB
  * \param  ptA Le point 3d d�but du segment
  * \param  ptB Le point 3d fin du segment
  * \param  ptC Le point 3d � projeter sur le segment AB
  * \param  Renvoie le point proj�t� du point 3d sur le segment AB ou renvoie ( 0, 0, 0 ) s'il n'y pas de projection
  */
AcGePoint3d projectionOn( AcGePoint3d ptA, AcGePoint3d ptB, AcGePoint3d ptC );


/**
  * \brief  Renvoie le projete orthogonal du point C sur le segment AB en 2d
  * \param  ptA Le point 3d d�but du segment
  * \param  ptB Le point 3d fin du segment
  * \param  ptC Le point 3d � projeter sur le segment AB
  * \param  Renvoie le point proj�t� du point 3d sur le segment AB ou renvoie ( 0, 0, 0 ) s'il n'y pas de projection
  */
AcGePoint3d projectionOn2d( AcGePoint3d ptA, AcGePoint3d ptB, AcGePoint3d ptC );


/**
  * \brief  Recuperer la normal a partir de trois points
  * \param  ptA Premier points
  * \param  ptB deuxieme point
  * \param  ptC troisieme point
  * \return le vecteur normale du plan forme par ces trois points.
  */
AcGeVector3d getNormal( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptC );


/**
  * \brief  Recuperer les trois vecteurs directeurs du repere orthonorme forme par trois points
  * \param  ptA Premier point
  * \param  ptB Second point
  * \param  ptC Troisieme point
  * \return La liste des trois vecteurs directeurs du repere orthonorme forme par trois points
  */
vector <AcGeVector3d> getRepOrthoNorm( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptC );


/**
 * \brief Renvoie true si le point I est sur le segment AB, entre A et B  false sinon
 * \param ptA Premier point du segment
 * \param ptB Second point du segment
 * \param ptI Point a tester pour savoir s'il appartient au segment [ AB ]
 * \param precision valeur de tol�rence de comparaison
 * \param scaleFactor choisir un scaleFactor puissance de 10 fn fonction de la distance de calcul plus on est dans les grandes distances plus en auguemente la valeur; exemple 1 10, 100, 1000,...
 * \param withVtx flag pour d�terminer si on prend en compte les sommets lors de la comparaison
 * \param applyFilter flag pour d�terminer si on applique un filtre de distance lors de la comparaison
 * \param distFilter distance de filtrage
 */
bool isPointOnLineAndInsidesTwoPoints( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& precision = 0.0001,
    const double scaleFactor = 10000,
    const bool& withVtx = false,
    const bool& applyFilter = false,
    const double& distFilter = 0.00 );


/**
 * \brief Renvoie true si le point I est sur le segment AB, entre A et B  false sinon
 * \param zMean Z moyen du point � v�rifier
 * \param ptA Premier point du segment
 * \param ptB Second point du segment
 * \param ptI Point a tester pour savoir s'il appartient au segment [ AB ]
 * \param precision valeur de tol�rence de comparaison
 * \param scaleFactor choisir un scaleFactor puissance de 10 fn fonction de la distance de calcul plus on est dans les grandes distances plus en auguemente la valeur; exemple 1 10, 100, 1000,...
 * \param withVtx flag pour d�terminer si on prend en compte les sommets lors de la comparaison
 * \param applyFilter flag pour d�terminer si on applique un filtre de distance lors de la comparaison
 * \param distFilter distance de filtrage
 */
bool isPointOnLineAndInsidesTwoPointsZMean( double& zMean,
    const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& precision = 0.0001,
    const double scaleFactor = 10000,
    const bool& withVtx = false,
    const bool& applyFilter = false,
    const double& distFilter = 0.00 );


/**
 * \brief Renvoie true si le point I est sur le segment AB, entre A et B  false sinon
 * \param zMean Z moyen du point � v�rifier
 * \param ptA Premier point du segment
 * \param ptB Second point du segment
 * \param ptI Point a tester pour savoir s'il appartient au segment [ AB ]
  * \param ptRes r�sultat avec le Z moyen
 * \param precision valeur de tol�rence de comparaison
 * \param withVtx flag pour d�terminer si on prend en compte les sommets lors de la comparaison
 * \param distFilter distance de filtrage
 */
bool isPointOnLineAndInsidesTwoPointsZMean( double& zMean,
    const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    AcGePoint3d& ptRes,
    const double& precision = 0.0001,
    const bool& withVtx = false,
    const double& distFilter = 0.00 );


/**
 * \brief Renvoie true si le point I est sur le segment AB des points d'une Arc, entre A et B  false sinon
 * \param ptA Premier point du segment
 * \param ptB Second point du segment
 * \param ptI Point a tester pour savoir s'il appartient au segment [ AB ]
 * \param precision valeur de tol�rence de comparaison
 * \param scaleFactor choisir un scaleFactor puissance de 10 fn fonction de la distance de calcul plus on est dans les grandes distances plus en auguemente la valeur; exemple 1 10, 100, 1000,...
 * \param withVtx flag pour d�terminer si on prend en compte les sommets lors de la comparaison
 * \param applyFilter flag pour d�terminer si on applique un filtre de distance lors de la comparaison
 * \param distFilter distance de filtrage
 */
bool isPointOnLineAndInsidesTwoPointsArcZMean( double& zMean,
    const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& precision = 0.0001,
    const double scaleFactor = 10000,
    const bool& withVtx = false,
    const bool& applyFilter = false,
    const double& distFilter = 0.00 );


/**
  * \brief  Projeter orthogonalement un point sur un plan (d�fini par un point et une normale )
  * \param  pointInPlan Point contenu dans le plan
  * \param  normal Normale du isPointOnLine
  * \param  pointToProject Point � projeter
  * \return Projet� orthogonal du point sur le plan
  */
AcGePoint3d projectPointOnPlane( const AcGePoint3d& pointInPlan,
    const AcGeVector3d& normal,
    const AcGePoint3d& pointToProject );

/**
  * \brief v�rifie si deux rectangle d�finie par le coin inf�reieur gauche et le coin sup�rieur doite se croisent sur le plan XY
  * \param firstRecLeftBottom coordonn�es du coin inf�rieur gauche du premier rectangle
  * \param firstRecRightTop coordonn�es du coin sup�rieur doite du premier rectangle
  * \param secondRecLeftBottom coordonn�es du coin inf�rieur gauche du second rectangle
  * \param rightRecRightTop coordonn�es du coin sup�rieur doite du second rectangle
  * \return bool true si se croise, false sinon
  */
bool are2RectIntersected( AcGePoint3d& firstRecLeftBottom, AcGePoint3d& firstRecRightTop, AcGePoint3d& secondRecLeftBottom, AcGePoint3d& rightRecRightTop );


/**
  * \brief v�rifie si un point est dans une rectangle droite (les cot�s sont parall�les aux axes X et Y)
    A     B
    ------
    |    |
    ------
    D     C

  * \param Coordo
  * \param cornerA, cornerB, cornerc, cornerD coordonn�es du coin du rectangle
  * \param ptTocheck coordonn�es du point � v�rifier
  * \return bool true si dedans, false sinon
  */
bool isPointInsideRect( AcGePoint3d& cornerA, AcGePoint3d& cornerB, AcGePoint3d& cornerC, AcGePoint3d& cornerD, AcGePoint3d ptTocheck );


/**
  * \brief Retourne les coordonn�es d'un point dans un segment avec une certaine distance
  * \param distFromFirstPt distance entre le point pt et le point � trouver
  * \param pt coordonn�es du point de r�f�rence
  * \param ptO coordonn�es du second point
  * \return AcGePoint3d
  */
AcGePoint3d getPointFrom( const float& distFromFirstPt, const AcGePoint3d& pt, const AcGePoint3d ptO );


/**
  * \brief Permet de recuperer la distance du point projet� sur le segment AB
  * \param ptSegA Point A du segment
  * \param ptSegB Point B du segment
  * \param pt Point � projet�
  * \param dist La distance � r�cuperer
  * \param ptProj Le point projet�
  * \return True si l'op�ration a r�ussi
  */
bool getDistProj2DOnSeg(
    const AcGePoint2d& ptSegA,
    const AcGePoint2d& ptSegB,
    const AcGePoint3d& pt,
    double& dist,
    AcGePoint3d& ptProj );

/**
    \brief V�rifie si un point est situ� dans un cercle ou non
    \param ptCenter Centre du cercle
    \param circleRadius Rayon du cercle
    \param ptToCheck Point � v�rifier
    \return True si le point est � l'int�rieur du cercle, False sinon
  */
bool isPointInside2DCircle( AcGePoint3d& ptCenter,
    double& circleRadius,
    const AcGePoint3d& ptToCheck );

/**
    \brief
    \param
    \param
    \param
    \return
  */
bool isPointInside2DCircle( AcDbCircle*& circle,
    const AcGePoint3d& ptToCheck );

/**
    \brief
    \param
    \param
    \param
    \return
  */
bool isPointOnLine2D( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& precision );


/**
  * \brief Permet de recuperer l'angle entre la droite form�e par les deux points en entr�e et l'abscisse
  * \param pt1 Le premier point
  * \param pt2 Le second point
  * \return L'angle entre la droite form�e par les deux points en entr�e et l'abscisse
  */
double getAngleFromAbscisse( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 );

/**
  * \brief Permet de recuperer le bounding box d'une entit�
  * \param pEntity l'entite concernee
  * \param box le bounding box
  * \param points les points min et le max du bounding box
  * \return Acad::ErrorStatus eOk si l'operation est bien terminee
  */
Acad::ErrorStatus getBoundingBox( AcDbEntity* entity, AcDbExtents& box, AcGePoint3dArray& points );

/**
  * \brief Permet de calculer l'intersection entre deux vecteurs
  * \param vec1 Le premier vecteur
  * \param vec2 Le second vecteur
  * \param ptA Le point d�but du vecteur 1
  * \param ptC Le point d�but du vecteur 2
  * \param intersectionPoint Le point d'intersection
  * \return Acad::ErrorStatus eOk si l'operation est bien terminee
  */
Acad::ErrorStatus computeIntersection( const AcGeVector2d& vec1,
    const AcGeVector2d& vec2,
    AcGePoint2d& ptA,
    AcGePoint2d& ptC,
    AcGePoint2d& intersectionPoint );

/**
  * \brief Permet de calculer si deux bounding box s'intersecte
  * \param box1pt1 Premier point du premier bounding box
  * \param box1pt2 Second point du prmier bounding box
  * \param box2Pt1 Premier point du second bounding box
  * \param box2pt2 Second point du second bounding box
  * \return True si les deux boundings box s'intersectent, False sinon
  */
bool isBoxIntersected( const AcGePoint2d& box1Pt1,
    const AcGePoint2d& box1Pt2,
    const AcGePoint2d& box2Pt1,
    const AcGePoint2d& box2Pt2 );

bool isBoxIntersected( AcDbExtents& bb1,
    AcDbExtents& bb2 );

/**
  * \brief Permet de projeter un point 2d sur un segment, c'est � dire recuperer le z sur le segment
  * \param ptA Point A du segment
  * \param ptB Point B du segment
  * \param ptToProject Le point � projeter
  * \param projectedPoin Le point proj�t�
  * \return Acad::ErrorStatus
  */
Acad::ErrorStatus getZPointToSegment( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptToProject,
    AcGePoint3d& projectedPoint );

/**
  * \brief Permet de calculer l'intersection entre deux AcDbCurve, le point est projet� sur le premier AcDbCurve, les courbes ne sont pas etendues
  * \param curve1 Le premier curve
  * \param curve2 Le second curve
  * \param intersectPoint Tableau contenant les points d'intersections
  * \return Acad::ErrorStatus
  */
Acad::ErrorStatus computeIntersection( const AcDbCurve* curve1,
    const AcDbCurve* curve2,
    AcGePoint3dArray& intersectPoint );


/**
  * \brief Permet de recuperer la projection d'un curve sur un plan
  * \param curve1 Le curve
  * \param curve2 Le plan
  * \param projectDir Vecteur de projection
  * \return Acad::ErrorStatus
  */
AcDbCurve* getProjCurveOnPlane( AcDbCurve* curve,
    const AcGePlane& plane,
    const AcGeVector3d& projectDir );

/**
  * \brief Calculer le multiple min
  * \param valueInput : Nombre � traiter
  * \param multiple : Nombre � traiter
  * \return nombre entier multiple min
*/
int multipleMin( const double& valueInput, const int& multiple );

/**
  * \brief Calcule le multiple max
  * \param valueInput : Nombre � traiter
  * \param multiple : Nombre � traiter
  * \return nombre entier multiple max
*/
int multipleMax( const double& valueInput, const int& multiple );

/**
  * \brief Check si un rectangle est � l'interieur d'un rectangle
  * \param ptMinR1 : point min du premier rectange
  * \param ptMinR2 : point max du premier rectange
  * \param ptMaxR1 : point min du second rectange
  * \param ptMaxR2 : point max du second rectange
  * \return booleen
*/
bool isRectInsideRect( const AcGePoint3d& ptMinR1,
    const AcGePoint3d& ptMaxR1,
    const AcGePoint3d& ptMinR2,
    const AcGePoint3d& ptMaxR2 );

/**
  * \brief Check si un rectangle est � l'interieur d'un rectangle
  * \param ptMinR1 : point min du premier rectange
  * \param ptMinR2 : point max du premier rectange
  * \param ptMaxR1 : point min du second rectange
  * \param ptMaxR2 : point max du second rectange
  * \param use3d   : Si on veut tester en 3d en 2d sinon
  * \return booleen
*/
bool isPointInsideRect( const AcGePoint3d& point,
    const AcGePoint3d& ptMin,
    const AcGePoint3d& ptMax,
    const bool& use3d = true );

/**
  * \brief Check Mettre les polilignes dans la matrice pour optimisation du recherche
  * \param ptMinR1 : Selection de la polyligne
  * \param ptMinR2 : Pas du poliligne
  * \param ptMaxR1 : Tolerance
  * \return La matrice qui classe les poliligens
*/
vector<vector<vector<AcDbObjectId>>> getSelectionMatrix( const ads_name& ssPolyligne, const int& pas = 1, const double& tol = .01 );

/**
  * \brief Check Mettre les polilignes dans la matrice pour optimisation du recherche
  * \param ptMinR1 : Selection de la polyligne
  * \param ptMinR2 : Pas du poliligne
  * \param ptMaxR1 : Tolerance
  * \return La matrice qui classe les poliligens
*/
vector<vector<vector<int>>> getSelectionIntMatrix( const ads_name& ssPolyligne, const int& pas = 1, const double& tol = .01 );

/**
  * \brief Verifier si deux bounding box s'intersectent en 3D
  * \param minPointBox1 : Point min de la premi�re Box
  * \param maxPointBox1 : Point max de la premi�re Box
  * \param minPointBox2 : Point min de la deuxi�me Box
  * \param maxPointBox2 : Point max de la deuxi�me Box
  * \return true si les deux bbox s'intersectent
*/
bool are2bBoxIntersected( const AcGePoint3d& minPointBox1, const AcGePoint3d& maxPointBox1, const AcGePoint3d& minPointBox2, const AcGePoint3d& maxPointBox2 );

bool isOverLapping1D( const double& x1A, const double& x2A, const double& x1B, const double& x2B );
