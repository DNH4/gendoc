/**
 * @file file.h
 * @author Romain LEMETTAIS
 * @brief Fichier contenant des fonctions utiles pour travailler avec les fichiers
 */

#pragma once
#include <fmapHeaders.h>
#include <acutmem.h>
#include <acdocman.h>
#include "dataBase.h"
#include "acadVariable.h"
#include "algorithm"
#include <dirent.h>

/**
 * \brief Renvoie le chemin complet du fichier dwg ouvert
 * \return le chemin complet du fichier dwg ouvert
 */
AcString getCurrentFileName();

/**
 * \brief Renvoie le dossier contenant le fichier dwg ouvert
 * \return le chemin complet du fichier dwg ouvert
 */
AcString getCurrentFileFolder();


/**
 * \brief Ouvre une boite de dialogue windows et demande a l'utilisateur de choisir un fichier
 * \param bToReadOnly     true si c'est pour lire un fichier, false si c'est pour ecrire
 * \param sExtension    Extensions des fichier a selectionnes separes par des ;
 * \param sTitle        Message que va voir l'utilisateur en haut a gauche de la boite
 * \param sDefaulFolder Dossier par defaut a ouvrir
 * \return le chemin complet du fichier selectionne
 */
AcString askForFilePath(
    const bool&     bToReadOnly     = true,
    const AcString& sExtension      = AcString::kEmpty,
    const AcString& sTitle          = AcString::kEmpty,
    const AcString& sDefaulFolder   = AcString::kEmpty );

/**
 * \brief Ouvre une boite de dialogue windows et demande a l'utilisateur de choisir un fichier en lecture
 * \param sExtension    Extensions des fichier a selectionnes separes par des ;
 * \param sTitle        Message que va voir l'utilisateur en haut a gauche de la boite
 * \param sDefaulFolder Dossier par defaut a ouvrir
 * \return le chemin complet du fichier selectionne
 */
AcString askToReadFilePath(
    const AcString& sExtension,
    const AcString& sTitle          = AcString::kEmpty,
    const AcString& sDefaulFolder   = AcString::kEmpty );

/**
 * \brief ouvre une boite de dialogue windows et demande a l'utilisateur de choisir un fichier en ecriture
 * \param sExtension    Extensions des fichier a selectionnes separes par des ;
 * \param sTitle        Message que va voir l'utilisateur en haut a gauche de la boite
 * \param sDefaulFolder Dossier par defaut a ouvrir
 * \return le chemin complet du fichier selectionne
 */
AcString askToWriteFilePath(
    const AcString& sExtension,
    const AcString& sTitle          = AcString::kEmpty,
    const AcString& sDefaulFolder   = AcString::kEmpty );


/**
 * \brief Renvoie le chemin d'acces au repertoire contenant un fichier
 */
AcString getFileDir(
    const AcString );


/**
 * \brief Renvoie le nom d'un fichier a partir de son chemin d'acces
 */
AcString getFileName(
    const AcString );


/**
 * \brief Renvoie l'extension d'un fichier a partir de son chemin d'acces
 */
AcString getFileExt(
    const AcString );


/**
 * \brief Separe le nom d'un fichier en trois entit�s (chemin d'acc�s au r�pertoire, nom du fichier et extension)
 * \param path Chemin d'acces complet au fichier en Acstring
 * \param dir Chemin d'acces au repertoire
 * \param name Nom du fichier
 * \param ext Extension du fichier
 * \return une AcString pour le chemin d'acces au repertoire, une pour le nom du fichier, une pour son extension
 */
bool splitPath(
    const AcString& path,
    AcString& dir,
    AcString& name,
    AcString& ext );


/**
 * \brief Test la presence du fichier dans le chemin specifie
 * \param path Chemin d'acces complet au fichier en string
 * \return true si le fichier est presente dans le chemin specifie, false sinon
 */
bool isFileExisting( const string& path );


/**
 * \brief Test la presence du fichier dans le chemin specifie
 * \param path Chemin d'acces complet au fichier en AcString
 * \return true si le fichier est presente dans le chemin specifie, false sinon
 */
bool isFileExisting( const AcString& path );


/**
 * \brief Cr�e un dossier au chemin sp�cifi�
 * \param newFolder Chemin vers le nouveau dossier
 * \return true si succ�s
 */
bool createFolder( string newFolder );


/**
* \brief Copie un fichier d'un endroit � un autre
* \param oldPath Ancien chemin vers le fichier
* \param oldPath Nouveau chemin vers le fichier
* \return true si succ�s
*/
bool copyFile( string oldPath, string newPath );

/**
* \brief Renvoie le nombre de lignes d'un fichier texte
* \param path Chemin vers le fichier texte
* \return Nombre de lignes du fichier
*/
int getNumberOfLines(string path);

/**
* \brief Renvoie les noms des fichiers dans un dossier
* \param folderFiles Chemin vers le dossier
* \return Vecteur contenant les noms des fichiers dans le dossier en entr�e
*/
vector<string> getFileNamesInFolder(const AcString& folderFiles);

//FileReader est utilise pour lire des fichiers dont on peut parametrer l'extension du fichier a lire
class FileReader {
    public:
        /**
        * \brief Constructeur par defaut
        * \param
        * \param
        * \return
        */
        FileReader();

		/**
		* \brief Constructeur   de copie
		* \param
		* \param
		* \return
		*/
		FileReader( const FileReader& fileReader);
        
        /**
        * \brief Constructeur qui ouvre un fichier
        * \param filepath Le chemin du fichier
        * \param separateur Le separateur utilise dans le fichier
        * \return
        */
        FileReader(
            const string& filePath,
            const string& separateur );
            
        FileReader(
            const AcString& filePath,
            const AcString& separateur );
            
        /**
        * \brief Destructeur
        * \param
        * \param
        * \return
        */
        ~FileReader();
        
        /**
        * \brief Initiateur de fileReader
        * \param filepath Le chemin du fichier
        * \param separateur Le separateur utilise dans le fichier
        * \return true si La lecture de la ligne d'apres est fait
        */
        bool init( string filepath, string separateur );
        
        bool init( AcString filepath, AcString separateur );
        
        /**
        * \brief Lire la Ligne d'apres dans un Fichier Texte
        * \param
        * \return true si La lecture de la ligne d'apres est fait
        */
        bool readNextLine();
        
        /**
        * \brief Aller a une certaine ligne
        * \param lineNumber La ligne ou on veut aller
        * \return true si on est arrive a la ligne voulue
        */
        bool goToLine( const int& lineNumber );
        
        /**
        * \brief Recuperer les strings de la ligne courant
        * \param
        * \return Retourne un vecteur qui stock les strings de la ligne courant
        */
        vector< string > getLine();
        vector< AcString > getLineAc();
        
        /**
        * \brief R�cup�re une ligne du fichier dont la premi�re colonne contient un mot-cl�
        * \param head Mot cl� � retrouver
        * \param line Vecteur contenant les diff�rentes cellules de la ligne
        * \return True si le mot-cl� a �t� trouv�, false sinon
        */
        bool FileReader::getLine( string head, vector<string>& line );
        
        /**
        * \brief Recuperer les strings de la ligne a tel Numero
        * \param lineNumber La ligne ou on veut aller
        * \return Retourne un vecteur qui stock les strings de la ligne voulue
        */
        vector< string > getLine( const int& lineNumber );
        vector< AcString > getLineAc( const int& lineNumber );
        
        /**
        * \brief Savoir si une string se trouve dans la ligne courante ou non
        * \param val String � rechercher
        * \return True si la string est dans la ligne, false sinon
        */
        bool isStringInLine( const string& val );
        
        
        /**
        * \brief Recuperer la valeur de la colonne du Fichier
        * \param columnNumber Le numero du colonne a recuperer
        * \param valueResult L'elements du colonne recuperes en type string
        * \return true si la valeur voulue est recupere
        */
        bool getValue(
            const int& columnNumber,
            string& valueResult );
            
        /**
        * \brief Recuperer le nom de la colonne du Fichier
        * \param columnName Le Nom du colonne a recuperer l'element
        * \param valueResult L'element du colonne recuperes en type string
        * \return true si la valeur voulue est recupere
        */
        bool getValue(
            const string& colomnName,
            string& valueResult );
            
        /**
        * \brief Recuperer la valeur d'un element dans le fichier dont on specifie le numero de la ligne et le nom de la colonne
        * \param lineNumber Le Numero de la ligne a recuperer les elements
        * \param columnName Le Nom de la colonne a recuperer les elements
        * \param valueResult L'element recupere en type string
        * \return true si la valeur voulue est recupere
        */
        bool getValue(
            const int& lineNumber,
            const string& columnName,
            string& valueResult );
            
        /**
        * \brief Recuperer la valeur d'un element dans le fichier dont on specifie le numero de la ligne et le numero de la colonne
        * \param lineNumber Le Numero de la ligne a recuperer l'element
        * \param columnName Le Numero de la colonne a recuperer l'element
        * \param valueResult L'element de la case recupere en type string
        * \return true si la valeur voulue est recupere
        */
        bool getValue(
            const int& lineNumber,
            const int& columnNumber,
            string& valueResult );
            
        /*bool getValue(
            const int& lineNumber,
            const int& columnnumber,
            AcString& valueResult );*/
        
        /**
        * \brief Recuperer la valeur d'un element dans le fichier dont on specifie le numero de la colonne
        * \param columnNumber Le Numero de la colonne a recuperer l'element
        * \param valueResult L'element de la case recupere en type double
        * \return true si la valeur voulue est recupere
        */
        bool getValue(
            const int& columnNumber,
            double& valueResult );
            
        /**
        * \brief Recuperer la valeur d'un element dans le fichier dont on specifie le nom de la colonne
        * \param lineNumber Le Numero de la colonne a recuperer l'element
        * \param valueResult L'element de la case recupere en type double
        * \return true si la valeur voulue est recupere
        */
        bool getValue(
            const string colomnName,
            double& valueResult );
            
        /*bool getValue(
            const AcString colomnName,
            double& valueResult );*/
        
        /**
        * \brief Recuperer la valeur d'un element dans le fichier dont on specifie le numero de la ligne et le nom de la colonne
        * \param lineNumber Le Numero de la ligne a recuperer les elements
        * \param columnName Le Nom de la colonne a recuperer les elements en type double
        * \param valueResult L'element recupere
        * \return true si la valeur voulue est recupere
        */
        bool getValue(
            const int& lineNumber,
            const string& columnName,
            double& valueResult );
            
        /*bool getValue(
            const int& lineNumber,
            const AcString& columnName,
            double& valueResult );*/
        /**
        * \brief Recuperer la valeur d'un element dans le fichier dont on specifie le numero de la ligne et le numero de la colonne
        * \param lineNumber Le Numero de la ligne a recuperer l'element
        * \param columnName Le Numero de la colonne a recuperer l'element
        * \param valueResult L'element de la case recupere en type double
        * \return true si la valeur voulue est recupere
        */
        bool getValue(
            const int& lineNumber,
            const int& columnNumber,
            double& valueResult );
            
        /**
        * \brief Recuperer une colonne dans le Fichier dont on specifie le numero
        * \param columnNumber Numero de la colonne
        * \param valueResult Vecteur de strin qui va contenir Les valeurs dans la colonne
        * \return true si la valeur voulue est recupere
        */
        bool getColonne(
            const int& colomnNumber,
            vector< string >& valueResult );
            
        /**
        * \brief Recuperer une colonne dans le Fichier dont on specifie le nom
        * \param columnName Nom de la colonne
        * \param valueResult Un Vecteur de string qui va contenir Les valeurs dans la colonne
        * \return true si la valeur voulue est recupere
        */
        bool getColonne(
            const string& colomnName,
            vector< string >& valueResult );
            
            
        /**
        * \brief Recuperer une colonne dans le Fichier dont on specifie le numero
        * \param columnName Nom de la colonne
        * \param valueResult Un vecteur de double qui va contenir Les valeurs dans la colonne
        * \return true si la valeur voulue est recupere
        */
        bool getColonne(
            const int& colomnNumber,
            vector< double >& valueResult );
            
        /**
        * \brief Recuperer une colonne dans le Fichier dont on specifie le nom
        * \param columnName Nom de la colonne
        * \param valueResult Un vecteur de double qui va contenir Les valeurs dans la colonne
        * \return true si la valeur voulue est recupere
        */
        bool getColonne(
            const string& coloneName,
            vector< double >& valueResult );
            
        /**
        * \brief Recuperer le chemin du fichier en cours
        * \param
        * \return Le chemin du fichier en cours
        */
        AcString getFilePath( );
        
        /**
        * \brief Parametrer le chemin du fichier en cours
        * \param
        * \return void
        */
        void setFilePath( const AcString& filePath );
        
        /**
        * \brief Recuperer le separateur de lecture dans le fichier
        * \param
        * \return void
        */
        AcString getSeparateur( );
        
        /**
        * \brief Parametrer le separateur de lecture dans le fichier
        * \param
        * \return void
        */
        void setSeparateur( const AcString& sep = " " );
        
        /**
        * \brief Recuperer la ligne active dans le Fichier
        * \param
        * \return int qui est le num�ro de la ligne active
        */
        int getActiveLineNumber();
        
        
        /**
        * \brief Recuperer le numero de la colonne dans le fichier
        * \param
        * \return int qui est le num�ro de la colonne
        */
        int getColumnNumber();
        
        /**
        * \brief Parametrer le numero de la colonne dans le fichier
        * \param
        * \return int qui est le numero de la colonne parametree
        */
        void setColumnNumber( const int& value );
        
        /**
        * \brief Recuperer le numero de ligne dans le fichier
        * \param
        * \return int qui est le numero de la ligne parametree
        */
        int getLineNumber();
        
        /**
        * \brief Parametrer le numero de ligne dans le fichier
        * \param
        * \return int qui est le numero de la ligne parametree
        */
        void setLineNumber( const int& value );
        
        /**
        * \brief Parametrer le numero de ligne active
        * \param value La nouvelle valeur
        * \return void
        */
        void setActiveLineNumber( int value );
        
    private:
        ifstream m_file; //Le fichier
        string m_filePath; //Le chemin vers le fichier
        string m_separateur; //Le separateur
        int m_activeLineNumber; //La ligne active du fichier
        int m_columnNumber; //La colomne
        int m_lineNumber; //La ligne
        vector< string > m_entete; //Vecteur d'entete
        vector< string > m_activeLine; //Vecteur contenant la les informations de la ligne active
};