#include "dataBase.h"
#include "acString.h"
#include "layer.h"

void addToModelSpace( AcDbEntity* acEntity )
{
    // recupere la database
    AcDbDatabase* pDB = acdbHostApplicationServices()->workingDatabase();
    
    // recupere la liste des blocks
    AcDbBlockTable* pBlockTable = NULL;
    pDB->getSymbolTable( pBlockTable, AcDb::kForRead );
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    
    // recupere le model space, puis ferme la liste des blocks
    pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite );
    pBlockTable->close();
    
    // recupere l'Id de l'entite pour l'ajouter a la database
    AcDbObjectId objectId = AcDbObjectId::kNull;
    pBlockTableRecord->appendAcDbEntity( objectId, acEntity );
    
    // on ferme le block et le point
    pBlockTableRecord->close();
}


void addToModelSpace( AcDbEntity* acEntity,
    AcDbObjectId& objectId )
{
    // recupere la database
    AcDbDatabase* pDB = acdbHostApplicationServices()->workingDatabase();
    
    // recupere la liste des blocks
    AcDbBlockTable* pBlockTable = NULL;
    pDB->getSymbolTable( pBlockTable, AcDb::kForRead );
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    
    // recupere le model space, puis ferme la liste des blocks
    pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite );
    pBlockTable->close();
    
    // recupere l'Id de l'entite pour l'ajouter a la database
    pBlockTableRecord->appendAcDbEntity( objectId, acEntity );
    
    // on ferme le block et le point
    pBlockTableRecord->close();
}


AcDbBlockTableRecord* openModelSpace()
{

    // recupere la database
    AcDbDatabase* pDB = acdbHostApplicationServices()->workingDatabase();
    
    // recupere la liste des blocks
    AcDbBlockTable* pBlockTable = NULL;
    pDB->getSymbolTable( pBlockTable, AcDb::kForRead );
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    
    // recupere le model space, puis ferme la liste des blocks
    pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite );
    pBlockTable->close();
    
    return pBlockTableRecord;
}


void addToModelSpaceAlreadyOpened( AcDbBlockTableRecord* pBlockTableRecord,
    AcDbEntity*           acEntity )
{

    // recupere l'Id de l'entite pour l'ajouter a la database
    AcDbObjectId objectId = AcDbObjectId::kNull;
    pBlockTableRecord->appendAcDbEntity( objectId, acEntity );
}

Acad::ErrorStatus addToModelSpaceAlreadyOpened( AcDbBlockTableRecord* pBlockTableRecord,
    AcDbEntity* acEntity, AcDbObjectId& objectId )
{
    // recupere l'Id de l'entite pour l'ajouter a la database
    return pBlockTableRecord->appendAcDbEntity( objectId, acEntity );
    
}



void closeModelSpace( AcDbBlockTableRecord* pBlockTableRecord )
{

    // on ferme le block et le point
    pBlockTableRecord->close();
}


Acad::ErrorStatus redrawEntity( AcDbEntity* entity )
{
    if( !entity )
        return Acad::eNullPtr;
        
    // On v�rifie que l'objet soit ouvert en �criture
    if( !entity->isWriteEnabled() )
        return Acad::eNotOpenForWrite;
        
    // On r�cup�re l'Id de l'entit�
    AcDbObjectId objectId = entity->objectId();
    
    // On la ferme
    Acad::ErrorStatus es = entity->close();
    
    if( es != Acad::eOk )
        return es;
        
    AcDbEntity* pEnt = NULL;
    
    // On la r�-ouvre
    return es = acdbOpenAcDbEntity( pEnt, objectId, AcDb::kForWrite );
}


Acad::ErrorStatus  insertEntityExternFile(
    AcDbEntity* entity,
    const AcString& filename,
    AcDbDatabase* database )
{
    Acad::ErrorStatus es = Acad::eNotApplicable;
    
    // Verifier la base de donn�es
    if( !database )
    {
        database = new AcDbDatabase();
        
        // Lire la base de donn�e du fichier en param�tre
        if( filename.compare( "" ) != 0 )
        {
            es = database->readDwgFile( filename );
            
            if( es != Acad::eOk )
            {
                delete database;
                database = NULL;
                database = new AcDbDatabase();
            }
        }
    }
    
    // Recuperer  la table des blocs de base externe
    AcDbBlockTable* pBlockTable = NULL;
    es = database->getSymbolTable( pBlockTable, AcDb::kForRead );
    
    // Verifier
    if( es != eOk )
        return es;
        
    // R�cuperer le model space
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    es =  pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite );
    
    //Fermer la bloc table
    pBlockTable->close();
    
    // Verifier
    if( es != eOk )
        return es;
        
    // Recuperer le calque de l'entit�
    AcString layer = entity->layer();
    
    // Recuperer la table de calque
    AcDbLayerTableRecord* layerEntity = NULL;
    es = getLayer( layerEntity, layer );
    
    // Verifier
    if( es != eOk || layerEntity == NULL )
        return es;
        
    // Recuperer la couleur de calque
    AcCmColor color = AcCmColor();
    
    // Verifier l'entit� calque
    if( layerEntity != NULL )
    {
        //On recupere la couleur
        color = layerEntity->color();
        
        //Fermer l'entit� calque
        layerEntity->close();
    }
    
    // Cr�er le nouvelle calque
    es =  createLayer( layer, color, database );
    
    if( es != eOk )
        return es;
        
    // Ajouter l'entit� � la model space
    AcDbObjectId objectId = AcDbObjectId::kNull;
    es = pBlockTableRecord->appendAcDbEntity( objectId, entity );
    
    // Verifier
    if( es != eOk || objectId == AcDbObjectId::kNull )
    {
        // Fermer le blocktable
        pBlockTableRecord->close();
        return es;
    }
    
    // Setter le calque
    entity->setLayer( layer );
    
    // Verifier si l'entit� est un texte pour le style de texte
    if( entity->isKindOf( AcDbText::desc() ) )
    {
        // Caster l'entit� en texte
        AcDbText* text = AcDbText::cast( entity );
        
        if( text )
        {
            // Modifier le style par d�faut-----
            text->setTextStyle( AcDbObjectId::kNull );
        }
    }
    
    // Fermer la nouvelle entit�
    entity->close();
    
    // On ferme le block
    pBlockTableRecord->close();
    
    // Si le fichier n'est pas vide on enregistre la base de donn�es
    if( filename.compare( "" ) != 0 )
        database->saveAs( filename, false, AcDb::kDHL_1800 );
        
    // Renvoyer le r�sultat
    return Acad::eOk;
}


Acad::ErrorStatus cloneEntity(
    AcDbEntity* pEntToClone,
    AcDbEntity* pClonedEnt,
    AcDbDatabase* pDbClonedEnt,
    const AcDb::OpenMode& mode )
{

    Acad::ErrorStatus es;
    
    // Id de l'entit� � cloner
    AcDbObjectIdArray objectIds;
    objectIds.append( pEntToClone->id() );
    
    // Id mapping dans lequel va �tre stock� l'id de l'entit� clon�e
    AcDbIdMapping idMap;
    
    // R�cuperer la table des blocs du dessin d'arriv�e
    AcDbBlockTable* pBlockTable = NULL;
    
    if( es = pDbClonedEnt->getSymbolTable( pBlockTable, AcDb::kForRead ) )
        return es;
        
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    
    // R�cuperer le model space, puis fermer la liste des blocks
    if( es = pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForWrite ) )
        return es;
        
    if( es = pBlockTable->close() )
        return es;
        
    // Cloner l'entit�
    if( es = pDbClonedEnt->deepCloneObjects( objectIds,
                pBlockTableRecord->id(),
                idMap ) )
        return es;
        
    // Fermer le model space
    if( es = pBlockTableRecord->close() )
        return es;
        
    // R�cup�rer le nouvel objectId
    AcDbIdPair idPair;
    idPair.setKey( pEntToClone->id() );
    bool res = idMap.compute( idPair );
    
    if( !res )
        return Acad::eNotApplicable;
        
    // Ouvrir la nouvelle entit�
    return acdbOpenAcDbEntity( pClonedEnt,
            idPair.value(),
            mode );
}


Acad::ErrorStatus importEntity(
    AcDbObjectIdArray& objectIdArray,
    const AcString& dwgFile )
{

    // R�cuperer la database
    AcDbDatabase* dbExtern = new AcDbDatabase( Adesk::kFalse );
    
    // Ouvrir la base de donn�es externe
    Acad::ErrorStatus es;
    
    if( es = dbExtern->readDwgFile( dwgFile ) )
    {
        //Sortir
        dbExtern->close();
        return es;
    }
    
    // R�cuperer la liste des blocks
    AcDbBlockTable* pBlockTable = NULL;
    dbExtern->getSymbolTable( pBlockTable, AcDb::kForRead );
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    
    // R�cuperer le model space, puis ferme la liste des blocks
    if( es = pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForRead ) )
    {
        dbExtern->close();
        return es;
    }
    
    // On ferme la table des blocs
    pBlockTable->close();
    
    // On cr�e un it�rateur sur les objets du model space
    AcDbBlockTableRecordIterator* it;
    pBlockTableRecord->newIterator( it );
    it->start();
    
    // On boucle jusqu'� ce qu'on est parcouru tous les objets du dessin
    AcDbEntity* ent;
    
    while( !it->done() )
    {
        // R�cup�rer l'entit� courante
        if( es = it->getEntity( ent, AcDb::kForRead ) )
            return es;
            
        objectIdArray.append( ent->objectId() );
        
        // On oublie pas de fermer l'entit�
        if( es = ent->close() )
            return es;
            
        // On incr�mente l'it�rateur
        it->step();
    }
    
    // Fermer le model space
    return pBlockTableRecord->close();
}


Acad::ErrorStatus importEntity(
    AcDbObjectIdArray& objectIdArray,
    const AcString& dwgFile,
    AcDbDatabase* dbExtern )
{
    // Ouvrir la base de donn�es externe
    Acad::ErrorStatus es;
    
    // R�cuperer la database
    if( dbExtern == NULL )
    {
        dbExtern = new AcDbDatabase( Adesk::kFalse );
        
        if( es = dbExtern->readDwgFile( dwgFile ) )
        {
            //Sortir
            dbExtern->close();
            return es;
        }
    }
    
    // R�cuperer la liste des blocks
    AcDbBlockTable* pBlockTable = NULL;
    es = dbExtern->getSymbolTable( pBlockTable, AcDb::kForWrite );
    AcDbBlockTableRecord* pBlockTableRecord = NULL;
    
    // R�cuperer le model space, puis ferme la liste des blocks
    if( es = pBlockTable->getAt( ACDB_MODEL_SPACE, pBlockTableRecord, AcDb::kForRead ) )
    {
        dbExtern->close();
        return es;
    }
    
    // On ferme la table des blocs
    pBlockTable->close();
    
    // On cr�e un it�rateur sur les objets du model space
    AcDbBlockTableRecordIterator* it;
    pBlockTableRecord->newIterator( it );
    it->start();
    
    // On boucle jusqu'� ce qu'on est parcouru tous les objets du dessin
    AcDbEntity* ent;
    
    while( !it->done() )
    {
        // R�cup�rer l'entit� courante
        if( es = it->getEntity( ent, AcDb::kForRead ) )
            return es;
            
        objectIdArray.append( ent->objectId() );
        
        // On oublie pas de fermer l'entit�
        if( es = ent->close() )
            return es;
            
        // On incr�mente l'it�rateur
        it->step();
    }
    
    // Fermer le model space
    return pBlockTableRecord->close();
}