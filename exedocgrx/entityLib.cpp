#pragma once
#include "entityLib.h"

AcString entityType(AcDbEntity* ent)
{
	AcString ac;
	
	if (ent->isKindOf(AcDb3dSolid::desc()))
		ac = strToAcStr("AcDb3dSolid");

	else if (ent->isKindOf(AcDbBlockBegin::desc()))
		ac = strToAcStr("AcDbBlockBegin");
	
	else if (ent->isKindOf(AcDbBlockReference::desc()))
		ac = strToAcStr("AcDbBlockReference");
	
	else if (ent->isKindOf(AcDbBody::desc()))
		ac = strToAcStr("AcDbBody");
	
	else if (ent->isKindOf(AcDb2dPolyline::desc()))
		ac = strToAcStr("AcDb2dPolyline");
	
	else if (ent->isKindOf(AcDb3dPolyline::desc()))
		ac = strToAcStr("AcDb3dPolyline");
	
	else if (ent->isKindOf(AcDbArc::desc()))
		ac = strToAcStr("AcDbArc");
	
	else if (ent->isKindOf(AcDbCircle::desc()))
		ac = strToAcStr("AcDbCircle");
	
	else if (ent->isKindOf(AcDbEllipse::desc()))
		ac = strToAcStr("AcDbEllipse");
	
	else if (ent->isKindOf(AcDbLeader::desc()))
		ac = strToAcStr("AcDbLeader");
	
	else if (ent->isKindOf(AcDbLine::desc()))
		ac = strToAcStr("AcDbLine");
	
	else if (ent->isKindOf(AcDbRay::desc()))
		ac = strToAcStr("AcDbRay");
	
	else if (ent->isKindOf(AcDbFace::desc()))
		ac = strToAcStr("AcDbFace");
	
	else if (ent->isKindOf(AcDbHatch::desc()))
		ac = strToAcStr("AcDbHatch");
	
	else if (ent->isKindOf(AcDbImage::desc()))
		ac = strToAcStr("AcDbImage");
	
	else if (ent->isKindOf(AcDbMLeader::desc()))
		ac = strToAcStr("AcDbMLeader");
	
	else if (ent->isKindOf(AcDbMLeader::desc()))
		ac = strToAcStr("AcDbMLeader");
	
	else if (ent->isKindOf(AcDbMText::desc()))
		ac = strToAcStr("AcDbMText");
	
	else if (ent->isKindOf(AcDbPolyFaceMesh::desc()))
		ac = strToAcStr("AcDbPolyFaceMesh");
	
	else if (ent->isKindOf(AcDbPolygonMesh::desc()))
		ac = strToAcStr("AcDbPolygonMesh");
	
	else if (ent->isKindOf(AcDbRegion::desc()))
		ac = strToAcStr("AcDbRegion");
	
	else if (ent->isKindOf(AcDbSection::desc()))
		ac = strToAcStr("AcDbSection");
	
	else if (ent->isKindOf(AcDbSolid::desc()))
		ac = strToAcStr("AcDbSolid");
	
	else if (ent->isKindOf(AcDbSubDMesh::desc()))
		ac = strToAcStr("AcDbSubDMesh");
	
	else if (ent->isKindOf(AcDbSurface::desc()))
		ac = strToAcStr("AcDbSurface");
	
	else if (ent->isKindOf(AcDbText::desc()))
		ac = strToAcStr("AcDbText");
	
	else if (ent->isKindOf(AcDbTrace::desc()))
		ac = strToAcStr("AcDbTrace");
	
	else if (ent->isKindOf(AcDbUnderlayReference::desc()))
		ac = strToAcStr("AcDbUnderlayReference");
	
	else if (ent->isKindOf(AcDbVertex::desc()))
		ac = strToAcStr("AcDbVertex");

	else if (ent->isKindOf(AcDbPolyline::desc()))
		ac = strToAcStr("AcDbPolyline");

	else if (ent->isKindOf(AcDbDimension::desc()))
		ac = strToAcStr("AcDbDimension");

	return ac;
}