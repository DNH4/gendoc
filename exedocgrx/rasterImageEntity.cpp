/**
 * @file rasterImageEntity.cpp
 * @author Romain LEMETTAIS
 * @brief Fonctions permettant de selectionner, creer et traiter les images raster
 */

//#define _USE_MATH_DEFINES
#include "rasterImageEntity.h"


long getSsRasterImage(
    ads_name&       ssName,
    const AcString& sLayerName )
{
    return getSelectionSet( ssName, "", "IMAGE", sLayerName );
}



long getSsOneRasterImage(
    ads_name&       ssName,
    const AcString& sLayerName )
{
    return getSingleSelection( ssName, "IMAGE", sLayerName );
}



long getSsAllRasterImage(
    ads_name&       ssName,
    const AcString& sLayerName )
{
    return getSelectionSet( ssName, "X", "IMAGE", sLayerName ) ;
}




AcDbRasterImage* getRasterImageFromSs(
    const ads_name&          ssName,
    const long&              iObject,
    const AcDb::OpenMode&    opMode )
{
    //Recuperer l'entite
    AcDbEntity* pEntityRasterImage = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en image
    return AcDbRasterImage::cast( pEntityRasterImage );
}



AcDbRasterImage* readRasterImageFromSs(
    const ads_name&          ssName,
    const long&              iObject )
{
    return getRasterImageFromSs( ssName, iObject, AcDb::kForRead );
}



AcDbRasterImage* writeRasterImageFromSs(
    const ads_name&          ssName,
    const long&              iObject )
{
    return getRasterImageFromSs( ssName, iObject, AcDb::kForWrite );
}


AcDbDictionary* getImageDictionary(
    const AcDb::OpenMode&    opMode )
{
    //Recuperer la base de donn�es
    AcDbDatabase* pDb = acdbHostApplicationServices()->workingDatabase();
    
    //Recuperer l'ID du dictionnaire des images
    AcDbObjectId idImageDict = AcDbRasterImageDef::imageDictionary( pDb );
    
    //Cas ou le dictionnaire n'existe pas
    if( idImageDict.isNull() )
    {
        //Creer le dictionnaire des images
        if( Acad::eOk != AcDbRasterImageDef::createImageDictionary( pDb, idImageDict ) )
        {
            //Sortir
            return NULL;
        }
    }
    
    //Declarer le dictionnaire
    AcDbDictionary* pImageDict = NULL;
    
    //Ouvrir le dictionnaire
    if( Acad::eOk != acdbOpenObject( ( AcDbObject*& )pImageDict, idImageDict, opMode ) )
    {
        //Sortir
        return NULL;
    }
    
    //Sortir
    return pImageDict;
}



AcDbRasterImageDef* getRasterImageDefinitionInDictionary(
    const AcString&             sRasterImageName,
    const AcDbDictionary* const pImageDict,
    const AcDb::OpenMode&       opMode )
{
    //Verifier si l'image est dans le dictionnaire
    if( !pImageDict->has( sRasterImageName ) )
    {
        //Sortir
        return NULL;
    }
    
    //Declarer la definition d'une image
    AcDbRasterImageDef* pImageDef = NULL;
    
    //Recuperer la definition existante
    if( Acad::eOk != pImageDict->getAt( sRasterImageName, ( AcDbObject*& )pImageDef, opMode ) )
    {
        //Sortir
        return NULL;
    }
    
    //Sortir
    return pImageDef;
}


AcDbRasterImageDef* getRasterImageDefinition(
    const AcString&         sRasterImageName,
    const AcDb::OpenMode&   opMode )
{
    //Ouvrir le dictionnaire
    AcDbDictionary* pImageDict = getImageDictionary( AcDb::kForRead );
    
    //Recuperer la definition de l'image
    AcDbRasterImageDef* pImageDef = getRasterImageDefinitionInDictionary( sRasterImageName, pImageDict, opMode );
    
    //Fermer le dictionnaire
    pImageDict->close();
    
    //Sortir
    return pImageDef;
}


AcDbRasterImageDef* getRasterImageDefinition(
    const AcDbObjectId&     idRasterImageDef,
    const AcDb::OpenMode&   opMode )
{
    //Declarer la definition de l'image
    AcDbRasterImageDef* pImageDef = NULL;
    
    //Recuperer la definition de l'image
    if( Acad::eOk != acdbOpenObject( pImageDef, idRasterImageDef, opMode ) )
        return NULL;
        
    //Sortir
    return pImageDef;
}


AcDbRasterImageDef* getRasterImageDefinition(
    const AcDbRasterImage*  obRasterImage,
    const AcDb::OpenMode&   opMode )
{
    return getRasterImageDefinition( obRasterImage->imageDefId(), opMode );
}


AcDbRasterImageDef* addRasterImageDefinition(
    const AcString&         sRasterImageName,
    const AcDb::OpenMode&   opMode )
{
    //Ouvrir le dictionnaire
    AcDbDictionary* pImageDict = getImageDictionary( AcDb::kForWrite );
    
    //Recuperer la definition de l'image
    AcDbRasterImageDef* pImageDef = getRasterImageDefinitionInDictionary( sRasterImageName, pImageDict, opMode );
    
    //Si l'image a �t� trouv�
    if( pImageDef )
    {
        //Sortir
        pImageDict->close();
        return pImageDef;
    }
    
    //Declarer la definition d'une image
    pImageDef = new AcDbRasterImageDef();
    
    //Declarer l'id de la definition de l'image
    AcDbObjectId idImageDef;
    
    //Ajouter l'image au dictionnaire
    if( Acad::eOk != pImageDict->setAt( sRasterImageName, pImageDef, idImageDef ) )
    {
        //Sortir
        pImageDict->close();
        delete pImageDef;
        return NULL;
    }
    
    //Fermer le dictionnaire
    pImageDict->close();
    
    //Si la definition doit etre ouverte en lecture
    if( opMode == AcDb::kForRead )
    {
        //Passer en mode lecture
        pImageDef->downgradeOpen();
    }
    
    //Sortir
    return pImageDef;
}




AcDbRasterImage* insertRasterImage(
    const AcString&         sFileToInsert,
    AcString                sRasterImageName,
    const AcDb::OpenMode&   opMode )
{
    //Verifier le nom de l'image
    if( sRasterImageName.isEmpty() )
        sRasterImageName = getFileName( sFileToInsert );
        
    //Recuperer la definition de l'image
    AcDbRasterImageDef* pImageDef = addRasterImageDefinition( sRasterImageName, AcDb::kForWrite );
    
    //Verifier si la definition est ouverte
    if( !pImageDef )
    {
        //Sortir
        return NULL;
    }
    
    //Definir le chemin d'acces de l'image
    if( Acad::eOk != pImageDef->setSourceFileName( sFileToInsert ) )
    {
        //Sortir
        pImageDef->close();
        return NULL;
    }
    
    //Charger l'image
    reloadRasterImage( pImageDef );
    
    //Recuperer l'id de la definition
    AcDbObjectId idImageDef = pImageDef->objectId();
    
    //Sortir
    pImageDef->close();
    
    //Cas ou le dictionnaire n'existe pas
    if( idImageDef.isNull() )
    {
        //Sortir
        //pImageDef->close();
        return NULL;
    }
    
    //Declarer une nouvelle reference d'image
    AcDbRasterImage* pImageRef = new AcDbRasterImage;
    
    //Definir l'image
    if( Acad::eOk != pImageRef->setImageDefId( idImageDef ) )
    {
        //Sortir
        //pImageDef->close();
        delete pImageRef;
        return NULL;
    }
    
    //Ajouter l'image au modelspace
    addToModelSpace( pImageRef );
    
    ////Creer le reacteur ???
    //AcDbObjectPointer<AcDbRasterImageDefReactor> rasterImageDefReactor;
    //rasterImageDefReactor.create();
    //
    //if( Acad::eOk != rasterImageDefReactor->setOwnerId( pImageRef->objectId() ) )
    //{
    //    //Sortir
    //    pImageDef->close();
    //    pImageRef->erase();
    //    pImageRef->close();
    //    return NULL;
    //}
    //
    ////Declarer l'identifiant du reacteur
    //AcDbObjectId idReactor;
    //
    ////Assigner le reacteur ???
    //if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->addAcDbObject( idReactor, rasterImageDefReactor.object() ) )
    //{
    //    //Sortir
    //    pImageDef->close();
    //    pImageRef->erase();
    //    pImageRef->close();
    //    return NULL;
    //}
    //
    ////Ajouter le reacteur sur la reference d'image ???
    //pImageRef->setReactorId( idReactor );
    //
    ////Ajouter le reacteur sur la definition d'image ???
    //pImageDef->addPersistentReactor( idReactor );
    //
    ////Fermer la definition de l'image
    //pImageDef->close();
    
    //Si la definition doit etre ouverte en lecture
    if( opMode == AcDb::kForRead )
    {
        //Passer en mode lecture
        pImageRef->downgradeOpen();
    }
    
    //Sortir
    return pImageRef;
}

AcDbRasterImage* insertRasterImage(
    const AcString&         sFileToInsert,
    const AcGePoint3d&      ptInsert,
    AcString                sRasterImageName,
    const AcDb::OpenMode&   opMode )
{
    //Inserer l'image en ecriture
    AcDbRasterImage* pImageRef = insertRasterImage( sFileToInsert, sRasterImageName, AcDb::kForWrite );
    
    //Verifier si l'image est charg�e
    if( !pImageRef )
        return NULL;
        
    //Deplacer l'image
    pImageRef->transformBy( AcGeMatrix3d::translation( ptInsert.asVector() ) );
    
    //Si la definition doit etre ouverte en lecture
    if( opMode == AcDb::kForRead )
    {
        //Passer en mode lecture
        pImageRef->downgradeOpen();
    }
    
    //Sortir
    return pImageRef;
}

AcDbRasterImage* insertRasterImage(
    const AcString&         sFileToInsert,
    const AcGePoint3d&      ptInsert,
    const AcGeVector3d&     vectX,
    const AcGeVector3d&     vectY,
    AcString                sRasterImageName,
    const AcDb::OpenMode&   opMode )
{
    //Inserer l'image en ecriture
    AcDbRasterImage* pImageRef = insertRasterImage( sFileToInsert, sRasterImageName, AcDb::kForWrite );
    
    //Verifier si l'image est charg�e
    if( !pImageRef )
        return NULL;
        
    //Modifier le point d'insertion, la dimension et l'orientation
    pImageRef->setOrientation( ptInsert, vectX, vectY ) ;
    
    //Si la definition doit etre ouverte en lecture
    if( opMode == AcDb::kForRead )
    {
        //Passer en mode lecture
        pImageRef->downgradeOpen();
    }
    
    //Sortir
    return pImageRef;
}




bool reloadRasterImage(
    AcDbRasterImageDef* const pImageDef )
{
    //Verifier si l'image est charg�e
    if( pImageDef->isLoaded() )
    
        //Decharger l'image
        if( Acad::eOk != pImageDef->unload() )
            return false;
            
    //Charger l'image
    return Acad::eOk == pImageDef->load();
}

