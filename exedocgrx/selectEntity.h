/**
 * @file selectEntity.h
 * @author Romain LEMETTAIS
 * @brief Outils de selection
 */


#pragma once
#include <aced.h>
#include <adscodes.h>
#include <dbmain.h>
#include "pointEntity.h"
#include <vector>


/**
  * \brief Permet de faire une selection en fonction de plusieurs parametres
  * \param ssName Pointeur sur la selection
  * \param ssType Type de selection ( X pour tout, S pour unique, sinon NULL si on veut laisser une selection quelconque )
  * \param objectType  Type d'objet a selectionner
  * \param layerName  Selectionner uniquement les objets dans ce calque
  * \param blockName  Block a selectionner si l'objet selectionne est un bloc
  * \param textValue  Valeur du texte si l'objet selectionne est un texte
  * \param isClosed True si la polyligne est fermee, false sinon
  * \param is3d True si la polyligne est 3d, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSelectionSet(
    ads_name         ssName,
    const AcString&  ssType     =   "",
    const AcString&  objectType =   "",
    const AcString&  layerName  =   "",
    const AcString&  blockName  =   "",
    const AcString&  textValue  =   "",
    const bool&      isClosed   =   false,
    const bool&      is3d       =   false );



/**
  * \brief Permet de faire une selection UNIQUE en fonction de plusieurs parametres
  * \param ssName Pointeur sur la selection
  * \param objectType  Type d'objet a selectionner
  * \param layerName  Selectionner uniquement les objets dans ce calque
  * \param blockName  Block a selectionner si l'objet selectionne est un bloc
  * \param textValue  Valeur du texte si l'objet selectionne est un texte
  * \param isClosed True si la polyligne est fermee, false sinon
  * \param is3d True si la polyligne est 3d, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSingleSelection(
    ads_name         ssName,
    const AcString&  objectType =   "",
    const AcString&  layerName  =   "",
    const AcString&  blockName  =   "",
    const AcString&  textValue  =   "",
    const bool&      isClosed   =   false,
    const bool&      is3d       =   false );




/**
  * \brief Permet de connaitre le nombre d'objets dans une selection
  * \param ssName Pointeur sur la selection
  * \return Retourne le nombre d'objets dans la selection
  */
long getSsLength( const ads_name ssName );


/**
  * \brief Verifie que la selection n'est pas vide
  * \param ssName Pointeur sur la selection
  * \return Retourne True si la selection n'est pas vide
  */
bool isSsValid( const ads_name ssname );

/**
  * \brief Permet d'obtenir l'identifiant de l'objet N
  * \param ssName Pointeur sur la selection
  * \param iObject Index de l'objet dont on veut l'identifiant
  * \return Retourne l'identifiant de l'objet N
  */
AcDbObjectId getObIdFromSs(
    const ads_name&        ssName,
    const long&            iObject = 0 );



/**
  * \brief Permet d'obtenir l'objet N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index de l'objet, par defaut 0
  * \param opMode Mode d'ouverture de l'objet, par defaut en lecture
  * \return Retourne l'objet N
  */
AcDbEntity* getEntityFromSs(
    const ads_name&          ssName,
    const long&              iObject = 0,
    const AcDb::OpenMode&    opMode  = AcDb::kForRead );


/**
  * \brief Permet d'obtenir l'objet N dans la selection en LECTURE
  * \param ssName Pointeur sur la selection
  * \param iObject Index de l'objet, par defaut 0
  * \return Retourne l'objet N
  */
AcDbEntity* readEntityFromSs(
    const ads_name        ssName,
    const long            iObject = 0 );



/**
  * \brief Permet d'obtenir l'objet N dans la selection en ECRITURE
  * \param ssName Pointeur sur la selection
  * \param iObject Index de l'objet, par defaut 0
  * \return Retourne l'objet N
  */
AcDbEntity* writeEntityFromSs(
    const ads_name        ssName,
    const long            iObject = 0 );


/**
  * \brief Permet de faire une selection quelconque dans un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsObject(
    ads_name        ssName,
    const AcString  layerName = "" );


/**
  * \brief Permet de faire une selection UNIQUE dans un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsOneObject(
    ads_name        ssName,
    const AcString  layerName = "" );


/**
  * \brief Permet de selectionnes TOUS les objets d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsAllObject(
    ads_name        ssName,
    const AcString  layerName = "" );


/**
  * \brief Permet de faire une selection quelconque sur les textes d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \param textValue Valeur du texte a selectionner
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsText(
    ads_name        ssName,
    const AcString  layerName  = "",
    const AcString  textValue  = "" );


/**
  * \brief Permet de faire une selection UNIQUE sur les textes d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \param textValue Valeur du texte a selectionner
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsOneText(
    ads_name        ssName,
    const AcString  layerName  = "",
    const AcString  textValue  = "" );


/**
  * \brief Permet de faire une selection UNIQUE sur les textes d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \param textValue Valeur du texte a selectionner
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsOneMText(
    ads_name        ssName,
    const AcString  layerName  = "",
    const AcString  textValue  = "" );


/**
  * \brief Permet de faire une selection sur tous les textes d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \param textValue Valeur du texte a selectionner
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsAllMText(
    ads_name        ssName,
    const AcString  layerName  = "",
    const AcString  textValue  = "" );


/**
  * \brief Permet de selectionner TOUS les textes d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \param textValue Valeur du texte a selectionner
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsAllText(
    ads_name        ssName,
    const AcString  layerName  = "",
    const AcString  textValue  = "" );



/**
  * \brief Fait une selection quelconque sur les polylignes 3d futurmap
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsFmPoly3D(
    ads_name        ssName,
    const AcString  layerName = "" );


/**
  * \brief Fait une selection unique sur les polylignes 3d futurmap
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes 1 ou 0
  */
long getSsOneFmPoly3D(
    ads_name        ssName,
    const AcString  layerName = "" );



/**
  * \brief Zoom sur une entit� deriv� du AcDbEntity
  * \param entity Pointeur sur une entite
  * \param height Hauteur de zoom
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus zoomOnEntity( AcDbEntity* entity,
    const double& height = 5.0 );


/**
  * \brief Permet de recuperer un texte a partir d'une selection
  * \param ssName La selection
  * \param iObject Numero du texte dans la selection
  * \return opMode Mode d'ouverture du texte
  */
AcDbText* getTextFromSs( const ads_name& ssName,
    const long& iObject,
    const AcDb::OpenMode& opMode );

/**
  * \brief Permet de faire une selection quelconque sur les Mtextes d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \param textValue Valeur du Mtexte a selectionner
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsMText(
    ads_name        ssName,
    const AcString  layerName  = "",
    const AcString  textValue  = "" );

/**
  * \brief Permet de recuperer un Mtexte a partir d'une selection
  * \param ssName La selection
  * \param iObject Numero du Mtexte dans la selection
  * \return opMode Mode d'ouverture du Mtexte
  */
AcDbMText* getMTextFromSs( const ads_name& ssName,
    const long& iObject,
    const AcDb::OpenMode& opMode );

/**
  * \brief Fait une selection quelconque sur les xlines
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsXline(
    ads_name        ssName,
    const AcString  layerName = "" );


/**
  * \brief Fait une selection unique sur les xlines
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes 1 ou 0
  */
long getSsOneXline(
    ads_name        ssName,
    const AcString  layerName = "" );

/**
  * \brief Permet de selectionnes TOUS les xlines d'un calque
  * \param ssName Pointeur sur la selection
  * \param layerName Calque des objets
  * \return Retourne le nombre d'objets selectionnes
  */
long getSsAllXLine(
    ads_name        ssName,
    const AcString  layerName = "" );

/**
  * \brief Permet de reperer  une xline a partir d'une selection
  * \param ssName La selection
  * \param iObject Numero du texte dans la selection
  * \return opMode Mode d'ouverture du texte
  */
AcDbXline* getXlineFromSs( const ads_name& ssName,
    const long& iObject,
    const AcDb::OpenMode& opMode =  AcDb::kForRead );


/**
  * \brief Cr�er une s�lection d�j� s�lectionn�e � la fin de la commande, ATTENTION: il faut toujours fermer l'entit� avant de la passer dans la fonction
  * \param pEnt Pointeur sur l'entit� � rajouter dans la s�lection
  * \return Acad::eOk
  */
Acad::ErrorStatus createPickFirstSelection( const AcDbEntity* pEnt );


/**
  * \brief Permet de r�cuperer tous les objets textes dans une selection
  * \param sstext Selection de texte
  * \param vecttextpointer Vecteur contenant les textes
  * \return Acad::eOk
  */
Acad::ErrorStatus getAllTextFromSs(
    const ads_name& sstext,
    std::vector<AcDbText*>& vecttextpointer,
    std::vector<double>& vectX,
    const AcDb::OpenMode& op = AcDb::kForRead );



/**
  * \brief Permet de recuperer la derni�re entit� inser�e
  * \param entity La derni�re entit�
  * \param op Mode d'ouverture
  * \return Acad::eOk
  */
Acad::ErrorStatus getLastEntityInserted(
    AcDbEntity*& entity,
    const AcDb::OpenMode& op = AcDb::kForRead );