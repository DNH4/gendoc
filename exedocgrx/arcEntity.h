/**
 * @file arcEntity.h
 * @author Romain LEMETTAIS
 * @brief Fichier contenant des fonctions utiles pour travailler avec les arcs
 */

#pragma once
#include <dbents.h>
#include "selectEntity.h"
#include <math.h>
#include "pointEntity.h"
#include <math.h>
#include <poly2DEntity.h>


/**
  * \brief Fait une selection quelconque sur les arc
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsArc(
    ads_name&  ssName,
    const AcString&  layerName = "" );


/**
  * \brief Selectionne un seul arc dans le dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsOneArc(
    ads_name&    ssName,
    const AcString&    layerName = "" );



/**
  * \brief Selectionne tous les arcs du dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsAllArc(
    ads_name&  ssName,
    const AcString&  layerName = "" );


/**
  * \brief Renvoie un pointeur sur l'arc numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de l'arc a recuperer, par defaut 0
  * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \param return Pointeur sur un arc
  */
AcDbArc* getArcFromSs(
    const ads_name& ssName,
    const long iObject          = 0,
    const AcDb::OpenMode opMode = AcDb::kForRead );


/**
  * \brief Renvoie un pointeur en LECTURE sur l'arc numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de l'arc a recuperer, par defaut 0
  * \param return Pointeur sur un arc
  */
AcDbArc* readArcFromSs(
    const ads_name&    ssName,
    const long        iObject = 0 );


/**
  * \brief Renvoie un pointeur en ECRITURE sur l'arc numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de l'arc a recuperer, par defaut 0
  * \param return Pointeur sur un arc
  */
AcDbArc* writeArcFromSs(
    const ads_name&        ssName,
    const long            iObject = 0 );



/**
  * \brief Cree un pointeur sur l'AcGeCircArc3d avec les memes caracteristiques que l'arc
  * \param arc Pointeur sur l'arc qui va etre copie
  * \return Pointeur sur un AcGeCircArc3d
  */
AcGeCircArc3d* getAcGeArc(
    AcDbArc*    arc );


/**
  * \brief Fonction pour discretiser un arc
  * \param arc Arc a discretiser
  * \param tol Fleche de discretisation
  * \image html C:\Futurmap\Dev-Outils\GStarCAD\GRX\FmapLib\documentation\images\fleche.png
  * \return Liste de points de l'arc entre lesquels on va approximer l'arc par des segments
  */
AcGePoint3dArray discretize(
    const             AcGeCircArc3d,
    const double&      tol );

/**
  * \brief Fonction pour discretiser un arc
  * \param arc Arc a discretiser
  * \param tol Fleche de discretisation
  * \param return Liste de points de l'arc entre lesquels on va approximer l'arc par des segments
  */
AcGePoint3dArray discretize(
    AcDbArc*    arc,
    const double&      tol );



/**
  * \brief Fonction pour calculer l'angle total d'un arc
  * \param arc Arc
  * \param return double l'angle
  */
double getTotalAngle(
    const AcDbArc*  arc );

/**
  * \brief Fonction pour calculer la bulge d'un arc
  * \param arc Arc
  * \param return double la bulge
  */
double getBulgeAt(
    AcDbArc*  arc );



/**
  * \brief Fonction pour calculer l'angle total d'un arc
  * \param startAngle
  * \param endAngle
  * \param return double l'angle
  */
double getTotalAngle( double startAngle, double endAngle );

/**
  * \brief Fonction pour calculer la bulge d'un arc
  * \param startAngle
  * \param endAngle
  * \param return double la bulge
  */
double getBulgeAt( double startAngle, double endAngle );


/**
  * \brief Fonction pour creer un arc passant par 3 points 2D
  * \param pt1 Premier point de l'arc
  * \param pt2 Second Point de l'arc
  * \param pt3 Dernier point de l'arc
  * \param return Renvoie un pointeur sur l'AcDbArc cree
  */
AcDbArc* arcByThreePoints(
    const AcGePoint2d& pt1,
    const AcGePoint2d& pt2,
    const AcGePoint2d& pt3 );

/**
  * \brief Fonction pour creer un arc passant par 3 points 3D
  * \param pt1 Premier point de l'arc
  * \param pt2 Second Point de l'arc
  * \param pt3 Dernier point de l'arc
  * \param return Renvoie un pointeur sur l'AcDbArc cree
  */
AcDbArc* arcByThreePoints(
    const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const AcGePoint3d& pt3 );


/**
  * \brief Permet de recuperer le point milieu d'un arc
  * \param pt1 Premier point de l'arc
  * \param pt2 Second Point de l'arc
  * \param bulge Le bulge de l'arc ( le sens de l'arc est decrit par le signe du bulge )
  * \param tol Tolerance de discretisation
  * \param return Le point milieu de l'arc
  */
AcGePoint2d getMidPoint( const AcGePoint2d& pt1, const AcGePoint2d& pt2, const double& bulge = 0.0, const double& tol = 0.001 );