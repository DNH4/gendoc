/**
 * @file print.h
 * @brief Fichier contenant des fonctions utiles pour afficher des messages dans la console GStarCAD
 */
#include "fmapHeaders.h"
#include "acString.h"

void print( const AcGePoint3d& point );
void print( const AcGePoint2d& point );

void print( const int& number );
void print( const string& str, const int& val );
void print( const string& str, const double& val );
void print( const string& str, const int& val );
void print( const unsigned __int64& number );
void print( const float& number );
void print( const double& number );
void print( const AcString& acStr );
void print( const string& str );
void print( const char str[] );
void print( const Acad::ErrorStatus& es );
void print( const AcGeVector2d& vec2d );
void print( const AcGeVector3d& vec3d );
void print( const vector< AcString >& vecAc );
void print( const string& str, const string& val );
void print( const AcString& str, const AcString& val );
void drawVectorDirection( const AcGeVector3d& vec,
    const AcGePoint3d& org,
    AcCmColor& color );

////Fonction templat�e pour printer les vecteurs
//template< typename AllType >
//void printVector( std::vector<AllType> vect )
//{
//    //Boucle pour afficher les elements du vecteur
//    for( int i = 0; i < vect.size(); i++ )
//    {
//        //Afficher l'element du i-�me vecteur
//        print( vect[i] );
//    }
//}