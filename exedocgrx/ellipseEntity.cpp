#include "ellipseEntity.h"


long getSsEllipse(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "", "ELLIPSE", layerName );
}


long getSsOneEllipse(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSingleSelection( ssName, "ELLIPSE", layerName );
}


long getSsAllEllipse(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "X", "ELLIPSE", layerName );
}


AcDbEllipse* getEllipseFromSs(
    const ads_name&         ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  entityEllipse = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ellipse
    return AcDbEllipse::cast( entityEllipse );
}


AcDbEllipse* readEllipseFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getEllipseFromSs( ssName, iObject, AcDb::kForRead );
}


AcDbEllipse* writeEllipseFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getEllipseFromSs( ssName, iObject, AcDb::kForWrite );
}

AcGeEllipArc3d* getAcGeEllipse(
    const AcDbEllipse*  ellipse )
{
    // UTILISER LA FONCTION LENGTH
    double ellipseMajorRadius = ellipse->majorAxis().length();
    double ellipseMinorRadius = ellipse->minorAxis().length();
    return new AcGeEllipArc3d(
            ellipse->center(),
            ellipse->majorAxis(),
            ellipse->minorAxis(),
            ellipseMajorRadius,
            ellipseMinorRadius
        );
}

AcGePoint3dArray discretize( AcDbEllipse*  ellipse, const double& fleche )
{
    // Recuperer les parametres de l'ellipse
    double param1, param2;
    
    // RETOURNER ACGEPOINT3DARRAY VIDE
    if( Acad::eOk != ellipse->getStartParam( param1 ) ) return NULL;
    
    if( Acad::eOk != ellipse->getEndParam( param2 ) ) return NULL;
    
    // Tableaux contenant la liste des points de discretisation
    AcGePoint3dArray    ptArray;
    
    // Tableau de parametres en chaque point de discretisation
    // Inutile dans notre cas
    AcGeDoubleArray     dArray;
    
    //discretiser la courbe
    getAcGeEllipse( ellipse )->getSamplePoints(
        param1,
        param2,
        fleche,
        ptArray,
        dArray
    );
    
    // Vider la liste
    dArray.setLogicalLength( 0 );
    
    // Renvoyer le tableau
    return ptArray;
}