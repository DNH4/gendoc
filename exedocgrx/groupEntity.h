/**
 * @file groupEntity.h
 * @author Romain LEMETTAIS
 * @brief Fichier contenant des fonctions utiles pour travailler avec les groupes
 */

#pragma once
#include <dbgroup.h>
#include "selectEntity.h"

/**
  * \brief Renvoie le groupe demande
  * \param groupName Nom du groupe
  * \param opMode Mode d'ouverture du groupe
  * \return Pointeur sur le groupe, renvoie NULL si le groupe n'a pas ete trouve
  */
AcDbGroup* getGroup(
    const AcString        groupName,
    const AcDb::OpenMode  opMode );

/**
  * \brief Renvoie le groupe demande
  * \param groupName    Nom du groupe
  * \param opMode       Dictionnaire des groupes
  * \param opMode       Mode d'ouverture du groupe
  * \return Pointeur sur le groupe, renvoie NULL si le groupe n'a pas ete trouve
  */
AcDbGroup* getGroup(
    const AcString        groupName,
    const AcDbDictionary* groupeDict,
    const AcDb::OpenMode  opMode );


/**
 * \brief Cree un groupe
 * \param groupName Nom du groupe
 * \param groupDict Dictionnaire dans lequel stocke le groupe
 * \return Pointeur sur le groupe
 */
AcDbGroup* createGroup(
    const AcString    groupName,
    AcDbDictionary*   groupeDict );


/**
 * \brief Cree un groupe
 * \param groupName Nom du groupe
 * \return Pointeur sur le groupe
 */
AcDbGroup* createGroup(
    const AcString    groupName );


/**
 * \brief Renvoie la liste des groupes
 * \param opMode Mode d'ouverture du groupe
 * \return Pointeur sur le dictionnaire des groupes
 */
AcDbDictionary* getGroupDictionary(
    const AcDb::OpenMode  opMode );


/**
 * \brief Efface tous les objets d'un groupe
 * \param groupobject pointeur sur le groupe � effacer (ouvert en ecriture)
 * \param bEraseGroup True s'il faut purger aussi le groupe lui meme
 * \return Nombre d'objets effac� (-1 si erreur)
 */
int eraseGroupObject(
    AcDbGroup* groupObject,
    const bool bEraseGroup );

/**
 * \brief Recuperer le groupe a partir d'une selection
 * \param ssGroup la selection
 * \param iObject index du groupe dans la selection
 * \param Opmode mode d'overture
 * \return AcdbGroup*
 */
AcDbGroup* getGroupFromSs(const ads_name& ssGroup,
	const long& iObject = 0,
	const AcDb::OpenMode& opmode = AcDb::kForRead);