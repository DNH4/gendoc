﻿/**
 * @file poly3DEntity.h
 * @author Romain LEMETTAIS
 * @brief Fichier contenant des fonctions utiles pour travailler avec les polylignes 2d
 */


#pragma once
#include <dbents.h>
#include "selectEntity.h"
#include "list.h"
#include "geray3d.h"
#include <tchar.h>
#include <vector>
#include "getPoints.h"
#include "userInput.h"

/**
  * \brief une constante pour spécifer la valeur de la grande distance
  */
#define BIG_DISTANCE_REF 1000000.0f


/**
  * \brief une constante pour spécifer la valeur de référence de comparaison avec une block
  */
#define BLOCK_DISTANCE_REF 0.03//0.003f//3mm 30mm

/**
  * \brief Fait une sélection quelconque sur les polylignes 3d
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \param isClosed True si la polyligne est fermee, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSsPoly3D(
    ads_name&         ssName,
    const AcString&   layerName = "",
    const bool&       isClosed = false );

/**
  * \brief Sélectionne une seul polyligne dans le dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \param isClosed True si la polyligne est fermee, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSsOnePoly3D(
    ads_name&         ssName,
    const AcString&   layerName = "",
    const bool&       isClosed = false );

/**
  * \brief Sélectionne toutes les polylignes 3d du dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \param isClosed True si la polyligne est fermee, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSsAllPoly3D(
    ads_name&         ssName,
    const AcString&   layerName = "",
    const bool&       isClosed = false );

/**
  * \brief Fait une sélection sur tout les polylignes 3d futurmap dans le dessin
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \param isClosed True si la polyligne est fermee, false sinon
  * \return Nombre de polylignes 3d futurmap selectionnes dans le dessin
  */
long getSsAllFmPoly3D(
    ads_name& ssName,
    const AcString layerName = "",
    const bool& isClosed = false );

/**
 * \brief Renvoie un pointeur sur la polyligne 3d numero N dans la selection
 * \param ssName Pointeur sur la selection
 * \param iObject Index dans la selection de la polyligne a recuperer, par defaut 0
 * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
 * \return Pointeur sur une polyligne
 */
AcDb3dPolyline* getPoly3DFromSs(
    const ads_name&             ssName,
    const long&           iObject   =   0,
    const AcDb::OpenMode& opMode = AcDb::kForRead );

/**
  * \brief Renvoie un pointeur en LECTURE sur la polyligne numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de la polyligne a recuperer, par defaut 0
  * \return Pointeur sur une polyligne
  */
AcDb3dPolyline* readPoly3DFromSs(
    const ads_name&  ssName,
    const long&       iObject = 0 );

/**
  * \brief Renvoie un pointeur en ECRITURE sur la polyligne numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de la polyligne a recuperer, par defaut 0
  * \return Pointeur sur une polyligne
  */
AcDb3dPolyline* writePoly3DFromSs(
    ads_name&              ssName,
    const long&            iObject = 0 );


/**
  * \brief Indique si la polyligne est fermee ou non
  * \param Polyligne qui nous intéresse
  * \return Renvoie true si la polyligne est fermee, false sinon
  */
bool isClosed(
    const AcDb3dPolyline* const poly );


/**
  * \brief Calcule la longueur d'une polyligne 3D
  * \param Polyligne qui nous intéresse
  * \return Renvoie la longueur
  */
double getLengthPoly(
    const AcDb3dPolyline* poly );

/**
  * \brief Calcule la longueur 2d d'une polyligne 3D
  * \param Polyligne qui nous intéresse
  * \return Renvoie la longueur
  */
double get2DLengthPoly(
    AcDb3dPolyline*& poly );

/**
  * \brief Retourne le nombre de sommets d'une polyligne 3D
  * \param poly Polyligne qui nous intéresse
  * \return Renvoie le nombre de sommets
  */
int getNumberOfVertex(
    AcDb3dPolyline*  poly );

/**
  * \brief Test si le premier et le dernier sommet de la polyligne 3D sont superposés, supprime aussi le dernier sommet si demandé
  * \param poly3D Polyligne qui nous intéresse
  * \param tol La valeur de la tolérance
  * \param hasToErase Si elle est true, on supprime le dernier sommet
  * \return true si le premier et le dernier sommet de la polyligne 3D sont superposés, false sinon
  */
bool isBeginEqualToEnd(
    AcDb3dPolyline* poly3D,
    const double& tol,
    bool hasToErase );

/**
  * \brief Test si le premier et le dernier sommet de la polyligne 3D sont superposés, supprime aussi le dernier sommet si demandé
  * \param poly3D Polyligne qui nous intéresse
  * \param vertex L'itérateur des sommets de la polyligne 3D
  * \param pt0 Le premier sommet de la polyligne 3D
  * \param pt1 Le dernier sommet de la polyligne 3D
  * \param id1 L'Id du dernier sommet de la polyligne 3D
  * \param tol La valeur de la tolérance
  * \param hasToErase Si elle est true, on supprime le dernier sommet
  * \return true si le premier et le dernier sommet de la polyligne 3D sont superposés, false sinon
  */
bool isBeginEqualToEnd(
    AcDb3dPolyline* poly3D,
    AcDb3dPolylineVertex* vertex,
    AcGePoint3d& pt0,
    AcGePoint3d& pt1,
    AcDbObjectId& id1,
    const double& tol,
    bool hasToErase );

/**
  * \brief Test si le premier, le deuxieme et le dernier sommet de la polyligne 3D sont superposés, supprime aussi le dernier sommet si demandé
  * \param poly3D Polyligne qui nous intéresse
  * \param vertex L'itérateur des sommets de la polyligne 3D
  * \param pt0 Le premier sommet de la polyligne 3D
  * \param id0 L'Id du premier sommet de la polyligne 3D
  * \param pt1 Le deuxieme sommet de la polyligne 3D
  * \param pt2 le dernier sommet de la polyligne 3D
  * \param tol La valeur de la tolérance
  * \param hasToErase Si elle est true, on supprime le dernier sommet
  * \return true si le premier, le deuxieme et le dernier sommet de la polyligne 3D sont superposés
  */
bool isOneAndTwoColiToEnd(
    AcDb3dPolyline* poly3D,
    AcDb3dPolylineVertex* vertex,
    AcGePoint3d& pt0,
    AcDbObjectId& id0,
    AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    const double& tol,
    bool hasToErase );

/**
  * \brief Fusionne les sommets d'une polyligne 3D à partir d'un vecteur contenant les Id des sommets à supprimer
  * \param poly3D La polyligne selectionnée
  * \param vertex L'itérateur des sommets de la polyligne 3D
  * \param coliVec Le vecteur contenant l'id des sommets à supprimer de la polyligne 3D
  * \return Renvoie le nombre de sommets supprimés dans le dessin, return -1 s'il y a une erreur
  */
int mergePoly3dFromVector(
    AcDbPolyline* poly3D,
    AcDb3dPolylineVertex* vertex,
    std::vector< AcDbObjectId >& colivec );

/**
  * \brief Supprime les sommets superposés dans une polyligne 3D
  * \param poly3D La polyligne selectionnée
  * \param tol La valeur de la tolérance de superposition
  * \return Renvoie le nombre de sommets supprimés dans le dessin, return -1 s'il y a une erreur
  */
int netPoly(
    AcDb3dPolyline* poly3D,
    const double& tol );

/**
  * \brief Fusionne les sommets colinéaires dans une polyligne 3D
  * \param poly3D La polyligne selectionnée
  * \param tol La valeur de la tolérance de colinéarité
  * \return Renvoie le nombre de sommets colinéaires fusionnés dans une polyligne 3D, return -1 s'il y a une erreur
  */
int coliPoly(
    AcDb3dPolyline* poly3D,
    const double& tol );


/**
  * \brief Supprime les sommets superposés et/ou fusionne les sommets colinéaires dans une polyligne 3D
  * \param poly La polyligne selectionnée
  * \param tol La valeur de la tolérance de superposition
  * \return Renvoie le nombre de sommets colinéaires fusionnés ou de sommets superposés supprimés dans une polyligne 3D, return -1 s'il y a une erreur
  */
int netColiPoly(
    AcDb3dPolyline* poly3D,
    const double& tol );

/**
  * \brief Vérifier si un point est dans une polyligne 3D
  * \param poly3D la polyligne 3D
  * \param point3D à tester
  * \return booleen
  */
bool isPointInPoly( AcDb3dPolyline* poly3D,
    const AcGePoint3d& point );

/**
  * \brief Vérifier si un point est dans une polyligne 3D
  * \param poly3D la polyligne 3D
  * \param point3D à tester
  * \return booleen
  */
bool isPointInPoly3dOnPlane(AcDb3dPolyline* poly3D,
	const AcGePoint3d& point);

/**
  * \brief Vérifier si un point est dans une polyligne 3D
  * \param poly3D la polyligne 3D
  * \param point3D à tester
  * \return booleen
  */
bool isPointInPolyByAngle(AcDb3dPolyline* poly3D,
	const AcGePoint3d& point);

/**
  * \brief Récuperer le sommet de la polyligne 3D à la position i
  * \param poly3D La polyligne 3D
  * \param index Index du sommet à récupérer
  * \param ptOutput Le point 3D récupéré
  * \return ErrorStatus
  */
Acad::ErrorStatus getPointAt( AcDb3dPolyline* poly3D,
    const int& index,
    AcGePoint3d& ptOutput );

/**
  * \brief Changer la valeur de la sommet de la polyligne 3D à la position i
  * \param poly3D La polyligne 3D
  * \param index Index du sommet à changer
  * \param ptInput Le point 3D qu'on veut insérer
  * \return ErrorStatus
  */
Acad::ErrorStatus setPointAt( AcDb3dPolyline* poly3D,
    const int& index,
    AcGePoint3d& ptInput );

/**
  * \brief Insérer un sommet dans la polyligne 3D à la position i
  * \param poly3D La polyligne 3D
  * \param index Index du sommet à insérer
  * \param ptInput Le point 3D qu'on veut changer la valeur
  * \return ErrorStatus
  */
Acad::ErrorStatus insertPointAt( AcDb3dPolyline* poly3D,
    const int& index,
    AcGePoint3d& ptInput );

/**
  * \brief Supprimer le sommet de la polyligne 3D à la position i
  * \param poly3D La polyligne 3D
  * \param index Index du sommet à supprimer
  * \return ErrorStatus
  */

Acad::ErrorStatus getPointAt( AcDb3dPolyline* poly3D, // Fonction à renommer ?
    const int& index,
    AcGePoint3d& ptOutput );

/**
  * \brief Récuperer les sommets d'intersections des deux polylignes 3D
  * \param firstPoly3DId Id d'une polyligne 3D
  * \param secondPoly3DId Id d'une autre polyligne 3D
  * \return AcGePoint3dArray Liste des points d'intersection
  */
AcGePoint3dArray  intersectPoly3D( const AcDbObjectId& firstPoly3DId,
    const AcDbObjectId&  secondPoly3DId );

/**
  * \brief Récuperer les sommets d'intersections des deux polylignes 3D
  * \param firstPoly3D un pointer sur  polyligne 3D
  * \param secondPoly3D un pointer sur autre polyligne 3D
  * \param useVertex True si on considere que deux vertex superposés est une intersection
  * \return AcGePoint3dArray Liste des points d'intersection
  */
AcGePoint3dArray  intersectPoly3D( AcDb3dPolyline*& firstPoly3D,
    AcDb3dPolyline*& secondPoly3D );

/**
  * \brief Vérifier si la distance entre le point le plus proche de la polyligne 3D et un point 3D est inférieur à une distance de référence, si oui retourne un vecteur de translation
  * \param poly3D La polyligne 3D à tester
  * \param distRef Distance de référence
  * \param vecOfTranslationRef Vecteur de translation
  * \return True s'il faut translater la polyligne, false sinon
  */
bool hasToTranslate( AcDb3dPolyline* poly3D,
    const double& distRef,
    AcGeVector3d& vecOfTranslationRef );

/**
  * \brief Projeter les points intersections sur la poly3D concernée et ensuite les insérer
  * \param poly3D la polyligne 3D
  * \param intersectPoint3DArray tableau des intersections
  * \param tol tolérance  de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus projectAndInsertPointsToPoly3D( AcDb3dPolyline*& poly3D,
    AcGePoint3dArray& intersectPoint3DArray,
    const double& tol );

/**
  * \brief insérer les points intersections sur la poly3D
  * \param poly3D la polyligne 3D
  * \param intersectPoint3DArray tableau des intersections
  * \param tol tolérance  de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus insertVertexesToPoly3D( AcDb3dPolyline*& poly3D,
    AcGePoint3dArray& intersectPoint3DArray,
    const double& tol );

/**
  * \brief insérer les points intersections sur la poly3D
  * \param poly3D la polyligne 3D
  * \param count nombre des points inseres
  * \param intersectPoint3DArray tableau des intersections
  * \param tol tolérance  de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus insertVertexesToPoly3D( AcDb3dPolyline*& poly3D,
    long& count,
    AcGePoint3dArray& intersectPoint3DArray,
    const double& tol );


/**
  * \brief trouver et insérer les points d'intersections de 2 polylines
  * \param firstPoly3D le première polyligne 3D
  * \param secondPoly3D le première polyligne 3D
  * \param count nombre des intersections trouvées
  * \param tol valeur de tolérance.
  * \param intersecteVert Booleen si on doit ajouter un sommet sur deux vertexs superposés
  * \return ErrorStatus
  */
Acad::ErrorStatus intersectTwoPoly3D( AcDb3dPolyline*& firstPoly3D,
    AcDb3dPolyline*& secondPoly3D,
    long& count,
    const double& tol = 0.1 );

/**
  * \brief insérer un points intersection sur la poly3D
  * \param poly3D la polyligne 3D
  * \param AcGePoint3d point d'intersection
  * \param tol tolérance de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus insertVertexToPoly3D( AcDb3dPolyline*& poly3D,
    AcGePoint3d& intersectPoint3D,
    const double& tol );


/**
  * \brief insérer un point intersection sur la poly3D
  * \param idPoly3D Id de la polyligne 3D
  * \param onePt3D point à insérer
  * \param tol tolérance de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus insertVertexToPoly3D( const AcDbObjectId& idPoly3D,
    AcGePoint3d onePt3D,
    const double& tol = 0.01 );

/**
  * \brief insérer un point intersection sur la poly3D si ce point se trouve à une distane inférieur à filter
  * \param idPoly3D Id de la polyligne 3D
  * \param onePt3D point à insérer
  * \param tol tolérance de calcul
  * \param filter filtre de tolérence de distance
  * \return ErrorStatus
  */
Acad::ErrorStatus insertVertexToPoly3DFilter( const AcDbObjectId& idPoly3D,
    AcGePoint3d onePt3D,
    const double& tol,
    const double& filter );

/**
  * \brief insérer de points intersections sur la poly3D
  * \param idPoly3D Id de la polyligne 3D
  * \param pointsToInsertArray points à insérer
  * \param tol tolérance  de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus insertVertexesToPoly3DId( const AcDbObjectId& idPoly3D,
    AcGePoint3dArray& pointsToInsertArray,
    const double& tol );



/**
  * \brief insérer de points intersectiosn sur la poly3D si ce point se trouve à une distane inférieur à filter
  * \param idPoly3D Id de la polyligne 3D
  * \param pointsToInsertArray points à insérer
  * \param tol tolérance  de calcul
  * \param filter filtre de tolérence de distance
  * \return ErrorStatus
  */
Acad::ErrorStatus insertVertexesToPoly3DId( const AcDbObjectId& idPoly3D,
    AcGePoint3dArray& pointsToInsertArray,
    const double& tol, const double& filter );

/**
  * \brief trouver les points d'intersections de 2 polylignes 3D
  * \param idFirstPoly3D id de la première polyligne 3D
  * \param idSecondPoly3D id de la seconde polyligne 3D
  * \param pointsOfInsectionArray intersections trouvées
  * \param tol valeur de tolérance
  * \return ErrorStatus
  */
Acad::ErrorStatus intersectTwoPoly3D( const AcDbObjectId& idFirstPoly3D,
    const AcDbObjectId& idSecondPoly3D,
    AcGePoint3dArray& pointsOfInsectionArray,
    const double& tol );

/**
  * \brief Projeter un tableau de points sur la poly3D
  * \param idPoly3D de la première polyligne 3D
  * \param pointToProject3DArray tableau des points à projeter
   * \param pointProjected3DArray tableau des points projétés
  * \param tol tolérance  de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus projectToPoly3D( const AcDbObjectId& idPoly3D,
    const AcGePoint3dArray& pointsToProject3DArray,
    AcGePoint3dArray& pointsProjected3DArray,
    const double& tol );

/**
  * \brief Projeter un tableau de points sur la poly3D
  * \param poly3D polyligne 3D
  * \param pointToProject3DArray tableau des points à projeter
   * \param pointProjected3DArray tableau des points projétés
  * \param tol tolérance  de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus projectToPoly3D( AcDb3dPolyline*& poly3D,
    const AcGePoint3dArray& pointsToProject3DArray,
    AcGePoint3dArray& pointsProjected3DArray,
    const double& tol = 0.01 );

/**
  * \brief Trouver l'id du vertex précédant le point à insérer dans la poly3D
  * \param idPoly3D de la polyligne 3D
  * \param onePt3D point à truover
  * \param indexVertId id rechercher
  * \param tol tolérance  de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus searchPreviewVertextIdToPoint( AcDb3dPolyline* poly3D,
    AcGePoint3d onePt3D,
    AcDbObjectId& indexVertId,
    const double& tol );


/**
  * \brief Trouver l'index du vertex précédant le point à insérer dans la poly3D
  * \param idPoly3D de la polyligne 3D
  * \param onePt3D point à truover
  * \param indexVert index rechercher
  * \param tol tolérance  de calcul
  * \return ErrorStatus
  */
Acad::ErrorStatus searchPreviewIndexToPoint( AcDb3dPolyline* poly3D,
    AcGePoint3d onePt3D,
    int& indexVert,
    const double& tol );


/**
  * \brief Trouver l'id du vertex précédant le point à insérer dans la poly3D
  * \param idPoly3D de la première polyligne 3D
  * \param onePt3D tableau point à truover
  * \param indexVertId id rechercher
  * \param tol tolérance  de calcul
  * \param filter filtre de tolérence de distance
  * \return ErrorStatus
  */
Acad::ErrorStatus searchPreviewVertextIdToPointFilter( AcDb3dPolyline* poly3D,
    AcGePoint3d onePt3D,
    AcDbObjectId& indexVertId,
    const double& tol = 0.001, const double& filter = 0.001 );


/**
  * \brief Récupérer les coordonnées d'un sommet de la polyligne 3D à la position i
  * \param poly3D La polyligne 3D
  * \param index Index du sommet à récupérer
  * \param ptOutput Le point 3D récupéré
  * \return ErrorStatus
  */
Acad::ErrorStatus getPointAt( AcDb3dPolyline poly3D,
    const int& index
    , AcGePoint3d& ptOutput );

/**
  * \brief Changer les coordonnées du sommet de la polyligne 3D à la position i
  * \param poly3D La polyligne 3D
  * \param index Index du sommet à changer
  * \param ptInput Le point 3D qu'on veut insérer
  * \return ErrorStatus
  */
Acad::ErrorStatus setPointAt( AcDb3dPolyline poly3D, const int& index, AcGePoint3d& ptInput );

/**
  * \brief Insérer un sommet dans la polyligne 3D à la position i
  * \param poly3D La polyligne 3D
  * \param index Index du sommet à insérer
  * \param ptInput Le point 3D qu'on veut changer la valeur
  * \return ErrorStatus
  */
Acad::ErrorStatus insertPointAt( AcDb3dPolyline poly3D, //utile de garder une version avec pointeur et une sans pointeur ?
    const int& index,
    AcGePoint3d& ptInput );

/**
  * \brief Supprimer le sommet de la polyligne 3D à la position i
  * \param poly3D La polyligne 3D
  * \param index Index du sommet à supprimer
  * \return ErrorStatus
  */
Acad::ErrorStatus removePointAt( AcDb3dPolyline* poly3D,
    const int& index );


/**
  * \brief Calcule le milieu d'une polyligne 3d
  * \param poly3D La polyligne 3D
  * \param milieu Point milieu de la polyligne
  * \return ErrorStatus Détail de la commande
  */
Acad::ErrorStatus getMilieu( AcDbCurve* poly,
    AcGePoint3d& milieu );

/**
    Doc à écrire
*/
Acad::ErrorStatus getMilieu( AcDbCurve* poly,
    AcGePoint2d& milieu );

/**
  * \brief Calcule le centroïde d'un polygone( polyligne 3d fermée )
  * \param poly3D Polyligne 3D fermée
  * \param milieu Centroïde du polygone
  * \return ErrorStatus Détail de la commande, si la polyligne n'est pas fermée retourne eInvalidInput
  */
Acad::ErrorStatus getCentroid( AcDb3dPolyline* poly3D,
    AcGePoint3d& centroid );

/**
  * \brief Ajoute des sommets entre les sommets d'une polyligne
  * \param poly La polyligne à ajouter des vertex
  * \param nbr Nombre de sommet à ajouter entre les sommets de la polyligne
  * \param dist Si l'espace entre les deux vertex est superieur à "dist", on ajoute "nbr" vertex entre les deux sommet
  * \return True si tout est OK
  */
bool addSomeVertexToPoly( AcDb3dPolyline* poly, const int& nbr, const int& dist );


/**
  * \brief
  * \param
  * \return
  */
std::vector<AcGePoint3d> addSomeVertexToPoly( std::vector<AcGePoint3d>& pt, const int& nbr, const int& dist );

/**
  * \brief
  * \param
  * \return
  */
std::vector<AcGePoint3d> poly3dToVector( AcDb3dPolyline* poly3D );


/**
  * \brief Permet de dessiner une polyligne 3D
  * \param tab Vecteur contenant les points 3D de la polyligne à dessinée
  * \param colorPoly Couleur de la polyligne
  * \param layer Calque ou on veut mettre la polyligne dans le dessin
  * \param coli Par défaut true : permet de supprimer les vertex colinéaires dans la polyligne dessinée
  * \return vide
  */
void drawPolyline3D( std::vector<AcGePoint3d>& tab,
    AcCmColor& colorPoly,
    AcString layer,
    const bool& coli = true,
    const bool& hasToZoom = false,
    const bool& closePoly = false );


/**
  * \brief Permet de dessiner une polyligne 3D
  * \param tab Vecteur contenant les points 3D de la polyligne à dessinée
  * \param colorPoly Couleur de la polyligne
  * \param layer Calque ou on veut mettre la polyligne dans le dessin
  * \param coli Par défaut true : permet de supprimer les vertex colinéaires dans la polyligne dessinée
  * \return vide
  */
void drawPolyline3D( AcGePoint3dArray& tab,
    AcCmColor& colorPoly,
    AcString layer,
    const bool& coli = true,
    const bool& hasToZoom = false,
    const bool& closePoly = false );


/**
  * \brief Permet de dessiner une polyligne 3D
  * \param tab Vecteur contenant les points 3D de la polyligne à dessinée
  * \param colorPoly Couleur de la polyligne
  * \param layer Calque ou on veut mettre la polyligne dans le dessin
  * \param coli Par défaut true : permet de supprimer les vertex colinéaires dans la polyligne dessinée
  * \return La polyligne
  */
AcDb3dPolyline* drawGetPolyline3D( std::vector<AcGePoint3d>& tab,
    AcCmColor& colorPoly,
    AcString layer,
    const bool& coli = true,
    const bool& hasToZoom = false );

/**
  * \brief
  * \param
  * \return
  */
AcDb3dPolyline* drawGetPolyline3D( AcGePoint3dArray& tab,
    const AcCmColor& colorPoly = AcCmColor(),
    const AcString& layer = _T( "" ),
    const bool& coli = true,
    const bool& hasToZoom = false );


/**
  * \brief Zoom sur un objet polyligne 3D
  * \param polyline3D Pointeur sur un polyligne 3D
  * \param height Hauteur de zoom
  * \return ErrorStatus Acad::eOk si l'opération s'effectue avec succès
  */
Acad::ErrorStatus zoomOn( AcDb3dPolyline* polyline3D,
    const double& height = 5.0 );


/**
  * \brief Permet de projeter une polyligne 3d sur un block
  * \param poly3d La polyligne en question
  * \param vectPoint Vecteur de point d'insertion de block
  * \param tol Tolerance
  * \return ErrorStatus Acad::eOk si l'opération s'effectue avec succès
  */
Acad::ErrorStatus projectPolyOnZBlock( AcDb3dPolyline* poly3d,
    std::vector<AcGePoint3d>& vectPoint,
    const double& tol );


/**
  * \brief Permet d'ajouter un sommet d'une polyligne sur le bloc
  * \param poly3d La polyligne en question
  * \param vectPoint ???
  * \param tol Tolerance
  * \return Nombre de sommet ajoutés
  */
int projectPolyOnBlock( AcDb3dPolyline*& poly3d,
    std::vector<AcGePoint3d>& vectPoint,
    const double& tol );

/**
  * \brief
  * \param
  * \return
  */
int projectPolyOnBlock( AcDb3dPolyline* poly3d,
    const AcGePoint3d& point3d,
    const double& tol );


/**
  * \brief Permet de projeter une polyligne 3d sur un block
  * \param poly3d La polyligne en question
  * \param block3D Le bloc en question
  * \param tol Tolerance
  * \return ErrorStatus Acad::eOk si l'opération s'effectue avec succès
  */
std::vector<AcGePoint3d> getVertexPointOfPolyInLayer(); //Doc ne correspond pas


/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus addTopoPointOnAllVertex( AcDbCircle* circle,
    std::vector<AcGePoint3d> vectPointTopo );


/**
  * \brief Réorganise les sommets de la polyligne de sorte à ce qu'ils soient ordonnés dans le sens trigo.
  * \param poly La polyligne 3d à réorganiser
  * \return Si la poly est ouverte, retourne une erreur
  */
Acad::ErrorStatus setPolyToTrigonomicSense( AcDb3dPolyline*& poly );

/**
  * \brief détermine si la polyligne est ordonnée dans le sens trigo.
  * \param poly La polyligne 3d à vérifier
  * \param res resultat true si oui sinon false
  * \return Si la poly est ouverte, retourne une erreur
  */
Acad::ErrorStatus isPolyTrigonomicSense( AcDb3dPolyline*& poly, bool& res );

/**
  * \brief Permet de savoir si un point est dans le bounding box d'une polyligne
  * \param poly3D La polyligne en question
  * \param ptSearch Le point a cherché
  * \return Si la poly est ouverte, retourne une erreur
  */
bool isPointInsidePolyBox( AcDb3dPolyline*& poly3D,
    const AcGePoint3d& ptSearch );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus isPointOnPoly( AcDb3dPolyline*& poly,
    const AcGePoint3d& pt,
    bool& isOnPoly,
    const double& precision = 0.005 );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus isPointOnPoly2D( AcDb3dPolyline*& poly,
    const AcGePoint3d& pt,
    bool& isOnPoly,
    const double& precision = 0.005 );


/**
  * \brief
  * \param
  * \return
  */
std::vector<int> getSuperposedIndex( AcDb3dPolyline* poly3d,
    const double& precision = 0.001 );


/**
  * \brief Récupère  la liste des Positions sommets ainsi que les positions en X de ces sommets
  * \param poly Polyligne qui nous intéresse
  * \param arPtVtx point de sommet de la Polyligne qui nous intéresse
  * \param xPosOther point X des sommet de la Polyligne qui nous intéresse
  * \return ErrorSattus
  */
Acad::ErrorStatus getVertexesPoly(
    AcDb3dPolyline*&  poly, AcGePoint3dArray& arPtVtx, vector< double >& xPosOther );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus
openPoly3d( const AcDbObjectId& idPoly,
    AcDb3dPolyline*& poly3d,
    const AcDb::OpenMode& mode = AcDb::kForRead );

/**
  * \brief
  * \param
  * \return
  */
bool isColiPoly( AcDb3dPolyline* poly3D,
    AcGePoint3d& ptPoly,
    const double& tol );


/**
  * \brief Permet de recuperer la longeur curviligne des vectexs entres tous les sommets des polylignes
  * \param poly3d La polyligne 3d où on va recuperer
  * \param vectDist Vecteur sortie contenant les longueur des vertexs
  * \return Retourne ErrorStatus
  */
Acad::ErrorStatus getDistVec( AcDb3dPolyline*& poly3d,
    vector<double>& distVec );


/**
  * \brief Permet d'ajouter des sommets ( contenues dans une liste de point ) à la polyligne
  * \param poly La polyligne en question
  * \param pos La liste de point 3d à ajouter à la polyligne
  * \param xPos Vecteur contenant les x des points 3d à ajouter à la polyligne
  * \return Retourne ErrorStatus
  */
Acad::ErrorStatus appendVertexToList( AcDb3dPolyline*& poly,
    AcGePoint3dArray& pos,
    vector< double >& xPos );


/**
  * \brief Permet de recuperer l'index du vertex d'avant et apres du point en entrée ( si ce point est un vertex de la polyligne )
  * \param poly3D La polyligne 3D en question
  * \param onePt3D Le point en entrée
  * \param prevIdxRes L'index resultat du vertex d'avant
  * \param nextIdxRes L'index resultat du vertex d'apres
  * \param xPos Vecteur contenant les x des points 3d à ajouter à la polyligne
  * \return Retourne ErrorStatus
  */
Acad::ErrorStatus getSurroundingVertexes( AcDb3dPolyline*& poly3D,
    const AcGePoint3d& onePt3D,
    long& prevIdxRes,
    long& nextIdxRes,
    const double& tol = 0.0001 );

//Pourquoi commentée ?
/**
  * \brief Permet de recuperer l'objectId du point en entrée ( si ce point est un vertex de la polyligne )
  * \param poly3D La polyligne 3D en question
  * \param onePt3D Le point en entrée
  * \param indexVertId L'objectId resultat du vertex de la polyligne
  * \param tol Tolerance de recherche
  * \return Retourne ErrorStatus
  */
//Acad::ErrorStatus getSurroundingVertexes( AcDb3dPolyline*& poly3D,
//    const AcGePoint3d& onePt3D,
//    AcDbObjectId& indexVertId,
//    const double& tol );


/**
  * \brief Permet de recuperer le vertex d'une polyligne la plus proche d'un point
  * \param point3d Le point 3d en question
  * \param vectPoint Vecteur de point 3d où on va chercher le vertex
  * \param tolerance La tolerance de recherche
  * \return Le vertex ( en point 3d ) d'une polyligne la plus proche du point
  */
AcGePoint3d getClosestVert( const AcGePoint3d& point3d,
    AcGePoint3dArray& vectPoint,
    vector<double>& vectX,
    const double& toleranceXY = 0.02,
    const double& toleranceZ = 0.03 );


/**
  * \brief Permet de recuperer tous les vertexs ( en point 3d ) des polylignes 3D d'une selection, et tri la liste si c'est voulue
  * \param ssPoly Selection sur les polyligne
  * \param arrayOfVertex Liste qui va contenir les vertexs ( en point 3d ) des polylignes 3D de la selection en entrée
  * \param vectX Vecteur de double qui va contenir les x des vertexs des polilignes 3D de la selection en entrée
  * \param hasToUseExtremities Booleen si on veut recuperer les extremités des polylignes de la selection
  * \param sort Booleen si on veut trier la liste de vertex des polylignes avec le vecteur de double d'abscisses
  * \return ErrorStatus
  */
Acad::ErrorStatus getVertexesOfAllPoly( const ads_name& ssPoly,
    AcGePoint3dArray& arrayOfVertex,
    vector<double>& vectX,
    const bool& hasToUseExtremities = true,
    const bool& sort = true );


/**
  * \brief Permet de recuperer tous les vertexs ( en point 3d ) des polylignes 3D d'un calque, et tri la liste si c'est voulue
  * \param ssPoly Selection sur les polyligne
  * \param arrayOfVertex Liste qui va contenir les vertexs ( en point 3d ) des polylignes 3D du calque en entrée
  * \param vectX Vecteur de double qui va contenir les x des vertexs des polilignes 3D du calque en entrée
  * \param hasToUseExtremities Booleen si on veut recuperer les extremités des polylignes du calque
  * \param sort Booleen si on veut trier la liste de vertex des polylignes avec le vecteur de double d'abscisses
  * \return ErrorStatus
  */
Acad::ErrorStatus getVertexesOfAllPoly( const AcString& layerPoly,
    AcGePoint3dArray& arrayOfVertex,
    vector<double>& vectX,
    const bool& hasToUseExtremities = true,
    const bool& sort = true );



/**
  * \brief Recuperer les extremités des polylignes dans une selection
  * \param ssPoly Selection sur les polylignes
  * \param arrayVertex Vecteur sortie contenant les extrémités
  * \param onlyBegin Booleen si on veut juste recuperer les vertexs de début des polylignes de la selection
  * \param onlyEnd Booleen si on veut juste recuperer les vertexs de fin des polylignes de la selection
  * \return ErrorStatus
  */
Acad::ErrorStatus getExtremitiesOfPoly( const ads_name& ssPoly,
    AcGePoint3dArray& arrayVertex,
    const bool& onlyBegin = false,
    const bool& onlyEnd  = false );

/**
  * \brief Permet de recuperer le vertex d'une polyligne la plus proche d'un point
  * \param point3d Le point 3d en question
  * \param poly3D Polyligne 3D
  * \param ptRes
  *\ parm iterVtxPoly3d itérateur sur les vertex pointant sur le sommet le plus proche
  * \return Le vertex ( en point 3d ) d'une polyligne la plus proche du point
  */
Acad::ErrorStatus  getClosestVtx( const AcGePoint3d& point3d, AcDb3dPolyline*& poly3D, AcGePoint3d& ptRes, AcDbObjectIterator*& iterVtxPoly3d );



/**
  * \brief Permet de fermer les polylignes dans un vecteur de pointeur de polyligne où il y a des doublons d'adresse, cette fonction est utilisée dans les preparations des polylignes
  * \param vectPoly Vecteur de pointeur sur les polylignes 3d à fermer, il faut d'abord supprimer les doublons dans ce vecteur
  * \return Void
  */
void closePoly3dPtr( vector< AcDb3dPolyline* >& vectPoly );


/**
  * \brief Retourne le vecteur de distance curviligne des polylignes depuis leeurs Id
  * \param idPoly Id de la Polyligne qui nous intéresse
  * \return ErrorStatus
  */
Acad::ErrorStatus getCurvDistPoly(
    const AcDbObjectIdArray&  arPolyIds,
    vector< vector< double >>& allVectDir );


/**
  * \brief Permet d'ajouter un point sur le point de la polyligne cliqué
  * \param poly3D La polyligne 3d
  * \param ptPoly La position du vertex à ajouter sur la polyligne
  * \return ErrorSattus
  */
Acad::ErrorStatus addVertexOnClickedPoint( AcDb3dPolyline* poly3D,
    const AcGePoint3d& ptPoly );


/**
  * \brief Preparer les polylignes 3d i.e. trier les vecteurs et le map
  * \param vectX Vecteur contenant les abscisses des vertex des polylignes
  * \param vectPoint3d Vecteur contenant les vertex des polylignes
  * \param vectObjId ???
  * \param polyMap Map contenant les objectIds des vertex des polylignes et les pointeur sur ces polylignes
  * \return Acad::ErrorStatus eOk si tout est Ok
  */
Acad::ErrorStatus preparePoly3d( std::vector<double>& vectX,
    AcGePoint3dArray& vectPoint3d,
    std::vector<AcDbObjectId>& vectObjId,
    std::vector<AcDb3dPolyline*>& vectPolyPointer );


/**
  * \brief Permet de savoir si un point en entrée est un vertex de polyligne ou non
  * \param poly3D La polyligne 3D en question
  * \param pt Le point qu'on va tester si c'est un sommet de polyligne ou non
  * \param tol La tolerance
  * \return True si c'est un vertex de la polyligne, false sinon
  */
bool isVertexOfPoly( AcDb3dPolyline* poly3D,
    const AcGePoint3d& pt,
    const double& tol = 0.001 );


/**
  * \brief Permet de recuperer les polylignes les plus proche d'un point
  * \param pt Le point en entrée
  * \param poly3DVec Vecteur contenant les polylignes 3D trouvés
  * \param vecPoint Vecteur contenant les points projétés sur les polylignes trouvés
  * \param tol La tolerance, par défaut 0.01 m
  * \return eOk si on a retrouvé la polyligne, false sinon
  */
Acad::ErrorStatus getClosestPolyToPoint( const AcGePoint3d& pt,
    vector<AcDb3dPolyline*>& poly3DVec,
    AcGePoint3dArray& vecPoint,
    const double& tol = 0.01 );


/**
  * \brief Permet de recuperer les intersections entre deux polyligne
  * \param poly3D Premiere polyligne
  * \param otherPoly3D Deuxieme polyligne
  * \param intersectPoint3DArray Tableau qui va contenir les points d'intersection des deux polylignes
  * \param count Compteur de point d'intersection
  * \param tol La tolerance
  * \return ErrorStatus
  */
Acad::ErrorStatus intersectTwoPoly3D( AcDb3dPolyline*& poly3D,
    AcDb3dPolyline*& otherPoly3D,
    AcGePoint3dArray& intersectPoint3DArray,
    long& count,
    const double& tol );


/**
  * \brief Permet de copier et d'ajouter des vertexs dans la copie de la polyligne, mais l'index de chaque vertex depend de sa distance au premier point de la polyligne
  * \param arNewPoint Tableau contenant les nouveaux point à ajouter dans la polyligne
  * \param poly3D La polyligne 3d
  * \param delEntity Si true redessinne la polyligne et supprime l'ancien
  * \return ErrorStatus
  */
Acad::ErrorStatus addArVertexOnPoly( const AcGePoint3dArray& arNewPoint,
    AcDb3dPolyline*& poly3D,
    const bool& delEntity = false );


/**
  * \brief Fonction qui permet de copier une partie d'une polyligne sur la polyligne active
  * \param usrPoly La polyligne active
  * \param retPt2 Le dernier sommet de la polyligne active
  * \return ErrorStatus
  */
Acad::ErrorStatus optionPolyline3D( AcDb3dPolyline*& usrPoly,
    AcGePoint3d& retPt2 );


/**
  * \brief Fonction qui permet recuperer les sommets d'une polyligne entre deux points cliqués
  * \param poly La polyligne
  * \param pt1 Le premier point sur la polyligne cliqué par l'utilisateur
  * \param pt2 Le second points sur la polyligne cliqué par l'utilisateur
  * \param newVec Tableau contenant les sommets de la polyligne entre les deux points cliqué par l'utilisateur
  * \return ErrorStatus
  */
Acad::ErrorStatus getSubVertexOfPoly3D( AcDb3dPolyline* poly,
    AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    AcGePoint3dArray& newVec );


/**
  * \brief Fonction qui permet de recuperer le segment de la polyligne qui contient le point en entrée
  * \param poly3D La polyligne
  * \param pt Le point qu'on va chercher le segment
  * \param vectRes Vecteur contenant les deux indexs de la polyligne contenant le point
  * \return ErrorStatus
  */
Acad::ErrorStatus getSegmentOfPointInPoly( AcDb3dPolyline* poly3D,
    const AcGePoint3d& pt,
    vector<int>& vectRes );


/**
  * \brief Permet de dessiner une polyligne avec l'option polyligne
  * \param pt Premier point de la polyligne, par défaut ( 0; 0; 0 )
  * \return ErrorStatus
  */
Acad::ErrorStatus drawNewPoly3D( const AcGePoint3d& fPoint = AcGePoint3d::kOrigin );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus continuePoly3D( AcDb3dPolyline* poly3D );


/**
  * \brief Discretise un segment avec une distance de discretisation.
  * \param pt1 Premier sommet
  * \param pt2 deuscieme sommet
  * \param dist Distance de discrétisation
  * \return tableau des points des segmets apres discretisation
*/
AcGePoint3dArray discretizeSeg( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const double& dist = 1 );


/**
  * \brief Discretise un polyligne3d avec une distance de discretisation.
  * \param poly la polyligne 3d a discretise
  * \param dist c'est la distance de discretisation.
  * \return retourn une erreur status.
*/
Acad::ErrorStatus discretizePoly( AcGePoint3dArray& ptAr,
    AcDb3dPolyline* poly,
    const double& dist = 1 );


/**
  * \fn Acad::ErrorStatus modifyPolyline3d( AcDb3dPolyline* poly, const AcGePoint3dArray& pointArray )
  * \brief Modifie les polylines par les valeurs du Tableau de point
  * \param poly la polyligne 3d a discretise
  * \param La listes des points modifie
  * \return retourn une erreur status.
 */
Acad::ErrorStatus modifyPolyline3d( AcDb3dPolyline* poly,
    const AcGePoint3dArray& pointArray );


/**
  * \fn Acad::ErrorStatus projectPoly3DOnPlane( AcDb3dPolyline* poly3D, const AcGePlane& plane, const AcGeVector3d& projectDir, const bool& erasePoly3d )
  * \brief Projete une polyligne 3D sur une plane
  * \param poly3D La polyligne 3d
  * \param plane Le plan de projection
  * \param projectDir Direction de la projection
  * \return Retourne la polyligne 3d projetée
 */
AcDb3dPolyline* getProjectedPoly3DOnPlane( AcDb3dPolyline* poly3D,
    const AcGePlane& plane,
    const AcGeVector3d& projectDir );

/**
 * \brief Renvoie un pointeur sur la polyligne 3d de la selection
 * \param ssName Pointeur sur la selection
 * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
 * \return Pointeur sur une polyligne
 */
AcDb3dPolyline* getPoly3DFromEntSel(
    const ads_name&             ssName,
    const AcDb::OpenMode& opMode = AcDb::kForRead );


/**
 * \brief Renvoie la listes des point du polyligne
 * \param ssName Polyligne
 * \return listes des point du polyligne
 */
AcGePoint3dArray getPoint3dArray(
	AcDb3dPolyline*);