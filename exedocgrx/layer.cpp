#include "layer.h"
#include "dataBase.h"
#include "print.h"
#include "selectEntity.h"


Acad::ErrorStatus getLayer(
    AcDbLayerTableRecord*&       layerEntity,
    const AcString&             layerName,
    const AcDb::OpenMode&       opMode,
    AcDbDatabase* database )
{

    // on r�cup�re la table des calques
    AcDbLayerTable* pTable = NULL;
    Acad::ErrorStatus es;
    
    if( database == NULL )
        es = acdbHostApplicationServices()->workingDatabase()->getLayerTable( pTable, AcDb::kForRead );
    else
        es = database->getLayerTable( pTable, AcDb::kForRead );
        
    // on v�rifie qu'on ait bien r�cup�r� la table
    if( es != Acad::eOk )
    {
        acutPrintf( _T( "\n Ne peut pas ouvrir la table des calques" ) );
        return es;
    }
    
    // on r�cup�re le calque, on retourne un message d'erreur si on l'a pas trouv�
    es = pTable->getAt( layerName, layerEntity, opMode );
    
    // on ferme la table des calques et on retourne le resultat
    pTable->close();
    
    return es;
}



Acad::ErrorStatus createLayer( const AcString& layerName,
    const AcCmColor& color,
    AcDbDatabase* database )
{
    //On initialise le resultat
    Acad::ErrorStatus result = Acad::eInvalidLayer;
    
    //On d�clare un pointeur sur un calque
    AcDbLayerTableRecord* layerEntity = NULL;
    
    //On recup�re le calque
    result = getLayer( layerEntity, layerName, AcDb::kForWrite, database );
    
    //Dans le cas o� le calque existe
    if( result == Acad::eOk )
    {
        //On ferme le calque
        layerEntity->close();
        
        return result;
    }
    
    //Dans le cas o� le calque n'existe pas: on va le cr�er
    AcDbLayerTable* pTable = NULL;
    
    //On v�rifie qu'on ait bien r�cup�r� la table
    if( database == NULL )
        result  = acdbHostApplicationServices()->workingDatabase()->getLayerTable( pTable, AcDb::kForWrite );
    else
        result = database->getLayerTable( pTable, AcDb::kForWrite );
        
    if( result != Acad::eOk )
    {
        acutPrintf( _T( "\nNe peut pas ouvrir la table des calques" ) );
        return result;
    }
    
    //On cr�e le calque
    layerEntity = new AcDbLayerTableRecord();
    
    //Lui donner le bon nom
    result = layerEntity->setName( layerName );
    
    if( result != Acad::eOk )
    {
    
        // Fermer la table des calques, puis quitter
        pTable->close();
        return result;
    }
    
    // Lui donner la bonne couleur
    layerEntity->setColor( color );
    
    //L'ajouter au dessin
    result = pTable->add( layerEntity );
    
    if( result != Acad::eOk )
    {
        layerEntity->close();
        pTable->close();
        return result;
    }
    
    //On ferme le calque
    layerEntity->close();
    
    //On ferme la table des calques
    return pTable->close();
}


Acad::ErrorStatus getLayer(
    AcDbLayerTableRecord*& layer,
    const AcDbObjectId& layerId,
    const AcDb::OpenMode& opMode )
{
    Acad::ErrorStatus es = acdbOpenObject( layer, layerId, opMode );
    
    //Soritr
    return es;
}


AcDbLayerTableRecord* readLayer(
    const AcDbObjectId& layerId )
{
    //initialiser la calque
    AcDbLayerTableRecord* layer = NULL;
    
    //recuperer la calque et l'erreur
    getLayer( layer, layerId, AcDb::kForRead );
    
    //retourner la calque
    return layer;
}


AcDbLayerTableRecord* writeLayer(
    const AcDbObjectId& layerId )
{
    //initialiser la calque
    AcDbLayerTableRecord* layer = NULL;
    
    //recuperer la calque et l'erreur
    getLayer( layer, layerId, AcDb::kForWrite );
    
    //retourner la calque
    return layer;
}



AcDbLayerTableRecord* getCurrentLayer(
    const AcDb::OpenMode& opMode )
{
    //initialiser la calque
    AcDbLayerTableRecord* layer = NULL;
    
    //recuperer l'id du calque courant
    AcDbObjectId layerId = acdbHostApplicationServices()->workingDatabase()->clayer();
    
    //recuperer la calque
    getLayer( layer, layerId, opMode );
    
    //retouer la calque courant
    return layer;
}



AcDbLayerTableRecord* readCurrentLayer( )
{
    return getCurrentLayer( AcDb::kForRead );
}



AcDbLayerTableRecord* writeCurrentLayer()
{
    return getCurrentLayer( AcDb::kForWrite );
}



AcDbObjectId getCurrentLayerId( )
{
    return acdbHostApplicationServices()->workingDatabase()->clayer();
}


AcString getCurrentLayerName( )
{
    AcDbLayerTableRecord* currentLayer = getCurrentLayer();
    return currentLayer->getName();
}


Acad::ErrorStatus activateLayer( const AcString& layer )
{

    AcDbLayerTableRecord* layerEntity;
    
    // R�cup�rer un pointeur sur le calque calque
    Acad::ErrorStatus es = getLayer( layerEntity,
            layer );
            
    if( es != Acad::eOk )
        return es;
        
    // Activer le calque comme calque courant
    acdbHostApplicationServices()->workingDatabase()->setClayer( layerEntity->id() );
    
    return Acad::eOk;
}


Acad::ErrorStatus activateLayer( AcDbEntity* pEnt )
{
    return activateLayer( pEnt->layer() );
}


vector <AcString> getLayerList()
{
    //Initialiser la table des calques
    AcDbLayerTable* table = NULL;
    
    //Initialiser le vecteur layerList
    vector <AcString> layerList( 0 );
    
    //Ouvrir la table de calque
    Acad::ErrorStatus es =  acdbHostApplicationServices()->workingDatabase()->getLayerTable( table, AcDb::kForRead );
    
    //Tester si la table est ouverte ou pas
    if( Acad::eOk != es )
    {
        acutPrintf( _T( "La table des calques ne pas �tre ouverte" ) );
        return layerList;
    }
    
    // Iterateur sur les calques
    AcDbLayerTableIterator* pIter;
    
    //On passe le calque 0
    table->newIterator( pIter );
    pIter->step();
    
    //Parcourir la liste des calques dans la table
    for( pIter; !pIter->done(); pIter->step() )
    {
        //On r�cup�re l'Id du calque courant
        AcDbObjectId recordId;
        es = pIter->getRecordId( recordId );
        
        if( es != Acad::eOk )
        {
            acutPrintf( _T( "Ne peut pas r�cup�rer le calque" ) );
            continue;
        }
        
        //On cast un object AcDbEntity en objet calque gr�ce � l'Id qu'on a r�cup�r�
        AcDbObject* pObj = NULL;
        es = acdbOpenAcDbObject( pObj, recordId, AcDb::kForWrite );
        
        AcDbLayerTableRecord* pRecord = NULL;
        pRecord = AcDbLayerTableRecord::cast( pObj );
        
        //Si le pointeur sur le calque est nul on continue la boucle
        if( es != Acad::eOk || pRecord == NULL )
        {
            acutPrintf( _T( "Ne peut pas r�cup�rer le calque" ) );
            
            if( pObj != NULL )
                pObj->close();
                
            if( pRecord != NULL )
                pRecord->close();
                
            continue;
        }
        
        //On r�cup�re le nom du calque
        AcString layerName;
        pRecord->getName( layerName );
        
        layerList.push_back( layerName );
        pRecord->close();
    }
    
    //On supprime l'iterateur
    delete pIter;
    
    //On ferme l'objet table
    table->close();
    
    //On renvoie le r�sultat
    return layerList;
}


Acad::ErrorStatus freezeLayerInViewport(
    const AcString& layerName,
    const AcDbObjectId& vpId,
    const bool& hasToFreeze )
{
    //On recup�re l'objet calque
    AcDbLayerTableRecord* layer = NULL;
    Acad::ErrorStatus es =  getLayer( layer, layerName, AcDb::kForWrite );
    
    if( es != Acad::eOk )
    {
        acutPrintf( _T( "\nImpossible de r�cup�rer le calque." ) );
        return es;
    }
    
    //R�cuperer l'id du calque
    AcDbObjectId layerId = layer->objectId();
    LONG_PTR layerOldId = layerId.asOldId();
    
    //Ouvrir l'objet AcDbViewportTableRecord
    AcDbObject* obj = NULL;
    es = acdbOpenAcDbObject( obj, vpId, AcDb::kForWrite );
    
    if( es != Acad::eOk )
    {
        acutPrintf( _T( "\nImpossible de r�cup�rer la vue courante." ) );
        layer->close();
        return es;
    }
    
    //Caster l'objet recup�rer en viewportTableRecord
    AcDbViewportTableRecord* vpTable = AcDbViewportTableRecord::cast( obj );
    
    if( !vpTable )
    {
        acutPrintf( _T( "\nImpossible de r�cup�rer la vue courante." ) );
        obj->close();
        layer->close();
        return Acad::eNullObjectPointer;
    }
    
    //R�cup�rer l'acgsview
    AcGsView* gs = vpTable->gsView();
    
    if( !gs )
    {
        acutPrintf( _T( "\nImpossible de r�cup�rer la vue courante." ) );
        layer->close();
        obj->close();
        return Acad::eNullObjectPointer;
    }
    
    // On g�le/ ou d�g�le le calque dans la fen�tre s�lectionn�e
    if( hasToFreeze )
        gs->freezeLayer( layerOldId );
    else
        gs->clearFrozenLayers();
        
    // On ferme tous les objets
    obj->close();
    layer->close();
    
    //Mettre a jour les vues
    acedVportTableRecords2Vports();
    
    return Acad::eOk;
}


bool isLayerExisting( const AcString& layerName )
{
    //Recuperer la liste des calques dans le dessin
    std::vector<AcString> listLayer = getLayerList();
    
    //Tester la validit� du vecteur
    if( listLayer.size() == 0 )
    {
        //Afficher un message
        print( "Aucun calque recup�r�" );
        
        //Retourner false
        return false;
    }
    
    //Boucle sur le vecteur
    for( int i = 0 ; i < listLayer.size(); i++ )
    {
        if( listLayer[i] == layerName )
            return true;
    }
    
    //Retourner le resultat
    return false;
}


Acad::ErrorStatus getColorLayer( AcCmColor& color,
    const AcString& layerName )
{
    //Dans le cas o� le calque n'existe pas: on va le cr�er
    AcDbLayerTable* pTable = NULL;
    
    //On v�rifie qu'on ait bien r�cup�r� la table
    Acad::ErrorStatus es = acdbHostApplicationServices()->workingDatabase()->getLayerTable( pTable, AcDb::kForWrite );
    
    if( es != Acad::eOk )
    {
        acutPrintf( _T( "\nNe peut pas ouvrir la table des calques" ) );
        return es;
    }
    
    // On r�cup�re le calque
    AcDbLayerTableRecord* layer;
    es = pTable->getAt( layerName,
            layer,
            AcDb::kForRead );
            
    if( es != Acad::eOk )
        return es;
        
    // Fermer la table de calque
    pTable->close();
    
    // On r�cup�re sa couleur
    color = layer->color();
    
    return Acad::eOk;
}


Acad::ErrorStatus createLayerFromExternDwg( const AcString& layerName,
    const AcString& dwgFile )
{
    // R�cuperer la database
    AcDbDatabase* dbExtern = new AcDbDatabase( Adesk::kFalse );
    
    // Ouvrir la base de donn�es externe
    Acad::ErrorStatus es;
    es = dbExtern->readDwgFile( dwgFile );
    
    if( es != Acad::eOk )
    {
        //Sortir
        dbExtern->close();
        return es;
    }
    
    AcDbLayerTable* pTable;
    
    // On v�rifie qu'on ait bien r�cup�r� la table des calques
    es  = dbExtern->getLayerTable( pTable, AcDb::kForRead );
    
    if( es != Acad::eOk )
        return es;
        
    // On r�cup�re le calque
    AcDbLayerTableRecord* layer;
    es = pTable->getAt( layerName,
            layer,
            AcDb::kForRead );
            
    // On r�cup�re sa couleur
    AcCmColor color;
    
    if( es == Acad::eOk )
        color = layer->color();
        
    else
        color = AcCmColor();
        
    // On ferme la table des calque et le calque
    pTable->close();
    
    if( layer )
        layer->close();
        
    // On cr�e le calque
    return createLayer( layerName,
            color );
}


std::vector<AcString> layerFileToLayerVector( const AcString& filePath )
{
    //Vecteur resultat
    std::vector<AcString> vecRes;
    
    if( !isFileExisting( filePath ) )
        return vecRes;
        
    //Ouvrir le fichier txt
    ifstream fTxtFile( acStrToStr( filePath ) );
    
    //Variable contenant le nom du calque
    string layer = "";
    
    //Boucle sur le fichier texte
    while( !fTxtFile.eof() )
    {
        //Essayer de recuperer les valeur
        try
        {
            fTxtFile >> layer;
        }
        
        catch( ... )
        {
            //Continuer la lecture du fichier
            continue;
        }
        
        //Supprimer l'espace au dernier fichier
        layer.erase( layer.find_last_not_of( " " ) + 1 );
        
        //Ajouter le calque dans le vecteur
        vecRes.push_back( strToAcStr( layer ) );
    }
    
    //Fermer le fichier
    fTxtFile.close();
    
    //Retourner le resutlat
    return vecRes;
}


AcString getLayer( const AcString& filePath,
    const bool& hasToUseLayer )
{
    //Initialisation du resultat
    AcString res = _T( "" );
    
    //Recuperer tous les calques du dessin
    std::vector<AcString> allLayer = getLayerList();
    
    //Vecteur de AcString contenant les calques � exclure
    //vector<AcString> exclude = fileLayerAsVectorLayer( filePath );
    
    vector<AcString> exclude = layerFileToLayerVector( filePath );
    
    //Tester que le vecteur est valide
    if( exclude.size() == 0 )
    {
        //Afficher une message
        print( "Impossible de recuperer les calques � exclure" );
        print( "Tous les calques sont pris en compte" );
        return res;
    }
    
    //Taille du exclude
    int tExclude = exclude.size();
    
    //Calque du dessin
    AcString var = "";
    
    //Boucle sur tous les calques � exclure ou � inclure
    for( int i = 0; i < tExclude; i++ )
    {
        //Recuperer le i-eme calque
        var = exclude[i];
        
        //Si c'est un calque � exclure
        if( !hasToUseLayer )
        {
            //Supprimer le calque dans le vecteur si elle est dedans
            allLayer.erase( std::remove( allLayer.begin(), allLayer.end(), var ), allLayer.end() );
        }
        
        //Si c'est un calque � inclure
        else
        {
            //Ajouter le i-eme calque dans le AcString
            res.append( var );
            
            //Ajouter une virgule
            if( i != tExclude - 1 )
                res.append( _T( "," ) );
        }
    }
    
    //Boucle pour ajouter les virgules
    if( !hasToUseLayer )
    {
        int t = allLayer.size();
        
        //Boucle pour ajouter les virgules
        for( int i = 0; i < t; i++ )
        {
            //Ajouter le i-eme calque
            res.append( allLayer[i] );
            
            //Ajouter une virgule
            if( i != t - 1 )
                res.append( _T( "," ) );
        }
    }
    
    //Retourner le resultat
    return res;
}


AcString getLayer( const AcString& filePath,
    const bool& isPoly,
    const bool& isName,
    const bool& isSelect )
{
    //Recuperer les valeurs en entr�e
    bool getPoly = isPoly;
    bool getName = isName;
    bool select = isSelect;
    
    //Corriger l'entr�e utilisateur car les polylignes 3d n'ont pas de nom
    if( getPoly )
    {
        getName = false;
        select = false;
    }
    
    //Resultat
    AcString result = _T( "" );
    
    //Creer un ifstream sur le fichier en entr�e
    ifstream txtFile( acStrToStr( filePath ) );
    
    //Vecteur qui va contenir les noms des calques
    vector<string> vecRes;
    
    //Ligne du fichier
    string line;
    
    //Boucle sur le fichier
    while( getline( txtFile, line ) )
    {
        //Splitter la ligne
        vector<string> splitted = splitString( line, "\t" );
        
        //Si on veut recuperer la calque des poly
        if( getPoly )
        {
            if( splitted[0] == "POLY3D" )
                vecRes.push_back( splitted[1] );
        }
        
        //Sinon
        else
        {
            //Verifier la taille du vecteur
            if( splitted.size() != 3 )
                continue;
                
            //Si on veut les blocs de selection
            if( select )
            {
                //Si on a besoin des nom de bloc de selection
                if( getName )
                {
                
                    if( splitted[0] == "SELECT" )
                        vecRes.push_back( splitted[1] );
                }
                
                //Si on a besoin des noms de calque de bloc de selection
                else
                {
                    if( splitted[0] == "SELECT" )
                        vecRes.push_back( splitted[2] );
                }
            }
            
            //Sinon on recupere le nom de bloc � inserer
            else
            {
                //Si on a besoin des noms de bloc
                if( getName )
                {
                    if( splitted[0] == "INSERT" )
                        vecRes.push_back( splitted[1] );
                }
                
                //Si on a besoin des noms de calque de bloc
                else
                {
                    if( splitted[0] == "INSERT" )
                        vecRes.push_back( splitted[2] );
                }
            }
        }
    }
    
    //Fermer le ifstream
    txtFile.close();
    
    //Recuperer la taille du vecteur
    long resSize = vecRes.size();
    
    //Boucle pour ajouter le virgule
    for( long i = 0; i < resSize; i++ )
    {
        result.append( strToAcStr( vecRes[i] ) );
        
        if( i != resSize - 1 )
            result.append( _T( "," ) );
    }
    
    //Sortir
    return result;
}


AcString getLayer( const vector<AcString>& layers,
    const bool& hasToUseLayer )
{
    //Initialisation du resultat
    AcString res = _T( "" );
    
    //Recuperer tous les calques du dessin
    std::vector<AcString> allLayer = getLayerList();
    
    //Tester que le vecteur est valide
    if( layers.size() == 0 )
    {
        //Afficher une message
        print( "Impossible de recuperer les calques � exclure ou � inclure" );
        print( "Tous les calques sont pris en compte" );
        return res;
    }
    
    //Taille du exclude
    int tExclude = layers.size();
    
    //Calque du dessin
    AcString var = "";
    
    //Boucle sur tous les calques � exclure ou � inclure
    for( int i = 0; i < tExclude; i++ )
    {
        //Recuperer le i-eme calque
        var = layers[i];
        
        //Si c'est un calque � exclure
        if( !hasToUseLayer )
        {
            //Supprimer le calque dans le vecteur si elle est dedans
            allLayer.erase( std::remove( allLayer.begin(), allLayer.end(), var ), allLayer.end() );
        }
        
        //Si c'est un calque � inclure
        else
        {
            //Ajouter le i-eme calque dans le AcString
            res.append( var );
            
            //Ajouter une virgule
            if( i != tExclude - 1 )
                res.append( _T( "," ) );
        }
    }
    
    //Boucle pour ajouter les virgules
    if( !hasToUseLayer )
    {
        int t = allLayer.size();
        
        //Boucle pour ajouter les virgules
        for( int i = 0; i < t; i++ )
        {
            //Ajouter le i-eme calque
            res.append( allLayer[i] );
            
            //Ajouter une virgule
            if( i != t - 1 )
                res.append( _T( "," ) );
        }
    }
    
    //Retourner le resultat
    return res;
}


Acad::ErrorStatus purgeLayer( const AcString& layerName,
    const bool& eraseLayer )
{
    //REsultat
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Tester la validit� du calque
    if( layerName == _T( "" ) )
        return er;
        
    //Declarer un calque
    AcDbLayerTableRecord* layer = NULL;
    
    //Recuperer le calque
    if( Acad::eOk != getLayer( layer, layerName, AcDb::kForWrite ) )
    {
        //Afficher un message
        acutPrintf( _T( "\nImpossible de recuperer le calque" ) );
        
        return er;
    }
    
    //Declarer une selection
    ads_name ssAll;
    
    //REcuperer tous les elements du calque
    long length = getSelectionSet( ssAll, "X", "", layerName );
    
    //Boucle pour supprimer les entit�s dans le calque
    for( int j = 0; j < length ; j++ )
    {
        //Recuperer l'entit�
        AcDbEntity* ent = getEntityFromSs( ssAll, j, AcDb::kForWrite );
        
        //Supprimer l'entit�
        ent->erase();
        ent->close();
    }
    
    //Si on supprime le calque
    if( eraseLayer )
        layer->erase();
        
    //Fermer le calque
    layer->close();
    
    //Liberer la selection
    acedSSFree( ssAll );
    
    //Sortir de la fonction
    return Acad::eOk;
}