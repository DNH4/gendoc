#pragma once
#include <dbents.h>
#include <dbmain.h>
#include "acString.h"
#include "dataBase.h"
#include <dbcolor.h>
#include <dbdict.h>

/**
  *  \brief R�cupere l'�chelle courante
  *  \return l'�chelle courante
  */
double getCurrentScale();

/**
  *  \brief change la couleur courante
  *  \return ErrorStatus
  */
Acad::ErrorStatus setCurrentScale( double scale );

/**
  *  \brief R�cupere la couleur courante
  *  \return la couleur courante
  */
AcCmColor getCurrentColor();

/**
  *  \brief change la couleur courante
  *  \return ErrorStatus
  */
Acad::ErrorStatus setCurrentColor( const AcCmColor& curColor );

/**
  *  \brief Recupere le type de ligne courant
  *  \return id du type de ligne courant
  */
AcDbObjectId getCurrentLineTypeId();

/**
  *  \brief change le type de ligne courante
  *  \return ErrorStatus
  */
Acad::ErrorStatus setCurrentLineTypeId( const AcDbObjectId& lineTypeId );

/**
  *  \brief R�cupere l'�paisseur de ligne courant
  *  \return l'�paisseur de ligne courant
  */
AcDb::LineWeight getCurrentLineWeight();

/**
  *  \brief change le type de ligne courante
  *  \return ErrorStatus
  */
Acad::ErrorStatus setCurrentLineWeight( AcDb::LineWeight lineWeight );

/**
  *  \brief change le calque courant
  *  \return ErrorStatus
  */
Acad::ErrorStatus setCurrentLayer( const AcDbObjectId&  layerId );

/**
  *  \brief change les propri�t�s courantes avec celui de l'entit�
  *  \return ErrorStatus
  */
Acad::ErrorStatus setCurrentProperties( const AcDbEntity* entity );