/**
 * @file hatchEntity.h
 * @author Nils van Haeringen
 * @brief Fonctions pour manipuler les hachures
 */

#pragma once
#include <aced.h>
#include <adscodes.h>
#include <dbmain.h>
#include <dbents.h>
#include <vector>
#include "selectEntity.h"
#include "pointEntity.h"


/**
    \brief Ins�re une hachure dans une polyligne 3d avec un motif donn�
    \param poly Vecteur de pointeurs vers les polylignes 3d repr�sentant les bordures de la hachure
    \param hatch Pointeur vers la hachure
    \param patternName Nom du motif de la hachure � appliquer
    \return ErrorStatus
*/
Acad::ErrorStatus insert3DHatch( std::vector<AcDb3dPolyline*> poly,
    AcDbHatch* hatch,
    const AcString& patternName );

/**
    \brief Ins�re une hachure dans une polyligne 2d avec un motif donn�
    \param poly Vecteur de pointeurs vers les polylignes 2d repr�sentant les bordures de la hachure
    \param hatch Pointeur vers la hachure
    \param patternName Nom du motif de la hachure � appliquer
    \return ErrorStatus
*/
Acad::ErrorStatus insert2DHatch( std::vector<AcDbPolyline*> poly,
    AcDbHatch* hatch,
    const AcString& patternName );

/**
    \brief Ins�re une hachure, en sp�cifiant l'ID de la polyligne 2D qui repr�sente sa bordure, avec un motif donn�
    \param objectId ID de la polyligne 2D fronti�re
    \param hatch Pointeur vers la hachure
    \param patternName Nom du motif de la hachure � appliquer
    \return ErrorStatus
*/
Acad::ErrorStatus insert2DHatch( AcDbObjectId objectId,
    AcDbHatch* hatch,
    const AcString& patternName );

/**
    \brief Ins�re une hachure, en sp�cifiant les ID des polylignes 2D qui repr�sentent sa bordure, avec un motif donn�
    \param objectIdArray Array contenant les ID des polylignes 2D fronti�res
    \param hatch Pointeur vers la hachure
    \param patternName Nom du motif de la hachure � appliquer
    \return ErrorStatus
*/
Acad::ErrorStatus insert2DHatch( AcDbObjectIdArray ObjectIdArray,
    AcDbHatch* hatch,
    const AcString& patternName );

/**
    \brief Ins�re une hachure, en sp�cifiant l'ID de la polyligne 3D qui repr�sente sa bordure, avec un motif donn�
    \param objectId ID de la polyligne 3D fronti�re
    \param hatch Pointeur vers la hachure
    \param patternName Nom du motif de la hachure � appliquer
    \return ErrorStatus
*/
Acad::ErrorStatus insert3DHatch( AcDbObjectId objectId,
    AcDbHatch* hatch,
    const AcString& patternName );

/**
    \brief Ins�re une hachure, en sp�cifiant les ID des polylignes 3D qui repr�sentent sa bordure, avec un motif donn�
    \param objectIdArray Array contenant les ID des polylignes 3D fronti�res
    \param hatch Pointeur vers la hachure
    \param patternName Nom du motif de la hachure � appliquer
    \return ErrorStatus
*/
Acad::ErrorStatus insert3DHatch( AcDbObjectIdArray objectIdArray,
    AcDbHatch* hatch,
    const AcString& patternName );

/**
    \brief Ins�re une hachure, en sp�cifiant les ID des polylignes 3D qui repr�sentent sa bordure, avec un motif, un calque, une couleur et un type de ligne donn�s
    \param objectIdArray Array contenant les ID des polylignes 3D fronti�res
    \param hatch Pointeur vers la hachure
    \param patternName Nom du motif de la hachure � appliquer
    \param layerName Nom du calque
    \param color Couleur � appliquer � la hachure
    \param lineTypeName Nom du type de ligne � appliquer � la hachure
    \return ErrorStatus
*/
Acad::ErrorStatus insert3DHatch( AcDbObjectIdArray objectIdArray,
    AcDbHatch* hatch,
    const AcString& patternName,
    const AcString& layerName,
    const AcCmColor& color,
    const AcString& lineTypeName );

/**
    \brief Ins�re une hachure, en sp�cifiant les ID des polylignes 2D qui repr�sentent sa bordure, avec un motif, un calque, une couleur et un type de ligne donn�s
    \param objectIdArray Array contenant les ID des polylignes 2D fronti�res
    \param hatch Pointeur vers la hachure
    \param patternName Nom du motif de la hachure � appliquer
    \param layerName Nom du calque
    \param color Couleur � appliquer � la hachure
    \param lineTypeName Nom du type de ligne � appliquer � la hachure
    \return ErrorStatus
*/
Acad::ErrorStatus insert2DHatch( AcDbObjectIdArray objectIdArray,
    AcDbHatch* hatch,
    const AcString& patternName,
    const AcString& layerName,
    const AcCmColor& color,
    const AcString& lineTypeName );

///---------------------------------------------------------------------------------------------<SELECTION>

/**
  * \brief Fait une selection quelconque sur les HACHURE 
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsHatch(
	ads_name         ssName,
	const AcString&   layerName = "");

/**
  * \brief Selectionne une seul HACHURE dans le dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \param isClosed True si la HACHURE est fermee, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSsOneHatch(
	ads_name          ssName,
	const AcString&   layerName = "");

/**
  * \brief Selectionne toutes les HACHURE  du dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsAllHatch(
	ads_name          ssName,
	const AcString&   layerName = "");

/**
 * \brief Renvoie un pointeur sur la HACHURE  numero N dans la selection
 * \param ssName Pointeur sur la selection
 * \param iObject Index dans la selection de la HACHURE a recuperer, par defaut 0
 * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
 * \return Pointeur sur une HACHURE
 */
AcDbHatch* getHatchFromSs(
	const ads_name&              ssName,
	const long&           iObject = 0,
	const AcDb::OpenMode& opMode = AcDb::kForRead);

/**
  * \brief Renvoie un pointeur en LECTURE sur la HACHURE numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de la HACHURE a recuperer, par defaut 0
  * \return Pointeur sur une HACHURE
  */
AcDbHatch* readHatchFromSs(
	const ads_name&          ssName,
	const long&       iObject = 0);

/**
  * \brief Renvoie un pointeur en ECRITURE sur le HACHURE numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de la HACHURE a recuperer, par defaut 0
  * \return Pointeur sur une HACHURE
  */
AcDbHatch* writeHatchFromSs(
	const ads_name&              ssName,
	const long&           iObject = 0);