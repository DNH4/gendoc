/**
 * @file lineType.h
 * @author Romain LEMETTAIS
 * @brief Fichier contenant les fonctions utiles pour les propriete de ligne
 */

#pragma once

#include <dbents.h>
#include <dbmain.h>
#include "acString.h"
#include "dataBase.h"
#include <dbcolor.h>
#include <dbdict.h>

/**
  * \brief Recuperer tout les lignes types
  * \return Listes des lignes types dessiner sur le modelSpace
  */
vector <AcString> getLineType();

/**
  * \brief V�rifiie si un ligne type fait partie du modelSpace
  * \return bool oui si exist autrement non
  */
Acad::ErrorStatus checkLineType(const AcString& strLintType, AcDbObjectId & lineTypeId);

/**
 *  \brief Recupere le type de ligne courant
 *  \return id du type de ligne courant
 */
AcDbObjectId getCurrentLineTypeId ();

/**
 *  \brief change le type de ligne courante
 *  \return ErrorStatus
 */
Acad::ErrorStatus setCurrentLineTypeId (const AcDbObjectId& lineTypeId);

/**
 *  \brief R�cupere l'�paisseur de ligne courant
 *  \return l'�paisseur de ligne courant
 */
AcDb::LineWeight getCurrentLineWeight ();

/**
 *  \brief change le type de ligne courante
 *  \return ErrorStatus
 */
Acad::ErrorStatus setCurrentLineTypeId ( AcDb::LineWeight lineWeight);