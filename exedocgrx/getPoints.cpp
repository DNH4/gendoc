#include <getPoints.h>


int wcsToUcs(
    const AcGePoint3d* pt )
{

    //Recuperer le SCU local
    struct resbuf l_rbUcs;
    l_rbUcs.restype = RTSHORT;
    l_rbUcs.resval.rint = 1;
    
    //Recuperer le SCU General
    struct resbuf l_rbWcs;
    l_rbWcs.restype = RTSHORT;
    l_rbWcs.resval.rint = 0;
    
    //Transformer le point
    return acedTrans(
            asDblArray( *pt ),
            &l_rbWcs,
            &l_rbUcs,
            Adesk::kFalse,
            asDblArray( *pt )
        );
}



int getUcsPoint(
    const AcString       message,
    const AcGePoint3d*        pt )
{
    //Recuperer le point
    return acedGetPoint( NULL, message, asDblArray( *pt ) );
}



int getUcsPoint(
    const AcString        message,
    const AcGePoint3d*    ptGet,
    const AcGePoint3d     ptFrom )
{
    //Recuperer le point
    return acedGetPoint( asDblArray( ptFrom ), message, asDblArray( *ptGet ) );
}



int getWcsPoint(
    const AcGePoint3d* pt )
{
    //Recuperer le SCU local
    struct resbuf l_rbUcs;
    l_rbUcs.restype = RTSHORT;
    l_rbUcs.resval.rint = 1;
    
    //Recuperer le SCU General
    struct resbuf l_rbWcs;
    l_rbWcs.restype = RTSHORT;
    l_rbWcs.resval.rint = 0;
    
    //Transformer le point
    return acedTrans(
            asDblArray( *pt ),
            &l_rbUcs,
            &l_rbWcs,
            Adesk::kFalse,
            asDblArray( *pt )
        );
}


int getUcsPoint(
    const AcGePoint3d* pt )
{
    //Recuperer le SCU local
    struct resbuf l_rbUcs;
    l_rbUcs.restype = RTSHORT;
    l_rbUcs.resval.rint = 1;
    
    //Recuperer le SCU General
    struct resbuf l_rbWcs;
    l_rbWcs.restype = RTSHORT;
    l_rbWcs.resval.rint = 0;
    
    //Transformer le point
    return acedTrans(
            asDblArray( *pt ),
            &l_rbWcs,
            &l_rbUcs,
            Adesk::kFalse,
            asDblArray( *pt )
        );
}



int getWcsPoint(
    const AcString        message,
    const AcGePoint3d*        ptGet )
{
    //Recuperer le point
    int l_iResult = getUcsPoint( message, ptGet );
    
    //Verifier le resultat
    if( RTNORM == l_iResult )
    
        //Transfortmer le point dans le SCU general
        getWcsPoint( ptGet );
        
    //Sortir
    return l_iResult;
}



int getWcsPoint(
    const AcString        message,
    const AcGePoint3d*    ptGet,
    const AcGePoint3d     ptFrom )
{
    //Tenasferer le point en local
    AcGePoint3d l_ptFrom = ptFrom;
    wcsToUcs( &l_ptFrom );
    
    //Recuperer le point
    int l_iResult = getUcsPoint( message, ptGet, l_ptFrom );
    
    //Verifier le resultat
    if( RTNORM == l_iResult )
    
        //Transfortmer le point dans le SCU general
        getWcsPoint( ptGet );
        
    //Sortir
    return l_iResult;
}



int getUcsBox(
    const AcString     message,
    const AcGePoint3d* ptGet,
    const AcGePoint3d  ptFrom )
{
    //Recuperer le point
    return acedGetCorner( asDblArray( ptFrom ), message, asDblArray( *ptGet ) );
}



int getUcsBox(
    const AcString     message1,
    const AcString     message2,
    const AcGePoint3d* p_ptBox1,
    const AcGePoint3d* p_ptBox2 )
{
    //Recuperer le premier point point
    int l_iResult = getUcsPoint( message1, p_ptBox1 );
    
    //Verifier le resultat
    if( RTNORM != l_iResult )
        return l_iResult;
        
    //Recuperer le second point de la boite
    return getUcsBox( message2, p_ptBox2, *p_ptBox1 );
}



int getWcsBox(
    const AcString     message,
    const AcGePoint3d* ptGet,
    const AcGePoint3d  ptFrom )
{
    //Tenasferer le point en local
    AcGePoint3d l_ptFrom = ptFrom;
    wcsToUcs( &l_ptFrom );
    
    //Recuperer le point
    int l_iResult = getUcsBox( message, ptGet, l_ptFrom );
    
    //Verifier le resultat
    if( RTNORM == l_iResult )
    
        //Transfortmer le point dans le SCU general
        getWcsPoint( ptGet );
        
    //Sortir
    return l_iResult;
}



int getWcsBox(
    const AcString     message1,
    const AcString     message2,
    const AcGePoint3d* p_ptBox1,
    const AcGePoint3d* p_ptBox2 )
{
    //Recuperer le premier point point
    int l_iResult = getUcsBox( message1, message2, p_ptBox1, p_ptBox2 );
    
    //Verifier le resultat
    if( RTNORM != l_iResult )
        return l_iResult;
        
    //Transformer les coordonnes des points
    getWcsPoint( p_ptBox1 );
    getWcsPoint( p_ptBox2 );
    
    //Sortir
    return l_iResult;
}



int getJigUcsPoint(
    const AcString      message,
    AcGePoint3d*  ptGet )
{
    //Transformer les coordonnees
    int l_iReturn = getWcsPoint( ptGet );
    
    //Verifier la transfo
    if( l_iReturn != RTNORM )
        return l_iReturn;
        
    //Lancer le jig
    l_iReturn = getJigWcsPoint( message, ptGet );
    
    //Verifier le resultat
    if( l_iReturn != RTNORM )
        return l_iReturn;
        
    //Transformer les coordonnees
    return wcsToUcs( ptGet );
}



int getJigWcsPoint(
    const AcString         message,
    AcGePoint3d*     ptGet )
{
    //Initialiser le jig
    jigPoint l_jigPoint = jigPoint();
    
    //demander a l'utilisateur le point
    int l_iResult = l_jigPoint.askPoint( message );
    
    //Recuperer el point
    *ptGet = l_jigPoint.getPoint();
    
    //Sortir
    return l_iResult;
    // return l_jigPoint.getPoint(message, ptGet);
}



AcGePoint3dArray* getListPoint(
    const AcString        message1,
    const AcString        message2 )
{
    //Recuperer le premier point
    AcGePoint3d l_ptStart;
    
    if( RTNORM != getWcsPoint( message1, &l_ptStart ) )
        return NULL;
        
    //Initialiser le jig
    jigListPoint l_jigPtList = jigListPoint( l_ptStart );
    
    //Recuperer la liste des points
    return l_jigPtList.getListPoint( message2 );
}



AcGePoint3dArray* getListPoint(
    const AcString        message,
    const AcGePoint3d     ptStart,
    const AcGeVector3d    vecNormal )
{
    //Initialiser le jig
    jigListPoint l_jigPtList = jigListPoint( ptStart, vecNormal );
    
    //Recuperer la liste des points
    return l_jigPtList.getListPoint( message );
}



double getWidth(
    const AcString        message,
    const AcGePoint3d     ptStart,
    const AcGePoint3d     ptEnd,
    const AcGeVector3d    vecNormal )
{
    //Initialiser le jig
    jigWidthPoint l_jigWidth = jigWidthPoint( ptStart, ptEnd, vecNormal );
    
    //Recuperer la liste des points
    return l_jigWidth.getWidth( message );
}

AcGePoint3d getRect(
    const AcString        message,
    const AcGePoint3d     ptStart,
    const AcGePoint3d     ptEnd,
    const AcGeVector3d    vecNormal )
{
    //Initialiser le jig
    jigRectPoint l_jigRect = jigRectPoint( ptStart, ptEnd, vecNormal );
    
    //Recuperer la liste des points
    return l_jigRect.getRect( message );
}


//-----------------------------------------------------------------------------------<jigPoint>
// Description: Constructeur
jigPoint::jigPoint( const AcGeVector3d p_vectNormal )
{
    //Definir le point
    m_ptCurent = new AcDbPoint;
    m_ptCurent->setNormal( p_vectNormal );
    m_iDragStatus = AcEdJig::kNormal;
}

// Description: Destructeur
jigPoint::~jigPoint()
{
    delete m_ptCurent;
}


//-----------------------------------------------------------------------------------<sampler>
// Description: Fonction appele lors du Drag
AcEdJig::DragStatus jigPoint::sampler()
{
    //Definir le mode d'aquisition
    setUserInputControls( ( UserInputControls )
        ( AcEdJig::kAccept3dCoordinates | AcEdJig::kNullResponseAccepted ) );
        
    //Recuperer le sommet
    AcGePoint3d l_ptTemp;
    m_iDragStatus = acquirePoint( l_ptTemp );
    
    //Verifier si l'aquisition est bonne
    if( m_iDragStatus != AcEdJig::kNormal )
        return m_iDragStatus;
        
    //Verifier si le point a change
    if( l_ptTemp == m_ptCurent->position() )
        return AcEdJig::kNoChange;
        
    //Modifie rle point courant
    m_ptCurent->setPosition( l_ptTemp );
    
    //Sortir
    return m_iDragStatus;
}


//-----------------------------------------------------------------------------------<update>
// Description: Met a jour la polyligne
Adesk::Boolean jigPoint::update()
{
    return Adesk::kTrue;
}


//-----------------------------------------------------------------------------------<entity>
// Description: Renvoei la polyligne
AcDbEntity* jigPoint::entity() const
{
    return m_ptCurent;
}


//-----------------------------------------------------------------------------------<jigListPoint>
// Description: Demande a l'utilisateur de saisir les points
// Parameter1   : Message a afficher
// Parameter2   : Point a renvoyer
// Return     : RTNORM si OK, RTCAN si annule, RTNONE si entre
int jigPoint::askPoint(
    const AcString     message )
{
    //Afficher le message
    acutPrintf( message );
    
    //Lancer le jig
    m_iDragStatus = drag();
    
    //Verifier le resultat
    if( m_iDragStatus == AcEdJig::kNormal )
        return RTNORM;
        
    if( m_iDragStatus == AcEdJig::kNull )
        return RTNONE;
        
    else
        return RTCAN;
}

//-----------------------------------------------------------------------------------<jigListPoint>
// Description: Demande a l'utilisateur de saisir les points
// Parameter1   : Message a afficher
// Parameter2   : Point a renvoyer
// Return     : RTNORM si OK, RTCAN si annule, RTNONE si entre
AcGePoint3d jigPoint::getPoint()
{
    return m_ptCurent->position();
}


//-----------------------------------------------------------------------------------<jigListPoint>
// Description: Constructeur
// Parameter1   : Premier point de la polyligne
// Parameter2   : Vecteur normal de la polyligne
jigListPoint::jigListPoint(
    const AcGePoint3d  p_ptStart,
    const AcGeVector3d p_vectNormal )
{
    //Definir le point actuel
    m_ptCurent = AcGePoint3d( 0.0, 0.0, 0.0 );
    
    //Definir le plan pour transformer les point 2d en 3D
    m_planPoly = AcGePlane( m_ptCurent, p_vectNormal );
    
    //Definir la polyligne
    m_poly2D = new AcDbPolyline;
    
    //Definir la normal a la polyligne
    m_poly2D->setNormal( p_vectNormal );
    
    //Ajouter le premier sommet
    m_poly2D->addVertexAt( 0, p_ptStart.convert2d( m_planPoly ) );
    m_poly2D->addVertexAt( 1, p_ptStart.convert2d( m_planPoly ) );
    
    //Fermer la polyligne
    m_poly2D->setClosed( Adesk::kTrue );
    
    //Modifier le point actuel
    m_ptCurent = p_ptStart;
}

// Description: Destructeur
jigListPoint::~jigListPoint()
{
    delete m_poly2D;
}


//-----------------------------------------------------------------------------------<sampler>
// Description: Fonction appele lors du Drag
AcEdJig::DragStatus jigListPoint::sampler()
{
    //Definir le mode d'aquisition
    setUserInputControls( ( UserInputControls )
        ( AcEdJig::kAccept3dCoordinates | AcEdJig::kNullResponseAccepted ) );
        
    //Recuperer le sommet
    AcGePoint3d l_ptTemp;
    DragStatus l_dragStat = acquirePoint( l_ptTemp );
    
    //Verifier si l'aquisition est bonne
    if( l_dragStat != AcEdJig::kNormal )
        return l_dragStat;
        
    //Verifier si le point a change
    if( l_ptTemp == m_ptCurent )
        return AcEdJig::kNoChange;
        
    //Modifie rle point courant
    m_ptCurent = l_ptTemp;
    
    //Sortir
    return l_dragStat;
}


//-----------------------------------------------------------------------------------<update>
// Description: Met a jour la polyligne
Adesk::Boolean jigListPoint::update()
{
    //Modifier la polyligne
    if( Acad::eOk == m_poly2D->setPointAt( 0, m_ptCurent.convert2d( m_planPoly ) ) )
        return Adesk::kTrue;
    else
        return Adesk::kFalse;
}


//-----------------------------------------------------------------------------------<entity>
// Description: Renvoei la polyligne
AcDbEntity* jigListPoint::entity() const
{
    return m_poly2D;
}


//-----------------------------------------------------------------------------------<jigListPoint>
// Description: Demande a l'utilisateur de saisir les points
// Parameter1   : Premier point de la polyligne
// Parameter2   : Vecteur normal de la polyligne
AcGePoint3dArray* jigListPoint::getListPoint(
    const AcString message )
{
    //Initialiser la iste des points
    AcGePoint3dArray* l_arPoint = new AcGePoint3dArray;
    l_arPoint->append( m_ptCurent );
    
    //Boucler tant que l'utilisateur clique un point
    acutPrintf( message );
    
    while( drag() == AcEdJig::kNormal )
    {
        //Ajouter le point a la liste
        l_arPoint->append( m_ptCurent );
        m_poly2D->addVertexAt( 0, m_ptCurent.convert2d( m_planPoly ) );
        
        //Demander le point suivant
        acutPrintf( message );
    }
    
    //Verifier la taille de la liste
    if( l_arPoint->logicalLength() > 2 )
        return l_arPoint;
        
    //Vider la liste
    l_arPoint->setLogicalLength( 0 );
    
    //Sortir
    return NULL;
}


//-----------------------------------------------------------------------------------<jigListPoint>
// Description: Constructeur
// Parameter1   : Premier point de la polyligne
// Parameter2   : Vecteur normal de la polyligne
jigWidthPoint::jigWidthPoint(
    const AcGePoint3d  p_ptStart,
    const AcGePoint3d  p_ptEnd,
    const AcGeVector3d p_vectNormal )
{
    //Definir le point actuel
    m_ptCurent = AcGePoint3d( 0.0, 0.0, 0.0 );
    
    //Definir le plan pour transformer les point 2d en 3D
    m_planPoly = AcGePlane( m_ptCurent, p_vectNormal );
    
    //Definir la polyligne
    m_poly2D = new AcDbPolyline;
    
    //Definir la normal a la polyligne
    m_poly2D->setNormal( p_vectNormal );
    
    //Convertir le points en 2D
    m_ptStart = p_ptStart.convert2d( m_planPoly );
    m_ptEnd = p_ptEnd.convert2d( m_planPoly );
    
    //Caclul du vecteur directeur
    m_vectAxe = getVector2d( m_ptStart, p_ptEnd.convert2d( m_planPoly ) );
    m_vectPerp = m_vectAxe.perpVector().normalize();
    
    //Cacluler les facteur de calcul de la largeur
    m_dXFactor = m_vectAxe.x / m_vectAxe.length();
    m_dYFactor = m_vectAxe.y / m_vectAxe.length();
    
    //Ajouter le premier sommet
    m_poly2D->addVertexAt( 0, m_ptStart );
    m_poly2D->addVertexAt( 1, m_ptEnd );
    m_poly2D->addVertexAt( 2, m_ptStart );
    m_poly2D->addVertexAt( 3, m_ptEnd );
    
    //Fermer la polyligne
    m_poly2D->setClosed( Adesk::kTrue );
    
    //Initialiser la largeur
    m_ptCurent = p_ptStart;
    m_dWidth = 0.0;
}

// Description: Destructeur
jigWidthPoint::~jigWidthPoint()
{
    delete m_poly2D;
}


//-----------------------------------------------------------------------------------<sampler>
// Description: Fonction appele lors du Drag
AcEdJig::DragStatus jigWidthPoint::sampler()
{
    //Definir le mode d'aquisition
    setUserInputControls( ( UserInputControls )
        ( AcEdJig::kAccept3dCoordinates |
            AcEdJig::kNullResponseAccepted |
            AcEdJig::kAcceptOtherInputString |
            kDisableDirectDistanceInput ) );
            
    //Recuperer le sommet
    AcGePoint3d l_ptTemp;
    DragStatus l_dragStat = acquirePoint( l_ptTemp );
    
    //Verifier si l'aquisition est bonne
    if( l_dragStat != AcEdJig::kNormal )
        return l_dragStat;
        
    //Verifier si le point a change
    if( l_ptTemp == m_ptCurent )
        return AcEdJig::kNoChange;
        
    //Transformer le point en 2D
    AcGePoint2d l_ptCurent = l_ptTemp.convert2d( m_planPoly );
    
    //Cacluler la distance a l'axe
    double l_dWidth = abs( m_dXFactor * ( l_ptCurent.y - m_ptStart.y ) -  m_dYFactor * ( l_ptCurent.x - m_ptStart.x ) );
    
    //Verifier si le point a change
    if( l_dWidth == m_dWidth )
        return AcEdJig::kNoChange;
        
    //Modifier le point courant
    m_ptCurent = l_ptTemp;
    m_dWidth   = l_dWidth;
    
    //Sortir
    return l_dragStat;
}


//-----------------------------------------------------------------------------------<update>
// Description: Met a jour la polyligne
Adesk::Boolean jigWidthPoint::update()
{
    //Cacluer le vecteur perpendiculaire
    AcGeVector2d l_vecxtPerp = m_dWidth * m_vectPerp;
    
    //Modifier la polyligne
    if(
        Acad::eOk == m_poly2D->setPointAt( 0, m_ptStart + l_vecxtPerp ) &&
        Acad::eOk == m_poly2D->setPointAt( 1, m_ptStart - l_vecxtPerp ) &&
        Acad::eOk == m_poly2D->setPointAt( 2, m_ptEnd   - l_vecxtPerp ) &&
        Acad::eOk == m_poly2D->setPointAt( 3, m_ptEnd   + l_vecxtPerp ) )
        return Adesk::kTrue;
    else
        return Adesk::kFalse;
}


//-----------------------------------------------------------------------------------<entity>
// Description: Renvoei la polyligne
AcDbEntity* jigWidthPoint::entity() const
{
    return m_poly2D;
}


//-----------------------------------------------------------------------------------<jigListPoint>
// Description: Demande a l'utilisateur de saisir les points
// Parameter1   : Premier point de la polyligne
// Parameter2   : Vecteur normal de la polyligne
double jigWidthPoint::getWidth(
    const AcString message )
{
    //Afficher le message
    acutPrintf( message );
    
    //LAncer le jig
    DragStatus l_dragStatus = drag();
    
    //Verifier le resultat
    if( l_dragStatus == AcEdJig::kNormal )
        m_dWidth = 2.0 * m_dWidth;
    else if( l_dragStatus == AcEdJig::kOther )
    {
        //Recuperere la valeur
        ACHAR l_sValue[2049];
        acquireString( l_sValue );
        
        //Convertir la valeur
        if( !stringToDouble( l_sValue, &m_dWidth ) )
            m_dWidth = -1.0;
    }
    
    else
        m_dWidth = -1.0;
        
    //Sortir
    return m_dWidth;
}

////////////////////

//-----------------------------------------------------------------------------------<jigListPoint>
// Description: Constructeur
// Parameter1   : Premier point de la polyligne
// Parameter2   : Vecteur normal de la polyligne
jigRectPoint::jigRectPoint(
    const AcGePoint3d  p_ptStart,
    const AcGePoint3d  p_ptEnd,
    const AcGeVector3d p_vectNormal )
{
    //Definir le point actuel
    m_ptCurent = AcGePoint3d( 0.0, 0.0, 0.0 );
    
    //Definir le plan pour transformer les point 2d en 3D
    m_planPoly = AcGePlane( m_ptCurent, p_vectNormal );
    
    //Definir la polyligne
    m_poly2D = new AcDbPolyline;
    
    //Definir la normal a la polyligne
    m_poly2D->setNormal( p_vectNormal );
    
    //Convertir le points en 2D
    m_ptStart = p_ptStart.convert2d( m_planPoly );
    m_ptEnd = p_ptEnd.convert2d( m_planPoly );
    
    //Caclul du vecteur directeur
    m_vectAxe = getVector2d( m_ptStart, p_ptEnd.convert2d( m_planPoly ) );
    m_vectPerp = m_vectAxe.perpVector().normalize();
    
    //Cacluler les facteur de calcul de la largeur
    m_dXFactor = m_vectAxe.x / m_vectAxe.length();
    m_dYFactor = m_vectAxe.y / m_vectAxe.length();
    
    //Ajouter le premier sommet
    m_poly2D->addVertexAt( 0, m_ptStart );
    m_poly2D->addVertexAt( 1, m_ptEnd );
    m_poly2D->addVertexAt( 2, m_ptStart );
    m_poly2D->addVertexAt( 3, m_ptEnd );
    
    //Fermer la polyligne
    m_poly2D->setClosed( Adesk::kTrue );
    
    //Initialiser la largeur
    m_ptCurent = p_ptStart;
    m_dWidth = 0.0;
}

// Description: Destructeur
jigRectPoint::~jigRectPoint()
{
    delete m_poly2D;
}


//-----------------------------------------------------------------------------------<sampler>
// Description: Fonction appele lors du Drag
AcEdJig::DragStatus jigRectPoint::sampler()
{
    //Definir le mode d'aquisition
    setUserInputControls( ( UserInputControls )
        ( AcEdJig::kAccept3dCoordinates |
            AcEdJig::kNullResponseAccepted |
            AcEdJig::kAcceptOtherInputString |
            kDisableDirectDistanceInput ) );
            
    //Recuperer le sommet
    AcGePoint3d l_ptTemp;
    DragStatus l_dragStat = acquirePoint( l_ptTemp );
    
    //Verifier si l'aquisition est bonne
    if( l_dragStat != AcEdJig::kNormal )
        return l_dragStat;
        
    //Verifier si le point a change
    if( l_ptTemp == m_ptCurent )
        return AcEdJig::kNoChange;
        
    //Transformer le point en 2D
    AcGePoint2d l_ptCurent = l_ptTemp.convert2d( m_planPoly );
    
    //Cacluler la distance a l'axe
    double l_dWidth = abs( m_dXFactor * ( l_ptCurent.y - m_ptStart.y ) - m_dYFactor * ( l_ptCurent.x - m_ptStart.x ) );
    
    //Verifier si le point a change
    if( l_dWidth == m_dWidth )
        return AcEdJig::kNoChange;
        
    //Modifier le point courant
    m_ptCurent = l_ptTemp;
    m_dWidth = l_dWidth;
    
    //Sortir
    return l_dragStat;
}


//-----------------------------------------------------------------------------------<update>
// Description: Met a jour la polyligne
Adesk::Boolean jigRectPoint::update()
{
    //Calculer le sens
    double sense = m_vectPerp.dotProduct( AcGeVector2d( m_ptCurent.x - m_ptStart.x, m_ptCurent.y - m_ptStart.y ) );
    
    //Cacluer le vecteur perpendiculaire
    AcGeVector2d l_vecxtPerp = std::copysign( 1.0, sense ) * m_dWidth * m_vectPerp;
    
    //Modifier la polyligne
    if(
        Acad::eOk == m_poly2D->setPointAt( 0, m_ptStart ) &&
        Acad::eOk == m_poly2D->setPointAt( 1, m_ptStart + l_vecxtPerp ) &&
        Acad::eOk == m_poly2D->setPointAt( 2, m_ptEnd + l_vecxtPerp ) &&
        Acad::eOk == m_poly2D->setPointAt( 3, m_ptEnd ) )
        return Adesk::kTrue;
    else
        return Adesk::kFalse;
}


//-----------------------------------------------------------------------------------<entity>
// Description: Renvoei la polyligne
AcDbEntity* jigRectPoint::entity() const
{
    return m_poly2D;
}


//-----------------------------------------------------------------------------------<jigListPoint>
// Description: Demande a l'utilisateur de saisir les points de diagonal du rectangle
// Parameter1   : Premier point de la polyligne
// Parameter2   : Vecteur normal de la polyligne
AcGePoint3d jigRectPoint::getRect(
    const AcString message )
{
    //Afficher le message
    acutPrintf( message );
    
    //LAncer le jig
    DragStatus l_dragStatus = drag();
    
    //Verifier si l'utilisateur a annul�e
    if( l_dragStatus == DragStatus::kCancel )
        return AcGePoint3d( -0.123456, -0.123456, -0.123456 );
        
    //Verifier le resultat
    if( l_dragStatus == AcEdJig::kNormal )
        m_dWidth = m_dWidth;
    else if( l_dragStatus == AcEdJig::kOther )
    {
        //Recuperere la valeur
        ACHAR l_sValue[2049];
        acquireString( l_sValue );
        
        //Convertir la valeur
        if( !stringToDouble( l_sValue, &m_dWidth ) )
            m_dWidth = -1.0;
    }
    
    else
        m_dWidth = -1.0;
        
    //Sortir : on renvoie la projection du point sur m_vectPerp
    double sense = m_vectPerp.dotProduct( AcGeVector2d( m_ptCurent.x - m_ptStart.x, m_ptCurent.y - m_ptStart.y ) );
    AcGeVector2d l_vecxtPerp = std::copysign( 1.0, sense ) * m_dWidth * m_vectPerp;
    AcGePoint2d rectCorner = m_ptEnd + l_vecxtPerp;
    
    return AcGePoint3d( rectCorner.x, rectCorner.y, 0 );
}
