#include "poly2DEntity.h"
#include "arcEntity.h"
#include "geometry.h"
#include <set>
#include <iterator>
#include <cstdlib>
#include <vector>
#include "groupEntity.h"
#include "print.h"

using namespace std ;

long getSsPoly2D(
    ads_name          ssName,
    const AcString&   layerName,
    const bool&       isClosed )
{
    return getSelectionSet( ssName, "", "LWPOLYLINE", layerName, "", "", isClosed );
}






long getSsOnePoly2D(
    ads_name          ssName,
    const AcString&   layerName,
    const bool&       isClosed )
{
    return getSingleSelection( ssName, "LWPOLYLINE", layerName, "", "", isClosed );
}




long getSsAllPoly2D(
    ads_name        ssName,
    const AcString& layerName,
    const bool&     isClosed )
{
    return getSelectionSet( ssName, "X", "LWPOLYLINE", layerName, "", "", isClosed );
}




AcDbPolyline* getPoly2DFromSs(
    const ads_name&               ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  l_pEntityLine = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbPolyline::cast( l_pEntityLine );
}




AcDbPolyline* readPoly2DFromSs(
    const ads_name&       ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getPoly2DFromSs( ssName, iObject, AcDb::kForRead );
}




AcDbPolyline* writePoly2DFromSs(
    const ads_name&              ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getPoly2DFromSs( ssName, iObject, AcDb::kForWrite );
}




AcDbPolyline* getPoly2D(
    const AcGePoint2dArray arPoint )
{
    //Recuperer la taille des tableaux
    int l_nPoint = arPoint.logicalLength();
    
    //Creer la polyligne
    AcDbPolyline* l_obPoly2D = new AcDbPolyline();
    
    //Boucler sur le tableau des points
    while( l_nPoint-- > 0 )
    {
        //Ajouter le sommet
        l_obPoly2D->addVertexAt( 0, arPoint[l_nPoint] );
    }
    
    //Sortir
    return l_obPoly2D;
}




AcDbPolyline* getPoly2D(
    const AcGePoint3dArray arPoint )
{
    //Recuperer la taille des tableaux
    int l_nPoint = arPoint.logicalLength();
    
    //Creer la polyligne
    AcDbPolyline* l_obPoly2D = new AcDbPolyline();
    
    //Boucler sur le tableau des points
    while( l_nPoint-- > 0 )
    {
        //Ajouter le sommet
        l_obPoly2D->addVertexAt( 0, AcGePoint2d( arPoint[l_nPoint].x, arPoint[l_nPoint].y ) );
    }
    
    //Sortir
    return l_obPoly2D;
}




AcDbPolyline* getPoly2D(
    const AcGePoint2dArray    arPoint,
    const AcGeDoubleArray     arBulge )
{
    //Recuperer la taille des tableaux
    int l_nPoint = arPoint.logicalLength();
    int l_nBulge = arBulge.logicalLength();
    
    //Verifier les deux tableaux
    if( l_nPoint != l_nBulge )
        return NULL;
        
    //Creer la polyligne
    AcDbPolyline* l_obPoly2D = new AcDbPolyline();
    
    //Boucler sur le tableau des points
    while( l_nPoint-- > 0 )
    {
        //Ajouter le sommet
        l_obPoly2D->addVertexAt( 0, arPoint[l_nPoint], arBulge[l_nPoint] );
    }
    
    //Sortir
    return l_obPoly2D;
}




AcDbPolyline* getPoly2D(
    const AcGePoint3dArray    arPoint,
    const AcGeDoubleArray     arBulge )
{
    //Recuperer la taille des tableaux
    int l_nPoint = arPoint.logicalLength();
    int l_nBulge = arBulge.logicalLength();
    
    //Verifier les deux tableaux
    if( l_nPoint != l_nBulge )
        return NULL;
        
    //Creer la polyligne
    AcDbPolyline* l_obPoly2D = new AcDbPolyline();
    
    //Boucler sur le tableau des points
    while( l_nPoint-- > 0 )
    {
        //Ajouter le sommet
        l_obPoly2D->addVertexAt( 0, AcGePoint2d( arPoint[l_nPoint].x, arPoint[l_nPoint].y ), arBulge[l_nPoint] );
    }
    
    //Sortir
    return l_obPoly2D;
}




double getAbscisse(
    const AcDbPolyline* poly,
    const unsigned int  index )
{
    //Declarer le resultat
    double l_dParam    = 0.0;
    double l_dAbscisse = 0.0;
    AcGePoint3d l_ptVertex;
    
    //Cas du premier sommet
    if( index == 0 &&
        Acad::eOk == poly->getStartParam( l_dParam ) &&
        Acad::eOk == poly->getDistAtParam( l_dParam, l_dAbscisse ) )
        return l_dAbscisse;
        
    //Cas du dernier sommet
    else if( index + 1 == poly->numVerts() &&
        Acad::eOk == poly->getEndParam( l_dParam ) &&
        Acad::eOk == poly->getDistAtParam( l_dParam, l_dAbscisse ) )
        return l_dAbscisse;
        
    //Cas normal
    else if( Acad::eOk == poly->getPointAt( index, l_ptVertex ) )
        return getAbscisse( poly, l_ptVertex, false );
        
    //Sinon, sortir vide
    return NULL;
}



double getAbscisse(
    const AcDbPolyline*  poly,
    const AcGePoint3d    vertex,
    const bool           toProj )
{
    //Declarer le resultat
    double l_dAbscisse = 0.0;
    AcGePoint3d l_ptVertex;
    
    //Cas ou il faut projeter el point
    if( toProj &&
        Acad::eOk == poly->getClosestPointTo( vertex, l_ptVertex ) &&
        Acad::eOk == poly->getDistAtPoint( l_ptVertex, l_dAbscisse ) )
        return l_dAbscisse;
        
    //Cas normal
    else if( Acad::eOk == poly->getDistAtPoint( vertex, l_dAbscisse ) )
        return l_dAbscisse;
        
    //Sinon, sortir vide
    return NULL;
}



bool getSide(
    const AcDbPolyline*   poly,
    const AcGePoint3d     pt )
{
    //Verifier si c'est en 2D
    AcGePoint3d l_ptPoly = pt;
    l_ptPoly.z = poly->elevation();
    
    //Projeter le point sur la polyligne
    AcGePoint3d l_ptClose;
    
    if( Acad::eOk != poly->getClosestPointTo( l_ptPoly, l_ptClose ) )
        return false;
        
    //Projeter le point sur la polyligne
    AcGeVector3d l_vectPoly;
    
    if( Acad::eOk != poly->getFirstDeriv( l_ptClose, l_vectPoly ) )
        return false;
        
    //Calculer le produit vectoriel
    double l_dCrossProd = ( l_ptPoly.x - l_ptClose.x ) * l_vectPoly.y - ( l_ptPoly.y - l_ptClose.y ) * l_vectPoly.x;
    
    //Sinon, sortir vide
    return l_dCrossProd > 0.0;
}





double getBulge(
    const AcGePoint3d p_ptStart,
    const AcGePoint3d p_ptMid,
    const AcGePoint3d p_ptEnd )
{
    //Calculer les vecteurs
    AcGeVector2d l_vect21 = getVector2d( p_ptMid, p_ptStart );
    AcGeVector2d l_vect23 = getVector2d( p_ptMid, p_ptEnd );
    
    //Corriger le vecteur 21
    l_vect21.x = -l_vect21.x;
    
    //Calculer les angles
    double l_dAngle = 0.5 * ( l_vect21.angle() +  l_vect23.angle() );
    
    //Calculer les cosinus et sinus
    double l_dCos = cos( l_dAngle );
    double l_dSin = sin( l_dAngle );
    
    //Verifie rle cossinus
    if( l_dCos == 0.0 )
        return 0.0;
        
    //Calcule rle bulge
    return l_dSin / l_dCos;
}




double getLength(
    const AcDbCurve* const poly )
{
    //Declarerle parametre
    double l_dParam = 0.0;
    
    //Recuperer le derneir parametre
    if( Acad::eOk != poly->getEndParam( l_dParam ) )
        return -1.0;
        
    //Declarer la longueur
    double l_dLength = 0.0;
    
    //Calculer l'abscisse max
    if( Acad::eOk != poly->getDistAtParam( l_dParam, l_dLength ) )
        return -1.0;
        
    //Sortir
    return l_dLength;
}




bool isClosed(
    const AcDbPolyline* const poly )
{
    return poly->isClosed() == Adesk::kTrue;
}




AcGePoint2dArray discretize( AcDbPolyline* poly2D, const double& tol )
{

    AcDbPolyline::SegType segmentType;
    AcGeLineSeg3d lineSeg;
    AcGeCircArc3d arcSeg;
    AcGePoint2dArray pointArray;
    
    // Recuperer le nombre de sommets de la polyligne2D
    int iVertex = 0;
    int nbVertex = poly2D->numVerts();
    AcGePoint2d pt = AcGePoint2d( 0, 0 );
    
    // Boucle sur les sommets
    while( iVertex < nbVertex )
    {
        // Recuperer le type de segment
        segmentType = poly2D->segType( iVertex );
        
        // Cas d'un segment droite
        if( ( segmentType == AcDbPolyline::SegType::kLine ||
                segmentType == AcDbPolyline::SegType::kPoint ) &&
            Acad::eOk == poly2D->getLineSegAt( iVertex, lineSeg ) )
        {
            //poly2D->getPointAt( iVertex, pt );
            AcGePoint3d pt3d;
            poly2D->getPointAt( iVertex, pt3d );
            pt = AcGePoint2d( pt3d.x, pt3d.y );
            pointArray.append( pt );
        }
        
        
        // Cas d'un arc
        else if( AcDbPolyline::SegType::kArc == segmentType &&
            Acad::eOk == poly2D->getArcSegAt( iVertex, arcSeg ) )
        {
            // Discretiser la courbe
            AcGePoint3dArray arcArray = discretize( arcSeg, tol );
            
            // On ajoute les points discretises de l'arc aux points discretises de la polyligne2D
            int length = arcArray.length() - 1;
            
            for( int i = 0; i < length; i++ )
                pointArray.append( getPoint2d( arcArray[ i ] ) );
                
            if( iVertex == nbVertex - 1 )
                pointArray.append( getPoint2d( arcArray[ iVertex ] ) );
                
            // Supprimer le taleau de points de l'arc
            arcArray.setLogicalLength( 0 );
        }
        
        // itererer sur les vertex
        iVertex ++;
    }
    
    return pointArray;
}


AcGePoint3dArray discretize3D( AcDbPolyline* poly2D, const double& tol )
{

    AcDbPolyline::SegType segmentType;
    AcGeLineSeg3d lineSeg;
    AcGeCircArc3d arcSeg;
    AcGePoint3dArray pointArray;
    
    // Recuperer le nombre de sommets de la polyligne2D
    int iVertex = 0;
    int nbVertex = poly2D->numVerts();
    AcGePoint2d pt = AcGePoint2d( 0, 0 );
    
    // Boucle sur les sommets
    while( iVertex < nbVertex )
    {
        // Recuperer le type de segment
        segmentType = poly2D->segType( iVertex );
        
        // Cas d'un segment droite
        if( ( segmentType == AcDbPolyline::SegType::kLine ||
                segmentType == AcDbPolyline::SegType::kPoint ) &&
            Acad::eOk == poly2D->getLineSegAt( iVertex, lineSeg ) )
        {
            //poly2D->getPointAt( iVertex, pt );
            AcGePoint3d pt3d;
            poly2D->getPointAt( iVertex, pt3d );
            //pt = AcGePoint2d(pt3d.x, pt3d.y);
            pointArray.append( pt3d );
        }
        
        
        // Cas d'un arc
        else if( AcDbPolyline::SegType::kArc == segmentType &&
            Acad::eOk == poly2D->getArcSegAt( iVertex, arcSeg ) )
        {
            // Discretiser la courbe
            AcGePoint3dArray arcArray = discretize( arcSeg, tol );
            
            // On ajoute les points discretises de l'arc aux points discretises de la polyligne2D
            int length = arcArray.length() - 1;
            
            for( int i = 0; i < length; i++ )
                pointArray.append( arcArray[i] );
                
            if( iVertex == nbVertex - 1 )
                pointArray.append( arcArray[iVertex] );
                
            // Supprimer le taleau de points de l'arc
            arcArray.setLogicalLength( 0 );
        }
        
        // itererer sur les vertex
        iVertex++;
    }
    
    return pointArray;
}



bool hasArc( ads_name& ss, const long& length )
{
    // Parcourir la selection
    for( int i = 0; i < length; i++ )
    {
        //Recuperer la polyligne de la selection
        AcDbPolyline* poly2D = getPoly2DFromSs( ss, i, AcDb::kForRead );
        
        // S'il y a un arc, on renvoie true
        if( poly2D->hasBulges() )
        {
            poly2D->close();
            return true;
        }
        
        poly2D->close();
    }
    
    // Pas d'arc, on renvoie false
    return false;
}




AcDbPolyline* draw3Dpoly2D( AcGePoint3dArray pointArray )
{
    // On cree la polyligne2D
    AcDbPolyline* poly2D = new AcDbPolyline();
    
    // On recupere les points de la liste
    AcGePoint3d pt1, pt2, pt3;
    pt1 = pointArray[ 0 ];
    pt2 = pointArray[ 1 ];
    pt3 = pointArray[ 2 ];
    
    // On cree un plan � partir de ces 3 points
    AcGePlane plan = AcGePlane( pt1, pt2, pt3 );
    
    // On parcourt les autres points de la liste pour verifier qu'ils soient dans le m�me plan
    int length = pointArray.length();
    
    for( int i = 3; i < length; i++ )
    {
        // Si le point n'est pas dans le m�me plan, on ne cree pas de polyligne2D, on renvoie NULL
        if( plan.signedDistanceTo( pointArray[ i ] ) )
            return NULL;
    }
    
    // Calcul de l'elevation du plan
    double elevation = plan.signedDistanceTo( AcGePoint3d::kOrigin );
    
    // Recuperer la normale du plan
    AcGeVector3d norm = getNormal( pt1, pt2, pt3 );
    
    // Definir la normale de la polyligne et son elevation
    poly2D->setNormal( norm );
    poly2D->setElevation( ( -elevation ) );
    
    // Creer la matrice de transformation d'un point en systeme de coordonnees generales
    // en systeme de coordonees du plan
    AcGeMatrix3d mat = AcGeMatrix3d::worldToPlane( plan.normal() );
    
    // Ajouter les sommets dans le bon systeme de coordonnees � la polyligne
    for( int i = 0; i < pointArray.size(); i++ )
    {
        pointArray[ i ].transformBy( mat );
        poly2D->addVertexAt( i, getPoint2d( pointArray[ i ] ) );
    }
    
    // Renvoyer la polyligne
    return poly2D;
}




int netPoly( AcDbPolyline* poly2D,
    const double& tol )
{
    //Declaration des points pour stocker les points � tester
    AcGePoint2d pt =  AcGePoint2d::kOrigin;
    AcGePoint2d ptPrev =  AcGePoint2d::kOrigin;
    poly2D->getPointAt( 0, ptPrev );
    
    //Variable qui va contenir le nombre de vertex supprimes
    int ver = 0;
    
    //Nombre de vertex dans la polyligne
    int numVertex = poly2D->numVerts();
    
    //Boucle de nettoyage des points superposes de la i-eme polyligne en selection
    for( int k = 1; k < numVertex; k++ )
    {
        //Recuperation des coordonnees du reste des vertex de la i-eme polyligne
        poly2D->getPointAt( k, pt );
        
        //Test de superposition avec une tolerance
        if( isEqual2d( pt, ptPrev, tol ) )
        {
            //Suppression des index superposes
            poly2D->removeVertexAt( k );
            
            //Decrementation des index du vertex pour stocker le nouveau point
            k--;
            numVertex--;
            
            //Incrementation du nombre de vertex supprime � chaque superposition
            ver++;
        }
        
        //Recuperation du dernier point pour continuer le test de superposition
        else
            ptPrev = pt;
    }
    
    //Test de superposition sur le premier et le dernier vertex, si superpose la polyligne sera fermee
    poly2D->getPointAt( 0, pt );
    
    if( isEqual2d( pt, ptPrev, tol ) )
    {
        poly2D->removeVertexAt( numVertex - 1 );
        poly2D->setClosed( true );
        ver++;
    }
    
    // Verifier le nombre de sommets de la polylignes
    numVertex = poly2D->numVerts();
    
    // S'il ne reste qu'un vertex sur la polyligne on supprime la polyligne
    if( numVertex <= 1 )
        poly2D->erase();
        
    poly2D->close();
    
    //Retourne le nombre de vertex superposes enleves dans la selection
    return ver;
}



int coliPoly( AcDbPolyline* poly2D,
    const double& tol )
{
    //Declaration des points 2D pour stocker les points � tester la colinearite
    AcGePoint2d pt0, pt1, pt2, first, last;
    
    //Declaration de deux entier pour contenir le deuxieme sommet (non vide) et le dernier sommet(non vide)
    int deuNv = 0, derNv = 0;
    
    //Declaration d'un booleen pour savoir si la polyligne est ouverte ou fermee
    bool closeOrOpen = false;
    
    //Variable qui va contenir le nombre de vertex fusionnes
    int ver = 0;
    
    //Nombre de vertex dans la polyligne
    int numVertex = poly2D->numVerts();
    int numVertexOrigin = numVertex;
    
    //Boucle dans la polyligne pour retrouver les points colineaires et fusionner s'il y en a
    for( int k = 2; k < numVertex; k++ )
    {
        //Trois points � tester la colinearite
        poly2D->getPointAt( k, pt2 );
        poly2D->getPointAt( k - 1, pt1 );
        poly2D->getPointAt( k - 2, pt0 );
        
        //Test si les trois points sont colineaires
        if( isPointOnLine( pt0, pt2, pt1, tol ) )
        {
            //Si les sommets sont colineaires on enleve le vertex
            poly2D->removeVertexAt( k - 1 );
            
            //Decrementation des indexs du vertex pour stocker le nouveau vertex
            k--;
            numVertex--;
        }
        
        //Incrementer le nombre des sommets fusionnes
        ver++;
    }
    
    //Dernier sommet non vide et deuxieme sommet non vide
    numVertex = poly2D->numVerts();
    poly2D->getPointAt( numVertex - 1, pt2 );
    poly2D->getPointAt( 0, pt0 );
    poly2D->getPointAt( 1, pt1 );
    
    //Si le dernier et le premier sommet sont  superposes on supprime le dernier sommet
    if( isEqual2d( pt0, pt2, tol ) )
    {
        poly2D->removeVertexAt( numVertex - 1 );
        closeOrOpen = true;
    }
    
    //Actualisation du dernier sommet
    numVertex = poly2D->numVerts();
    poly2D->getPointAt( numVertex - 1, pt2 );
    
    //Test si le premier sommet, le second sommet (non vide), le dernier sommet (non vide) sont colineaire
    if( isPointOnLine( pt2, pt1, pt0, tol ) )
        poly2D->removeVertexAt( 0 );
        
    //Test si le premier sommet, le dernier et l'avant dernier sommet sont colineaire
    poly2D->getPointAt( numVertex - 2, pt2 );
    poly2D->getPointAt( 0, pt1 );
    poly2D->getPointAt( numVertex - 1, pt0 );
    
    if( isPointOnLine( pt2, pt1, pt0 ) )
        poly2D->removeVertexAt( numVertex - 1 );
        
    ver++;
    
    //Si la polyligne est ouverte on le ferme
    if( closeOrOpen )
        poly2D->setClosed( true );
        
    poly2D->close();
    
    return ver;
}


int netPoly2( AcDbPolyline* poly2D,
    const bool& erase,
    const double& tol )
{
    //Recuperer les sommets de la polyligne
    AcGePoint2dArray vtxArray;
    vector<double> vtxX;
    Acad::ErrorStatus er = getVertexOfPoly2D( poly2D, vtxArray, vtxX );
    
    //Recuperer la taille du vecteur
    int taille = vtxArray.size();
    
    //Verifier
    if( taille == 0 )
        return taille;
        
    //Commpteur
    int count = 0;
    
    //Boucle sur les points
    for( int i = 0; i < vtxArray.size() - 1 ; i++ )
    {
        //Tester les points
        if( isEqual2d( vtxArray[i], vtxArray[i + 1], tol ) )
        {
            //Supprimer le point
            vtxArray.erase( vtxArray.begin() + ( i + 1 ) );
            
            i--;
            
            //Iterer le compteur
            count++;
        }
    }
    
    //Cr�er une polyligne
    AcDbPolyline* newPoly = new AcDbPolyline();
    
    //Taille de la polyligne
    taille = vtxArray.size();
    
    //Ajouter les vertex de la polyligne
    for( int i = 0; i < taille; i++ )
        newPoly->addVertexAt( i, vtxArray[i] );
        
    //Ajouter la polyligne au modelspace
    addToModelSpace( newPoly );
    
    //Copier les propriet�s de la polyligne d'origine
    newPoly->setPropertiesFrom( poly2D );
    
    //Fermer la polyligne sur elle m�me
    newPoly->setClosed( poly2D->isClosed() );
    
    //Fermer la polyligne
    newPoly->close();
    
    //Si on veut suppimer la polyligne
    if( erase )
    {
        poly2D->erase();
        poly2D->close();
    }
    
    //Retourner le nombre de vertex supprim�
    return count;
}


int coliPoly2( AcDbPolyline* poly2D,
    const bool& erase,
    const double& tol )
{
    //Recuperer les sommets de la polyligne
    AcGePoint2dArray vtxArray;
    vector<double> vtxX;
    Acad::ErrorStatus er = getVertexOfPoly2D( poly2D, vtxArray, vtxX );
    
    //Recuperer la taille du vecteur
    int taille = vtxArray.size();
    
    //Verifier
    if( taille == 0 )
        return taille;
        
    //Compteur
    int count = 0;
    
    //Boucle sur les points
    for( int i = 0; i < vtxArray.size() - 2 ; i++ )
    {
        //Tester les points
        if( isPointOnLine( vtxArray[i], vtxArray[i + 2], vtxArray[i + 1], tol ) )
        {
            //Supprimer le point
            vtxArray.erase( vtxArray.begin() + ( i + 1 ) );
            
            i--;
            
            //Iterer le compteur
            count++;
        }
    }
    
    //Cr�er une polyligne
    AcDbPolyline* newPoly = new AcDbPolyline();
    
    //Taille de la polyligne
    taille = vtxArray.size();
    
    //Ajouter le vertex de la polyligne
    for( int i = 0; i < taille; i++ )
        newPoly->addVertexAt( i, vtxArray[i] );
        
    //Ajouter la polyligne au modelspace
    addToModelSpace( newPoly );
    
    //Copier les proprietes de la polyligne d'origine
    newPoly->setPropertiesFrom( poly2D );
    
    //Fermer la polyligne sur elle m�me
    newPoly->setClosed( poly2D->isClosed() );
    
    //Fermer la polyligne
    newPoly->close();
    
    //Si on veut supprimer la polyligne
    if( erase )
    {
        poly2D->erase();
        poly2D->close();
    }
    
    //Retourner le nombre de vertex supprim�
    return count;
}


int netColiPoly( AcDbPolyline* poly2D,
    const double& tol )
{
    //Declaration des points pour stocker les points � tester la colinearite
    AcGePoint2d pt, ptPrev;
    AcGePoint2d pt0, pt1, pt2;
    
    //Variable qui va contenir le nombre de vertex superposes ou fusionnes qui sont supprimes
    int ver = 0;
    
    //Nombre de vertex dans la polyligne
    int numVertex = poly2D->numVerts();
    
    //Booleen qui  indique si la polyligne est ouverte ou non
    bool closeOrOpen = false;
    
    //Premier point et dernier point de la polyligne, Si le dernier et le premier sommet sont  superposes on supprime le dernier sommet
    poly2D->getPointAt( 0, pt0 );
    poly2D->getPointAt( numVertex - 1, pt2 );
    
    if( isEqual2d( pt0, pt2, tol ) )
        closeOrOpen = true;
        
    //Boucle de nettoyage des points superposes et colineaires de la i-eme polyligne en selection
    for( int k = 1; k < numVertex; k++ )
    {
        //Recuperation des coordonnees du reste des vertex de la i-eme polyligne
        poly2D->getPointAt( k, pt );
        
        //Test de superposition avec une tolerance et suppression des index superposes
        if( isEqual2d( pt, ptPrev, tol ) )
        {
            poly2D->removeVertexAt( k );
            
            //Decrementation des index du vertex pour stocker le nouveau point
            k--;
            numVertex--;
            
            ver++;
        }
        
        //Recuperation du dernier point pour continuer le test de superposition
        ptPrev = pt;
        
        //Trois points � tester la colinearite
        poly2D->getPointAt( k + 1, pt2 );
        poly2D->getPointAt( k, pt1 );
        poly2D->getPointAt( k - 1, pt0 );
        
        //Test si les trois points sont non colineaires, si colineaire on supprime le vertex
        if( isPointOnLine( pt0, pt2, pt1, tol ) )
        {
            poly2D->removeVertexAt( k );
            
            //Decrementation des indexs du vertex pour stocker le nouveau vertex
            k--;
            numVertex--;
            
            ver++;
        }
    }
    
    //Si le premier et le dernier sommet sont superposes on supprime le dernier sommet
    if( closeOrOpen == true )
        poly2D->removeVertexAt( numVertex - 1 );
        
    //Dernier sommet non vide et deuxieme sommet non vide
    numVertex = poly2D->numVerts();
    poly2D->getPointAt( numVertex - 1, pt2 );
    poly2D->getPointAt( 1, pt1 );
    poly2D->getPointAt( 0, pt0 );
    
    //Test si le premier sommet, le second sommet (non vide), le dernier sommet (non vide) sont colineaire
    if( isPointOnLine( pt2, pt1, pt0, tol ) )
        poly2D->removeVertexAt( 0 );
        
    ver++;
    
    //Si la polyligne est ouverte on le ferme
    if( closeOrOpen )
        poly2D->setClosed( true );
        
    poly2D->close();
    
    //Retourne la valeur de vertex supprimes et fusionnes
    return ver;
}

bool isPointInPoly( AcDbPolyline* poly2D, const AcGePoint2d& point )
{
    bool isPoint = false;
    
    // on teste si la polyligne est ouverte
    if( !poly2D->isClosed() )
    {
        acutPrintf( _T( "la polyligne n'est pas fermee\n" ) );
        return isPoint;
    }
    
    AcGeVector2d vecX = AcGeVector2d( 1, 0 );
    
    int intersectionCounter = 0;
    
    // on cree une ligne horizontale semi infinie dans le sens des x croissants, avec pour depart le point
    // en question
    AcGeRay2d rayon = AcGeRay2d( point, vecX );
    int numVerts = poly2D->numVerts();
    
    for( int i = 0; i < numVerts; i++ )
    {
    
        int varI = i + 1;
        
        if( i == numVerts - 1 )
            varI = 0;
            
        // on va creer un segment entre chaque sommet de la polyligne
        AcGePoint2d ptI, ptVarI;
        poly2D->getPointAt( i, ptI );
        poly2D->getPointAt( varI, ptVarI );
        AcGeLineSeg2d segment = AcGeLineSeg2d( ptI, ptVarI );
        
        AcGePoint2d ptOut;
        
        if( segment.intersectWith( rayon, ptOut ) )
            intersectionCounter++;
    }
    
    
    // si la ligne horizontale qu'on a definit precedement coupe la polyligne 0, 2, 4 .. etc fois, alors
    // le point est dans la polyligne, si le nombre est impair, il est � l'exterieur de la polyligne
    if( intersectionCounter % 2 != 0 )
        isPoint = true;
        
        
    return isPoint;
}


bool isPointInPolyByAngle( AcDbPolyline* poly2D, const AcGePoint3d& point )
{
    long double pi = 3.14159265358979323846;
    
    bool isPoint = false;
    
    // on teste si la polyligne est ouverte
    if( !isClosed( poly2D ) )
    {
        acutPrintf( _T( "la polyligne n'est pas fermee\n" ) );
        return isPoint;
    }
    
    int numVerts = poly2D->numVerts();
    double angle = 0.0;
    
    
    //Boucler sur les sommets
    AcGeVector3d vec1;
    AcGeVector3d vec2;
    int i = 0;
    
    AcGePoint3d pt1, pt2;
    
    for( int i = 0; i < numVerts; i++ )
    {
        poly2D->getPointAt( i, pt1 );
        
        if( i == numVerts - 1 )
            poly2D->getPointAt( 0, pt2 );
            
        else
            poly2D->getPointAt( i + 1, pt2 );
            
        vec1 = getVector3d( point, pt1 );
        vec2 = getVector3d( point, pt2 );
        
        vec1.z = 0;
        vec2.z = 0;
        
        double crossProdz = vec1.crossProduct( vec2 ).z;
        
        //Recuperer les angles
        angle += std::copysign( 1, crossProdz ) * vec1.angleTo( vec2 );
    }
    
    if( fabs( angle ) >= ( pi * 2 ) - 0.01745329251994329576922 && fabs( angle ) <= ( pi * 2 ) + 0.01745329251994329576922 )
        isPoint = true;
        
    return isPoint;
}


Acad::ErrorStatus getCentroid( AcDbPolyline* poly,
    AcGePoint2d& centroid )
{
    if( !poly->isClosed() )
        return Acad::eInvalidInput;
        
    //Declaration par d�faut de la valeur de retour
    Acad::ErrorStatus es = Acad::eOk;
    
    //Si l'objet n'est pas une polyligne
    if( poly->objectId() == AcDbObjectId::kNull ) return eNullObjectId;
    
    centroid = AcGePoint2d::kOrigin;
    AcGePoint2d vertex;
    double numVerts = poly->numVerts();
    
    //Boucle de lecture des sommets de la polyligne
    for( int i = 0; i < numVerts; i++ )
    {
        // Une polyligne ferm�e poss�de n+1 sommets car le d�but et la fin de la polyligne ont les m�mes coordonn�es
        // On r�alise donc une it�ration � vide
        if( i > 0 )
        {
            // On ouvre le sommet courant
            es = poly->getPointAt( i, vertex );
            
            if( es != Acad::eOk )
                return es;
                
            // On additionne tous les points de la polyligne
            centroid = addPoints( centroid, vertex );
        }
    }
    
    // On moyenne le r�sultat, en n'oubliant pas de retirer le vertex que l'on a saut� au d�but
    centroid = AcGePoint2d(
            centroid.x / ( numVerts - 1 ),
            centroid.y / ( numVerts - 1 ) );
            
    return Acad::eOk;
}

Acad::ErrorStatus getCentroid( AcDbPolyline* poly,
    AcGePoint3d& centroid )
{
    if( !poly->isClosed() )
        return Acad::eInvalidInput;
        
    //Declaration par d�faut de la valeur de retour
    Acad::ErrorStatus es = Acad::eOk;
    
    //Si l'objet n'est pas une polyligne
    if( poly->objectId() == AcDbObjectId::kNull ) return eNullObjectId;
    
    AcGePoint2d tempCentroid = AcGePoint2d::kOrigin;
    AcGePoint2d vertex;
    double numVerts = poly->numVerts();
    
    //Boucle de lecture des sommets de la polyligne
    for( int i = 0; i < numVerts; i++ )
    {
        // Une polyligne ferm�e poss�de n+1 sommets car le d�but et la fin de la polyligne ont les m�mes coordonn�es
        // On r�alise donc une it�ration � vide
        if( i > 0 )
        {
            // On ouvre le sommet courant
            es = poly->getPointAt( i, vertex );
            
            if( es != Acad::eOk )
                return es;
                
            // On additionne tous les points de la polyligne
            tempCentroid = addPoints( tempCentroid, vertex );
        }
    }
    
    // On moyenne le r�sultat, en n'oubliant pas de retirer le vertex que l'on a saut� au d�but
    centroid = AcGePoint3d(
            tempCentroid.x / ( numVerts - 1 ),
            tempCentroid.y / ( numVerts - 1 ),
            0 );
            
    return Acad::eOk;
}


void drawPolyline( std::vector<double> tabx, std::vector<double> taby, AcString layer, const bool& coli, const bool& hasToZoom )
{
    //Creer un objet polyline
    AcDbPolyline* poly2D = new AcDbPolyline;
    
    //Taille de la polyligne
    int size = tabx.size();
    
    //Ajouter les vertex de la polyligne
    for( int i = 0; i < size; i++ )
        poly2D->addVertexAt( i, AcGePoint2d( tabx[i], taby[i] ) );
        
    //Nettoyer la polyligne si c'est demand�
    if( coli )
        int k = coliPoly( poly2D, 0.1 );
        
    //Ajouter la polyligne au modelspace
    addToModelSpace( poly2D );
    
    //Ajouter la polyligne dans la calque trajectoire
    poly2D->setLayer( layer );
    
    if( hasToZoom )
        zoomOn( poly2D );
        
    //Fermer la polyligne
    poly2D->close();
    
}


void drawPolyline( std::vector<AcGePoint3d>& tabx, AcCmColor& colorPoly, AcString layer, const bool& coli, const bool& hasToZoom )
{
    //Creer un objet polyline
    AcDbPolyline* poly2D = new AcDbPolyline;
    
    //Taille de la polyligne
    int size = tabx.size();
    
    //Ajouter les vertex de la polyligne
    for( int i = 0; i < size; i++ )
        poly2D->addVertexAt( i, AcGePoint2d( tabx[i].x, tabx[i].y ) );
        
    //Nettoyer la polyligne si c'est demand�
    if( coli )
        int k = coliPoly( poly2D, 0.1 );
        
    //Changer la couleur
    poly2D->setColor( colorPoly );
    
    //Ajouter la polyligne au modelspace
    addToModelSpace( poly2D );
    
    //Ajouter la polyligne dans la calque trajectoire
    poly2D->setLayer( layer );
    
    if( hasToZoom )
        zoomOn( poly2D );
        
    //Fermer la polyligne
    poly2D->close();
}


AcDbPolyline* drawGetPolyline( std::vector<AcGePoint3d>& tabx, AcCmColor& colorPoly, AcString layer, const bool& coli )
{
    //Creer un objet polyline
    AcDbPolyline* poly2D = new AcDbPolyline;
    
    //Taille de la polyligne
    int size = tabx.size();
    
    //Ajouter les vertex de la polyligne
    for( int i = 0; i < size; i++ )
        poly2D->addVertexAt( i, AcGePoint2d( tabx[i].x, tabx[i].y ) );
        
    //Nettoyer la polyligne si c'est demand�
    if( coli )
        int k = coliPoly( poly2D, 0.1 );
        
    //Changer la couleur
    poly2D->setColor( colorPoly );
    
    //Ajouter la polyligne au modelspace
    addToModelSpace( poly2D );
    
    //Ajouter la polyligne dans la calque
    poly2D->setLayer( layer );
    
    AcDbEntity* entity = AcDbEntity::cast( poly2D );
    
    redrawEntity( entity );
    
    //Fermer la polyligne
    poly2D->close();
    
    //Retourner la polyligne
    return poly2D;
}


AcDbPolyline* drawGetPolyline( std::vector<AcGePoint3d> tabx,  AcString layer, const bool& isClosed )
{
    //Creer un objet polyline
    AcDbPolyline* poly2D = new AcDbPolyline;
    
    //Taille de la polyligne
    int size = tabx.size();
    
    //Ajouter les vertex de la polyligne
    for( int i = 0; i < size; i++ )
        poly2D->addVertexAt( i, AcGePoint2d( tabx[i].x, tabx[i].y ) );
        
    if( isClosed )
        poly2D->setClosed( true );
        
    //Ajouter la polyligne au modelspace
    addToModelSpace( poly2D );
    
    //Ajouter la polyligne dans la calque trajectoire
    poly2D->setLayer( layer );
    
    //ON NE FERME PAS LA POLYLIGNE
    
    //Retourner la polyligne
    return poly2D;
}


Acad::ErrorStatus zoomOn( AcDbPolyline* polyline2D,
    const double& height )
{
    return zoomOnEntity( polyline2D, height );
}


Acad::ErrorStatus
openPoly( const AcDbObjectId& idPoly,
    AcDbPolyline*& poly,
    const AcDb::OpenMode& mode )
{

    Acad::ErrorStatus es;
    AcDbEntity* pEnt;
    
    // On ouvre l'entit�
    es = acdbOpenAcDbEntity(
            pEnt,
            idPoly,
            mode );
            
            
    //Si la poly
    // Retour s'il y a une erreur
    if( es != Acad::eOk )
        return es;
        
    // On cast en cercle
    poly = AcDbPolyline::cast( pEnt );
    
    return Acad::eOk;
    
}



Acad::ErrorStatus getVertexOfPoly2D( AcDbPolyline* poly2D,
    AcGePoint2dArray& vertexArray,
    vector<double>& xVertexArray )
{
    //Resultat par defaut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Nombre de vertex dans la polyligne
    int numVertex = poly2D->numVerts();
    
    //Boucle de nettoyage des points superposes de la i-eme polyligne en selection
    for( int p = 0; p < numVertex; p++ )
    {
        //Le vertex
        AcGePoint2d pt;
        
        //Recuperation des coordonnees du reste des vertex de la i-eme polyligne
        poly2D->getPointAt( p, pt );
        
        //Ajouter dans le vecteur
        vertexArray.push_back( pt );
        xVertexArray.push_back( pt.x );
    }
    
    //Verifier avant de sortir
    if( vertexArray.size() == 0 )
        return er;
        
    return Acad::eOk;
}

Acad::ErrorStatus getVertexOfPoly2D( AcDbPolyline* poly2D,
    AcGePoint3dArray& vertexArray,
    vector<double>& xVertexArray )
{
    //Resultat par defaut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Nombre de vertex dans la polyligne
    int numVertex = poly2D->numVerts();
    
    // R�cuperer l'elevation de la polyligne
    double elevation = poly2D->elevation();
    
    //Boucle de nettoyage des points superposes de la i-eme polyligne en selection
    for( int p = 0; p < numVertex; p++ )
    {
        //Le vertex
        AcGePoint3d pt;
        
        //Recuperation des coordonnees du reste des vertex de la i-eme polyligne
        poly2D->getPointAt( p, pt );
        
        
        //Ajouter dans le vecteur
        vertexArray.push_back( pt );
        xVertexArray.push_back( pt.x );
    }
    
    //Verifier avant de sortir
    if( vertexArray.size() == 0 )
        return er;
        
    return Acad::eOk;
}


Acad::ErrorStatus getVertexesOfPoly2D( AcDbPolyline* poly2D,
    const AcGePoint3dArray& arNewPoint,
    vector<Sommet>& vertexes,
    vector<double>& xVertexArray )
{
    //Declaration
    Acad::ErrorStatus es = Acad::eNotApplicable;
    
    //Recuperer le nombre de sommets
    int size = poly2D->numVerts();
    
    if( size == 0 )
        return es;
        
    //Boucler sur les sommets de la polyligne
    for( int i = 0; i < size; i++ )
    {
        Sommet sommet;
        //Les coordonn�es
        AcGePoint3d pt;
        
        es = poly2D->getPointAt( i, pt );
        
        if( es != eOk )
            return es;
            
        //la bulge
        double bulge = 0.0;
        es = poly2D->getBulgeAt( i, bulge );
        
        if( es != eOk )
            return es;
            
        //La distance curviligne
        double distance = 0.0;
        es = poly2D->getDistAtPoint( pt, distance );
        
        if( es != eOk )
            return es;
            
        //Recuperer les informations du sommet
        sommet.index = i;
        sommet.distance = distance;
        sommet.bulge = bulge;
        sommet.point = pt;
        
        //Ajouter dans le vecteur
        vertexes.push_back( sommet );
        xVertexArray.push_back( pt.x );
    }
    
    //ajouter les autres points
    int sizeNewPoint = arNewPoint.size();
    
    if( sizeNewPoint > 0 )
    {
        for( int iNewPoint = 0; iNewPoint < sizeNewPoint;  iNewPoint++ )
        {
            Sommet sommet;
            //Recuperer le i�me point
            AcGePoint3d pt = arNewPoint[iNewPoint];
            double distance = 0.0;
            es = poly2D->getDistAtPoint( pt, distance );
            
            if( es != eOk )
            {
                //Si le point n'est pas sur la polyligne
                if( es == Acad::eInvalidInput )
                    continue;
                    
                //Sinon
                else
                    return es;
            }
            
            sommet.index = vertexes.size() + iNewPoint;
            sommet.point = pt;
            sommet.bulge = 2;
            sommet.distance = distance;
            
            vertexes.push_back( sommet );
        }
    }
    
    
    //Verifier avant de sortir
    if( vertexes.size() < 2 )
        return Acad::eNotApplicable;
        
    return Acad::eOk;
}


Acad::ErrorStatus addArVertexOnPoly( const AcGePoint3dArray& arNewPoint,
    AcDbPolyline*& poly,
    const bool& delEntity )
{
    //Resultat par d�faut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Recuperer la taille du tableau
    int taille = arNewPoint.size();
    
    //Verifier
    if( taille == 0 )
        return Acad::eOk;
        
    //Recuperer les sommets de la polyligne
    vector<Sommet> vecPoly;
    vector<double> xPoly;
    er = getVertexesOfPoly2D( poly,
            arNewPoint,
            vecPoly,
            xPoly );
            
    //Verifier
    if( er != Acad::eOk )
        return er;
        
    //Recuperer le nombre de sommets
    int sizeVertexes = vecPoly.size();
    
    if( sizeVertexes < 2 )
        return Acad::eNotApplicable;
        
    //Trier le vecteur
    er = sortListUsingCurveDistance( poly,
            vecPoly );
            
    //Verifier
    if( Acad::eOk != er )
        return er;
        
    //Creer la nouvelle polyligne
    AcDbPolyline* newPoly = new AcDbPolyline();
    
    //Calculer les bulges
    for( int iVertex = 0; iVertex < sizeVertexes; iVertex++ )
    {
        //Recuperer le i�me vertex
        Sommet vertex = vecPoly[iVertex];
        
        //Recuperer la bulge
        double bulge = vertex.bulge;
        
        if( bulge == 2 )
        {
            //Recuperer le sommet precedent
            Sommet vertexPrev = vecPoly[iVertex - 1];
            
            //Verifier son bulge
            if( vertexPrev.bulge == 0.0 )
                bulge = 0.0;
            else
            {
                //Recuperer le sommet suivant
                Sommet vertexNext = vecPoly[iVertex + 1];
                
                //Cr�er un arc � partir des trois sommets recuper�s
                AcDbArc* arcTemp =  arcByThreePoints( vertexPrev.point, vertex.point, vertexNext.point );
                
                //Recuperer le centre de l'arc
                AcGePoint3d center = arcTemp->center();
                
                //Supprimer l'arc
                delete arcTemp;
                
                //Calculer le bulge
                AcGeVector2d vec1 = getVector2d( center, vertex.point );
                AcGeVector2d vec2 = getVector2d( center, vertexNext.point );
                
                double normVec1 = sqrt( ( vec1.x * vec1.x ) + ( vec1.y * vec1.y ) );
                double normVec2 = sqrt( ( vec2.x * vec2.x ) + ( vec2.y * vec2.y ) );
                
                double angle = acos( ( vec1.dotProduct( vec2 ) ) / ( normVec1 * normVec2 ) );
                
                bulge  = tan( angle / 4 );
            }
        }
        
        //Ajouter le sommet dans la nouvelle polyligne 2D
        er = newPoly->addVertexAt( iVertex, getPoint2d( vertex.point ), bulge );
        
        if( er != eOk )
        {
            delete newPoly;
            return er;
        }
        
        
    }
    
    //Ajouter dans le modelSpace
    addToModelSpace( newPoly );
    
    //Copier les propri�t�s
    newPoly->setPropertiesFrom( poly );
    
    //Fermer la polyligne
    newPoly->close();
    
    //Si l'utilisateur veut supprimer la polyligne
    if( delEntity )
    {
        //Verifier si la polyligne est en lecture
        if( poly->isWriteEnabled() )
            poly->erase( true );
        else
            return Acad::eWasOpenForRead;
    }
    
    //Retourner ok
    return Acad::eOk;
}


AcDbPolyline* mergePoly( const ads_name& ssPoly )
{
    //Taille du selection
    long taille = 0;
    
    //Recuperer la taille du selection
    acedSSLength( ssPoly, &taille );
    
    if( taille == 0 )
        return NULL;
        
    //Vecteur de point
    AcGePoint2dArray vertex;
    
    //Boucle sur la selection
    for( int i = 0; i < taille ; i++ )
    {
        //Recuperer la polyligne
        AcDbPolyline* poly = getPoly2DFromSs( ssPoly,
                i,
                AcDb::kForRead );
                
        if( poly )
        {
            AcGePoint2dArray verPoly;
            vector<double> xPoly;
            
            //Recuperer les sommets
            Acad::ErrorStatus er = getVertexOfPoly2D( poly,
                    verPoly,
                    xPoly );
                    
            if( er != Acad::eOk )
            {
                poly->close();
                
                continue;
            }
            
            //Ajouter le point dans le vecteur
            vertex.insert( vertex.end(), verPoly.begin(), verPoly.end() );
            
            //Fermer la polyligne
            poly->close();
        }
    }
    
    int s = vertex.size();
    
    if( s == 0 )
        return NULL;
        
    else
    {
        //Creer la polyligne fusionn�s
        AcDbPolyline* res = new AcDbPolyline();
        
        //Ajouter les sommets
        for( int i = 0; i < s ; i++ )
            res->addVertexAt( i, vertex[i] );
            
        //Retourner la polyligne
        return res;
    }
}


Acad::ErrorStatus  getAllPoly2DFromSs( const ads_name& ssPoly,
    vector<AcDbPolyline*>& vectPolyPointer,
    vector<double>& vectX,
    const AcString& layer,
    const bool isLayer,
    const AcDb::OpenMode& op )
{
    //Recuperer le nombre des polyligne dans la s�lection
    long ln = 0;
    
    if( RTNORM != acedSSLength( ssPoly, &ln ) )
        return Acad::eNotApplicable;
        
    //Boucler sur la selection
    for( int i = 0; i < ln; i++ )
    {
        //Recupere la i�me polyligne
        AcDbPolyline* poly = getPoly2DFromSs( ssPoly, i, op );
        
        if( !poly )
            continue;
            
        AcGePoint2d pt ;
        poly->getPointAt( 0, pt );
        double x = pt.x;
        
        if( layer.compare( _T( "" ) ) != 0 )
        {
            AcString layerpoly =  poly->layer();
            
            if( isLayer )
            {
                if( layerpoly.compare( layer ) == 0 )
                {
                    vectPolyPointer.push_back( poly );
                    vectX.push_back( x );
                }
                
            }
            
            else
            {
                if( layerpoly.compare( layer ) != 0 )
                {
                    vectPolyPointer.push_back( poly );
                    vectX.push_back( x );
                }
            }
            
        }
        
        else
        {
            vectPolyPointer.push_back( poly );
            vectX.push_back( x );
        }
    }
    
    //Verifier la taille du vecteur
    if( vectPolyPointer.size() == 0 )
        print( "Aucun polyligne trouv�" );
        
    //Renvoyer le r�sultat
    return Acad::eOk;
}



void closePoly2dPtr( vector< AcDbPolyline* >& vectPoly )
{
    int size = vectPoly.size();
    
    sort( vectPoly.begin(),
        vectPoly.end() );
        
    vectPoly.erase( unique( vectPoly.begin(),
            vectPoly.end() ) );
            
    for( int i = 0; i < size; i++ )
    {
        if( vectPoly[ i ] )
            vectPoly[ i ]->close();
    }
}


Acad::ErrorStatus preparePoly2d( std::vector<double>& vectX,
    AcGePoint2dArray& vectPoint2d,
    std::vector<AcDbObjectId>& vectObjId,
    std::vector<AcDb2dPolyline*>& vectPolyPointer )
{
    //Taille du vecteur d'abscisse
    int taille = vectX.size();
    
    //TEST
    int taille2 = vectPoint2d.size();
    
    //Barre de progression
    ProgressBar prog = ProgressBar( _T( "Pr�paration des polylignes" ), taille );
    
    //Declaration de min
    int min = 0;
    
    //Boucle sur le vecteur d'abscisse
    for( int i = 0; i < taille - 1; i++ )
    {
        //Recuperer l'index du min
        min = i;
        
        //Boucle de test
        for( int j = i + 1; j < taille; j++ )
        {
            if( vectX[j] < vectX[min] )
                min = j;
        }
        
        //Swapper le vecteur, le AcArray et le map
        if( min != i )
        {
            std::swap( vectX[ i ], vectX[ min ] );
            vectPoint2d.swap( i, min );
            std::swap( vectObjId[i], vectObjId[min] );
            std::swap( vectPolyPointer[i], vectPolyPointer[min] );
        }
        
        //Progresser la barre de progression
        prog.moveUp( i );
    }
    
    //Retourner le resultat
    return Acad::eOk;
}


Acad::ErrorStatus preparePoly2d( std::vector<double>& vectX,
    std::vector<AcDbPolyline*>& vectPolyPointer )
{
    //Taille du vecteur d'abscisse
    int taille = vectX.size();
    
    
    //Barre de progression
    ProgressBar prog = ProgressBar( _T( "Pr�paration des polylignes" ), taille );
    
    //Declaration de min
    int min = 0;
    
    //Boucle sur le vecteur d'abscisse
    for( int i = 0; i < taille - 1; i++ )
    {
        //Recuperer l'index du min
        min = i;
        
        //Boucle de test
        for( int j = i + 1; j < taille; j++ )
        {
            if( vectX[j] < vectX[min] )
                min = j;
        }
        
        //Swapper le vecteur, le AcArray et le map
        if( min != i )
        {
            std::swap( vectX[ i ], vectX[ min ] );
            std::swap( vectPolyPointer[i], vectPolyPointer[min] );
        }
        
        //Progresser la barre de progression
        prog.moveUp( i );
    }
    
    //Retourner le resultat
    return Acad::eOk;
}


Acad::ErrorStatus projectThisPolyOnPLane( AcDbPolyline* poly2D,
    const AcGePlane& plane,
    const AcGeVector3d& projectDir )
{
    if( !poly2D )
        return Acad::eNotApplicable;
        
    //Resultat
    AcDbPolyline* polyRes = NULL;
    
    AcGePoint3d pt;
    
    Acad::ErrorStatus er = Acad::eOk;
    
    //Nombre de vertex dans la polyligne
    int numVertex = poly2D->numVerts();
    
    //Boucle de nettoyage des points superposes de la i-eme polyligne en selection
    for( int k = 1; k < numVertex; k++ )
    {
        //Recuperer le sommet
        if( er = poly2D->getPointAt( k, pt ) ) return er;
        
        getWcsPoint( &pt );
        
        //Projeter le point sur le plan
        AcGePoint3d ptProj =  pt.project( plane, projectDir );
        
        //Changer le point
        poly2D->setPointAt( k, getPoint2d( ptProj ) );
    }
    
    return er;
}


AcDbPolyline* getProjectedPoly2DOnPlane( AcDbPolyline* poly2D,
    const AcGePlane& plane,
    const AcGeVector3d& projectDir )
{
    //Resultat par d�faut
    AcDbPolyline* polyRes = NULL;
    
    //Verifier la polyligne
    if( poly2D == NULL )
        return polyRes;
        
    //Projeter la polyligne sur le plan
    AcDbCurve* curve = getProjCurveOnPlane( poly2D, plane, projectDir );
    
    if( !curve )
        return NULL;
        
    //Caster en polyligne 3d
    if( curve->isKindOf( AcDbPolyline::desc() ) )
        polyRes = AcDbPolyline::cast( curve );
        
    //Retourner NULL
    return polyRes;
}


Acad::ErrorStatus coliPoly( AcDbPolyline* poly2D,
    AcGePoint2dArray& ptarray,
    const bool& netPoly,
    const bool& erase,
    const double& tol )
{
    //Recuperer les sommets de la polyligne
    AcGePoint2dArray vtxArray;
    vector<double> vtxX;
    Acad::ErrorStatus er = getVertexOfPoly2D( poly2D, vtxArray, vtxX );
    
    //Recuperer la taille du vecteur
    int taille = vtxArray.size();
    
    //Verifier
    if( taille == 0 )
        return eNotApplicable;
        
    //Compteur
    int count = 0;
    
    
    //Boucle sur les points
    for( int i = 0; i < vtxArray.size() - 2; i++ )
    {
    
        //Tester les points
        if( isPointOnLine( vtxArray[i], vtxArray[i + 2], vtxArray[i + 1], tol ) )
        {
        
            ptarray.push_back( vtxArray[i + 1] );
            
            //Supprimer le point
            if( netPoly )
            {
                vtxArray.erase( vtxArray.begin() + ( i + 1 ) );
                i--;
            }
            
            
            
            //Iterer le compteur
            count++;
        }
    }
    
    // Compter le nombre de sommets erreurs
    int szError = ptarray.size();
    
    // Si egal � 0, sortir et renvoyer eOk
    if( szError == 0 )
        return eOk;
        
    // Sinon , corriger la polyligne si c'est demand�
    if( netPoly )
    {
        //Cr�er une polyligne
        AcDbPolyline* newPoly = new AcDbPolyline();
        
        //Taille de la polyligne
        taille = vtxArray.size();
        
        //Ajouter le vertex de la polyligne
        for( int i = 0; i < taille; i++ )
            newPoly->addVertexAt( i,  vtxArray[i] );
            
        //Ajouter la polyligne au modelspace
        addToModelSpace( newPoly );
        
        //Copier les proprietes de la polyligne d'origine
        newPoly->setPropertiesFrom( poly2D );
        
        //Fermer la polyligne sur elle m�me
        newPoly->setClosed( poly2D->isClosed() );
        
        //Fermer la polyligne
        newPoly->close();
    }
    
    //Si on veut supprimer la polyligne origine
    if( erase )
    {
        poly2D->erase();
        poly2D->close();
    }
    
    //Retourner le nombre de vertex supprim�
    return eOk;
}