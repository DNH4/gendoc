/**
 * @file fmapHeaders.h
 * @brief Headers a inclure dans tous les autres fichiers.h
 */

#pragma once

#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <vector>


#include <oaidl.h>
#include <aced.h>
#include <acgs.h>
#include <adslib.h>
#include <geray2d.h >
#include <dbapserv.h>
#include "tchar.h"

#include "dbcfilrs.h"
#include "dbidmap.h"
#include "dbeval.h"
#include "dbobjptr.h"
#include "dbents.h"
#include "dbdynblk.h"
#include "dbcolor.h"

//#define pi 3.14159265359

using namespace std;