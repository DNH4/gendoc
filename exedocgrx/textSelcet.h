//-----------------------------------------------------------------------------------<txtFR.h>
// Society: Futurmap
// Author : Romain LEMETTAIS
// Application: AutoCAD 2015
// Decription: all text of the project in French
// Decription (fr): Tous les textes du projes en francais


//-----------------------------------------------------------------------------------<Include>
#include <tchar.h>

//-----------------------------------------------------------------------------------<Selection>
#define TXT_SELECTOBJECT  _T("\nSelectionner des objets :")
#define TXT_SELECTSIZE    _T("\n%d objets selectionnes")
#define TXT_SELECTERROR1  _T("\nSelection invalide : %d elements selectionnes.")