#pragma once
#include <vector>
#include <dbmain.h>
#include "layer.h"
#include "entityLib.h"

//cr�er la structure de cellule
struct FmapCell
{
    AcGePoint3d min = AcGePoint3d::kOrigin;
    AcGePoint3d max = AcGePoint3d::kOrigin;
    int x, y;
    std::vector<AcDbObjectId> objects;
};

//cr�er la matrice
struct FmapMat
{
    AcGePoint3d min = AcGePoint3d::kOrigin;
    AcGePoint3d max = AcGePoint3d::kOrigin;
    int step = 1;
    std::vector<std::vector<FmapCell>> objects;
};

/**
  * \brief g�n�rer une grille de matrice pour une selection
  * \param ssEntity Pointeur sur la selection des entit�s
  * \param step  pas de la grille
  * \return true si l'op�ration a �t� bien pass�e
  */
bool generateFmapMat( ads_name& ssEntity,
    FmapMat& matrix,
    const int& step );

/**
  * \brief recuperer les cellules qui contient un elements dans la grille
  * \param matrix : la matrice dans laquelle on cherche les cellules correspondantes
  * \param tExtents : les coordonn�es de l'extents avec quoi on cherche les cellules associ�es
  * \return true si l'op�ration a �t� bien pass�e
  */
bool getObjectsFromMatrix( FmapMat& matrix,
    vector<AcDbObjectId>& objects,
    AcDbExtents& tExtents );

/**
  * \brief purger les doublons dans un vecteur d'objectid
  * \param vecteur des AcDbObjectId
  * \return true si l'op�ration a �t� bien pass�e
  */
bool uniqueObjectId( vector<AcDbObjectId>& objects );

