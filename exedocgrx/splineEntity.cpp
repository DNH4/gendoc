/**

  * \file       splineEntity.cpp
  * \author     Marielle H.R
  * \brief      Fichier contenant des fonctions utiles pour travailler avec les splines
  * \date       16 juillet 2019
 */

#pragma once
#include "splineEntity.h"


long getSsSpline(
    ads_name&  ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "", "SPLINE", layerName );
}



long getSsOneSpline(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSingleSelection( ssName, "SPLINE", layerName );
}


long getSsAllSpline(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSelectionSet( ssName, "X", "SPLINE", layerName ) ;
}


AcDbSpline* getSplineFromSs(
    const ads_name& ssName,
    const long iObject,
    const AcDb::OpenMode opMode )
{
    //Recuperer l'entite
    AcDbEntity*  pEntity = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbSpline::cast( pEntity );
}


AcDbSpline* readSplineFromSs(
    const ads_name&    ssName,
    const long        iObject )
{
    //Recuperer l'entite
    return getSplineFromSs( ssName, iObject, AcDb::kForRead );
}


AcDbSpline* writeSplineFromSs(
    const ads_name&        ssName,
    const long            iObject )
{
    //Recuperer l'entite
    return getSplineFromSs( ssName, iObject, AcDb::kForWrite );
}


AcGeNurbCurve3d* getNurbCurve3d( AcDbSpline* spline, Adesk::Boolean isClosed )
{
    //declarer des variables
    AcGePoint3dArray fitPoints;
    int degree;
    double fitTolerance = 0.0;
    Adesk::Boolean rational;
    Adesk::Boolean tangentsExist;
    AcGeVector3d startTangent;
    AcGeVector3d endTangent;
    Adesk::Boolean periodic;
    AcGePoint3dArray controlPoints;
    AcGeDoubleArray knots;
    AcGeDoubleArray weights;
    double controlPtTol;
    double knotTol;
    
    //recuperer les donnees de la spline
    spline->getFitData( fitPoints, degree, fitTolerance,  tangentsExist, startTangent, endTangent );
    AcGeNurbCurve3d* nurbCurve3D = new AcGeNurbCurve3d( fitPoints );
    //retourner le resultat
    return nurbCurve3D;
}


AcGePoint3dArray discretize( AcDbSpline* spline, const double tol, Adesk::Boolean isClosed )
{
    //recuperer les deux points extremites du spline
    AcGePoint3d pts, pte;
    AcGePoint3dArray ptarray;
    double params, parame;
    spline->getStartPoint( pts );
    spline->getEndPoint( pte );
    spline->getParamAtPoint( pts, params );
    spline->getParamAtPoint( pte, parame );
    double length = 0.0;
    
    //recuperer la longueur de la spline du startpoint au endpoint
    if( isClosed == Adesk::kTrue )
    {
        AcGePoint3d ptt;
        double paramt, lengtht;
        int nbft = spline->numFitPoints();
        spline->getFitPointAt( nbft - 2, ptt );
        spline->getParamAtPoint( ptt, paramt );
        length = getNurbCurve3d( spline )->length( params, paramt );
        lengtht = getNurbCurve3d( spline )->length( paramt, parame );
        length = length + lengtht;
    }
    
    else length = getNurbCurve3d( spline )->length( params, parame );
    
    int size = ( int )( length / tol );
    double lengthTemp = tol;
    
    for( int i = 0; i < size; i++ )
    {
        AcGePoint3d ptTemp;
        spline->getPointAtDist( lengthTemp, ptTemp );
        ptarray.append( ptTemp );
        lengthTemp += tol;
    }
    
    return ptarray;
}