/**
  * \file       splineEntity.h
  * \author     Marielle H.R
  * \brief      Fichier contenant des fonctions utiles pour travailler avec les splines
  * \date       16 juillet 2019
 */

#pragma once
#include <dbents.h>
#include "selectEntity.h"
#include "arcEntity.h"
#include <math.h>


/**
  * \brief Fait une selection quelconque sur les splines
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsSpline(
    ads_name&  ssName,
    const AcString&  layerName = "" );

/**
  * \brief Selectionne un seul spline dans le dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsOneSpline(
    const ads_name&    ssName,
    const AcString&    layerName = "" );


/**
  * \brief Selectionne tous les splines du dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsAllSpline(
    const ads_name&  ssName,
    const AcString&  layerName = "" );



/**
  * \brief Renvoie un pointeur sur le spline numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du spline a recuperer, par defaut 0
  * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \param return Pointeur sur un spline
  */
AcDbSpline* getSplineFromSs(
    const ads_name& ssName,
    const long iObject          = 0,
    const AcDb::OpenMode opMode = AcDb::kForRead );


/**
  * \brief Renvoie un pointeur en LECTURE sur le spline numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du spline a recuperer, par defaut 0
  * \param return Pointeur sur un spline
  */
AcDbSpline* readSplineFromSs(
    const ads_name&    ssName,
    const long        iObject = 0 );


/**
  * \brief Renvoie un pointeur en ECRITURE sur le spline numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du cercle a recuperer, par defaut 0
  * \param return Pointeur sur un spline
  */
AcDbSpline* writeSplineFromSs(
    const ads_name&        ssName,
    const long            iObject = 0 );

/**
  * \brief fonction pour recuperer la representation d'une B-spline rationnelle non uniforme dans un espace 3D.
  * \param spline Pointeur sur le spline
  * \return Pointeur sur un AcGeNurbCurve3d
  */
AcGeNurbCurve3d* getNurbCurve3d( AcDbSpline* spline, Adesk::Boolean isClosed = Adesk::kFalse );

/**
  * \brief fonction pour discertiser un spline
  * \param spline Pointeur sur le spline
  * \param tol Fleche de discretisation
  * \return Pointeur sur un AcGePoint3dArray
  */
AcGePoint3dArray discretize( AcDbSpline* spline, const double tol,Adesk::Boolean isClosed = Adesk::kFalse );

