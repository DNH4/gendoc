/**
 * @file boundingBox.h
 * @brief Fonctions permettant de travailler avec les bounding boxes d'une AcDbCurve
 */

#pragma once
#include "fmapHeaders.h"
#include "progressBar.h"
#include "pointEntity.h"
#include "lineEntity.h"
#include "poly2DEntity.h"
#include "poly3DEntity.h"

struct Segment
{
    //Premier point du segment
    AcGePoint3d pt1;
    
    //Second point du segment
    AcGePoint3d pt2;
    
    //Booleen
    bool is3d;
    
    //Bulge de la polyligne
    double bulge;
};


/**
  * \brief Permet de trier un vecteur de boundingBox dans le sens croissant des xMinPoints des boundingsBoxes
  * \param bbVec Vecteur qui va contient les boundingsBoxes � trier
  * \param bbVecXMinPoint Vecteur qui contient les xMinPoint des boundingsBoxes
  * \param segVec Vecteur contient les segments des boundingsBoxes
  * \return ErrorStatus
  */
Acad::ErrorStatus sortBoundingBox( vector<AcDbExtents>& bbVec,
    vector<double>& bbVecXMinPoint,
    vector<Segment>& segVec );


/**
  * \brief Fonction pour recuperer les indexs d'un vecteur de boundingBox associ� � une AcDbCurve en entr�e
  * \param curve La curve en question
  * \param indexVec Vecteur qui va contenir les indexs trouv�s, le min et le max
  * \param segVec Vecteur contenant les segments
  * \return ErrorStatus
  */
Acad::ErrorStatus getIntersectedBoundingBoxes( AcDbCurve* curve,
    vector<int>& indexVec,
    const vector<Segment>& segVec );


/**
  * \brief Fonction qui permet de faire une dichotomie dans un vecteur de BoundingBox
  * \param pt1 Le premier point
  * \param pt2 Le deuxieme point
  * \param indexVec Vecteur qui va contenir les indexs trouv�s, le min et le max
  * \param segVec Vecteur contenant les segments d�ja tri�
  * \return ErrorStatus
  */
Acad::ErrorStatus dichotomBoundingBox( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    vector<int>& indexesVec,
    const vector<Segment>& segVec );


/**
  * \brief Fonction qui permet de preparer les boundings boxs d'un AcDbCurve
  * \param curve La curve en question
  * \param bbVec Vecteur qui va contenir les boundingBoxes
  * \param bbVecXMinPoint Vecteur qui va contenir les xMins des boundingsBoxes
  * \param segVec Vecteur qui va contenir les segments
  * \return ErrorStatus
  */
template < typename segType >
Acad::ErrorStatus computeCurveBoundingBox( AcDbCurve* curve,
    vector<AcDbExtents>& bbVec,
    vector<double>& bbVecXMinPoint,
    vector<segType>& segVec )
{
    //Resultat
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Tester si c'est une polyligne 3d
    if( curve->isKindOf( AcDb3dPolyline::desc() ) )
    {
        //Convertir en polyligne3d
        AcDb3dPolyline* poly3D = AcDb3dPolyline::cast( curve );
        
        if( poly3D )
        {
            //Declarer les vertex et creer un iterateur sur les sommets de la polyligne 3D
            AcDb3dPolylineVertex* vertex = NULL;
            AcDbObjectIterator* iterPoly3D = poly3D->vertexIterator();
            
            //Initialiser l'iterateur sur la polyligne
            iterPoly3D->start();
            
            //Ouvrir le premier sommet de la polyligne
            er = poly3D->openVertex( vertex,
                    iterPoly3D->objectId(),
                    AcDb::kForRead );
                    
            if( Acad::eOk != er )
                return er;
                
            //Ouvrir le sommet de la polyligne
            AcGePoint3d pt1 = vertex->position();
            
            //Fermer le vertex
            vertex->close();
            
            //Passer sur le vertex suivant
            iterPoly3D->step();
            
            //Boucle sur les polylignes
            for( ; !iterPoly3D->done(); iterPoly3D->step() )
            {
                //Ouvrir le sommet de la polyligne
                er = poly3D->openVertex( vertex,
                        iterPoly3D->objectId(),
                        AcDb::kForRead );
                        
                if( Acad::eOk != er )
                    return er;
                    
                //Recuperer le sommet
                AcGePoint3d pt2 = vertex->position();
                
                //Fermer le vertex
                vertex->close();
                
                //Creer le segment
                Segment seg;
                seg.pt1 = pt1;
                seg.pt2 = pt2;
                seg.is3d = true;
                
                //Ajouter le segment dans le vecteur
                segVec.push_back( seg );
                
                //Creer une polyligne � partir des deux points
                AcGePoint3dArray arPoly;
                arPoly.push_back( pt1 );
                arPoly.push_back( pt2 );
                AcDb3dPolyline* polyTemp = new AcDb3dPolyline( AcDb::k3dSimplePoly,
                    arPoly );
                    
                //Recuperer la bounding box de la polyligne
                AcDbExtents polyEx;
                er = polyTemp->getGeomExtents( polyEx );
                
                //Verifier qu'on a bien recuperer le bounding box
                if( er != Acad::eOk )
                {
                    //Liberer la m�moire occup� par la polyligne
                    delete polyTemp;
                    
                    return er;
                }
                
                //Liberer la m�moire occup� par la polyligne
                delete polyTemp;
                
                //Ajouter la bounding box de la polyligne dans le vecteur
                bbVec.push_back( polyEx );
                
                //Ajouter le xMin du boundingBox
                bbVecXMinPoint.push_back( polyEx.minPoint().x );
                
                //Changer les points et les objectId
                pt1 = pt2;
            }
            
            //Supprimer l'iterateur sur les polylignes
            delete iterPoly3D;
            
            //Verifier avant de sortir
            if( bbVec.size() == 0 || segVec.size() == 0 )
                return Acad::eNotApplicable;
                
            //Retourner eOk
            return Acad::eOk;
        }
        
        //Fermer la polyligne
        poly3D->close();
    }
    
    //Tester si c'est une polyligne 2d
    else if( curve->isKindOf( AcDbPolyline::desc() ) )
    {
        //Convertir en polyligne 2d
        AcDbPolyline* poly2D = AcDbPolyline::cast( curve );
        
        if( poly2D )
        {
            //Recuperer le nombre de vertex de la polyligne
            int numVert = poly2D->numVerts();
            
            //Tester
            if( numVert < 2 )
                return er;
                
            //Le premier point de la polyligne
            AcGePoint3d ptStart;
            poly2D->getPointAt( 0, ptStart );
            
            //Boucle sur les sommets de la polyligne
            for( int i = 1; i < numVert; i++ )
            {
                //Recuperer le point
                AcGePoint3d pt;
                poly2D->getPointAt( i, pt );
                
                //Creer le segment 2d
                Segment seg;
                
                //Recuperer les valeurs du segment 2d
                poly2D->getBulgeAt( i - 1, seg.bulge );
                seg.pt1 = ptStart;
                seg.pt2 = pt;
                seg.is3d = false;
                
                //Ajouter le segment
                segVec.push_back( seg );
                
                //Creer une polyligne temporaire
                AcDbPolyline* polyTemp = new AcDbPolyline();
                polyTemp->addVertexAt( 0,
                    getPoint2d( ptStart ),
                    seg.bulge );
                polyTemp->addVertexAt( 1,
                    getPoint2d( pt ) );
                    
                //Recuperer la bounding box de la polyligne
                AcDbExtents polyEx;
                er = polyTemp->getGeomExtents( polyEx );
                
                //Liberer la m�moire occup� par la polyligne
                delete polyTemp;
                
                //Verifier qu'on a bien recuperer le boundingBox
                if( er != Acad::eOk )
                {
                    //Liberer la m�moire occup� par la polyligne
                    delete polyTemp;
                    
                    return er;
                }
                
                //Ajouter le boundingBox de la polyligne dans le vecteur
                bbVec.push_back( polyEx );
                
                //Ajouter le xMin du bounding Box
                bbVecXMinPoint.push_back( polyEx.minPoint().x );
                
                //Changer les points
                ptStart = pt;
            }
            
            //Verifier avant de sortir
            if( bbVec.size() == 0 || segVec.size() == 0 )
                return Acad::eNotApplicable;
                
            //Retourner eOk
            return Acad::eOk;
        }
        
        //Fermer la polyligne
        poly2D->close();
    }
    
    //Sinon retourner le resultat par d�faut
    return er;
}


/**
  * \brief Fonction qui permet de recuperer les intersections d'un curve avec des segments
  * \param curve La curve en question
  * \param bbVec Vecteur qui va contenir les boundingBoxes
  * \param segVec Vecteur qui va contenir les segments
  * \param areIntersected Booleen qui va dire si on a des intersections ou non
  * \param segOfIntersection Segment de l'intersection
  * \param intersectionPoints Les points d'intersections entre la curve et le
  * \return ErrorStatus
  */
template < typename segType >
Acad::ErrorStatus isIntersectedWithCurve( AcDbCurve* curve,
    vector<AcDbExtents>& bbVec,
    vector<segType>& segVec,
    bool& areIntersected,
    segType& segOfIntersection,
    AcGePoint3dArray& intersectionPoints )
{
    //Resulat
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Taille du vecteur de x
    int taille = bbVec.size();
    
    //Verifier avant de calculer
    if( segVec.size() != taille || taille == 0 )
        return er;
        
    //Initialiser la valeur du booleen
    areIntersected = false;
    
    //Vecteur qui va contenir les indexs des segments
    vector<int> indexVec;
    
    //Recuperer le segment en question
    er = getIntersectedBoundingBoxes( curve,
            indexVec,
            segVec );
            
    //Verifier
    if( er != Acad::eOk )
        return er;
        
    //Recuperer les indexs min et max des boundings boxs
    int iMin = indexVec[0];
    int iMax = indexVec[indexVec.size() - 1];
    
    //Boucle sur les vecteurs
    for( int i = iMin; i <= iMax; i++ )
    {
        //Recuperer le premier bounding box
        AcDbExtents bb = bbVec[i];
        
        //Recuperer le i-eme segment
        segType seg = segVec[i];
        
        //Tester si c'est une polyligne 2d
        if( seg.bulge == 0 )
        {
            //Creer la polyligne 2d avec l'arc
            AcDbPolyline* polyTemp = new AcDbPolyline();
            polyTemp->addVertexAt( 0, getPoint2d( seg.pt1 ) );
            polyTemp->addVertexAt( 1, getPoint2d( seg.pt2 ) );
            polyTemp->setBulgeAt( 0, seg.bulge );
            
            //Recuperer les intersections entres la ligne et le segment
            er = computeIntersection( polyTemp,
                    curve,
                    intersectionPoints );
                    
            //Liberer la memoire sur la polyligne
            delete polyTemp;
            
            //Verifier si on a une intersection ou non
            if( Acad::eOk != er )
                continue;
        }
        
        else
        {
            //Creer la polyligne 2d avec l'arc
            AcDbLine* lineTemp = new AcDbLine( seg.pt1,
                seg.pt2 );
                
            //Recuperer les intersections entres la ligne et le segment
            er = computeIntersection( lineTemp,
                    curve,
                    intersectionPoints );
                    
            //Liberer la memoire sur la polyligne
            delete lineTemp;
            
            //Verifier si on a une intersection ou non
            if( Acad::eOk != er )
                continue;
        }
        
        //Mettre le booleen � true
        areIntersected = true;
        
        //Recuperer le segment
        segOfIntersection = seg;
        
        //Retourner Ok
        return Acad::eOk;
    }
    
    //Sortir
    return er;
}

/**
  * \brief V�rifie si une bounding box est contient ou est contenue dans l'une des bounding boxes d'un vecteur
  * \param boundingBoxes Vecteur contenant boundingBoxes
  * \param isContained 1 si la bounding box est contenue dans une des bounding boxes, -1 si elle en contient une et 0 sinon
  * \param index Renvoie l'indice de la bounding box qui contient/est contenue
  * \param bB Bounding box �tudi�e
  * \return True si une bounding box a �t� trouv�e, false sinon
  */
bool isContainedBy( vector<AcDbExtents>& boundingBoxes,
    int& isContained,
    int& index,
    const AcDbExtents& bB );

/**
  * \brief V�rifie s'il y a une intersection entre deux bounding box
  * \param box1 le premier bounding box
  * \param box2 le second bounding box
  * \return True si une intersection a �t� trouv�e, false sinon
  */
bool are2BoxIntersected( const AcDbExtents& box1, const AcDbExtents& box2 );

/**
  * \brief
  * \param
  * \param
  * \return
  */
bool isBoundingBoxEqual( const AcDbExtents& box1,
    const AcDbExtents& box2,
    const double& tolXYZ = 0.01 );

/**
  * \brief
  * \param
  * \param
  * \return
  */
Acad::ErrorStatus drawBoundingBox( AcDbExtents box,
    AcDbDatabase* pDb );


Acad::ErrorStatus drawBoundingBox3d( const AcDbExtents& box,
    const AcCmColor& color = AcCmColor() );



/**
  * \brief
  * \param
  * \param
  * \return
  */
bool isInBoundingBox( const AcDbExtents& box,
    const AcGePoint3d& pt,
    const bool& is3d = false );