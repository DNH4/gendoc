#include "list.h"

// Compares two intervals according to staring times.
bool comparePair( const pair<double, AcGePoint3d>& p1,
    const pair<double, AcGePoint3d>& p2 )
{
    return ( p1.first < p2.first );
}


bool isTheSame( const double& num1, const double& num2, const double& precision )
{
    if( abs( num1 - num2 ) < precision )
        return true;
    else
        return false;
}


bool compareDoubleAsc( const double& pt1, const double pt2 )
{
    return ( pt1 < pt2 );
}



bool findClosestPointInArray( const AcGePoint3d& ptRef,
    AcGePoint3dArray& arPt,
    AcGePoint3d& ptRes,
    double tol )
{
    bool res = false;
    long size = arPt.size();
    
    if( size == 0 )
        return false;
        
    vector<pair<double, AcGePoint3d>> vecDist;
    
    for( long counter = 0; counter < size ; counter++ )
    {
        AcGePoint3d tempPt = arPt[counter];
        
        //1. On calcul la distance
        double dist = tempPt.distanceTo( ptRef );
        
        //2. On filtre selon la tol�rance
        if( dist <= tol )
        {
            pair<double, AcGePoint3d> oneData;
            oneData.first = dist;
            oneData.second = tempPt;
            
            vecDist.push_back( oneData );
        }
        
        continue;
    }
    
    if( vecDist.size() > 0 )
    {
        //3. On trie en mode croissant
        sort( vecDist.begin(), vecDist.end(), comparePair );
        vector<pair<double, AcGePoint3d>>::iterator it = vecDist.begin();
        
        //4. On change la valeur de retour
        res = true;
        ptRes = ( *it ).second;
    }
    
    return res;
}


int dichotom( const std::vector< double >& xPos,
    const double& x,
    const double& epsilon )
{
    //Initialisation des variables
    int debut = 0;
    int fin = xPos.size();
    bool trouve = false;
    double mil = 0;
    
    //Boucle de decoupage
    while( !trouve && debut <= fin )
    {
        //Recuperer l'index du milieu du vecteur
        mil = ( debut + fin ) / 2;
        
        //Tester si on a le meme point avec une certaine tolerance
        if( abs( xPos[ mil ] - x ) < epsilon )
            trouve = true;
            
        //Sinon on passe sur la deuxieme moiti du vecteur
        else if( x > xPos[ mil ] )
            debut = mil + 1;
            
        else
            fin = mil - 1;
    }
    
    //Test de sortie de la fonction
    if( abs( xPos[ mil ] - x ) > epsilon )
    {
        trouve = false;
        return -1;
    }
    
    //Retourner l'index du milieu du vecteur
    return mil;
}


std::vector<int> dichotomToVec( const std::vector<double>& xPos,
    const double& x,
    const double& epsilon )
{
    //Declaration du resultat
    std::vector<int> vectRes;
    
    //Recuperer l'index du point dans le vecteur
    int index = dichotom( xPos, x, epsilon );
    
    //Recuperer la taille du vecteur
    long taille = xPos.size();
    
    //Ajouter l'index dans le vecteur
    vectRes.push_back( index );
    
    //Boucle sur l'index trouv vers le sens +
    for( int i = index + 1; i < taille; i++ )
    {
        //Tester si le point suivant est le mme
        if( isTheSame( x, xPos[ i ], epsilon ) )
        {
            //Ajouter l'index dans le vecteur
            vectRes.push_back( i );
        }
        
        else
            break;
    }
    
    //Boucle sur l'index trouv dans le sens -
    if( index > 0 )
    {
        for( int i = index - 1 ; i >= 0 ; i-- )
        {
            //Tester si le point d'avant est le mme
            if( isTheSame( x, xPos[i], epsilon ) )
            {
                //Ajouter l'index sur la premiere element du vecteur
                vectRes.insert( vectRes.begin(), i );
            }
            
            else
                break;
        }
    }
    
    //Retourner le resultat
    return vectRes;
}


std::vector<int> dichotomToVecX( const std::vector<double>& xPos,
    const double& xmin,
    const double& xmax )
{
    //Declaration du resultat
    std::vector<int> vectRes;
    
    //Recuperer la taille du vecteur
    long taille = xPos.size();
    int index = 0;
    
    if( taille % 2 == 0 )
        index = taille / 2;
    else index = ( taille + 1 ) / 2;
    
    
    //Boucle sur l'index trouv� vers le sens +
    for( int i = 0; i < taille; i++ )
    {
        //Tester si le point suivant est le m�me
        if( xmin <= xPos[i] && xPos[i] <= xmax )
        {
            //Ajouter l'index dans le vecteur
            vectRes.push_back( i );
        }
        
        
    }
    
    //Retourner le resultat
    return vectRes;
}


Acad::ErrorStatus sortList(
    AcGePoint3dArray& vectPoint,
    vector< double >& xPos )
{
    //Longueur de la liste de points
    int length = vectPoint.length();
    
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Tri" ), length );
    
    for( int i = 0; i < length - 1; i++ )
    {
        //Recuperer l'index du min
        int min = i;
        
        //Boucle de test
        for( int j = i + 1; j < length; j++ )
        {
            if( xPos[ j ] < xPos[ min ] )
                min = j;
        }
        
        //Swaper le vecteur et les AcArrays
        if( min != i )
        {
            std::swap( xPos[ i ], xPos[ min ] );
            vectPoint.swap( i, min );
        }
        
        //Progresser
        prog.moveUp( i );
    }
    
    return Acad::eOk;
}


Acad::ErrorStatus sortList(
    vector<AcGePoint3d>& vectPoint,
    vector< double >& xPos )
{
    //Longueur de la liste de points
    int length = vectPoint.size();
    
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Tri" ), length );
    
    for( int i = 0; i < length - 1; i++ )
    {
        //Recuperer l'index du min
        int min = i;
        
        //Boucle de test
        for( int j = i + 1; j < length; j++ )
        {
            if( xPos[j] < xPos[min] )
                min = j;
        }
        
        //Swaper le vecteur et les AcArrays
        if( min != i )
        {
            std::swap( xPos[i], xPos[min] );
            std::swap( vectPoint[i], vectPoint[min] );
        }
        
        //Progresser
        prog.moveUp( i );
    }
    
    return Acad::eOk;
}


Acad::ErrorStatus sortList(
    AcGePoint2dArray& vectPoint,
    vector< double >& xPos )
{
    //Longueur de la liste de points
    int length = vectPoint.length();
    
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Tri" ), length );
    
    for( int i = 0; i < length - 1; i++ )
    {
        //Recuperer l'index du min
        int min = i;
        
        //Boucle de test
        for( int j = i + 1; j < length; j++ )
        {
            if( xPos[ j ] < xPos[ min ] )
                min = j;
        }
        
        //Swaper le vecteur et les AcArrays
        if( min != i )
        {
            std::swap( xPos[ i ], xPos[ min ] );
            vectPoint.swap( i, min );
        }
        
        //Progresser
        prog.moveUp( i );
    }
    
    return Acad::eOk;
}


Acad::ErrorStatus eraseDuplicate( AcGePoint3dArray& pointArray,
    const double& tol )
{
    //Resultat par d�faut
    Acad::ErrorStatus er = Acad::eOk;
    
    //Recuperer la taille
    int taille = pointArray.size();
    
    //Verifier
    if( taille == 0 )
        return Acad::eNotApplicable;
        
    //Boucle sur les elements du tableau
    for( int i = 1 ; i < pointArray.size(); i++ )
    {
        if( isEqual3d( pointArray[i - 1], pointArray[i], tol ) )
        {
            //Supprimer l'element
            pointArray.erase( pointArray.begin() + i );
            
            i--;
        }
    }
    
    //Retourner eOk
    return er;
}


Acad::ErrorStatus eraseDuplicate( AcGePoint2dArray& pointArray,
    const double& tol )
{
    //Resultat par d�faut
    Acad::ErrorStatus er = Acad::eOk;
    
    //Recuperer la taille
    int taille = pointArray.size();
    
    //Verifier
    if( taille == 0 )
        return Acad::eNotApplicable;
        
    //Boucle sur les elements du tableau
    for( int i = 1 ; i < pointArray.size(); i++ )
    {
        if( isEqual2d( pointArray[i - 1], pointArray[i], tol ) )
        {
            //Supprimer l'element
            pointArray.erase( pointArray.begin() + i );
            
            i--;
        }
    }
    
    //Retourner eOk
    return er;
}


Acad::ErrorStatus sortList( vector<double>& xPos )
{
    // Longueur de la liste de points
    int length = xPos.size();
    
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Tri" ), length );
    
    for( int i = 0; i < length - 1; i++ )
    {
        //Recuperer l'index du min
        int min = i;
        
        //Boucle de test
        for( int j = i + 1; j < length; j++ )
        {
            if( xPos[ j ] < xPos[ min ] )
                min = j;
        }
        
        //Swaper le vecteur et les AcArrays
        if( min != i )
            std::swap( xPos[ i ], xPos[ min ] );
            
        //Progresser
        prog.moveUp( i );
    }
    
    return Acad::eOk;
}


bool isInList( const AcGePoint3d& point,
    std::vector<double>& xPos,
    AcGePoint3dArray& pos,
    int& index,
    const double& tolXYZ,
    const double& tolDichotom,
    const bool& del )
{
    if( !xPos.size() || !pos.length() )
        return false;
        
    ///Le vectPoint3d doit etre dej� tri� en X ( ou Y ) pour que le calcul soit rapide
    //Initialisation du resultat
    bool result = false;
    
    //Recuperer la taille du vecteur
    long taille = xPos.size();
    
    if( taille == 0 )
        return false;
        
    //Vecteur contenant les indexs des m�me x
    std::vector<int> indexs;
    
    //Recuperer les indexs des points equivalent en x
    indexs = dichotomToVec( xPos, point.x, tolDichotom );
    
    //Taille du vecteur d'index
    int size = indexs.size();
    
    //Boucle sur les indexs recuper�s
    if( indexs.size() == 1 && indexs[ 0 ] == -1 )
        return false;
        
    //Boucle sur les indexs recuper�s
    for( int j = 0; j < size; j++ )
    {
        int a = indexs[j];
        AcGePoint3d ptTemp = pos[ a ];
        
        if( isEqual3d( pos[indexs[j]], point, tolXYZ ) )
        {
            // Supprimer l'element si c'est demand�
            if( del )
            {
                pos.erase( pos.begin() + indexs[ j ] );
                xPos.erase( xPos.begin() + indexs[ j ] );
            }
            
            //Retourner true
            index = indexs[ j ];
            return true;
        }
    }
    
    return false;
}



bool isInList( const AcGePoint3d& point,
    std::vector<double>& xPos,
    vector<AcGePoint3d>& pos,
    int& index,
    const double& tolXYZ,
    const double& tolDichotom,
    const bool& del )
{
    if( !xPos.size() || !pos.size() )
        return false;
        
    ///Le vectPoint3d doit etre dej� tri� en X ( ou Y ) pour que le calcul soit rapide
    //Initialisation du resultat
    bool result = false;
    
    //Recuperer la taille du vecteur
    long taille = xPos.size();
    
    if( taille == 0 )
        return false;
        
    //Vecteur contenant les indexs des m�me x
    std::vector<int> indexs;
    
    //Recuperer les indexs des points equivalent en x
    indexs = dichotomToVec( xPos, point.x, tolDichotom );
    
    //Taille du vecteur d'index
    int size = indexs.size();
    
    //Boucle sur les indexs recuper�s
    if( indexs.size() == 1 && indexs[0] == -1 )
        return false;
        
    //Boucle sur les indexs recuper�s
    for( int j = 0; j < size; j++ )
    {
        int a = indexs[j];
        AcGePoint3d ptTemp = pos[a];
        
        if( isEqual3d( pos[indexs[j]], point, tolXYZ ) )
        {
            // Supprimer l'element si c'est demand�
            if( del )
            {
                pos.erase( pos.begin() + indexs[j] );
                xPos.erase( xPos.begin() + indexs[j] );
            }
            
            //Retourner true
            index = indexs[j];
            return true;
        }
    }
    
    return false;
}



bool isInList( const AcGePoint2d& point,
    std::vector<double>& xPos,
    AcGePoint3dArray& pos,
    std::vector<double>& vectZ,
    std::vector<int>& indexZ,
    const double& tolXY,
    const double& tolDichotom,
    const bool& del )
{
    ///Le vectPoint3d doit etre tri� en X ( ou Y ) pour que le calcul soit rapide
    //Initialisation du resultat
    bool res = false;
    
    //Vecteur contenant les indexs des m�me x<<
    std::vector<int> indexs;
    
    if( !xPos.size() || !pos.length() )
        return false;
        
    //Recuperer les indexs des points equivalent en x<<
    indexs = dichotomToVec( xPos, point.x, tolDichotom );
    
    indexZ.resize( 0 );
    vectZ.resize( 0 );
    
    int size = indexs.size();
    
    if( indexs.size() == 1 && indexs[ 0 ] == -1 )
        return res;
        
    //Boucle sur les indexs recuper�s<<
    for( int j = 0; j < size; j++ )
    {
        if( isEqual2d( getPoint2d( pos[ indexs[ j ] ] ), point, tolXY ) )
        {
            // On ajoute le Z du point dans le vecteur de z
            vectZ.push_back( pos[ indexs[ j ] ].z );
            indexZ.push_back( indexs[ j ] );
            
            // Supprimer l'element si c'est demand�
            if( del )
            {
                pos.erase( pos.begin() + indexs[ j ] );
                xPos.erase( xPos.begin() + indexs[ j ] );
            }
            
            res = true;
        }
    }
    
    return res;
}


bool isInList( const AcGePoint3d& point3d,
    std::vector<double>& vectDouble,
    AcGePoint3dArray& acPoint3d,
    AcGePoint3dArray& neighbourPoint3d,
    const double& tolerance )
{
    ///Le vectPoint3d doit etre tri� en X ( ou Y ) pour que le calcul soit rapide
    //Initialisation du resultat
    bool result = false;
    
    if( !vectDouble.size() )
        return false;
        
    //Vider le vecteur
    neighbourPoint3d.clear();
    neighbourPoint3d.resize( 0 );
    
    //Recuperer la taille du vecteur
    long taille = vectDouble.size();
    
    //Vecteur contenant les indexs des m�me x
    std::vector<int> indexs;
    
    //Recuperer les indexs des points equivalent en x
    indexs = dichotomToVec( vectDouble, point3d.x, tolerance );
    
    //Taille du vecteur d'index
    int size = indexs.size();
    
    //Boucle sur les indexs recuper�s
    if( indexs.size() == 1 && indexs[ 0 ] == -1 )
        return false;
        
    //Boucle sur les indexs recuper�s
    for( int j = 0; j < size; j++ )
    {
        if( isEqual3d( acPoint3d[ indexs[ j ] ], point3d, tolerance ) )
        {
            //Ajouter les points voisins dans le AcArray
            neighbourPoint3d.append( acPoint3d[indexs[j]] );
        }
    }
    
    if( neighbourPoint3d.size() == 0 )
        return false;
    else
        return true;
}


bool isInUniqueList2d( const AcGePoint2d& pt2d,
    std::vector<double>& vectDouble,
    AcGePoint3dArray& acPoint3d,
    int& index,
    const double& tolerance )
{
    //Verifier l'entr�e
    if( !vectDouble.size() )
        return false;
        
    //Recuperer les indexs des points equivalent en x
    vector<int> indexs = dichotomToVec( vectDouble, pt2d.x, tolerance );
    
    //Recuperer la taille du vecteur
    int size = indexs.size();
    
    //Verifier
    if( size == 1 && indexs[ 0 ] == -1 )
        return false;
        
    //Boucle sur les indexs recuper�s
    for( int j = 0; j < size; j++ )
    {
        //Recuperer le points
        AcGePoint2d pt = getPoint2d( acPoint3d[indexs[j]] );
        
        //Verifier si on a le meme point
        if( isEqual2d( pt, pt2d, tolerance ) )
        {
            //Recuperer l'index
            index = indexs[j];
            
            //Retourner true
            return true;
        }
    }
    
    //Retourne false
    return false;
}



Acad::ErrorStatus addInList( const AcGePoint3d& point,
    AcGePoint3dArray& acPoint3d,
    vector<double>& vectDouble,
    const double& tolX )
{
    Acad::ErrorStatus es = eNotApplicable;
    
    long size = acPoint3d.size();
    
    if( size  == 0 )
    {
        acPoint3d.append( point );
        vectDouble.push_back( point.x );
        return es;
    }
    
    
    long idx = -1;
    bool found = false;
    
    
    //V�rification par rapport au premier �l�ment
    AcGePoint3d pt = acPoint3d[0];
    double diffX = point.x - pt.x;
    
    if( diffX <= 0 )
    {
        //On sauvegarde l'index
        idx = 0;
        //Insertion du point
        acPoint3d.insertAt( idx, point );
        
        //Insertion
        vector<double>::iterator it = vectDouble.begin();
        vectDouble.insert( it, point.x );
        es = Acad::eOk;
        found = true;
        
        return es;
        
    }
    
    double diffBu = 1;
    
    for( long counter = 0; counter < size; counter++ )
    
    {
        //1. On compare les points pour trouver
        
        //1.b On prend le point et on compare selon X ou Y
        AcGePoint3d pt = acPoint3d[counter];
        
        //Calcul de la diff�rence en X
        double diffX = point.x - pt.x;
        
        if( ( diffX > 0 ) && ( diffX <= diffBu ) )
        {
            //On sauvegarde l'index et la diff�rence
            idx = counter;
            diffBu = diffX;
            found = true;
            continue;
        }
        
        else
        {
            //Si on a trouv� on break
            if( found )
                break;
                
            continue;
            
        }
        
    }
    
    if( idx != -1 ) // On a trouv� on insert
    {
        //On insert apres le point du coup on incr�mente
        //Insertion du point
        acPoint3d.insertAt( idx + 1, point );
        vector<double>::iterator it = vectDouble.begin();
        vectDouble.insert( it + idx, point.x );
        es = Acad::eOk;
    }
    
    else
    {
        //On insert � la fin
        acPoint3d.append( point );
        vectDouble.push_back( point.x );
        es = Acad::eOk;
    }
    
    
    return es;
}


Acad::ErrorStatus addDoubleInVector( const double& dc,
    std::vector< double >& vectDouble,
    const double& tol )
{
    vectDouble.push_back( dc );
    
    std::sort( vectDouble.begin(), vectDouble.end(), compareDoubleAsc );
    
    return Acad::eOk;
}


Acad::ErrorStatus sortListUsingDistance( const AcGePoint3d& ptRef,
    AcGePoint3dArray& arPoint )
{
    //Resultat par d�faut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Recuperer la taille du vecteur
    int length = arPoint.size();
    
    //Verifier
    if( length == 0 )
        return er;
        
    //Vecteur de distance
    vector<double> vecDist;
    
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Tri" ), length * 2 );
    
    //Boucle pour recuperer les distances
    for( int i = 0; i < length; i++ )
    {
        //Ajouter la distance
        vecDist.push_back( getDistance2d( arPoint[i], ptRef ) );
        
        //Progresser
        prog.moveUp( i );
    }
    
    //Boucle de tri
    for( int i = 0; i < length - 1; i++ )
    {
        //Recuperer l'index du min
        int min = i;
        
        //Boucle de test
        for( int j = i + 1; j < length; j++ )
        {
            if( vecDist[ j ] < vecDist[ min ] )
                min = j;
        }
        
        //Swaper le vecteur et les AcArrays
        if( min != i )
        {
            std::swap( vecDist[ i ], vecDist[ min ] );
            arPoint.swap( i, min );
        }
        
        //Progresser
        prog.moveUp( i );
    }
    
    return Acad::eOk;
}


Acad::ErrorStatus sortListUsingCurveDistance( AcDb3dPolyline* poly3D,
    AcGePoint3dArray& arPoint )
{
    //Resultat par d�faut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Recuperer la taille du vecteur
    int length = arPoint.size();
    
    //Verifier
    if( length == 0 )
        return er;
        
    //Vecteur de distance
    vector<double> vecDist;
    
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Tri" ), length * 2 );
    
    //Boucle pour recuperer les distances
    for( int i = 0; i < length; i++ )
    {
        //Distance
        double d;
        
        //Recuperer la distance curviligne
        er = poly3D->getDistAtPoint( arPoint[i], d );
        
        //Verifier
        if( er != Acad::eOk )
            return er;
            
        //Ajouter la distance curviligne dans le vecteur
        vecDist.push_back( d );
        
        //Progresser
        prog.moveUp( i );
    }
    
    //Boucle de tri
    for( int i = 0; i < length - 1; i++ )
    {
        //Recuperer l'index du min
        int min = i;
        
        //Boucle de test
        for( int j = i + 1; j < length; j++ )
        {
            if( vecDist[ j ] < vecDist[ min ] )
                min = j;
        }
        
        //Swaper le vecteur et les AcArrays
        if( min != i )
        {
            std::swap( vecDist[ i ], vecDist[ min ] );
            arPoint.swap( i, min );
        }
        
        //Progresser
        prog.moveUp( i );
    }
    
    return Acad::eOk;
}


Acad::ErrorStatus sortListUsingCurveDistance( AcDbPolyline* poly2D,
    vector<Sommet>& vertexes )
{
    //Declaration du r�sultat
    Acad::ErrorStatus es = Acad::eNotApplicable;
    
    //Recuperer la taille du vecteur
    int nbSommet = vertexes.size();
    
    if( nbSommet < 2 )
        return es;
        
    //Initialisation de la barre de progression
    ProgressBar prog = ProgressBar( _T( "Tri" ), nbSommet );
    
    //Boucler sur le sommet
    for( int i = 0; i < nbSommet - 1; i++ )
    {
        //Recuperer l'index du min
        int min = i;
        
        //Boucle de test
        for( int j = i + 1; j < nbSommet; j++ )
        {
            if( vertexes[j].distance < vertexes[min].distance )
                min = j;
        }
        
        //Swapper le vecteur
        if( min != i )
            std::swap( vertexes[i], vertexes[min] );
            
        //Progresser
        prog.moveUp( i );
    }
    
    //Renvoyer le r�sultat et sortir
    return Acad::eOk;
}



Acad::ErrorStatus addInList( const AcGePoint3d& point,
    vector<AcGePoint3d>& acPoint3d,
    vector<double>& vectDouble,
    const double& tolX )
{
    Acad::ErrorStatus es = eNotApplicable;
    
    long size = acPoint3d.size();
    
    if( size  == 0 )
    {
        acPoint3d.push_back( point );
        vectDouble.push_back( point.x );
        return es;
    }
    
    long idx = -1;
    bool found = false;
    
    //V�rification par rapport au premier �l�ment
    AcGePoint3d pt = acPoint3d[0];
    double diffX = point.x - pt.x;
    
    if( diffX <= 0 )
    {
        //On sauvegarde l'index
        idx = 0;
        //Insertion du point
        acPoint3d.insert( acPoint3d.begin() + idx, point );
        
        //Insertion
        vector<double>::iterator it = vectDouble.begin();
        vectDouble.insert( it, point.x );
        es = Acad::eOk;
        found = true;
        
        return es;
        
    }
    
    double diffBu = 1;
    
    for( long counter = 0; counter < size; counter++ )
    
    {
        //1. On compare les points pour trouver
        
        //1.b On prend le point et on compare selon X ou Y
        AcGePoint3d pt = acPoint3d[counter];
        
        //Calcul de la diff�rence en X
        double diffX = point.x - pt.x;
        
        if( ( diffX > 0 ) && ( diffX <= diffBu ) )
        {
            //On sauvegarde l'index et la diff�rence
            idx = counter;
            diffBu = diffX;
            found = true;
            continue;
        }
        
        else
        {
            //Si on a trouv� on break
            if( found )
                break;
                
            continue;
            
        }
        
    }
    
    if( idx != -1 ) // On a trouv� on insert
    {
        //On insert apres le point du coup on incr�mente
        //Insertion du point
        acPoint3d.insert( acPoint3d.begin() + ( idx + 1 ), point );
        vector<double>::iterator it = vectDouble.begin();
        vectDouble.insert( it + idx, point.x );
        es = Acad::eOk;
    }
    
    else
    {
        //On insert � la fin
        acPoint3d.push_back( point );
        vectDouble.push_back( point.x );
        es = Acad::eOk;
    }
    
    
    return es;
}