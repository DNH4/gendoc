/**
 * @file rasterImageEntity.h
 * @author Romain LEMETTAIS
 * @brief Fonctions permettant de selectionner, creer et traiter les images raster
 */

#pragma once

#include <dataBase.h>
#include <dbobjptr.h>
#include "selectEntity.h"
#include "file.h"




/**
    * \brief Demande à l'utilisaeteur de selectionner des images
    * \param ssName     Pointeur sur la selection
    * \param sLayerName  Calque a selectionner
    * \return Nombre d'elements selectionnes
    */
long getSsRasterImage(
    ads_name&       ssName,
    const AcString& sLayerName = "" );


/**
    * \brief Demande à l'utilisaeteur de selectionner une seule image
    * \param ssName     Pointeur sur la selection
    * \param sLayerName  Calque a selectionner
    * \return Nombre d'elements selectionnes
    */
long getSsOneRasterImage(
    ads_name&       ssName,
    const AcString& sLayerName = "" );



/**
    * \brief Selectionne tous les images du dessin
    * \param ssName     Pointeur sur la selection
    * \param sLayerName  Calque a selectionner
    * \return Nombre d'elements selectionnes
    */
long getSsAllRasterImage(
    ads_name&       ssName,
    const AcString& sLayerName = "" );


/**
    * \brief Renvoie un pointeur sur l'image numero N dans la selection
    * \param ssName     Pointeur sur la selection
    * \param iObject    Index dans la selection de l'image a recuperer, par defaut 0
    * \param opMode     Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
    * \return Pointeur sur une image ouvert en lecture ou ecriture
    */
AcDbRasterImage* getRasterImageFromSs(
    const ads_name&          ssName,
    const long&              iObject = 0,
    const AcDb::OpenMode&    opMode  = AcDb::kForRead );


/**
    * \brief Renvoie un pointeur en LECTURE sur l'image numero N dans la selection
    * \param ssName     Pointeur sur la selection
    * \param iObject    Index dans la selection de l'image a recuperer, par defaut 0
    * \return Pointeur sur une image ouvert en lecture
    */
AcDbRasterImage* readRasterImageFromSs(
    const ads_name&  ssName,
    const long&      iObject = 0 );


/**
    * \brief Renvoie un pointeur en ECRITURE sur l'image numero N dans la selection
    * \param ssName     Pointeur sur la selection
    * \param iObject    Index dans la selection de l'image a recuperer, par defaut 0
    * \return Pointeur sur une image ouvert en ecriture
    */
AcDbRasterImage* writeRasterImageFromSs(
    const ads_name&  ssName,
    const long&      iObject = 0 );


/**
    * \brief Renvoie un pointeur sur le dictionnaire des image
    * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
    * \return Pointeur sur le dictionnaire
    */
AcDbDictionary* getImageDictionary(
    const AcDb::OpenMode&    opMode  = AcDb::kForRead );


/**
    * \brief Renvoie la definition d'une image à partir du nom
    * \param sRasterImageName   Nom de l'image
    * \param pImageDict         Le dictionnaire des images
    * \param opMode             Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
    * \return Pointeur sur la definition de l'image
    */
AcDbRasterImageDef* getRasterImageDefinitionInDictionary(
    const AcString&             sRasterImageName,
    const AcDbDictionary* const pImageDict,
    const AcDb::OpenMode&       opMode  = AcDb::kForRead );


/**
    * \brief Renvoie la definition d'une image à partir du nom
    * \param idRasterImageDef   Identfiant de la definition d'image
    * \param opMode             Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
    * \return Pointeur sur la definition de l'image
    */
AcDbRasterImageDef* getRasterImageDefinition(
    const AcDbObjectId&     idRasterImageDef,
    const AcDb::OpenMode&   opMode  = AcDb::kForRead );


/**
    * \brief Renvoie la definition d'une image à partir du nom
    * \param sRasterImageName   Nom de l'image
    * \param opMode             Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
    * \return Pointeur sur la definition de l'image
    */
AcDbRasterImageDef* getRasterImageDefinition(
    const AcString&         sRasterImageName,
    const AcDb::OpenMode&   opMode  = AcDb::kForRead );


/**
    * \brief Renvoie la definition d'une image à partir du nom
    * \param obRasterImage      Pointeur sur la reference d'image
    * \param opMode             Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
    * \return Pointeur sur la definition de l'image
    */
AcDbRasterImageDef* getRasterImageDefinition(
    const AcDbRasterImage*  obRasterImage,
    const AcDb::OpenMode&   opMode  = AcDb::kForRead );



/**
    * \brief Ajoute au dictionnaire la definition d'une nouvelle image et renvoie un pointeur sur cette definition. Si l'image existe deja, renvoie l'image existant
    * \param sRasterImageName   Nom de l'image
    * \param opMode             Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
    * \return Pointeur sur la definition de l'image
    */
AcDbRasterImageDef* addRasterImageDefinition(
    const AcString&         sRasterImageName,
    const AcDb::OpenMode&   opMode  = AcDb::kForRead );


/**
    * \brief Insere une image dans le dessin et renvoie un pointeur ouvert en ecriture sur cette image
    * \param sFileToInsert      Chemin absolut du fichier image
    * \param sRasterImageName   Nom de l'image (nom du fichier si nul)
    * \param opMode             Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
    * \return Pointeur sur une image ouvert en ecriture
    */
AcDbRasterImage* insertRasterImage(
    const AcString&         sFileToInsert,
    AcString                sRasterImageName    = "",
    const AcDb::OpenMode&   opMode              = AcDb::kForRead );



/**
    * \brief Insere une image dans le dessin et renvoie un pointeur ouvert en ecriture sur cette image
    * \param sFileToInsert      Chemin absolut du fichier image
    * \param ptInsert           Point d'insertion de l'image
    * \param sRasterImageName   Nom de l'image (nom du fichier si nul)
    * \return Pointeur sur une image ouvert en ecriture
    */
AcDbRasterImage* insertRasterImage(
    const AcString&         sFileToInsert,
    const AcGePoint3d&      ptInsert,
    AcString                sRasterImageName    = "",
    const AcDb::OpenMode&   opMode              = AcDb::kForRead );


/**
    * \brief Insere une image dans le dessin et renvoie un pointeur ouvert en ecriture sur cette image
    * \param sFileToInsert      Chemin absolut du fichier image
    * \param ptInsert           Point d'insertion de l'image
    * \param vectX              Vecteur X de l'image qui definit l'orientation et la largeur
    * \param vectY              Vecteur Y de l'image qui definie la hauteur de l'image
    * \param sRasterImageName   Nom de l'image (nom du fichier si nul)
    * \return Pointeur sur une image ouvert en ecriture
    */
AcDbRasterImage* insertRasterImage(
    const AcString&         sFileToInsert,
    const AcGePoint3d&      ptInsert,
    const AcGeVector3d&     vectX,
    const AcGeVector3d&     vectY,
    AcString                sRasterImageName    = "",
    const AcDb::OpenMode&   opMode              = AcDb::kForRead );


/**
    * \brief Charge ou recharge une image
    * \param pImageDef pointeur sur une definition d'image ouverte en ecriture
    * \return true si l'iamge est chargée
    */
bool reloadRasterImage(
    AcDbRasterImageDef* const pImageDef );


