/**
 * @file dataBase.h
 * @author Romain LEMETTAIS
 * @brief Fonctions pour ouvrir/ fermer/ recuperer la base de donnees, utile pour ajouter des objets a la base de donnees
 */


#pragma once
#include <aced.h>
#include <adscodes.h>
#include <dbmain.h>
#include "dbapserv.h"


/**
 * \brief Ajoute une entite au model space
 * \param acEntity Entite a ajouter a la Database
 */
void addToModelSpace( AcDbEntity* acEntity );


/**
 * \brief Ouvre le model space
 * \return Renvoie un pointeur sur la database
 */
AcDbBlockTableRecord* openModelSpace();


/**
 * \brief Ajoute une entite au model space deja ouvert
 * \param pBlockTableRecord Pointeur sur le model space
 * \param acEntity Entite a ajouter a la Database
 */
void addToModelSpaceAlreadyOpened( AcDbBlockTableRecord* pBlockTableRecord,
    AcDbEntity* acEntity );

/**
 * \brief Ajoute une entite au model space deja ouvert
 * \param pBlockTableRecord Pointeur sur le model space
 * \param acEntity Entite a ajouter a la Database
 * \param AcDbObjectId Id de la nouvel entity
 * \return ErrorStatus
 */
Acad::ErrorStatus addToModelSpaceAlreadyOpened( AcDbBlockTableRecord* pBlockTableRecord,
    AcDbEntity* acEntity, AcDbObjectId& objectId );

/**
 * \brief Ferme le model space
 * \param pBlockTableRecord Pointeur sur le model space
 */
void closeModelSpace( AcDbBlockTableRecord* pBlockTableRecord );


/**
 * \brief Ajoute une entite au model space
 * \param acEntity Entite � ajouter a la Database
 * \param objectId id de l' Entite ajout�e � la Database
 */
void addToModelSpace( AcDbEntity* acEntity,
    AcDbObjectId& objectId );

/**
 * \brief redessine une entit�
 * \param entity entit� � redesissinerS
 * \return ErrorStatus
 */
Acad::ErrorStatus redrawEntity( AcDbEntity* entity );


/**
 * \brief
 * \param
 * \return
 */
Acad::ErrorStatus  insertEntityExternFile(
    AcDbEntity* entity,
    const AcString& filename = "",
    AcDbDatabase* database = NULL );


/**
 * \brief Fonction pour cloner une entit�
 * \param pEntToClone Entit� � cloner
 * \param pClonedEnt Le clone qui vient d'�tre cr��
 * \param pDbClonedEnt Base de donn�es dans laquelle le clone va �tre ins�r�, par d�faut la base de donn�es du dessin courant
 * \param mode Mode d'ouverture du clone, par d�faut en �criture
 * \return ErrorStatus
 */
Acad::ErrorStatus cloneEntity(
    AcDbEntity* pEntToClone,
    AcDbEntity* pClonedEnt,
    AcDbDatabase* pDbClonedEnt = acdbHostApplicationServices()->workingDatabase(),
    const AcDb::OpenMode& mode = AcDb::kForWrite );


/**
 * \brief Fonction pour r�cup�rer les objectId de toutes les entit�s d'un dessin
 * \param objectIdArray Liste qui va contenir la liste des objectId
 * \param dwgFile Chemin du fichier qui contient les entit�s
 * \return ErrorStatus
 */
Acad::ErrorStatus importEntity(
    AcDbObjectIdArray& objectIdArray,
    const AcString& dwgFile,
    AcDbDatabase* dbExtern = NULL );


