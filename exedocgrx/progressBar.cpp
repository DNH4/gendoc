#include "progressBar.h"


ProgressBar::ProgressBar(
    const ACHAR*    textLabel,
    const long&      size )
{
    //Initialiser les membres
    m_factor =  100.0 / size;
    
    //Initialiser la barre de progression
    acedSetStatusBarProgressMeter( textLabel, 0, 100 );
}


ProgressBar::~ProgressBar()
{
    //Fermer la barre de progression
    acedRestoreStatusBar();
}


int ProgressBar::moveUp(
    const long& position )
{
    // Declarer l'avancement
    int l_iposition = m_factor * position;
    
    // Calculer la nouvelle position de la barre
    return acedSetStatusBarProgressMeterPos( l_iposition );
}
