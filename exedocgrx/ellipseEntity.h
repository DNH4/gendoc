/**
 * @file ellipseEntity.h
 * @author Romain LEMETTAIS
 * @brief Fichier contenant des fonctions utiles pour travailler avec les ellipses
 */

#pragma once

#include <dbents.h>
#include <dbelipse.h>
#include <geell3d.h>
#include "selectEntity.h"
#include "lineEntity.h"
#include "poly2DEntity.h"

//-----------------------------------------------------------------------------------<SelectEllipse>

/**
  * \brief Faire une selection d'ellipses
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsEllipse(
    ads_name& ssName,
    const AcString& layerName = "" );

/**
  * \brief Faire une selection composee d'une seule ellipse
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsOneEllipse(
    ads_name& ssName,
    const AcString& layerName = "" );

/**
  * \brief Selectionner toutes les ellipses du dessin
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsAllEllipse(
    ads_name& ssName,
    const AcString& layerName = "" );

/// A CHANGER ( S'INSPIRER DE ARCENTITY )
/**
  * \brief Pour avoir l'objet elipse dans la selection numero i
  * \param ssName   Selection de l'ellipse
  * \param iObject  Numero de l'objet de la selection
  * \return l'ellipse de la selection
  */
AcDbEllipse* getEllipseFromSs(
    const ads_name&        ssName,
    const long&           iObject      = 0,
    const AcDb::OpenMode& blocktable   = AcDb::kForRead );

/**
  * \brief Renvoie un pointeur en ECRITURE sur l'ellipse numero N dans la selection
  * \param ssName   Selection de l'ellipse
  * \param iObject  Index dans la selection de l'arc a recuperer, par defaut 0
  * \return Pointeur sur un Ellipse
  */
AcDbEllipse* readEllipseFromSs(
    const ads_name&    ssName,
    const long&        iObject = 0 );

/**
  * \brief Renvoie un pointeur en ECRITURE sur l'ellipse numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de l'ellipse a recuperer, par defaut 0
  * \return Pointeur sur un arc
  */
AcDbEllipse* writeEllipseFromSs(
    const ads_name&    ssName,
    const long&        iObject = 0 );

/**
  * \brief Recuperer l'objet AcGeEllipArc3d
  * \param ellipse  Pointeur sur un AcDbEllipse
  * \return Pointeur sur un objet AcGeEllipArc3d
  */
AcGeEllipArc3d* getAcGeEllipse(
    const AcDbEllipse*    ellipse );

/**
  * \brief Obtenir une liste de points de discretisation de l'ellipse
  * \param ellipse  Ellipse a discretiser
  * \param fleche Fleche de discretisation
  * \return Tableau de points discretises
  */
AcGePoint3dArray discretize( AcDbEllipse*  ellipse, const double& fleche );