/**
 * @file acadVariable.h
 * @brief Outils de récupération du contenu d'un AcString
 */

#pragma once
#include <aced.h>
#include <adscodes.h>
#include <dbmain.h>
#include "dbapserv.h"
#include "acString.h"

/**
 * \brief Change la valeur d'une variable
 * \param sVarName Nom de la variable
 * \param sVarValue Valeur de la variable
 */
void setStringVariable( const AcString& sVarName,
    const AcString& sVarValue );


/**
 * \brief Renvoie la valeur d'une variable AutoCAD de type texte
 * \param sVarName Nom de la variable
 */
AcString getStringVariable( const AcString& sVarName );

/**
 * \brief Renvoie la valeur d'une variable AutoCAD de type reel
 * \param Valeur de la variable
 */
double getRealVariable( const AcString& sVarName );

/**
 * \brief Renvoie la valeur d'une variable AutoCAD de type reel
 * \param Valeur de la variable
 */
int getIntVariable( const AcString& sVarName );

/**
 * \brief Renvoie la valeur d'une variable AutoCAD de type 0 ou 1
 * \param Valeur de la variable
 */
bool getBoolVariable( const AcString& sVarName );