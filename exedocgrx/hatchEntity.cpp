#pragma once
#include "hatchEntity.h"
#include <vector>
#include "print.h"


Acad::ErrorStatus insert3DHatch( std::vector<AcDb3dPolyline*> poly,
    AcDbHatch* hatch,
    const AcString& patternName )
{
    Acad::ErrorStatus es;
    
    for( int i = 0; i < poly.size(); i++ )
    {
        // Cr�ation d'un array contenant l'ID des poylignes 3D
        AcDbObjectIdArray idArray;
        
        idArray.append( poly[i]->objectId() );
        
        // Ajout de la polyligne en tant que bordure � la hachure
        if( es = hatch->appendLoop( kExternalCurve3d, idArray ) )
            return es;
    }
    
    // Ajout du motif � la hachure
    return hatch->setPattern( AcDbHatch::kPreDefined, patternName );
}

Acad::ErrorStatus insert2DHatch( std::vector<AcDbPolyline*> poly,
    AcDbHatch* hatch,
    const AcString& patternName )
{
    Acad::ErrorStatus es;
    
    for( int i = 0; i < poly.size(); i++ )
    {
        // Cr�ation d'un array contenant l'ID des poylignes 3D
        AcDbObjectIdArray idArray;
        
        idArray.append( poly[i]->objectId() );
        
        // Ajout de la polyligne en tant que bordure � la hachure
        if( es = hatch->appendLoop( kExternalCurve2d, idArray ) )
            return es;
    }
    
    // Ajout du motif � la hachure
    return hatch->setPattern( AcDbHatch::kPreDefined, patternName );
}

Acad::ErrorStatus insert2DHatch( AcDbObjectId objectId,
    AcDbHatch* hatch,
    const AcString& patternName )
{
    Acad::ErrorStatus es;
    
    AcDbEntity* pEnt;
    acdbOpenAcDbEntity( pEnt, objectId, AcDb::kForRead );
    AcDbPolyline* pPoly = AcDbPolyline::cast( pEnt );
    
    if( pPoly != NULL )
    {
        AcDbObjectIdArray idArray;
        idArray.append( objectId );
        
        // Ajout de la polyligne en tant que bordure � la hachure
        if( es = hatch->appendLoop( kExternalCurve2d, idArray ) )
            return es;
    }
    
    // Ajout du motif � la hachure
    return hatch->setPattern( AcDbHatch::kPreDefined, patternName );
}

Acad::ErrorStatus insert2DHatch( AcDbObjectIdArray objectIdArray,
    AcDbHatch* hatch,
    const AcString& patternName )
{
    Acad::ErrorStatus es;
    
    int length = objectIdArray.size();
    
    // On v�rifie que les entit�s sont bien des polylignes 2D
    for( int i = 0; i < length; i++ )
    {
        AcDbEntity* pEnt;
        acdbOpenAcDbEntity( pEnt, objectIdArray[i], AcDb::kForRead );
        AcDbPolyline* pPoly = AcDbPolyline::cast( pEnt );
        
        if( pPoly != NULL )
        {
            AcDbObjectIdArray idArray;
            idArray.append( objectIdArray[i] );
            
            // Ajout de la polyligne en tant que bordure � la hachure
            if( es = hatch->appendLoop( kExternalCurve2d, idArray ) )
                return es;
        }
    }
    
    // Ajout du motif � la hachure
    return hatch->setPattern( AcDbHatch::kPreDefined, patternName );
}

Acad::ErrorStatus insert3DHatch( AcDbObjectId objectId,
    AcDbHatch* hatch,
    const AcString& patternName )
{
    Acad::ErrorStatus es;
    
    AcDbEntity* pEnt;
    acdbOpenAcDbEntity( pEnt, objectId, AcDb::kForRead );
    AcDb3dPolyline* pPoly = AcDb3dPolyline::cast( pEnt );
    
    if( pPoly != NULL )
    {
        AcDbObjectIdArray idArray;
        idArray.append( objectId );
        
        // Ajout de la polyligne en tant que bordure � la hachure
        if( es = hatch->appendLoop( kExternalCurve3d, idArray ) )
            return es;
    }
    
    // Ajout du motif � la hachure
    return hatch->setPattern( AcDbHatch::kPreDefined, patternName );
}

Acad::ErrorStatus insert3DHatch( AcDbObjectIdArray objectIdArray,
    AcDbHatch* hatch,
    const AcString& patternName )
{
    Acad::ErrorStatus es;
    
    int length = objectIdArray.size();
    
    // On v�rifie que les entit�s sont bien des polylignes 3D
    for( int i = 0; i < length; i++ )
    {
        AcDbEntity* pEnt;
        acdbOpenAcDbEntity( pEnt, objectIdArray[i], AcDb::kForRead );
        AcDb3dPolyline* pPoly = AcDb3dPolyline::cast( pEnt );
        
        if( pPoly != NULL )
        {
            AcDbObjectIdArray idArray;
            idArray.append( objectIdArray[i] );
            
            // Ajout de la polyligne en tant que bordure � la hachure
            if( es = hatch->appendLoop( kExternalCurve3d, idArray ) )
                return es;
        }
    }
    
    // Ajout du motif � la hachure
    return hatch->setPattern( AcDbHatch::kPreDefined, patternName );
}


Acad::ErrorStatus insert2DHatch( AcDbObjectIdArray objectIdArray,
    AcDbHatch* hatch,
    const AcString& patternName,
    const AcString& layerName,
    const AcCmColor& color,
    const AcString& lineTypeName )
{
    Acad::ErrorStatus es;
    
    // On ins�re la hachure avec un motif donn�
    if( es = insert2DHatch( objectIdArray, hatch, patternName ) )
    {
        hatch->close();
        return es;
    }
    
    // On modifie le calque
    if( es = hatch->setLayer( layerName ) )
    {
        hatch->close();
        return es;
    }
    
    // On modifie la couleur
    if( es = hatch->setColor( color ) )
    {
        hatch->close();
        return es;
    }
    
    // On modifie le type de ligne
    if( es = hatch->setLinetype( lineTypeName ) )
    {
        hatch->close();
        return es;
    }
    
    return es;
}

Acad::ErrorStatus insert3DHatch( AcDbObjectIdArray objectIdArray,
    AcDbHatch* hatch,
    const AcString& patternName,
    const AcString& layerName,
    const AcCmColor& color,
    const AcString& lineTypeName )
{
    Acad::ErrorStatus es;
    
    // On ins�re la hachure avec un motif donn�
    if( es = insert3DHatch( objectIdArray, hatch, patternName ) )
    {
        hatch->close();
        return es;
    }
    
    // On modifie le calque
    if( es = hatch->setLayer( layerName ) )
    {
        hatch->close();
        return es;
    }
    
    // On modifie la couleur
    if( es = hatch->setColor( color ) )
    {
        hatch->close();
        return es;
    }
    
    // On modifie le type de ligne
    if( es = hatch->setLinetype( lineTypeName ) )
    {
        hatch->close();
        return es;
    }
    
    return es;
}




///---------------------------------------------------------------------------------------------<SELECTION>

long getSsHatch(
    ads_name          ssName,
    const AcString&   layerName )
{
    return getSelectionSet( ssName, "", "HATCH", layerName );
}




long getSsOneHatch(
    ads_name          ssName,
    const AcString&   layerName )
{

    return getSingleSelection( ssName, "HATCH", layerName );
}




long getSsAllHatch(
    ads_name        ssName,
    const AcString& layerName )
{
    return getSelectionSet( ssName, "X", "HATCH", layerName );
}




AcDbHatch* getHatchFromSs(
    const ads_name&               ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  hatch = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbHatch::cast( hatch );
}




AcDbHatch* readHatchFromSs(
    const ads_name&       ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getHatchFromSs( ssName, iObject, AcDb::kForRead );
}




AcDbHatch* writeHatchFromSs(
    const ads_name&              ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getHatchFromSs( ssName, iObject, AcDb::kForWrite );
}
