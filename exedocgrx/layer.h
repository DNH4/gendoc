/**
 *  @file layer.h
 *  @author Romain LEMETTAIS
 *  @brief Fonctions utiles pour travailler avec les calques
 */


#pragma once
#include "fmapHeaders.h"
#include "file.h"
#include <sstream>
#include <algorithm>

/**
    * \brief Retourne un calque � partir de son nom
    * \param layerEntity Pointeur sur le calque
    * \param layerName Nom du calque
    * \param opMode Mode d'ouverture, par defaut en lecture seulement
    * \param database Pointeur vers la database dans laquelle on r�cup�re le calque (par d�faut database courante)
    * \return ErrorStatus Message de sortie pour savoir si la recuperation du calque a fonctionne
    */
Acad::ErrorStatus getLayer(
    AcDbLayerTableRecord*&      layerEntity,
    const AcString&             layerName,
    const AcDb::OpenMode&       opMode = AcDb::kForRead,
    AcDbDatabase* database = NULL );


/**
  *  \brief Cr�e un calque si celui-ci n'existe pas, ne fait rien sinon
  *  \param layerName Nom du calque
  *  \param color Couleur du calque
  *  \param database Pointeur vers la database dans laquelle on veut cr�er le calque (par d�faut database courante)
  *  \return ErrorStatus
  */
Acad::ErrorStatus createLayer( const AcString& layerName,
    const AcCmColor& color = AcCmColor(),
    AcDbDatabase* database = NULL );


/**
 *  \brief Retourne un calque a partir de son identifiant
 *  \param layer Pointeur sur la calque
 *  \param layerId Identifiant du calque
 *  \param opMode Mode d'ouverture, par defaut en lecture seulement
 *  \return Pointeur sur un calque
 */

Acad::ErrorStatus getLayer(
    AcDbLayerTableRecord*& layer,
    const AcDbObjectId& layerId,
    const AcDb::OpenMode& opMode );

/**
 *  \brief Retourne un calque a partir de son identifiant en LECTURE seulement
 *  \param layerId Identifiant du calque
 *  \return Pointeur sur un calque
 */
AcDbLayerTableRecord* readLayer(
    const AcDbObjectId& layerId );


/**
 *  \brief Retourne un calque � partir de son identifiant en ECRITURE seulement
 *  \param layerId Identifiant du calque
 *  \return Pointeur sur un calque
 */
AcDbLayerTableRecord* writeLayer(
    const AcDbObjectId& layerId );



/**
 *  \brief R�cup�re le calque courant
 *  \param opMode Mode d'ouverture du calque, par defaut en lecture
 *  \return Pointeur sur un calque
 */
AcDbLayerTableRecord* getCurrentLayer(
    const AcDb::OpenMode& opMode = AcDb::kForRead );


/**
 *  \brief R�cup�re le calque courant en LECTURE seulement
 *  \return Pointeur sur un calque
 */
AcDbLayerTableRecord* readCurrentLayer();


/**
 *  \brief R�cup�re le calque courant en ECRITURE seulement
 *  \return Pointeur sur un calque
 */
AcDbLayerTableRecord* writeCurrentLayer();


/**
 *  \brief R�cup�re l'identifiant du calque courant
 *  \return Identifiant du calque courant
 */
AcDbObjectId getCurrentLayerId();

/**
 * \brief R�cup�re le nom du calque courant
 * \return Nom du calque courant
 */
AcString getCurrentLayerName();


/**
 *  \brief Changer le calque courant
 *  \param layer Nom du nouveau calque courant
 *  \return ErrorStatus
 */
Acad::ErrorStatus activateLayer( const AcString& layer );


/**
 *  \brief Changer le calque courant
 *  \param pEnt Pointeur vers le nouveau calque courant
 *  \return ErrorStatus
 */
Acad::ErrorStatus activateLayer( AcDbEntity* pEnt );


/**
 *  \brief R�cup�rer la liste du nom du calque
 *  \return Vecteur Acstring du nom de tout les calques
 */
vector <AcString> getLayerList();


/**
 *  \brief Geler un calque dans une certaine fen�tre
 *  \param layerName Nom du calque � geler
 *  \param vpId ObjectId du viewport dans lequel geler le calque
 *  \param hasToFreeze Si True, g�le le calque, si False, clear le calque
 *  \return ErrorStatus
 */
Acad::ErrorStatus freezeLayerInViewport(
    const AcString& layerName,
    const AcDbObjectId& vpId,
    const bool& hasToFreeze = true );

/**
 *  \brief Permet de tester si un calque est dans le dessin
 *  \param layerName Nom du calque
 *  \return True si le calque est dans le dessin, false sinon
 */
bool isLayerExisting( const AcString& layerName );


/**
  * \brief R�cup�rer un calque depuis un autre dessin
  * \param layer Pointeur sur l'objet calque d'AutoCAD
  * \param dwgFile Chemin d'acc�s vers le fichier externe
  * \return ErrorStatus
  */
Acad::ErrorStatus createLayerFromExternDwg( const AcString& layerName,
    const AcString& dwgFile );

/**
  * \brief R�cup�rer les calques d'un fichier texte ligne par ligne
  * \param filePath Le chemin vers le fichier texte
  * \return Vecteur contenant chaque ligne du fichier texte
  */
std::vector<AcString> layerFileToLayerVector( const AcString& filePath );


/**
  * \brief R�cup�rer les calques � inclure ou � exclure (tr�s utile pendant une selection)
  * \param filePath Le chemin vers le fichier texte
  * \param hasToUseLayer True si on veut inclure les calques dans le fichier, False si on veut les exclure
  * \return Vecteur contenant les calques s�par�s avec une virgule
  */
AcString getLayer( const AcString& filePath,
    const bool& hasToUseLayer );

/**
  * \brief R�cup�rer les calques � inclure ou � exclure (tr�s utile pendant une selection)
  * \param filePath Le chemin vers le fichier texte
  * \param isPoly si c'est une polyligne ca recupere les noms des calques des polylignes, sinon celui des blocs
  * \param isName si ispoly est un bloc ca recuperer les noms des blocs et non son calque
  * \return Vecteur contenant les calques s�par�s avec une virgule
  */
AcString getLayer( const AcString& filePath,
    const bool& isPoly,
    const bool& isName,
    const bool& isSelect );

/**
  * \brief R�cup�rer les calques � inclure ou � exclure (tr�s utile pendant une selection)
  * \param layers Vecteur contenant les noms des calques
  * \param hasToUseLayer True si on veut inclure les calques dans le fichier, False si on veut les exclure
  * \return Vecteur contenant les calques s�par�s avec une virgule
  */
AcString getLayer( const vector<AcString>& layers,
    const bool& hasToUseLayer );


/**
  * \brief Permet de purger les �l�ments d'un calque
  * \param layerName Nom du calque � purger
  * \param eraseLayer Si oui on supprime aussi le calque
  * \return ErrorStatus
  */
Acad::ErrorStatus purgeLayer( const AcString& layerName,
    const bool& eraseLayer = false );

/**
  * \brief R�cup�re la couleur d'un calque en fonction de son nom
  * \param color Couleur du calque r�cup�r�e
  * \param layerName Nom du calque dont on veut r�cup�rer la couleur
  * \return ErrorStatus
*/
Acad::ErrorStatus getColorLayer( AcCmColor& color,
    const AcString& layerName );