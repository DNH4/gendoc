#include "groupEntity.h"


AcDbGroup* getGroup(
    const AcString        groupName,
    const AcDbDictionary* groupeDict,
    const AcDb::OpenMode  opMode )
{
    //Declarer l'identifiant du groupe
    AcDbObjectId l_groupID;
    
    //Recuperer le groupe
    if( Acad::eOk != groupeDict->getAt( groupName, l_groupID ) )
        return NULL;
        
    //Declarer le groupe
    AcDbGroup* l_obGroup = NULL;
    
    //Recuperere le groupe
    if( Acad::eOk != acdbOpenObject( l_obGroup, l_groupID, opMode ) )
        return NULL;
        
    //Sotir
    return l_obGroup;
}


AcDbGroup* getGroup(
    const AcString        groupName,
    const AcDb::OpenMode  opMode )
{
    //Recuperer la liste des groupes
    AcDbDictionary* l_groupDict = getGroupDictionary( AcDb::kForRead );
    
    //Verifier le dictionnaire
    if( !l_groupDict )
        return NULL;
        
    //Declarer le groupe
    AcDbGroup* l_obGroup = getGroup( groupName, l_groupDict, opMode );
    
    //Fermer le dictionnaire
    l_groupDict->close();
    
    //Sotir
    return l_obGroup;
}


AcDbGroup* createGroup(
    const AcString    groupName,
    AcDbDictionary*   groupeDict )
{
    //Creer le groupe
    AcDbGroup* l_obGroup = new AcDbGroup( groupName );
    
    //Declarer l'ID du groupe
    AcDbObjectId l_groupID;
    
    //L'ajouter au dictionnaire
    if( Acad::eOk == groupeDict->setAt( groupName, l_obGroup, l_groupID ) )
    {
        //Sotir
        return l_obGroup;
    }
    
    //Cas ou le groupe n'a pas pu etre cr��
    
    //Supprimer le groupe
    delete l_obGroup;
    
    //Sortir
    return NULL;
}


AcDbGroup* createGroup(
    const AcString    groupName )
{
    //Recuperer la liste des groupes
    AcDbDictionary* l_groupDict = getGroupDictionary( AcDb::kForWrite );
    
    //Verifier le dictionnaire
    if( !l_groupDict )
        return NULL;
        
    //Creer le groupe
    AcDbGroup* l_obGroup = createGroup( groupName, l_groupDict );

	//Fermer le dictionnaire
	l_groupDict->close();

    //Sotir
    return l_obGroup;
}


AcDbDictionary* getGroupDictionary( const AcDb::OpenMode p_openMode )
{
    //Declarer la liste des groupes
    AcDbDictionary* l_groupTable;
    
    //Recuperer la liste
    if( Acad::eOk != acdbHostApplicationServices()->workingDatabase()->getGroupDictionary( l_groupTable, p_openMode ) )
        return NULL;
        
    //Sorir
    return l_groupTable;
}


int eraseGroupObject(
    AcDbGroup* groupObject,
    const bool bEraseGroup )
{
    //Verifier si le groupe est ouvert
    if( !groupObject->isWriteEnabled() )
        return -1;
        
    //Cas ou le groupe est vide
    if( groupObject->numEntities() == 0 )
    {
        //Effacer le grouep si besoin
        if( bEraseGroup )
            groupObject->erase();
            
        //Sortir
        return 0;
    }
    
    //Initialiser le compterur
    long nGroup = 0;
    
    //Initialiser un  iterateur
    AcDbGroupIterator* iterGroup = groupObject->newIterator();
    
    //Initialiser  un objet
    AcDbObject* obGroup = NULL;
    
    //Boucler sur les objets
    while( !iterGroup->done() )
    {
        //Ouvrir l'objet en ecriture
        if( Acad::eOk == iterGroup->getObject( obGroup, AcDb::kForWrite ) )
        {
            //Effacer l'objet
            obGroup->erase();
            obGroup->close();
            
            //Compter
            nGroup++;
        }
        
        //Iterer
        iterGroup->next();
    }
    
    //Vider le groupe
    //free(iterGroup);
    groupObject->clear();
    
    //Effacer le grouep si besoin
    if( bEraseGroup )
        groupObject->erase();
        
    //Sortir
    return nGroup;
}

AcDbGroup* getGroupFromSs(const ads_name& ssGroup,
	const long& iObject,
	const AcDb::OpenMode& opmode) 
{
	//Recuperer l'id dy gtoupe
	AcDbObjectId objID = getObIdFromSs(ssGroup, iObject);

	//Verifier l'object id n'est pas null
	if (objID.isNull())
		return NULL;

	//Recuperer l'objet
	AcDbObject* obj = NULL;

	//Recuperer l'objet 
	if (Acad::eOk != acdbOpenObject(obj, objID, opmode))
		return NULL;

	//Verifie si l'object est un groupe ou pas
	if (obj->isKindOf(AcDbGroup::desc()))
		return AcDbGroup::cast(obj);

	return NULL;
}