/**
 * @file fmapMath.h
 * @author Romain LEMETTAIS
 * @brief Fonctions mathematiques pratiques
 */

#pragma once
#include "math.h"
#include <vector>


using namespace std;

/**
 * \brief On n'est pas dans la convention C++11 du coup il n'y a pas de fonction arrondi encore, celle-ci en fait office
 * \param value Valeur a arrondir
 * \param numberOfDigits A combien de chiffres apres la virgule on veut arrondir, par defaut on arrondit a l'unite
 */
double round( const double& value,
    const int& numberOfDigits = 0 );

/**
 * \brief fonction qui arrondi en fonction d'un mod�le de d�cimal
 * \param value Valeur a arrondir
 * \param model model de d�cimal � utiliser
 */
double round( const double& value,
    const long& factor = 0 );

/**
 * \brief calcul de moyen de deux valeur d�cimal
 * \param val1 premi�re Valeur
 * \param val2 deuxi�me Valeur
 */
double mean( const double& val1,
    const double& val2 );

/**
 * \brief
 * \param
 * \param
 */
long factor( const double& model = 0 );