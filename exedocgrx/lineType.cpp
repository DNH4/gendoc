#pragma once
#include "lineType.h"

vector <AcString> getLineType()
{
    // Initialiser le vecteur de type de ligne
    vector <AcString> vecLineType;
    
    // Recuperer toutes les types de ligne definie
    AcDbLinetypeTable* lineTypeTable;
    acdbHostApplicationServices()->workingDatabase()->getSymbolTable( lineTypeTable, AcDb::kForRead );
    
    // Creer une iterateur de type de line
    AcDbLinetypeTableIterator* iter ;
    lineTypeTable->newIterator( iter );
    
    // Boucler sur l'iterateur
    for( ; !iter->done(); iter->step() )
    {
        // Recuperer l'objet type de ligne
        AcDbLinetypeTableRecord* tableblockRecord;
        
        //V erifier qu'il n'y a pas d'erreur
        if( !iter->getRecord( tableblockRecord, AcDb::kForRead ) )
        {
            // Recuperer en string le type de ligne
            AcString stringTextType ;
            
            // Verifier qu'il n'y a pas d'erreur
            // Et qu'il n'y a pas de type de ligne creer automatique par gstarcad
            if( !tableblockRecord->getName( stringTextType ) &&  stringTextType.find( _T( "_" ) ) )
                vecLineType.push_back( stringTextType );
        }
    }
    
    // Suprimer le pointeur sur l'iterateur
    delete iter;
    
    // Fermer la table des types de ligne
    lineTypeTable->close();
    
    // Renvoyer le resultat
    return vecLineType;
}

Acad::ErrorStatus checkLineType( const AcString& strLintType, AcDbObjectId& lineTypeId )
{
    // Récupération de la liste des types de lignes
    vector <AcString> vecLT = getLineType();
    
    if( vecLT.size() == 0 )
        return Acad::eKeyNotFound;
        
    // Vérification
    vector <AcString>::iterator it;
    
    // On effectue une recherche case sensitive
    it = find( vecLT.begin(), vecLT.end(), strLintType );
    
    // Si trouvé
    if( it != vecLT.end() )
    {
        // Recuperer l'id 
        AcDbLinetypeTable* lineTypeTable;
        Acad::ErrorStatus err = acdbHostApplicationServices()->workingDatabase()->getSymbolTable( lineTypeTable, AcDb::kForRead );
        
        if( err == Acad::eOk )
        {
            err = lineTypeTable->getAt( strLintType, lineTypeId );
            // Fermer la table des types de ligne
            lineTypeTable->close();
            
            if( err == Acad::eOk )
                return Acad::eOk;
        }
        
        return Acad::eKeyNotFound;
    }
    
    return Acad::eKeyNotFound; 
}
