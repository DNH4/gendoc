#include "pointEntity.h"
#include "faceEntity.h"

//#ifndef M_PI
//   #define TOLERANCE pow(10,-4)
//#endif


AcGePoint3d adsToAcGe( ads_point pt )
{
    AcGePoint3d point;
    point.x = pt[ 0 ];
    point.y = pt[ 1 ];
    point.z = pt[ 2 ];
    return point;
}


void acGeToAds( const AcGePoint3d& pt, ads_point& ads_pt )
{
    ads_pt[ 0 ] = pt.x;
    ads_pt[ 1 ] = pt.y;
    ads_pt[ 2 ] = pt.z;
}


long getSsPoint(
    ads_name&         ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "", "POINT", layerName );
}



long getSsOnePoint(
    ads_name         ssName,
    const AcString&  layerName )
{
    return getSingleSelection( ssName, "POINT", layerName );
}



long getSsAllPoint(
    ads_name&         ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "X", "POINT", layerName );
}



AcDbPoint* getPointFromSs(
    const ads_name&               ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity* l_pEntityPoint = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbPoint::cast( l_pEntityPoint );
}




AcDbPoint* readPointFromSs(
    const ads_name&               ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getPointFromSs( ssName, iObject, AcDb::kForRead );
}




AcDbPoint* writePointFromSs(
    const ads_name&               ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getPointFromSs( ssName, iObject, AcDb::kForWrite );
}


void drawPoint( AcGePoint3d pt,
    const AcString& layer )
{

    AcDbPoint* point = new AcDbPoint( pt );
    
    addToModelSpace( point );
    
    point->setLayer( layer );
    point->setColorIndex( 256 );
    
    point->close();
}


void drawPoint( AcGePoint2d pt,
    const AcString& layer )
{
    AcDbPoint* point = new AcDbPoint( getPoint3d( pt ) );
    
    addToModelSpace( point );
    
    point->setLayer( layer );
    point->setColorIndex( 256 );
    
    point->close();
}


AcDbPoint* drawPoint(
    const AcGePoint3d& pt,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom )
{
    //Creer une ligne
    AcDbPoint* point = new AcDbPoint( pt );
    
    //ajouter le point dans la modelspace
    addToModelSpaceAlreadyOpened( p_blocktable, point );
    
    if( hasToZoom )
        zoomOn( pt );
        
        
    //fermer le point
    point->close();
    
    return point;
}


AcDbPoint* drawPoint(
    const AcGePoint3d& pt,
    AcCmColor color,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom )
{
    //Creer une ligne
    AcDbPoint* point = new AcDbPoint( pt );
    
    point->setColor( color );
    
    //ajouter le point dans la modelspace
    addToModelSpaceAlreadyOpened( p_blocktable, point );
    
    if( hasToZoom )
        zoomOn( pt );
        
        
    //fermer le point
    point->close();
    
    return point;
}

AcDbPoint* drawPoint(
    const AcGePoint2d&  pt,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom )
{
    //Sortir
    return drawPoint( getPoint3d( pt ), p_blocktable, hasToZoom );
}



AcDbPoint* drawPoint(
    const double& dX,
    const double& dY,
    const double& dZ,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom )
{
    //Sortir
    return drawPoint( AcGePoint3d( dX, dY, dZ ), p_blocktable, hasToZoom );
}



AcDbPoint* drawPoint(
    const double&  dX,
    const double&  dY,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom )
{
    //Sortir
    return drawPoint( dX, dY, 0.0, p_blocktable, hasToZoom );
}



void drawRedCross( const AcGePoint2d& pt )
{

    double crossLength = 0.1;
    
    AcDbPolyline* redCross = new AcDbPolyline();
    redCross->reset( Adesk::kFalse, 7 );
    
    redCross->addVertexAt( 0, AcGePoint2d( pt.x - crossLength, pt.y + crossLength ) );
    redCross->addVertexAt( 1, AcGePoint2d( pt.x, pt.y ) );
    redCross->addVertexAt( 2, AcGePoint2d( pt.x + crossLength, pt.y + crossLength ) );
    redCross->addVertexAt( 3, AcGePoint2d( pt.x, pt.y ) );
    redCross->addVertexAt( 4, AcGePoint2d( pt.x + crossLength, pt.y - crossLength ) );
    redCross->addVertexAt( 5, AcGePoint2d( pt.x, pt.y ) );
    redCross->addVertexAt( 6, AcGePoint2d( pt.x - crossLength, pt.y - crossLength ) );
    
    AcCmColor color = AcCmColor();
    color.setRed( 0 );
    color.setBlue( 0 );
    color.setGreen( 0 );
    redCross->setColor( color );
    
    redCross->setThickness( 10 );
    
    addToModelSpace( redCross );
    
    redCross->close();
}

void drawRedCross( const AcGePoint2d& pt,
    AcDbObjectId& idCross )
{

    double crossLength = 0.1;
    
    AcDbPolyline* redCross = new AcDbPolyline();
    redCross->reset( Adesk::kFalse, 7 );
    
    redCross->addVertexAt( 0, AcGePoint2d( pt.x - crossLength, pt.y + crossLength ) );
    redCross->addVertexAt( 1, AcGePoint2d( pt.x, pt.y ) );
    redCross->addVertexAt( 2, AcGePoint2d( pt.x + crossLength, pt.y + crossLength ) );
    redCross->addVertexAt( 3, AcGePoint2d( pt.x, pt.y ) );
    redCross->addVertexAt( 4, AcGePoint2d( pt.x + crossLength, pt.y - crossLength ) );
    redCross->addVertexAt( 5, AcGePoint2d( pt.x, pt.y ) );
    redCross->addVertexAt( 6, AcGePoint2d( pt.x - crossLength, pt.y - crossLength ) );
    
    AcCmColor color = AcCmColor();
    color.setRed( 0 );
    color.setBlue( 0 );
    color.setGreen( 0 );
    redCross->setColor( color );
    
    redCross->setThickness( 10 );
    
    addToModelSpace( redCross );
    idCross = redCross->id();
    
    redCross->close();
}

void drawRedCross( const AcGePoint3d& pt )
{

    double crossLength = 0.1;
    
    AcDb3dPolyline* redCross = new AcDb3dPolyline();
    
    addToModelSpace( redCross );
    
    AcDb3dPolylineVertex* vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x - crossLength, pt.y + crossLength, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x, pt.y, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x + crossLength, pt.y + crossLength, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x, pt.y, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x + crossLength, pt.y - crossLength, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x, pt.y, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x - crossLength, pt.y - crossLength, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    AcCmColor color = AcCmColor();
    color.setRed( 0 );
    color.setBlue( 255 );
    color.setGreen( 0 );
    redCross->setColor( color );
    
    redCross->close();
}

void drawRedCross( const AcGePoint3d& pt,
    AcDbObjectId& idCross )
{

    double crossLength = 0.1;
    
    AcDb3dPolyline* redCross = new AcDb3dPolyline();
    
    addToModelSpace( redCross );
    idCross = redCross->id();
    
    AcDb3dPolylineVertex* vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x - crossLength, pt.y + crossLength, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x, pt.y, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x + crossLength, pt.y + crossLength, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x, pt.y, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x + crossLength, pt.y - crossLength, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x, pt.y, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    vertex = new AcDb3dPolylineVertex( AcGePoint3d( pt.x - crossLength, pt.y - crossLength, pt.z ) );
    redCross->appendVertex( vertex );
    vertex->close();
    
    AcCmColor color = AcCmColor();
    color.setRed( 0 );
    color.setBlue( 255 );
    color.setGreen( 0 );
    redCross->setColor( color );
    
    redCross->close();
}

AcGePoint3d getPoint3d(
    const AcGePoint2d&    pt,
    const double& dZ )
{
    return AcGePoint3d( pt.x, pt.y, dZ );
}

AcGePoint2d getPoint2d(
    const AcGePoint3d& pt )
{
    return AcGePoint2d( pt.x, pt.y );
}


double getDistance2d(
    const AcGePoint3d&      pt1,
    const AcGePoint3d&      pt2 )
{
    return getPoint2d( pt1 ).distanceTo( getPoint2d( pt2 ) );
}


double getDistance2d(
    const AcGePoint2d&  pt1,
    const AcGePoint3d&  pt2 )
{
    return pt1.distanceTo( getPoint2d( pt2 ) );
}


double getDistance2d(
    const AcGePoint2d&  pt1,
    const AcGePoint2d&  pt2 )
{
    return pt1.distanceTo( pt2 );
}



AcGeVector2d getVector2d(
    const AcGeVector3d&     p_vector )
{
    return AcGeVector2d( p_vector.x, p_vector.y );
}



AcGeVector2d getVector2d(
    const AcGePoint3d&     pt1,
    const AcGePoint3d&     pt2 )
{
    return AcGeVector2d( pt2.x - pt1.x, pt2.y - pt1.y );
}


AcGeVector2d getVector2d(
    const AcGePoint2d&     pt1,
    const AcGePoint2d&     pt2 )
{
    return AcGeVector2d( pt2.x - pt1.x, pt2.y - pt1.y );
}



AcGeVector3d getVector3d(
    const AcGeVector2d&        p_vector,
    const double&              dZ )
{
    return AcGeVector3d( p_vector.x, p_vector.y, dZ );
}



AcGeVector3d getVector3d(
    const AcGePoint3d&     pt1,
    const AcGePoint3d&     pt2 )
{
    return AcGeVector3d( pt2.x -  pt1.x, pt2.y - pt1.y, pt2.z - pt1.z );
}



AcGeVector3d getVector3d(
    const AcGePoint2d&     pt1,
    const AcGePoint2d&     pt2 )
{
    return AcGeVector3d( pt2.x -  pt1.x, pt2.y - pt1.y, 0 );
}



AcGeVector3d getVector3d(
    const double&     angle,
    const double&     length,
    const double&     dZ )
{
    return AcGeVector3d( length * cos( angle ), length * sin( angle ), dZ );
}



AcGeVector3d getMeanVector3d( const AcGeVector3d& vec1,
    const AcGeVector3d& vec2 )
{

    double meanX = ( vec1[ 0 ] + vec2[ 0 ] ) / 2;
    double meanY = ( vec1[ 1 ] + vec2[ 1 ] ) / 2;
    double meanZ = ( vec1[ 2 ] + vec2[ 2 ] ) / 2;
    
    return AcGeVector3d( meanX, meanY, meanZ );
}


AcGeVector3d getMeanVector3d( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const AcGePoint3d& pt3 )
{

    AcGeVector3d vec1 = getVector3d( pt1, pt2 );
    AcGeVector3d vec2 = getVector3d( pt2, pt3 );
    
    return getMeanVector3d( vec1, vec2 );
}


AcGeVector2d getUnitVector2d(
    const AcGeVector3d&  p_vector )
{
    return getVector2d( p_vector ).normal();
}


AcGeVector2d getUnitVector2d(
    const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 )
{
    return getVector2d( pt1, pt2 ).normal();
}



AcGeVector2d getUnitVector2d(
    const AcGePoint2d&    pt1,
    const AcGePoint2d&    pt2 )
{
    return getVector2d( pt1, pt2 ).normal();
}




AcGeVector3d getUnitVector3d(
    const AcGePoint3d&   pt1,
    const AcGePoint3d&   pt2 )
{
    return getVector3d( pt1, pt2 ).normal();
}



AcGeVector3d getUnitVector3d(
    const AcGePoint2d&   pt1,
    const AcGePoint2d&   pt2 )
{
    return getVector3d( pt1, pt2 ).normal();
}



AcGePoint3d midPoint3d(
    const AcGePoint3d&   pt1,
    const AcGePoint3d&   pt2 )
{
    return AcGePoint3d(
            0.5 * ( pt2.x + pt1.x ),
            0.5 * ( pt2.y + pt1.y ),
            0.5 * ( pt2.z + pt1.z )
        );
}



AcGePoint3d midPoint3d(
    const AcGePoint2d&   pt1,
    const AcGePoint2d&   pt2 )
{
    return AcGePoint3d(
            0.5 * ( pt2.x + pt1.x ),
            0.5 * ( pt2.y + pt1.y ),
            0.0
        );
}


AcGePoint2d midPoint2d(
    const AcGePoint3d&   pt1,
    const AcGePoint3d&   pt2 )
{
    return AcGePoint2d(
            0.5 * ( pt2.x + pt1.x ),
            0.5 * ( pt2.y + pt1.y )
        );
}



AcGePoint2d midPoint2d(
    const AcGePoint2d&   pt1,
    const AcGePoint2d&   pt2 )
{
    return AcGePoint2d(
            0.5 * ( pt2.x + pt1.x ),
            0.5 * ( pt2.y + pt1.y )
        );
}




bool isZero(
    const double&        ptTest,
    const double&        tol )
{
    //Verifier la distance
    if( ptTest > tol )
        return false;
        
    //Sortir
    
    return true;
}



bool isEqual2d(
    const AcGePoint2d&    pt1,
    const AcGePoint2d&    pt2,
    const double&         tolXY )
{
    return isZero( pt1.distanceTo( pt2 ), tolXY );
}



bool isEqual2d(
    const AcGePoint3d&    pt1,
    const AcGePoint3d&    pt2,
    const double&         tolXY )
{
    return isZero( getDistance2d( pt1, pt2 ), tolXY );
}




bool isEqual3d(
    const AcGePoint3d&    pt1,
    const AcGePoint3d&    pt2,
    const double&         tolXY,
    const double&         tolZ )
{
    //Initialiser la valeur en Z
    double l_dTolZ = tolZ;
    
    //Verifier la valeur en Z
    if( !isZero( abs( pt2.z - pt1.z ), l_dTolZ ) )
        return false;
        
    //Verifier la valeur en XY
    if( !isZero( getDistance2d( pt1, pt2 ), tolXY ) )
        return false;
        
    //Sortir
    return true;
}



bool isEqual3d(
    const AcGePoint3d&    pt1,
    const AcGePoint3d&    pt2,
    const double&         tolXYZ )
{
    //Verifier la valeur en XYZ
    if( !isZero( pt1.distanceTo( pt2 ), tolXYZ ) )
        return false;
        
    //Sortir
    return true;
}



AcGePoint2d addPoints( const AcGePoint2d&    pt1,
    const AcGePoint2d&    pt2 )
{
    return AcGePoint2d( pt1.x + pt2.x, pt1.y + pt2.y );
}



AcGePoint3d addPoints( const AcGePoint3d&    pt1,
    const AcGePoint3d&    pt2 )
{
    return AcGePoint3d( pt1.x + pt2.x, pt1.y + pt2.y, pt1.z + pt2.z );
}





Acad::ErrorStatus zoomOn( const AcGePoint3d& pt3D,
    const double& height )
{
    Acad::ErrorStatus result = Acad::eOk;
    //Declarer un pointeur sur un Viewport
    AcDbViewportTableRecordPointer currentView;
    
    //Ouvrir la vue actuelle
    result = currentView.open( acedActiveViewportId(), AcDb::kForWrite ) ;
    
    if( result != Acad::eOk )
    {
        acutPrintf( _T( "\nProbl�me d'ouverture de la fen�tre" ) );
        return result ;
    }
    
    //Modifier les propri�t�s du viewPort
    currentView->setTarget( pt3D );
    currentView->setHeight( height );
    currentView->setWidth( height );
    currentView->setCenterPoint( AcGePoint2d( 0.0, 0.0 ) );
    currentView->setFrontClipDistance( height );
    currentView->setBackClipDistance( - height );
    
    
    //Fermer la vue active et mettre � jour les vues
    acedVportTableRecords2Vports();
    currentView->close();
    
    //renvoyer le resultat
    return result;
}


Acad::ErrorStatus zoomOn( const AcGePoint3d& pt3D,
    const double& height,
    const double& width )
{
    //Declarer le resultat
    Acad::ErrorStatus result = Acad::eOk;
    
    //Declarer un pointeur sur un Viewport
    AcDbViewportTableRecordPointer currentView;
    
    //Ouvrir la vue actuelle
    result = currentView.open( acedActiveViewportId(), AcDb::kForWrite ) ;
    
    if( result != Acad::eOk )
    {
        acutPrintf( _T( "\nProbl�me d'ouverture de la fen�tre" ) );
        return result ;
    }
    
    //Modifier les propri�t�s du viewPort
    currentView->setTarget( pt3D );
    currentView->setHeight( height );
    currentView->setWidth( width );
    currentView->setCenterPoint( AcGePoint2d( 0.0, 0.0 ) );
    
    //Fermer la vue active et mettre � jour les vues
    currentView->close();
    acedVportTableRecords2Vports();
    
    //Renvoyer le resultat
    return result;
}



Acad::ErrorStatus zoomOn( const AcGePoint2d& pt2D,
    const double& height )
{
    return zoomOn( getPoint3d( pt2D ), height );
}



Acad::ErrorStatus zoomOn( AcDbPoint* point,
    const double& height )
{
    return zoomOn( point->position(), height );
}


double getMeanZ( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const AcGePoint3d& ptZSearch )
{
    // Calcul des distances
    double ZValue = 0.0f;
    
    double dist2Dpt12 = getDistance2d( pt1, pt2 );
    double dist2Dpt1Search = getDistance2d( pt1, ptZSearch );
    
    double diffZ12 = abs( pt1.z - pt2.z );
    
    double diffPtSearch = diffZ12 * dist2Dpt1Search / dist2Dpt12;
    
    // Calcul Z
    
    if( pt1.z > pt2.z )
        ZValue = pt2.z + diffPtSearch;
    else
        ZValue = pt1.z + diffPtSearch;
        
    return ZValue;
}

AcGePoint3d getMeanPt( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 )
{
    return AcGePoint3d( ( pt1.x + pt2.x ) / 2, ( pt1.y + pt2.y ) / 2, ( pt1.z + pt2.z ) / 2 );
}

bool isInsideBox( const AcGePoint3d& pt,
    const AcDbExtents& box )
{
    //Recuperer les deux points limites du box
    AcGePoint3d ptBottomLeft = box.minPoint();
    AcGePoint3d ptTopRight = box.maxPoint();
    
    //Tester si le point est dans le box
    if( pt.x >= ptBottomLeft.x && pt.x <= ptTopRight.x
        && pt.y >= ptBottomLeft.y && pt.y <= ptTopRight.y )
        return true;
        
    //Sinon retourner falkse par d�faut
    return false;
}
