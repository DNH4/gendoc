/**
 * @file blockEntity.h
 * @author Romain LEMETTAIS
 * @brief Fichier contenant des fonctions utiles pour travailler avec les blocs
 */


#pragma once
#include <dbents.h>
#include "selectEntity.h"
#include "pointEntity.h"
#include "progressBar.h"
#include "dataBase.h"
#include "layer.h"
#include "circleEntity.h"
#include "acString.h"
#include <dbgrip.h>

#include "dbdynblk.h"

/** \brief Fait une selection quelconque de blocs dans le dessin
  * \param ssName Pointeur sur la selection
  * \param layerName Calque dans lequel on va selectionner le bloc
  * \param blockName Nom du bloc a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsBlock(
    ads_name&         ssName,
    const AcString&  layerName = "",
    const AcString&  blockName = "" );



/** \brief Selectionne un seul bloc dans le dessin
  * \param ssName Pointeur sur la selection
  * \param layerName Calque dans lequel on va selectionner le bloc
  * \param blockName Nom du bloc a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsOneBlock(
    ads_name&          ssName,
    const AcString&   layerName = "",
    const AcString&   blockName = "" );



/** \brief Selectionne tous les blocs dans le dessin
  * \param ssName Pointeur sur la selection
  * \param layerName Calque dans lequel on va selectionner le bloc
  * \param blockName Nom du bloc a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsAllBlock(
    ads_name&         ssName,
    const AcString&  layerName = "",
    const AcString&  blockName = "" );



/** \brief Renvoie le bloc numero N de la selection
  * \brief Renvoie un pointeur sur le bloc numero N dans la selection
  * \param ssName : Pointeur sur la selection
  * \param iObject: Index dans la selection du bloc a recuperer, par defaut 0
  * \param opMode: Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \return Pointeur sur le bloc
  */
AcDbBlockReference* getBlockFromSs(
    const ads_name& ssName,
    const long& iObject = 0,
    const AcDb::OpenMode opMode = AcDb::kForRead );



/**
  * \brief Renvoie un pointeur en LECTURE sur le bloc numero N dans la selection
  * \param ssName : Pointeur sur la selection
  * \param iObject: Index dans la selection du bloc a recuperer, par defaut 0
  * \param return: Pointeur sur le bloc
  */
AcDbBlockReference* readBlockFromSs(
    const ads_name& ssName,
    const long iObject = 0 );



/**
  * \brief Renvoie un pointeur en ECRITURE sur le bloc numero N dans la selection
  * \param ssName : Pointeur sur la selection
  * \param iObject: Index dans la selection du bloc a recuperer, par defaut 0
  * \param return: Pointeur sur le bloc
  */
AcDbBlockReference* writeBlockFromSs(
    const ads_name& ssName,
    const long& iObject = 0 );


/**
  * \brief Renvoie un pointeur sur la polyligne de la d�finition du bloc
  * \param pBlock Pointeur sur la r�f�rence du bloc en question
  * \return Pointeur sur la polyligne de d�finition du bloc
  */
AcDbPolyline* getPolyMemberFromBlock( AcDbBlockReference* pBlock );


/**
  * \brief Renvoie l'objet attribut qui a ete demande
  * \param blockRef Pointeur sur la reference du bloc
  * \param attName Nom de l'attribut
  * \param opMode: Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \return Pointeur sur un objet AutoCAD
  */
AcDbAttribute* getAttributObject(
    AcDbBlockReference* blockRef,
    const AcString& attName,
    const AcDb::OpenMode& opMode = AcDb::kForRead );



/**
  * \brief Renvoie la valeur de l'attribut qui a ete demande
  * \param blockRef Pointeur sur la reference de bloc
  * \param attName Nom de l'attribut
  * \return Renvoie une valeur string
  */
AcString getAttributValue(
    AcDbBlockReference*   blockRef,
    const AcString&        attName );



/**
  * \brief Recuperer l'attribut d'un bloc sous forme de string
  * \param blockRef Pointeur sur la reference de bloc
  * \param attName  Nom de l'attribut
  * \param attValue Valeur de l'attribut � recuperer
  * \return true si l'attribut est bien un AcString
  */
bool getStringAttributValue(
    AcDbBlockReference*   blockRef,
    const AcString&       attName,
    AcString&             attValue );



/**
  * \brief Recuperer l'attribut d'un bloc sous forme de double
  * \param blockRef Pointeur sur la reference de bloc
  * \param attName  Nom de l'attribut
  * \param attValue Valeur de l'attribut � recuperer
  * \return true si l'attribut est bien un double
  */
bool getDoubleAttributValue(
    AcDbBlockReference*   blockRef,
    const AcString&       attName,
    double&               attValue );



/**
  * \brief Recuperer l'attribut d'un bloc sous forme de int
  * \param blockRef Pointeur sur la reference de bloc
  * \param attName  Nom de l'attribut
  * \param attValue Valeur de l'attribut � recuperer
  * \return true si l'attribut est bien un double
  */
bool getIntAttributValue(
    AcDbBlockReference*   blockRef,
    const AcString&       attName,
    int&                  attValue );



/**
  * \brief Modifie la valeur de l'attribut demande
  * \param blockRef Pointeur sur la reference de bloc
  * \param attName Nom de l'attribut
  * \param value Nouvelle valeur de l'attribut
  * \return renvoie true si la valeur a ete correctement changee, false s'il y a eu un probleme
  */
bool setAttributValue(
    AcDbBlockReference*      blockRef,
    const AcString&           attName,
    const AcString&           value );



/**
  * \brief Modifie la valeur de l'attribut demande
  * \param blockRef Pointeur sur la reference de bloc
  * \param attName Nom de l'attribut
  * \param value Nouvelle valeur de l'attribut
  * \param iAttDec Format d'unite dans lequel seront exprimees les valeurs dans AutoCAD
  * \return renvoie true si la valeur a ete correctement changee, false s'il y a eu un probleme
  */
bool setAttributValue(
    AcDbBlockReference*   blockRef,
    const AcString&        attName,
    const double&          value,
    const int&             iAttDec = 8 );


/**
  * \brief Modifie la valeur de la propriete demandee
  * \param pBlkRef Pointeur sur la reference de bloc
  * \param propertyName Nom de l'attribut
  * \param value Nouvelle valeur de la propriete en double
  */
Acad::ErrorStatus setPropertyFromBlock(
    AcDbBlockReference*    pBlkRef,
    const AcString&        propertyName,
    const double&          value );


/**
  * \brief Modifie la valeur de la propriete demandee
  * \param pBlkRef Pointeur sur la reference de bloc
  * \param propertyName Nom de l'attribut
  * \param value Nouvelle valeur de la propriete en Acstring
  */
Acad::ErrorStatus setPropertyFromBlock(
    AcDbBlockReference*    pBlkRef,
    const AcString&        propertyName,
    const AcString&          value );

/**
  * \brief Modifie la valeur de la propriete demandee
  * \param pBlkRef Pointeur sur la reference de bloc
  * \param propertyName Nom de l'attribut
  * \param value Nouvelle valeur de la propriete en int
  */
Acad::ErrorStatus setPropertyFromBlock(
    AcDbBlockReference*    pBlkRef,
    const AcString&        propertyName,
    const AcString&          value );


/**
  * \brief Renvoie la valeur de la propriete demandee
  * \param pBlkRef Pointeur sur la reference de bloc
  * \param propertyName Nom de la propriete
  * \return Renvoie la valeur de la propriete, un double en l'occurence
  */
double getPropertyFromBlock(
    AcDbBlockReference* pBlkRef,
    const AcString& propertyName );


/**
  * \brief Renvoie la valeur de la propriete demandee
  * \param pBlkRef Pointeur sur la reference de bloc
  * \param propertyName Nom de la propriete
  * \return Renvoie la valeur de la propriete, un AcString en l'occurence
  */
AcString getPropertyStrFromBlock(
    AcDbBlockReference* pBlkRef,
    const AcString& propertyName );



/**
  * \brief Donne l'identifiant d'une definition de bloc
  * \param blockName Nom du bloc
  * \return AcDbObjectId de la definition de bloc
  */
AcDbObjectId getBlockDefId(
    const AcString&       blockName );

/**
  * \brief Donne l'identifiant d'une definition de bloc
  * \param blockName Nom du bloc
  * \param database base de données externe
  * \return AcDbObjectId de la definition de bloc
  */
AcDbObjectId getBlockDefId(
    const AcString&       blockName,
    AcDbDatabase* database );




/**
  * \brief Retourne un pointeur sur une definition de bloc en fonction de son nom. SANS DBACCESS
  * \param blockName Nom du bloc
  * \param opMode: Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \return Pointeur sur la definition du bloc
  */
AcDbBlockTableRecord* getBlockDef(
    const AcString& blockName,
    const AcDb::OpenMode&  opMode  = AcDb::kForRead );

/**
  * \brief Retourne un pointeur sur une definition de bloc � partir d'une de ses r�f�rences
  * \param pBlkRef Pointeur vers la r�f�rence
  * \param opMode: Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \return Pointeur sur la definition du bloc
  */
AcDbBlockTableRecord* getBlockDef(
    AcDbBlockReference* pBlkRef,
    const AcDb::OpenMode&  opMode  = AcDb::kForRead );



/**
  * \brief Retourne un pointeur en lecture sur la definition de bloc. SANS DBACCESS
  * \param blockName Nom du bloc
  * \return Pointeur sur la definition du bloc
  */
AcDbBlockTableRecord* readBlockDef(
    const AcString&         blockName );




/**
  * \brief Retourne un pointeur en ecriture sur la definition de bloc. SANS DBACCESS
  * \param blockName Nom du bloc
  * \return Pointeur sur la definition du bloc
  */
AcDbBlockTableRecord* writeBlockDef(
    const AcString&         blockName );


/**
  * \brief Dessine une reference de bloc dans un dessin
  * \param blockName Nom de la definition du bloc
  * \param position Point d'insertion du bloc
  * \param colorIndex Index de la couleur ( 1: Rouge, 2: Jaune, 3: Vert, 4: Cyan, 5: Bleu, 6: Magenta, 256: couleur calque )
  * \param angle ???
  * \return True si le block a bien ete ajoute la base de donnees, false sinon
  */
void drawBlock(
    const AcString& blockName,
    const AcGePoint3d& position,
    const double& angle = 0,
    const int& colorIndex = 256 );

/**
  * \brief Dessine une reference de bloc dans un dessin
  * \param blockName Nom de la definition du bloc
  * \param blockLayer Nom du calque de l'insertion
  * \param position Point d'insertion du bloc
  * \param angle ???
  * \param colorIndex Index de la couleur ( 1: Rouge, 2: Jaune, 3: Vert, 4: Cyan, 5: Bleu, 6: Magenta, 256: couleur calque )
  * \return True si le block a bien ete ajoute la base de donnees, false sinon
  */
void drawBlock(
    const AcString& blockName,
    const AcString& blockLayer,
    const AcGePoint3d& position,
    const double& angle = 0,
    const int& colorIndex = 256 );

/**
  * \brief Ajoute une reference de block dans le dessin en fonction de l'identifiant de sa d�finition
  * \param pBlkRef      Pointeur sur une reference bloc
  * \param blockId    Identifiant de la definition du bloc
  * \param position     Point ou on eut inserer le bloc
  * \param angle        Valeur de l'angle entre l'horizontale et la petite fleche du bloc
  * \param scaleX       Valeur de l'echelle X (1 par defaut)
  * \param scaleY       Valeur de l'echelle Y (= scaleX par defaut)
  * \param scaleZ       Valeur de l'echelle Z (= scaleX par defaut)
  * \return eOk si l'insertion s'est bien passee, false sinon
  */
Acad::ErrorStatus drawBlockReference(
    AcDbBlockReference* pBlkRef,
    AcDbObjectId blockId,
    AcGePoint3d position,
    double angle = 0.0,
    double scaleX = 1.0,
    double scaleY = 0.0,
    double scaleZ = 0.0,
    const AcGeVector3d& vecNormal = AcGeVector3d::kZAxis );


/**
  * \brief Ajoute une r�f�rence de block dans le dessin en fonction du nom de sa d�finition
  * \param pBlkRef      Pointeur sur une reference bloc
  * \param blockName    Nom de la definition du bloc
  * \param position     Point o� on veut inserer le bloc
  * \param angle        Valeur de l'angle entre l'horizontale et la petite fleche du bloc
  * \param scaleX       Valeur de l'echelle X (1 par defaut)
  * \param scaleY       Valeur de l'echelle Y (= scaleX par defaut)
  * \param scaleZ       Valeur de l'echelle Z (= scaleX par defaut)
  * \return eOk si l'insertion s'est bien passee, false sinon
  */
Acad::ErrorStatus drawBlockReference(
    AcDbBlockReference* pBlkRef,
    AcString blockName,
    AcGePoint3d position,
    double angle = 0.0,
    double scaleX = 1.0,
    double scaleY = 0.0,
    double scaleZ = 0.0,
    const AcGeVector3d& vecNormal = AcGeVector3d::kZAxis );


/**
  * \brief Ajoute une reference de block dans le dessin
  * \param blockId      ID de la d�finition � laquelle on ajoute une r�f�rence
  * \param position     Point o� on veut ins�rer le bloc
  * \param angle        Valeur de l'angle entre l'horizontale et la petite fleche du bloc
  * \param scaleX       Valeur de l'echelle X (1 par defaut)
  * \param scaleY       Valeur de l'echelle Y (= scaleX par defaut)
  * \param scaleZ       Valeur de l'echelle Z (= scaleX par defaut)
  * \return Pointeur sur la reference de bloc
  */
AcDbBlockReference* insertBlockReference(
    AcDbObjectId blockId,
    AcGePoint3d position,
    double angle = 0.0,
    double scaleX = 1.0,
    double scaleY = 0.0,
    double scaleZ = 0.0,
    const AcGeVector3d& vecNormal = AcGeVector3d::kZAxis );


/**
  * \brief Ajoute une reference de block dans le dessin
  * \param blockName    Nom de la definition du bloc
  * \param position     Point ou on eut inserer le bloc
  * \param angle        Valeur de l'angle entre l'horizontale et la petite fleche du bloc
  * \param scaleX       Valeur de l'echelle X (1 par defaut)
  * \param scaleY       Valeur de l'echelle Y (= scaleX par defaut)
  * \param scaleZ       Valeur de l'echelle Z (= scaleX par defaut)
  * \return Pointeur sur la reference de bloc
  */
AcDbBlockReference* insertBlockReference(
    AcString blockName,
    AcGePoint3d position,
    double angle = 0.0,
    double scaleX = 1.0,
    double scaleY = 0.0,
    double scaleZ = 0.0,
    const AcGeVector3d& vecNormal = AcGeVector3d::kZAxis );


/**
  * \brief Ajoute une reference de block dans le dessin
  * \param blockName    Nom de la definition du bloc
  * \param blockLayer   Nom de calque du bloc
  * \param position     Point o� on veut ins�rer le bloc
  * \param angle        Valeur de l'angle entre l'horizontale et la petite fleche du bloc
  * \param scaleX       Valeur de l'echelle X (1 par defaut)
  * \param scaleY       Valeur de l'echelle Y (= scaleX par defaut)
  * \param scaleZ       Valeur de l'echelle Z (= scaleX par defaut)
  * \return Pointeur sur la reference de bloc
  */
AcDbBlockReference* insertBlockReference(
    const AcString& blockName,
    const AcString& blockLayer,
    AcGePoint3d position,
    double angle = 0.0,
    double scaleX = 1.0,
    double scaleY = 0.0,
    double scaleZ = 0.0,
    const AcGeVector3d& vecNormal = AcGeVector3d::kZAxis );



/**
  * \brief Ins�rer un bloc dynamique dans le dessin, en passant une liste de propri�t�s et une liste de valeurs, avec la valuer i qui correspond � la valeur de la propri�t� i
  * \param blockName    Nom de la definition du bloc
  * \param position     Point o� on veut ins�rer le bloc
  * \param propNames    Liste des noms des propri�t�s � modifer
  * \param values       Pointeur sur les valeurs des propri�t�s, ATTENTION pour les Acstring il ne faut pas passer des AcString mais des ACHAR* avec la fonction acStrToAcharPointeur
  * \param blockLayer   Nom du calque du bloc
  * \param rotation     Rotation du bloc
  * \param scaleX       Valeur de l'echelle X (1 par defaut)
  * \param scaleY       Valeur de l'echelle Y (= scaleX par defaut)
  * \param scaleZ       Valeur de l'echelle Z (= scaleX par defaut)
  * \return Pointeur sur la reference de bloc, ATTENTION vous �tes responsable de sa fermetureS
  */
AcDbBlockReference* insertDynamicBlockReference( const AcString& blockName,
    const AcGePoint3d& position,
    const AcStringArray& propNames = AcStringArray(),
    vector< void* >& values = vector< void* >( 0, NULL ),
    const AcString& blockLayer = _T( "" ),
    const double& rotation = 0,
    const double& scaleX = 1,
    const double& scaleY = 1,
    const double& scaleZ = 1,
    const AcGeVector3d& vecNormal = AcGeVector3d::kZAxis );


/**
  * \brief Ajoute toute les definitions d'attribut de la definition � la reference
  * \param pBlkRef      Pointeur sur une reference bloc
  * \return True si l'insertion s'est bien passee, false sinon
  */
bool addAllAttributeToBlock(
    AcDbBlockReference* pBlkRef );


/**
  * \brief Modifie les echelle X, Y et Z d'un bloc
  * \param pBlkRef      Pointeur sur une reference bloc
  * \param scaleX       Valeur de l'echelle X
  * \param scaleY       Valeur de l'echelle Y (= scaleX par defaut)
  * \param scaleZ       Valeur de l'echelle Z (= scaleX par defaut)
  * \return True si l'insertion s'est bien passee, false sinon
  */
bool setScaleFactors(
    AcDbBlockReference* pBlkRef,
    double scaleX,
    double scaleY = 0.0,
    double scaleZ = 0.0 );


/**
  * \brief Renvoie le AcDbBlockTable du dessin courant
  * \param opMode: Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \return AcDbBlockTable du dessin courant
  */
AcDbBlockTable* getBlockTable(
    const AcDb::OpenMode& opMode = AcDb::kForRead );

/**
  * \brief Renvoie le AcDbBlockTable du dessin externe
  * \param opMode: Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \param database: base de données externe
  * \return AcDbBlockTable du dessin courant
  */
AcDbBlockTable* getBlockTable(
    const AcDb::OpenMode& opMode,
    AcDbDatabase* database );

/**
  * \brief Copie la definition d'un dessin externe vers le dessin actuel. Ne fait rien si le bloc existe deja
  * \param sFilePath    fichier DWG ou DWT
  * \param sBlockName   Nom du bloc
  * \return L'ID de la d�finition de bloc
  */
AcDbObjectId importExternBlockDefinition(
    const AcString& sFilePath,
    const AcString& sBlockName );

/**
  * \brief Modifie le point d'insertion d'un bloc en deplacant tous les attributs
  * \param obBlockRef   pointeur sur le bloc � modifier
  * \param ptInsert     nouveau point d'insertion
  * \return True si �a fonctionne
  */
bool setBlockPosition(
    AcDbBlockReference* obBlockRef,
    const AcGePoint3d&  ptInsert );

/**
  * \brief Modifie le rotation d'un bloc en deplacant tous les attributs
  * \param obBlockRef   Pointeur sur le bloc � modifier
  * \param dRotation    Nouvelle rotation du bloc
  * \return True si ca a fonctionne
  */
bool setBlockRotation(
    AcDbBlockReference* obBlockRef,
    const double&       dRotation );

/**
  * \brief Modifie la position et la rotation d'un bloc en deplacant tous les attributs
  * \param obBlockRef   Pointeur sur le bloc � modifier
  * \param ptInsert     Nouveau point d'insertion
  * \param dRotation    Nouvelle rotation du bloc
  * \return True si ca a fonctionne
  */
bool setBlockPositionRotation(
    AcDbBlockReference* obBlockRef,
    const AcGePoint3d&  ptInsert,
    const double&       dRotation );

/**
  * \brief Recuperer le nom d'un bloc statique
  * \param name         nom du bloc � recuperer
  * \param obBlockRef   Pointeur sur le bloc
  * \return True si ca a fonctionne
  */
ErrorStatus getStaticBlockName(
    AcString& name,
    AcDbBlockReference* obBlockRef );

/**
  * \brief Recuperer le nom d'un bloc statique
  * \param name         nom du bloc � recuperer
  * \param idBlockRecord   Id de la d�finition du bloc
  * \return True si ca a fonctionne
  */
ErrorStatus getStaticBlockName(
    AcString& name,
    const AcDbObjectId& idBlockRecord );

/**
  * \brief Recuperer le nom d'un bloc dynamique
  * \param name         nom du bloc à recuperer
  * \param block   Pointeur sur le bloc
  * \ return ErrorStatus
  */
ErrorStatus getDynamicBlockName( AcString& name,
    AcDbBlockReference* block );

/**
  * \brief Recuperer le nom d'un bloc
  * \param name         nom du bloc � recuperer
  * \param idBlockRecord   Id de la d�finition du bloc
  * \return True si ca a fonctionne
  */
ErrorStatus getBlockName(
    AcString& name,
    AcDbBlockReference* block );

/**
  * \brief Recuperer les noms de tous les blocs du dessin
  * \return Liste des noms de tous les blocs
  */
vector <AcString> getBlockList();

/**
  * \brief Recuperer la liste des attributs et ses valeurs
  * \param bRef ???
  * \return std::map<AcString, AcString> contenant la liste des attributs et leur valeur
  */
map <AcString, AcString> getBlockAttWithValuesList( AcDbBlockReference* bRef );


/**
  * \brief Recuperer le point d'insertion de tous les bloc dans un calque
  * \param layerName Le nom du calque des blocks
  * \return Vecteur contenant le point d'insertion de tous les blocks du calque
  */
std::vector<AcGePoint3d> getInsertPointOfBlockInLayer( const AcString& layerName );


/**
  * \brief
  * \param
  * \return
  */
std::vector<AcGePoint3d> getInsertPointOfBlockInLayer( const AcString& layerName,
    std::vector<double>& vectX );



/**
  * \brief Recuperer le point d'insertion de tous les bloc dans une selection
  * \param ssBlock Selection sur les bloc
  * \return Vecteur contenant le point d'insertion de tous les blocks du selection
  */
std::vector<AcGePoint3d> getInsertPointOfSSBlock( const ads_name& ssBlock );



/**
  * \brief Recuperer le point d'insertion de tous les bloc dans une selection et met les points dans deux vecteurs ( l'un avec z = 0, et l'autre z > 0 )
  * \param layerName Selection sur les bloc
  * \param vecZ Vecteur sortie contenant les points avec les points avec Z > 0
  * \param vec0 Vecteur sortie contenant les points avec les points avec Z < 0
  * \return ErrorStatus
  */
Acad::ErrorStatus getInsertPointOfSSBlock( const ads_name& ssBlock,
    std::vector<AcGePoint3d>& vecZ,
    std::vector<AcGePoint3d>& vec0 );


/**
  * \brief Recuperer le point d'insertion de tous les bloc dans un calque
  * \param layerName Le nom du calque des blocks
  * \return AcGePoint3dArray contenant le point d'insertion de tous les blocks du calque
  */
AcGePoint3dArray getInsertPointArrayOfBlockInLayer( const AcString& layerName );

/**
  * \brief
  * \param
  * \return
  */
AcGePoint3dArray getInsertPointArrayOfBlockInLayer( const AcString& layerName,
    std::vector<double>& vectX );


/**
  * \brief Recuperer le point d'insertion de tous les bloc dans une selection
  * \param ssBlock ???
  * \param arPt ???
  * \param vecX ???
  * \return ErrorStatus
  */
Acad::ErrorStatus getInsertPointArrayOfSSBlock( const ads_name& ssBlock,
    AcGePoint3dArray& arPt,
    vector<double>& vecX,
    const bool& is2D = false );


/**
  * \brief Trie les blocks topo pour enlever les doublons
  * \param ssBlock S�lection de blocs
  * \param xPos Vecteur contenant les x des points d'insertion des bloc topo dans un calque defini
  * \param blockPos Point d'insertion des blocs dans un calque defini
  * \param blockIds ObjectIds des blocs dans un calque defini
  * \return ErrorStatus
  */
Acad::ErrorStatus prepareBlocks(
    ads_name& ssBlock,
    vector< double >& xPos,
    AcGePoint3dArray& blockPos,
    AcDbObjectIdArray& blockIds );


/**
  * \brief Permet de supprimer les doublons de blocs topo appartenant � un calque defini
  * \param xPos Vecteur tri� contenant les x des points d'insertion des bloc topo dans un calque defini
  * \param blockPos Point d'insertion tri� des blocs dans un calque defini
  * \param blockIds ObjectIds tri� des blocs dans un calque defini
  * \param precision Tolerance en XYZ
  * \return ErrorStatus
  */
Acad::ErrorStatus cleanDoublons( vector<double>& xPos,
    AcGePoint3dArray& blockPos,
    AcDbObjectIdArray& blockIds,
    const double& precision = 0.01 );


/**
  * \brief Garde un seul point topo si jamais il y a 2 points topo avec le m�me (x,y) mais avec des z diff�rents
  * \param xPos Vecteur tri� contenant les x des points d'insertion des bloc topo
  * \param blockPos Point d'insertion tri� des blocs
  * \param blockIds ObjectIds tri�s des blocs dans un calque defini
  * \param tolXY Tolerance en XY
  * \param tolZ Tolerance en Z
  * \return ErrorStatus
  */
Acad::ErrorStatus cleanDoublonsWithZ( vector<double>& xPos,
    AcGePoint3dArray& blockPos,
    AcDbObjectIdArray& blockIds,
    const double& tolXY = 0.01,
    const double& tolZ = 0.01 );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus
openBlock( const AcDbObjectId& idBlock,
    AcDbBlockReference*& block,
    const AcDb::OpenMode& mode = AcDb::kForRead );

/**
  * \brief Ouvre un attribut (selon mode d'ouverture) en fonction de son ID
  * \param attributeId ID de l'attribut � ourir
  * \param pAtt Pointeur vers l'attribut
  * \param mode Mode d'ouverture
  * \return ErrorStatus
  */
Acad::ErrorStatus
openAttribute( const AcDbObjectId& attributeId,
    AcDbAttribute*& AcDbAttribute,
    const AcDb::OpenMode& mode = AcDb::kForRead );

/**
  * \brief Ouvre un attribut de d�finition (selon mode d'ouverture) en fonction de son ID
  * \param attributeId ID de l'attribut de d�finition � ourir
  * \param pAtt Pointeur vers l'attribut de d�finition
  * \param mode Mode d'ouverture
  * \return ErrorStatus
  */
Acad::ErrorStatus
openAttributeDefinition( const AcDbObjectId& attributeDefId,
    AcDbAttributeDefinition*& pAttDef,
    const AcDb::OpenMode& mode = AcDb::kForRead );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus cleanSelectionFromDoublons(
    ads_name& ss );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus cleanSelectionFromDoublonsXY(
    ads_name& ssBlocToKeep, ads_name& ssotherbloc, int& deleted );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus cleanDoublons( const AcGePoint3d& ptSearch, vector<double>& xPos,
    AcGePoint3dArray& blockPos,
    AcDbObjectIdArray& blockIds,
    const double& precision = 0.0 );

/**
  * \brief
  * \param
  * \return
  */
AcDbBlockReference* getClosestBlock( const AcGePoint3d& circleCenter,
    const ads_name& ssBlock,
    const AcDb::OpenMode& opMode = AcDb::kForRead );

/**
  * \brief
  * \param
  * \return
  */
AcGePoint3d getProjectionOnBlock( AcDbBlockReference* block,
    const AcGePoint3d& pointToProject );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus exportBlocks( const AcString& blockName,
    const AcString& newFile );

/**
  * \brief Pour un bloc r�f�rence donn�, renvoie la liste des noms de ses attributs
  * \param block3D Pointeur vers la r�f�rence de bloc
  * \return std::vector<AcString> contenant le nom des attributs
  */
vector<AcString> getAttributesNamesListOfBlockRef( AcDbBlockReference* block3D );

/**
  * \brief Pour une d�finition de bloc donn�e, renvoie la liste des noms de ses attributs
  * \param block3D Pointeur vers la d�finition de bloc
  * \return std::vector<AcString> contenant le nom des attributs
  */
vector<AcString> getAttributesNamesListOfBlockDef( AcDbBlockTableRecord* pBlkDef );

/**
  * \brief Pour un bloc r�f�rence donn�, renvoie la liste des noms des attributs de sa d�finition
  * \param block3D Pointeur vers la r�f�rence de bloc
  * \return std::vector<AcString> contenant le nom des attributs de la d�finition
  */
vector<AcString> getAttributesNamesListOfBlockDef( AcDbBlockReference* pBlkRef );

/**
  * \brief Pour un bloc r�f�rence donn�, renvoie une map de ses attributs contenant l'identifiant de l'attribut (cl�) et son nom
  * \param pBlkRef Pointeur vers la r�f�rence de bloc
  * \return std::map<AcString, AcDbObjectId> contenant la map des attributs
  */
std::map<AcString, AcDbObjectId> getAttributesRef( AcDbBlockReference* pBlkRef );

/**
  * \brief Pour une d�finition de bloc donn�e, renvoie une map de ses attributs contenant l'identifiant de l'attribut (cl�) et son nom
  * \param pBlkDef Pointeur vers la d�finition de bloc
  * \return std::map<AcString, AcDbObjectId> contenant la map des attributs
  */
std::map<AcString, AcDbObjectId> getAttributesDef( AcDbBlockTableRecord* pBlkDef );

/**
  * \brief Recuperer la polyligne fermee dans la definition du bloc
  * \param polyClosed la polyligne fermee a recuperer
  * \param blockRef la reference de bloc
  * \return ErrorStatus
  */
Acad::ErrorStatus getClosedPolyBlock( AcDb3dPolyline*& polyClosed, AcDbBlockReference* blockRef );


/**
  * \brief Recuperer la polyligne fermee dans la definition du bloc
  * \param polyClosed la polyligne fermee a recuperer
  * \param blockRef la reference de bloc
  * \return ErrorStatus
  */
Acad::ErrorStatus getClosedPolyBlock( AcDb3dPolyline*& polyClosed, AcGePoint3dArray& ptarray,  AcDbBlockReference* blockRef );

/**
  * \brief
  * \param
  * \return
  */
AcDbBlockReference* insertBlockExternFile(
    AcDbBlockReference* pBlock,
    const AcString& sFilePath = "",
    AcDbDatabase* pDB = NULL );

/**
  * \brief
  * \param
  * \return
  */
AcDbBlockReference* insertBlockExternFile(
    const AcString& pBlockName,
    const AcGePoint3d& pPosition,
    const AcString& layer,
    const double angle = 0.0,
    const AcString& sFilePath = "",
    AcDbDatabase* pDB = NULL );

/**
    \brief Synchronise un attribut (en fonction de son nom) entre une r�f�rence et une d�finition.
    \brief Si l'attribut est pr�sent dans la r�f�rence mais pas dans la d�finition, il est effac� de la r�f�rence.
    \brief Si l'attribut est pr�sent dans la d�finition mais pas dans la r�f�rence, il est ajout� � la r�f�rence.
    \brief Si l'attribut est pr�sent dans la d�finition et dans la r�f�rence, il est mis � jour dans la r�f�rence avec la valeur de la d�finition.
    \param pBlkRef Pointeur vers la r�f�rence �tudi�e
    \param attName Nom de l'attribut �tudi�
    \param attDefMap Map des attributs de la d�finition correspondant � la r�f�rence contenant les informations nom et ID des attributs.
    \return ErrorStatus
  */
Acad::ErrorStatus synchronizeAttribute(
    AcDbBlockReference* pBlkRef,
    const AcString& attName,
    const map< AcString, AcDbObjectId >& attDefMap );

/**
  * \brief Pour un bloc r�f�rence donn�, met � jour un de ses attributs en fonction d'un attribut de d�finition de bloc
  * \param pBlkRef Pointeur vers la r�f�rence de bloc
  * \param refAttId ID de l'attribut de la r�f�rence
  * \param defAttId ID de l'attribut de la d�finition
  * \return ErrorStatus
  */
Acad::ErrorStatus updateAtt(
    AcDbBlockReference* pBlkRef,
    AcDbObjectId refAttId,
    AcDbObjectId defAttId );

/**
  * \brief Supprime un attribut caract�ris� par son ID
  * \param refAttId ID de l'attribut de la r�f�rence
  * \return ErrorStatus
  */
Acad::ErrorStatus removeAtt(
    AcDbObjectId refAttId );

/**
  * \brief Pour un bloc r�f�rence donn�, ajoute un nouvel attribut en fonction d'un attribut de d�finition de bloc
  * \param pBlkRef Pointeur vers la r�f�rence de bloc
  * \param defAttId ID de l'attribut de la d�finition
  * \return ErrorStatus
  */
Acad::ErrorStatus createAtt(
    AcDbBlockReference* pBlkRef,
    AcDbObjectId defAttId );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus insertBlockWithJig(
    int& res,
    const AcString& blockName,
    const double& scaleX,
    const double& scaleY,
    const double& rotation = -1 );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus getLastBlockInserted( AcDbBlockReference*& pBlock,
    const AcDb::OpenMode& opMode = AcDb::kForRead );

/**
 * \brief   Supprime le dernier bloc inséré
 * \returns ErrorStatus
 */
Acad::ErrorStatus eraseLastInseredBlock();

/**
  * \brief Pour v�rifier si une r�f�rence de bloc est un bloc dynamique
  * \param pBlock Pointeur vers la r�f�rence de bloc
  * \return bool; true si c'est un bloc dynamique, false sinon
  */
bool isDynamicBlock( AcDbBlockReference* pBlock );


/**
  * \brief Permet de recuperer les proprietes d'un bloc avec leur valeur
  * \param pBlock Le bloc
  * \param propName Vecteur contenant les nom des proprietes
  * \param propValue Vecteur contenant les valeurs des proprietes
  * \return ErrorStatus
  */
Acad::ErrorStatus getBlockPropWithValueList(
    AcDbBlockReference* pBlock,
    AcStringArray& propName,
    vector<void*>& propValue );


/**
  * \brief Permet de recuperer les proprietes d'un bloc avec leur valeur
  * \param pBlock Le bloc
  * \param propName Vecteur de AcString contenant les nom des proprietes
  * \param propValue Vecteur de AcString contenant les valeurs des proprietes, ces proprietes doivent etre castés selon le type de propriete demande par le bloc
  * \return ErrorStatus
  */
Acad::ErrorStatus getBlockPropWithValueList(
    AcDbBlockReference* pBlock,
    AcStringArray& propName,
    AcStringArray& propValue );

/**
  * \brief Permet de
  * \param
  * \return ErrorStatus
  */
Acad::ErrorStatus setLayerOfAttribute(
    AcDbBlockReference* pBlock,
    const AcString& attributeName,
    const AcString& layerName );
