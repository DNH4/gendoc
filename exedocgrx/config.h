#pragma once
#include <tchar.h>
#include "acString.h"

#define VERSION_FOLDER_STR "C:\\Futurmap\\Outils\\GStarCAD2020\\"
#define DEV_VERSION_FOLDER_STR "C:\\Futurmap\\Dev-Outils\\GStarCAD2020\\"
#define GABARIT_FOLDER_STR "C:\\Futurmap\\Outils\\Gabarit-DWG\\"
#define GSTAR_SOFT "C:\\Program Files\\Gstarsoft\\GstarCAD2020\\"