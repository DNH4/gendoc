#include "geometry.h"
#include "print.h"
#include "fmapMath.h"
#include <math.h>


#ifndef M_PI
    #define M_PI       3.14159265358979323846
#endif

vector< AcGePoint3d > getVertexTrigonometricSense( AcDbPolyline* poly )
{
    // vecteur de sortie avec les points de la polyligne ranges dans l'odre trigo
    vector< AcGePoint3d > vertex;
    vertex.resize( 0 );
    
    // true si la poly est deja dans le sens trigo
    bool trigo;
    
    // on recupere le nombre de sommets de la polyligne
    int size = poly->numVerts();
    
    AcGePoint3d ptA, ptB, ptC, pt;
    int indexA, indexB, indexC;
    
    // on initialise B avec le p1er point de la poly
    poly->getPointAt( 0, ptB );
    indexB = 0;
    
    // on recherche le point le plus haut
    for( int i = 0; i < size; i++ )
    {
        poly->getPointAt( i, pt );
        
        if( pt.y > ptB.y )
        {
            ptB = pt;
            indexB = i;
        }
    }
    
    indexA = indexB - 1;
    indexC = indexB + 1;
    
    if( indexB == 0 )
    {
        indexA = size - 1;
        indexC = 1;
    }
    
    if( indexB == size - 1 )
    {
        indexA = size - 2;
        indexC = 0;
    }
    
    // on definit les vecteurs unitaires BA et BC
    poly->getPointAt( indexA, ptA );
    poly->getPointAt( indexC, ptC );
    AcGeVector2d vecBA = AcGeVector2d( ptA.x - ptB.x, ptA.y - ptB.y ).normal();
    AcGeVector2d vecBC = AcGeVector2d( ptC.x - ptB.x, ptC.y - ptB.y ).normal();
    
    // si A est a gauche de B la poly est dans le sens horaire
    if( vecBA.x < vecBC.x )
        trigo = false;
        
    else
        trigo = true;
        
    // si la poly est deja trigo on rajoute juste les points dans le vecteur
    if( trigo )
    {
        for( int i = 0; i < size; i++ )
        {
            poly->getPointAt( i, pt );
            vertex.push_back( pt );
        }
    }
    
    // Sinon il faut les ajouter dans le sens inverse
    else
    {
        for( int i = size - 1; i >= 0; i-- )
        {
            poly->getPointAt( i, pt );
            vertex.push_back( pt );
        }
    }
    
    return vertex;
}

vector< AcGePoint3d > getVertexTrigonometricSense( AcDbPolyline* poly, bool& trigo )
{
    // vecteur de sortie avec les points de la polyligne ranges dans l'odre trigo
    vector< AcGePoint3d > vertex;
    vertex.resize( 0 );
    
    // true si la poly est deja dans le sens trigo
    
    
    // on recupere le nombre de sommets de la polyligne
    int size = poly->numVerts();
    
    AcGePoint3d ptA, ptB, ptC, pt;
    int indexA, indexB, indexC;
    
    // on initialise B avec le p1er point de la poly
    poly->getPointAt( 0, ptB );
    indexB = 0;
    
    // on recherche le point le plus haut
    for( int i = 0; i < size; i++ )
    {
        poly->getPointAt( i, pt );
        
        if( pt.y > ptB.y )
        {
            ptB = pt;
            indexB = i;
        }
    }
    
    indexA = indexB - 1;
    indexC = indexB + 1;
    
    if( indexB == 0 )
    {
        indexA = size - 1;
        indexC = 1;
    }
    
    if( indexB == size - 1 )
    {
        indexA = size - 2;
        indexC = 0;
    }
    
    // on definit les vecteurs unitaires BA et BC
    poly->getPointAt( indexA, ptA );
    poly->getPointAt( indexC, ptC );
    AcGeVector2d vecBA = AcGeVector2d( ptA.x - ptB.x, ptA.y - ptB.y ).normal();
    AcGeVector2d vecBC = AcGeVector2d( ptC.x - ptB.x, ptC.y - ptB.y ).normal();
    
    // si A est a gauche de B la poly est dans le sens horaire
    //La taille du polyligne doit etre superieur a deux
    if( size > 2 )
    {
        if( vecBA.x < vecBC.x )
            trigo = false;
            
        else
            trigo = true;
    }
    
    // si la poly est deja trigo on rajoute juste les points dans le vecteur
    if( trigo )
    {
        for( int i = 0; i < size; i++ )
        {
            poly->getPointAt( i, pt );
            vertex.push_back( pt );
        }
    }
    
    // Sinon il faut les ajouter dans le sens inverse
    else
    {
        for( int i = size - 1; i >= 0; i-- )
        {
            poly->getPointAt( i, pt );
            vertex.push_back( pt );
        }
    }
    
    return vertex;
}

bool isVertexInTrigoSens( AcGePoint3dArray& ptA, const int& choise )
{
    bool trigo = false;
    AcDbPolyline* poly = new AcDbPolyline();
    
    int i = 0;
    AcGePoint3dArray ptInverse;
    
    //Ajout des sommets
    for( AcGePoint3d p : ptA )
    {
        poly->addVertexAt( i, AcGePoint2d( p.x, p.y ) );
        i++;
        ptInverse.insert( ptInverse.begin(), p );
    }
    
    getVertexTrigonometricSense( poly, trigo );
    poly->close();
    
    if( trigo && choise == 2 || !trigo && choise == 1 )
        ptA = ptInverse;
        
    return trigo;
}

bool isPointOnLine( const AcGePoint2d& ptA, const AcGePoint2d& ptB, const AcGePoint2d& ptI, const double& precision )
{

    AcGeVector2d vecIA = AcGeVector2d( ptA.x - ptI.x, ptA.y - ptI.y ).normalize();
    AcGeVector2d vecIB = AcGeVector2d( ptB.x - ptI.x, ptB.y - ptI.y ).normalize();
    
    double a = abs( vecIA.crossProduct( vecIB ) );
    double b = vecIA.dotProduct( vecIB );
    
    
    if( a < precision && b < 0 )
        return true;
        
    else
        return false;
}


bool isPointOnLine( const AcGePoint3d& ptA, const AcGePoint3d& ptB, const AcGePoint3d& ptI, const double& precision )
{

    AcGeVector3d vecIA = AcGeVector3d( ptA.x - ptI.x, ptA.y - ptI.y, ptA.z - ptI.z ).normalize();
    AcGeVector3d vecIB = AcGeVector3d( ptB.x - ptI.x, ptB.y - ptI.y, ptA.z - ptI.z ).normalize();
    
    double a = abs( ( vecIA.crossProduct( vecIB ) ).length() );
    double b = vecIA.dotProduct( vecIB );
    
    if( a < precision && b < 0 )
        return true;
        
    else
        return false;
}


bool isPointOnLine3D( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& tolXY,
    const double& tolZ )
{
    //Tester si ptI est sur le segment ptA ptB en 2d
    if( isPointOnLine( getPoint2d( ptA ), getPoint2d( ptB ), getPoint2d( ptI ), tolXY ) )
    {
        //Recuperer la distance entre les deux points et le point central
        double d1 = getDistance2d( ptA, ptI );
        double d2 = getDistance2d( ptB, ptI );
        
        //Recuperer le z moyen
        double moy = ( ptB.z * d1 + ptA.z * d2 ) / ( d1 + d2 );
        
        //Tester si le z du bloc est sur le z moyen des deux points
        if( isTheSame( ptI.z, moy, tolZ ) )
            return true;
    }
    
    //Retourner false
    return false;
}


double distanceTo( AcGePoint2d ptA, AcGePoint2d ptB, AcGePoint2d ptC )
{

    AcGeVector2d vecAC = AcGeVector2d( ptC.x - ptA.x, ptC.y - ptA.y );
    AcGeVector2d vecBC = AcGeVector2d( ptC.x - ptB.x, ptC.y - ptB.y );
    AcGeVector2d vecAB = AcGeVector2d( ptB.x - ptA.x, ptB.y - ptA.y );
    AcGeVector2d vecBA = AcGeVector2d( ptA.x - ptB.x, ptA.y - ptB.y );
    
    if( vecAC.dotProduct( vecAB ) < 0 || vecBC.dotProduct( vecBA ) < 0 )
        return min( ptC.distanceTo( ptA ), ptC.distanceTo( ptB ) );
        
    double projDir = vecAC.dotProduct( vecAB.normalize() );
    
    double hypotenuse = vecAC.lengthSqrd();
    
    double l = hypotenuse - pow( projDir, 2 );
    
    if( l <= 0 )
        return 0;
        
    return sqrt( l );
}

double distance2DTo( const AcGePoint3d& ptA, const AcGePoint3d& ptB, const AcGePoint3d& ptC )
{

    AcGeVector2d vecAC = AcGeVector2d( ptC.x - ptA.x, ptC.y - ptA.y );
    AcGeVector2d vecBC = AcGeVector2d( ptC.x - ptB.x, ptC.y - ptB.y );
    AcGeVector2d vecAB = AcGeVector2d( ptB.x - ptA.x, ptB.y - ptA.y );
    AcGeVector2d vecBA = AcGeVector2d( ptA.x - ptB.x, ptA.y - ptB.y );
    
    if( vecAC.dotProduct( vecAB ) < 0 || vecBC.dotProduct( vecBA ) < 0 )
        return min( ptC.distanceTo( ptA ), ptC.distanceTo( ptB ) );
        
    double projDir = vecAC.dotProduct( vecAB.normalize() );
    
    double hypotenuse = vecAC.lengthSqrd();
    
    double l = hypotenuse - pow( projDir, 2 );
    
    if( l <= 0 )
        return 0;
        
    return sqrt( l );
}

AcGePoint3d projectionOn( AcGePoint2d ptA, AcGePoint2d ptB, AcGePoint2d ptC )
{

    AcGeVector2d vecAC = AcGeVector2d( ptC.x - ptA.x, ptC.y - ptA.y );
    AcGeVector2d vecBC = AcGeVector2d( ptC.x - ptB.x, ptC.y - ptB.y );
    AcGeVector2d vecAB = AcGeVector2d( ptB.x - ptA.x, ptB.y - ptA.y );
    
    AcGeVector2d projDir = vecAC.dotProduct( vecAB.normalize() ) * vecAB.normalize();
    
    AcGePoint3d pt = AcGePoint3d( ptA.x +  projDir.x, ptA.y +  projDir.y, 0 );
    
    // si la projection tombe pas sur le segment, retourn ( 0, 0, 0 )
    if( ! isPointOnLine( ptA, ptB,  getPoint2d( pt ) ) )
        return AcGePoint3d( 0, 0, 0 );
        
    return pt;
}


AcGePoint3d projectionOn( AcGePoint3d ptA, AcGePoint3d ptB, AcGePoint3d ptC )
{

    AcGeVector3d vecAC = AcGeVector3d( ptC.x - ptA.x, ptC.y - ptA.y, ptC.z - ptA.z );
    AcGeVector3d vecBC = AcGeVector3d( ptC.x - ptB.x, ptC.y - ptB.y, ptC.z - ptA.z );
    AcGeVector3d vecAB = AcGeVector3d( ptB.x - ptA.x, ptB.y - ptA.y, ptC.z - ptA.z );
    
    AcGeVector3d projDir = vecAC.dotProduct( vecAB.normalize() ) * vecAB.normalize();
    
    AcGePoint3d pt = AcGePoint3d( ptA.x +  projDir.x, ptA.y +  projDir.y, ptA.z +  projDir.z );
    
    // si la projection tombe pas sur le segment, retourn ( 0, 0, 0 )
    if( ! isPointOnLine( ptA, ptB, pt ) )
        return AcGePoint3d( 0, 0, 0 );
        
    return pt;
}

AcGePoint3d projectionOn2d( AcGePoint3d ptA, AcGePoint3d ptB, AcGePoint3d ptC )
{

    ptA.z = 0.0;
    ptB.z = 0.0;
    ptC.z = 0.0;
    
    AcGeVector3d vecAC = AcGeVector3d( ptC.x - ptA.x, ptC.y - ptA.y, ptC.z - ptA.z );
    AcGeVector3d vecBC = AcGeVector3d( ptC.x - ptB.x, ptC.y - ptB.y, ptC.z - ptA.z );
    AcGeVector3d vecAB = AcGeVector3d( ptB.x - ptA.x, ptB.y - ptA.y, ptC.z - ptA.z );
    
    AcGeVector3d projDir = vecAC.dotProduct( vecAB.normalize() ) * vecAB.normalize();
    
    AcGePoint3d pt = AcGePoint3d( ptA.x +  projDir.x, ptA.y +  projDir.y, ptA.z +  projDir.z );
    
    // si la projection tombe pas sur le segment, retourn ( 0, 0, 0 )
    if( ! isPointOnLine( ptA, ptB, pt ) )
        return AcGePoint3d( 0, 0, 0 );
        
    return pt;
}

AcGeVector3d getNormal( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptC )
{

    // D�claration des  deux vectuers que 'lon va utiliser pour calculer la normale et la normale
    AcGeVector3d vecAB, vecAC, n;
    vecAB = getVector3d( ptA, ptB );
    vecAC = getVector3d( ptA, ptC );
    
    // Le vecteur normal est le produit vectoriel des deux veteurs
    n = vecAC.crossProduct( vecAB );
    
    // On met le vecteur normal unitaire
    n.normalize();
    
    return n;
}


vector <AcGeVector3d> getRepOrthoNorm( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptC )
{
    //initialiser la listes des trois vecteurs
    vector< AcGeVector3d > newRepere( 0 );
    
    //recuperer la normal du plan
    AcGeVector3d norm = getNormal( ptA, ptB, ptC );
    
    //prenons vecAb comme vecteur directeur du repere du plan
    AcGeVector3d vecX = getVector3d( ptA, ptB );
    
    //cherchons la troiseme vecteur qui est le produit vectoriel entre la normale et vecAb
    AcGeVector3d vecY = norm.crossProduct( vecX );
    
    //completer la liste des trois vecteurs
    newRepere.push_back( vecX );
    newRepere.push_back( vecY );
    newRepere.push_back( norm );
    
    //retourner la nouvelle repere
    return newRepere;
}


AcGePoint3d projectPointOnPlane( const AcGePoint3d& pointInPlan,
    const AcGeVector3d& normal,
    const AcGePoint3d& pointToProject )
{

    AcGeLine3d droite = AcGeLine3d( pointToProject,
            normal );
            
    AcGePlane plan = AcGePlane( pointInPlan,
            normal );
            
    AcGePoint3d projectedPoint;
    
    plan.intersectWith( droite,
        projectedPoint );
        
    return projectedPoint;
    
}


bool isPointOnLineAndInsidesTwoPoints( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& precision,
    const double scaleFactor,
    const bool& withVtx,
    const bool& applyFilter,
    const double& distFilter )
{
    bool result = false;
    // On V�rifie si on prend en compte les vertex
    
    // 1. en prend en compte les vertex
    if( withVtx )
    {
        if( applyFilter )
        {
            if( isEqual3d( ptI, ptA, distFilter ) || isEqual3d( ptI, ptB, distFilter ) )
                return true;
        }
        
        else
        {
            if( isEqual3d( ptI, ptA, precision ) || isEqual3d( ptI, ptB, precision ) )
                return true;
        }
        
    }
    
    else
    {
        if( applyFilter )
        {
            if( isEqual3d( ptI, ptA, distFilter ) || isEqual3d( ptI, ptB, distFilter ) )
                return false;
        }
        
        else
        {
            if( isEqual3d( ptI, ptA, precision ) || isEqual3d( ptI, ptB, precision ) )
                return false;
        }
        
        
        
    }
    
    AcGeVector3d vecIA, vecIB, vecAB;
    
    if( scaleFactor > 1 )
    {
        // On arrondit les valeurs avec sacleFactor chiffres d�cimals
        double xIA = ( long )( ( ptA.x - ptI.x ) * scaleFactor );
        double yIA = ( long )( ( ptA.y - ptI.y ) * scaleFactor );
        double zIA = ( long )( ( ptA.z - ptI.z ) * scaleFactor );
        
        double xIB = ( long )( ( ptB.x - ptI.x ) * scaleFactor );
        double yIB = ( long )( ( ptB.y - ptI.y ) * scaleFactor );
        double zIB = ( long )( ( ptB.z - ptI.z ) * scaleFactor );
        
        double xAB = ( long )( ( ptB.x - ptA.x ) * scaleFactor );
        double yAB = ( long )( ( ptB.y - ptA.y ) * scaleFactor );
        double zAB = ( long )( ( ptB.z - ptA.z ) * scaleFactor );
        
        
        vecIA = AcGeVector3d( ( double )( xIA / scaleFactor ), ( double )( yIA / scaleFactor ), ( double )( zIA / scaleFactor ) ).normalize();
        vecIB = AcGeVector3d( ( double )( xIB / scaleFactor ), ( double )( yIB / scaleFactor ), ( double )( zIB / scaleFactor ) ).normalize();
        vecAB = AcGeVector3d( ( double )( xAB / scaleFactor ), ( double )( yAB / scaleFactor ), ( double )( zAB / scaleFactor ) ).normalize();
    }
    
    else
    {
        vecIA = AcGeVector3d( ( ptA.x - ptI.x ), ( ptA.y - ptI.y ), ( ptA.z - ptI.z ) ).normalize();
        vecIB = AcGeVector3d( ( ptB.x - ptI.x ), ( ptB.y - ptI.y ), ( ptB.z - ptI.z ) ).normalize();
        vecAB = AcGeVector3d( ( ptB.x - ptA.x ), ( ptB.y - ptA.y ), ( ptB.z - ptA.z ) ).normalize();
    }
    
    
    
    double a = abs( ( vecIA.crossProduct( vecIB ) ).length() );//Alignement
    double b = vecIA.dotProduct( vecIB );// � l'int�rieur
    
    long tempA = ( long )( a * scaleFactor );
    
    if( scaleFactor > 1 )
        a = ( double )( tempA / scaleFactor );
        
    if( a <= precision && b < 0 ) // On met inf�rieur ou �gale pour les calculs relatifs �des arc
        return true;
        
    else
        return false;
        
        
    return result;
}

bool isPointOnLineAndInsidesTwoPointsZMean( double& zMean,
    const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& precision,
    const double scaleFactor,
    const bool& withVtx,
    const bool& applyFilter,
    const double& distFilter )
{
    bool result = false;
    // On V�rifie si on prend en compte les vertex
    
    // 1. en prend en compte les vertex
    if( withVtx )
    {
        if( applyFilter )
        {
            if( isEqual3d( ptI, ptA, distFilter ) || isEqual3d( ptI, ptB, distFilter ) )
                return true;
        }
        
        else
        {
            if( isEqual3d( ptI, ptA, precision ) || isEqual3d( ptI, ptB, precision ) )
                return true;
        }
        
    }
    
    else
    {
        if( applyFilter )
        {
            if( isEqual3d( ptI, ptA, distFilter ) || isEqual3d( ptI, ptB, distFilter ) )
                return false;
        }
        
        else
        {
            if( isEqual3d( ptI, ptA, precision ) || isEqual3d( ptI, ptB, precision ) )
                return false;
        }
        
        
        
    }
    
    // Projection du point avant v�rification
    
    AcGeVector3d vecIA, vecIB, vecAB, vecAI;
    
    if( scaleFactor > 1 )
    {
        // On arrondit les valeurs avec sacleFactor chiffres d�cimals
        double xIA = ( long )( ( ptA.x - ptI.x ) * scaleFactor );
        double yIA = ( long )( ( ptA.y - ptI.y ) * scaleFactor );
        double zIA = ( long )( ( ptA.z - ptI.z ) * scaleFactor );
        
        double xIB = ( long )( ( ptB.x - ptI.x ) * scaleFactor );
        double yIB = ( long )( ( ptB.y - ptI.y ) * scaleFactor );
        double zIB = ( long )( ( ptB.z - ptI.z ) * scaleFactor );
        
        double xAB = ( long )( ( ptB.x - ptA.x ) * scaleFactor );
        double yAB = ( long )( ( ptB.y - ptA.y ) * scaleFactor );
        double zAB = ( long )( ( ptB.z - ptA.z ) * scaleFactor );
        
        vecIA = AcGeVector3d( ( double )( xIA / scaleFactor ), ( double )( yIA / scaleFactor ), ( double )( zIA / scaleFactor ) ).normalize();
        vecIB = AcGeVector3d( ( double )( xIB / scaleFactor ), ( double )( yIB / scaleFactor ), ( double )( zIB / scaleFactor ) ).normalize();
        vecAB = AcGeVector3d( ( double )( xAB / scaleFactor ), ( double )( yAB / scaleFactor ), ( double )( zAB / scaleFactor ) ).normalize();
    }
    
    else
    {
        vecIA = AcGeVector3d( ( ptA.x - ptI.x ), ( ptA.y - ptI.y ), ( ptA.z - ptI.z ) ).normalize();
        vecIB = AcGeVector3d( ( ptB.x - ptI.x ), ( ptB.y - ptI.y ), ( ptB.z - ptI.z ) ).normalize();
        vecAB = AcGeVector3d( ( ptB.x - ptA.x ), ( ptB.y - ptA.y ), ( ptB.z - ptA.z ) ).normalize();
    }
    
    AcGeVector2d vecIA2d, vecIB2d, vecAI2d, vecAB2d;
    
    vecIA2d = getVector2d( vecIA );
    vecIB2d = getVector2d( vecIB );
    vecAB2d = getVector2d( vecAB );
    
    double a = abs( vecIA2d.crossProduct( vecIB2d ) ); //Alignement
    
    double b = vecIA2d.dotProduct( vecIB2d );// � l'int�rieur
    
    long tempA = ( long )( a * scaleFactor );
    
    if( scaleFactor > 1 )
        a = ( double )( tempA / scaleFactor );
        
    if( a <= precision && b < 0 ) // On met inf�rieur ou �gale pour les calculs relatifs �des arc
    {
        zMean = getMeanZ( ptA, ptB, ptI );
        return true;
    }
    
    else
        return false;
        
        
    return result;
}


bool isPointOnLineAndInsidesTwoPointsArcZMean( double& zMean,
    const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    const double& precision,
    const double scaleFactor,
    const bool& withVtx,
    const bool& applyFilter,
    const double& distFilter )

{
    bool result = false;
    // On V�rifie si on prend en compte les vertex
    
    // 1. en prend en compte les vertex
    if( withVtx )
    {
        if( applyFilter )
        {
            if( isEqual3d( ptI, ptA, distFilter ) || isEqual3d( ptI, ptB, distFilter ) )
                return true;
        }
        
        else
        {
            if( isEqual3d( ptI, ptA, precision ) || isEqual3d( ptI, ptB, precision ) )
                return true;
        }
        
    }
    
    else
    {
        if( applyFilter )
        {
            if( isEqual3d( ptI, ptA, distFilter ) || isEqual3d( ptI, ptB, distFilter ) )
                return false;
        }
        
    }
    
    AcGeVector3d vecIA, vecIB, vecAB;
    
    if( scaleFactor > 1 )
    {
        // On arrondit les valeurs avec sacleFactor chiffres d�cimals
        double xIA = ( long )( ( ptA.x - ptI.x ) * scaleFactor );
        double yIA = ( long )( ( ptA.y - ptI.y ) * scaleFactor );
        double zIA = ( long )( ( ptA.z - ptI.z ) * scaleFactor );
        
        double xIB = ( long )( ( ptB.x - ptI.x ) * scaleFactor );
        double yIB = ( long )( ( ptB.y - ptI.y ) * scaleFactor );
        double zIB = ( long )( ( ptB.z - ptI.z ) * scaleFactor );
        
        double xAB = ( long )( ( ptB.x - ptA.x ) * scaleFactor );
        double yAB = ( long )( ( ptB.y - ptA.y ) * scaleFactor );
        double zAB = ( long )( ( ptB.z - ptA.z ) * scaleFactor );
        
        
        vecIA = AcGeVector3d( ( double )( xIA / scaleFactor ), ( double )( yIA / scaleFactor ), ( double )( zIA / scaleFactor ) ).normalize();
        vecIB = AcGeVector3d( ( double )( xIB / scaleFactor ), ( double )( yIB / scaleFactor ), ( double )( zIB / scaleFactor ) ).normalize();
        vecAB = AcGeVector3d( ( double )( xAB / scaleFactor ), ( double )( yAB / scaleFactor ), ( double )( zAB / scaleFactor ) ).normalize();
    }
    
    else
    {
        vecIA = AcGeVector3d( ( ptA.x - ptI.x ), ( ptA.y - ptI.y ), ( ptA.z - ptI.z ) ).normalize();
        vecIB = AcGeVector3d( ( ptB.x - ptI.x ), ( ptB.y - ptI.y ), ( ptB.z - ptI.z ) ).normalize();
        vecAB = AcGeVector3d( ( ptB.x - ptA.x ), ( ptB.y - ptA.y ), ( ptB.z - ptA.z ) ).normalize();
    }
    
    AcGeVector2d vecIA2d, vecIB2d;
    
    vecIA2d = getVector2d( vecIA );
    vecIB2d = getVector2d( vecIB );
    
    
    
    
    double a = abs( vecIA2d.crossProduct( vecIB2d ) ); //Alignement
    double b = vecIA2d.dotProduct( vecIB2d );// � l'int�rieur
    
    long tempA = ( long )( a * scaleFactor );
    
    if( scaleFactor > 1 )
        a = ( double )( tempA / scaleFactor );
        
    if( a < 1.0 && b < 0 ) // l'alignement est moins important que l'emplacement entre deux points
    {
        zMean = getMeanZ( ptA, ptB, ptI );
        return true;
    }
    
    else
        return false;
        
        
    return result;
}


bool isPointOnLineAndInsidesTwoPointsZMean( double& zMean,
    const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptI,
    AcGePoint3d& ptRes,
    const double& precision,
    const bool& withVtx,
    const double& distFilter )
{
    bool result = false;
    /*
    // On travail en 2D donc on transforme les points avant
    AcGePoint2d ptA2d, ptB2d, ptI2d;
    AcGeVector2d vecAB2d, vecAI2d;
    
    // Points
    ptA2d = AcGePoint2d( ptA.x, ptA.y );
    ptB2d = AcGePoint2d( ptB.x, ptB.y );
    ptA2d = AcGePoint2d( ptI.x, ptI.y );
    
    // Vecteurs
    vecAB2d = AcGeVector2d( ( ptB2d.x - ptA2d.x ), ( ptB2d.y - ptA2d.y ) );
     vecAI2d = AcGeVector2d( ( ptI2d.x - ptA2d.x ), ( ptI2d.y - ptA2d.y ) );
    
     // On calcul la project� orthogonale
     AcGeVector2d vecAC = AcGeVector2d( ptC.x - ptA.x, ptC.y - ptA.y );
    AcGeVector2d vecBC = AcGeVector2d( ptC.x - ptB.x, ptC.y - ptB.y );
    AcGeVector2d vecAB = AcGeVector2d( ptB.x - ptA.x, ptB.y - ptA.y );
    
    AcGeVector2d projDir = vecAI2d.dotProduct( vecAB.normalize() ) * vecAB.normalize();
    
    AcGePoint3d pt = AcGePoint3d( ptA.x +  projDir.x, ptA.y +  projDir.y, 0 );
    
    // si la projection tombe pas sur le segment, retourn ( 0, 0, 0 )
    if( ! isPointOnLine( ptA, ptB,  getPoint2d( pt ) ) )
        return AcGePoint3d( 0, 0, 0 );
    
    return pt;
     //
     */
    return result;
}

bool are2RectIntersected( AcGePoint3d& firstRecLeftBottom, AcGePoint3d& firstRecRightTop, AcGePoint3d& secondRecLeftBottom, AcGePoint3d& rightRecRightTop )
{
    double maxLeft = 0.0f;
    
    if( firstRecLeftBottom.x > secondRecLeftBottom.x )
        maxLeft = firstRecLeftBottom.x;
    else
        maxLeft = secondRecLeftBottom.x;
        
    double minRight = 0.0f;
    
    if( firstRecRightTop.x > rightRecRightTop.x )
        minRight = rightRecRightTop.x;
    else
        minRight = firstRecRightTop.x;
        
    double maxBottom = 0.0f;
    
    if( firstRecLeftBottom.y > secondRecLeftBottom.y )
        maxBottom = firstRecLeftBottom.y;
    else
        maxBottom = secondRecLeftBottom.y;
        
    double minTop = 0.0f;
    
    if( firstRecRightTop.y > rightRecRightTop.y )
        minTop = rightRecRightTop.y;
    else
        minTop = firstRecRightTop.y;
        
    return ( ( maxLeft <= minRight ) && ( maxBottom <= minTop ) );
}


bool isPointInsideRect( AcGePoint3d& cornerA, AcGePoint3d& cornerB, AcGePoint3d& cornerC, AcGePoint3d& cornerD, AcGePoint3d ptTocheck )
{

    double distp12 = distance2DTo( cornerA, cornerB, ptTocheck );
    double distp23 = distance2DTo( cornerB, cornerC, ptTocheck );
    double distp34 = distance2DTo( cornerC, cornerD, ptTocheck );
    double distp41 = distance2DTo( cornerD, cornerA, ptTocheck );
    
    
    double width = AcGeVector3d( cornerA.x - cornerB.x, cornerA.y - cornerB.y, cornerA.z - cornerB.z ).length();
    double ep = AcGeVector3d( cornerA.x - cornerD.x, cornerA.y - cornerD.y, cornerA.z - cornerD.z ).length();
    
    // On traite suivant X
    if( ( distp12 <= width ) && ( distp34 <= width ) )
    {
        // On traite suivant Y
        if( ( distp23 <= ep ) && ( distp41 <= ep ) )
            return true;
    }
    
    return false;
}

double distanceOnXYTo( AcGePoint3d ptA, AcGePoint3d ptB, AcGePoint3d ptC )
{
    AcGeVector2d vecAC = AcGeVector2d( ptC.x - ptA.x, ptC.y - ptA.y );
    AcGeVector2d vecBC = AcGeVector2d( ptC.x - ptB.x, ptC.y - ptB.y );
    AcGeVector2d vecAB = AcGeVector2d( ptB.x - ptA.x, ptB.y - ptA.y );
    AcGeVector2d vecBA = AcGeVector2d( ptA.x - ptB.x, ptA.y - ptB.y );
    
    if( vecAC.dotProduct( vecAB ) < 0 || vecBC.dotProduct( vecBA ) < 0 )
        return min( ptC.distanceTo( ptA ), ptC.distanceTo( ptB ) );
        
    double projDir = vecAC.dotProduct( vecAB.normalize() );
    
    double hypotenuse = vecAC.lengthSqrd();
    
    double l = hypotenuse - pow( projDir, 2 );
    
    if( l <= 0 )
        return 0;
        
    return sqrt( l );
}

AcGePoint3d getPointFrom( const float& distFromFirstPt, const AcGePoint3d& pt, const AcGePoint3d ptO )
{
    AcGePoint3d res;
    float hyp = AcGeVector3d( ( ptO.x - pt.x ), ( ptO.y - pt.y ), ( ptO.z - pt.z ) ).length();
    
    float sinAngle = ( ptO.y - pt.y ) / hyp;
    float sinPiMinusAngle = sinAngle;
    
    float cosAngle = ( ptO.x - pt.x ) / hyp;
    float cosPiMinusAngle = -cosAngle;
    
    res.x = pt.x + ( distFromFirstPt * cosAngle );
    res.y = pt.y + ( distFromFirstPt * sinAngle );
    
    return res;
}


bool getDistProj2DOnSeg( const AcGePoint2d& ptSegA, const AcGePoint2d& ptSegB, const AcGePoint3d& pt, double& dist, AcGePoint3d& ptProj )
{
    bool result = true;
    
    AcGeVector2d vecAC = AcGeVector2d( pt.x - ptSegA.x, pt.y - ptSegA.y );
    AcGeVector2d vecBC = AcGeVector2d( pt.x - ptSegB.x, pt.y - ptSegB.y );
    AcGeVector2d vecAB = AcGeVector2d( ptSegB.x - ptSegA.x, ptSegB.y - ptSegA.y );
    
    AcGeVector2d projDir = vecAC.dotProduct( vecAB.normalize() ) * vecAB.normalize();
    
    ptProj = AcGePoint3d( ptSegA.x +  projDir.x, ptSegA.y +  projDir.y, 0 );
    
    // si la projection tombe pas sur le segment, retourn ( 0, 0, 0 )
    if( ! isPointOnLine( ptSegA, ptSegB,  getPoint2d( ptProj ) ) )
        return false;
        
    dist  = getPoint2d( pt ).distanceTo( getPoint2d( ptProj ) );
    
    return result;
}


bool isPointInside2DCircle( AcDbCircle*& circle,
    const AcGePoint3d& ptToCheck )
{
    AcGePoint3d ptCenter = circle->center();
    
    double cirlcleRadius = circle->radius();
    
    // On calcul la distance 2d entre le point et le cercle
    AcGePoint2d ptCenter2D, ptToCheck2D;
    ptCenter2D = AcGePoint2d( ptCenter.x, ptCenter.y );
    ptToCheck2D = AcGePoint2d( ptToCheck.x, ptToCheck.y );
    
    double dist2D = ptCenter2D.distanceTo( ptToCheck2D );
    
    if( dist2D < cirlcleRadius )
        return true;
    else
        return false;
        
}


bool isPointInside2DCircle( AcGePoint3d& ptCenter, double& cirlcleRadius, const AcGePoint3d& ptToCheck )
{
    // On calcul la distance 2d entre le point et le cercle
    AcGePoint2d ptCenter2D, ptToCheck2D;
    ptCenter2D = AcGePoint2d( ptCenter.x, ptCenter.y );
    ptToCheck2D = AcGePoint2d( ptToCheck.x, ptToCheck.y );
    
    double dist2D = ptCenter2D.distanceTo( ptToCheck2D );
    
    if( dist2D < cirlcleRadius )
        return true;
    else
        return false;
}


bool isPointOnLine2D( const AcGePoint3d& ptA, const AcGePoint3d& ptB, const AcGePoint3d& ptI, const double& precision )
{
    //Recuperer les vecteur
    AcGeVector2d vecAI = AcGeVector2d( ptI.x - ptA.x, ptI.y - ptA.y );
    AcGeVector2d vecBI = AcGeVector2d( ptI.x - ptB.x, ptI.y - ptB.y );
    AcGeVector2d vecAB = AcGeVector2d( ptB.x - ptA.x, ptB.y - ptA.y );
    
    AcGeVector2d projDir = vecAI.dotProduct( vecAB.normalize() ) * vecAB.normalize();
    
    AcGePoint3d pt = AcGePoint3d( ptA.x +  projDir.x, ptA.y +  projDir.y, 0 );
    
    //Recuperer la distance entre le point trouv� et le point I
    double len = getVector2d( ptI, pt ).length();
    
    if( len < precision )
        return true;
    else
        return false;
}


double getAngleFromAbscisse( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 )
{
    //Restulat
    double res = 0;
    
    //Creer un vecteur 2d avec entre le pt1 et le pt2
    AcGeVector2d vect = getVector2d( pt1, pt2 );
    
    //Recuperer l'angle par rapport � l'horizontal
    res = vect.angle();
    
    //Retourner le resultat
    return res;
}


Acad::ErrorStatus getBoundingBox( AcDbEntity* entity,
    AcDbExtents& box,
    AcGePoint3dArray& points )
{
    //Recuperer
    Acad::ErrorStatus es = entity->getGeomExtents( box );
    
    if( es != Acad::eOk )
    {
        print( "Impossible de recup�rer le bounding box de cette entit�." );
        return es;
    }
    
    if( !entity )
    {
        print( es );
        es = Acad::eNullEntityPointer;
        return es;
    }
    
    //Recuperer les points min et max
    AcGePoint3d ptMin = box.minPoint();
    AcGePoint3d ptMax = box.maxPoint();
    AcGePoint3d ptMinRigth = AcGePoint3d( ptMax.x, ptMin.y, ptMin.z );
    AcGePoint3d ptMaxLeft = AcGePoint3d( ptMin.x, ptMax.y, ptMax.z );
    
    //Ajouter les points dans le tableau
    points.append( ptMin );
    points.append( ptMinRigth );
    points.append( ptMax );
    points.append( ptMaxLeft );
    
    //Renvoyer le resultat
    return es;
}


Acad::ErrorStatus computeIntersection( const AcGeVector2d& vec1,
    const AcGeVector2d& vec2,
    AcGePoint2d& ptA,
    AcGePoint2d& ptC,
    AcGePoint2d& intersectionPoint )
{
    //Recuperer le parametre sur le premier vecteur
    double m = -( -vec1.x * ptA.y + vec1.x * ptC.y + vec1.y * ptA.x - vec1.y * ptC.x ) / ( vec1.x * vec2.y - vec1.y * vec2.x );
    
    //Recuperer le parametre sur le deuxieme vecteur
    double k = -( ptA.x * vec2.y - ptC.x * vec2.y - vec2.x * ptA.y + vec2.x * ptC.y ) / ( vec1.x * vec2.y - vec1.y * vec2.x );
    
    //Verifier qu'on a une intersection
    if( k >= 0 && k <= 1 && m >= 0 && m <= 1 ) // ou if( m >= 0 && m <= 1 )
    {
        //Recuperer le point d'intersection
        intersectionPoint = ptA + ( k * vec1 ); //ou intersectionPoint = ptC + ( m * vec2 );
        
        //Retourner ok
        return Acad::eOk;
    }
    
    return Acad::eNotApplicable;
}


bool isBoxIntersected( const AcGePoint2d& box1Pt1,
    const AcGePoint2d& box1Pt2,
    const AcGePoint2d& box2Pt1,
    const AcGePoint2d& box2Pt2 )
{
    //Recuperer les deux autres points du premier box
    AcGePoint2d box1Pt3 = AcGePoint2d( box1Pt1.x, box1Pt2.y );
    AcGePoint2d box1Pt4 = AcGePoint2d( box1Pt2.x, box1Pt1.y );
    
    //Recuperer les deux autres points du second box
    AcGePoint2d box2Pt3 = AcGePoint2d( box2Pt1.x, box2Pt2.y );
    AcGePoint2d box2Pt4 = AcGePoint2d( box2Pt2.x, box2Pt1.y );
    
    //Recuperer le point milieu des deux box
    AcGePoint2d box1Center = midPoint2d( box1Pt1, box1Pt2 );
    AcGePoint2d box2Center = midPoint2d( box2Pt1, box2Pt2 );
    
    //Tester si l'un des points appartient � l'autre box ou vice versa
    if( box1Center.x >= min( box2Pt1.x, box2Pt2.x ) && box1Center.x <= max( box2Pt1.x, box2Pt2.x )
        && box1Center.y >= min( box2Pt1.y, box2Pt2.y ) && box1Center.y <= max( box2Pt1.y, box2Pt2.y ) )
        return true;
    else if( box2Center.x >= min( box1Pt1.x, box1Pt2.x ) && box2Center.x <= max( box1Pt1.x, box1Pt2.x )
        && box2Center.y >= min( box1Pt1.y, box1Pt2.y ) && box2Center.y <= max( box1Pt1.y, box1Pt2.y ) )
        return true;
    else if( box1Pt1.x >= min( box2Pt1.x, box2Pt2.x ) && box1Pt1.x <=  max( box2Pt1.x, box2Pt2.x )
        && box1Pt1.y >= min( box2Pt1.y, box2Pt2.y ) && box1Pt1.y <= max( box2Pt1.y, box2Pt2.y ) )
        return true;
    else if( box1Pt2.x >= min( box2Pt1.x, box2Pt2.x ) && box1Pt2.x <= max( box2Pt1.x, box2Pt2.x )
        && box1Pt2.y >= min( box2Pt1.y, box2Pt2.y ) && box1Pt2.y <= max( box2Pt1.y, box2Pt2.y ) )
        return true;
    else if( box1Pt3.x >= min( box2Pt1.x, box2Pt2.x ) && box1Pt3.x <= max( box2Pt1.x, box2Pt2.x )
        && box1Pt3.y >= min( box2Pt1.y, box2Pt2.y ) && box1Pt3.y <= max( box2Pt1.y, box2Pt2.y ) )
        return true;
    else if( box1Pt4.x >= min( box2Pt1.x, box2Pt2.x ) && box1Pt4.x <= max( box2Pt1.x, box2Pt2.x )
        && box1Pt4.y >= min( box2Pt1.y, box2Pt2.y ) && box1Pt4.y <= max( box2Pt1.y, box2Pt2.y ) )
        return true;
    else if( box2Pt1.x >= min( box1Pt1.x, box1Pt2.x ) && box2Pt1.x <= max( box1Pt1.x, box1Pt2.x )
        && box2Pt1.y >= min( box1Pt1.y, box1Pt2.y ) && box2Pt1.y <= max( box1Pt1.y, box1Pt2.y ) )
        return true;
    else if( box2Pt2.x >= min( box1Pt1.x, box1Pt2.x ) && box2Pt2.x <= max( box1Pt1.x, box1Pt2.x )
        && box2Pt2.y >= min( box1Pt1.y, box1Pt2.y ) && box2Pt2.y <= max( box1Pt1.y, box1Pt2.y ) )
        return true;
    else if( box2Pt3.x >= min( box1Pt1.x, box1Pt2.x ) && box2Pt3.x <= max( box1Pt1.x, box1Pt2.x )
        && box2Pt3.y >=  min( box1Pt1.y, box1Pt2.y )  && box2Pt3.y <=  max( box1Pt1.y, box1Pt2.y ) )
        return true;
    else if( box2Pt4.x >= min( box1Pt1.x, box1Pt2.x ) && box2Pt4.x <= max( box1Pt1.x, box1Pt2.x )
        && box2Pt4.y >= min( box1Pt1.y, box1Pt2.y ) && box2Pt4.y <= max( box1Pt1.y, box1Pt2.y ) )
        return true;
    else if( ( box1Pt1.x >= min( box2Pt1.x, box2Pt2.x ) && box1Pt1.x <= max( box2Pt1.x, box2Pt2.x )
            && box1Pt2.x >= min( box2Pt1.x, box2Pt2.x ) && box1Pt2.x <= max( box2Pt1.x, box2Pt2.x )
            && box2Pt1.y >= min( box1Pt1.y, box1Pt2.y ) && box2Pt1.y <= max( box1Pt1.y, box1Pt2.y )
            && box2Pt2.y >= min( box1Pt1.y, box1Pt2.y ) && box2Pt2.y <= max( box1Pt1.y, box1Pt2.y ) )
        || ( box2Pt1.x >= min( box1Pt1.x, box1Pt2.x ) && box2Pt1.x <= max( box1Pt1.x, box1Pt2.x )
            && box2Pt2.x >= min( box1Pt1.x, box1Pt2.x ) && box2Pt2.x <= max( box1Pt1.x, box1Pt2.x )
            && box1Pt1.y >= min( box2Pt1.y, box2Pt2.y ) && box1Pt1.y <= max( box2Pt1.y, box2Pt2.y )
            && box1Pt2.y >= min( box2Pt1.y, box2Pt2.y ) && box1Pt2.y <= max( box2Pt1.y, box2Pt2.y ) ) )
        return true;
    else
        return false;
}


bool isBoxIntersected( AcDbExtents& bb1,
    AcDbExtents& bb2 )
{
    //Box temporaire
    AcDbExtents boxTemp = bb2;
    
    //Recuperer les points
    AcGePoint3d bb1Min = bb1.minPoint();
    AcGePoint3d bb1Max = bb1.maxPoint();
    AcGePoint3d bb2Min = bb2.minPoint();
    AcGePoint3d bb2Max = bb2.maxPoint();
    
    //Trier de gauche � droite
    if( bb1Min.x > bb2Min.x )
    {
        boxTemp = bb2;
        bb2 = bb1;
        bb1 = boxTemp;
    }
    
    //RectA.X1 < RectB.X2 && RectA.X2 > RectB.X1 && RectA.Y1 > RectB.Y2 && RectA.Y2 < RectB.Y1
    if( bb1Min.x < bb2Max.x && bb1Max.x > bb2Min.x && bb1Min.y > bb2Max.y && bb1Max.y < bb2Min.y )
        return true;
    else
        return false;
}


Acad::ErrorStatus getZPointToSegment( const AcGePoint3d& ptA,
    const AcGePoint3d& ptB,
    const AcGePoint3d& ptToProject,
    AcGePoint3d& projectedPoint )
{
    //Resultat
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Recuperer la distance entre les deux points et le point central
    double d1 = getDistance2d( ptA, ptToProject );
    double d2 = getDistance2d( ptB, ptToProject );
    
    //Recuperer le z moyen
    double z = ( ptB.z * d1 + ptA.z * d2 ) / ( d1 + d2 );
    
    //Recuperer le point
    projectedPoint = AcGePoint3d( ptToProject.x, ptToProject.y, z );
    
    //Retourner ok
    return Acad::eOk;
}


Acad::ErrorStatus computeIntersection( const AcDbCurve* curve1,
    const AcDbCurve* curve2,
    AcGePoint3dArray& intersectPoint )
{
    //Resultat par d�faut
    Acad::ErrorStatus er = Acad::eNotApplicable;
    
    //Creer un plan de projection en 2d
    AcGePlane plane = AcGePlane( AcGePoint3d::kOrigin,
            AcGeVector3d::kXAxis,
            AcGeVector3d::kYAxis );
            
    //Appeler la fonction qui va projeter le point sur le plan et reprojeter le point sur curve1
    er = curve1->intersectWith( curve2,
            AcDb::kOnBothOperands,
            plane,
            intersectPoint );
            
    //Verifier
    if( er != Acad::eOk )
        return er;
        
    //Retourner eOk
    return Acad::eOk;
}



AcDbCurve* getProjCurveOnPlane( AcDbCurve* curve,
    const AcGePlane& plane,
    const AcGeVector3d& projectDir )
{
    if( curve == NULL )
        return NULL;
        
    //Resultat
    AcDbCurve* curveRes;
    
    //Recuperer le curve projet�
    Acad::ErrorStatus er;
    
    if( er = curve->getProjectedCurve( plane, projectDir, curve ) ) return NULL;
    
    //Retourner le curve
    return curve;
}


int multipleMin( const double& valueInput, const int& multiple )
{
    int modulo = ( int )valueInput % multiple;
    int valueOutput = valueInput - modulo - ( valueInput - ( int )valueInput );
    return valueOutput;
}


int multipleMax( const double& valueInput, const int& multiple )
{
    double value = valueInput + 1;
    int modulo = ( int )value % multiple;
    
    int valueOutput = value;
    
    while( modulo != 0 )
    {
        valueOutput = valueOutput + 1;
        modulo = valueOutput % multiple;
    }
    
    //int valueOutput = valueInput - modulo - (valueInput - lround(valueInput));
    return valueOutput;
}


bool isRectInsideRect( const AcGePoint3d& ptMinR1,
    const AcGePoint3d& ptMaxR1,
    const AcGePoint3d& ptMinR2,
    const AcGePoint3d& ptMaxR2 )
{
    if( ptMinR1.x >= ptMinR2.x &&
        ptMinR1.y >= ptMinR2.y &&
        ptMinR1.z >= ptMinR2.z &&
        ptMaxR1.x <= ptMaxR2.x &&
        ptMaxR1.y <= ptMaxR2.y &&
        ptMaxR1.z <= ptMaxR2.z )
        return true;
        
    return false;
}


bool isPointInsideRect( const AcGePoint3d& point,
    const AcGePoint3d& ptMin,
    const AcGePoint3d& ptMax,
    const bool& use3d )
{
    if( point.x >= ptMin.x &&
        point.y >= ptMin.y &&
        point.y <= ptMax.y &&
        point.x <= ptMax.x )
    {
        //Si on test en 3d
        if( use3d )
        {
            if( point.z >= ptMin.z &&
                point.z <= ptMax.z )
                return true;
                
            return false;
        }
        
        return true;
    }
    
    return false;
}


vector<vector<vector<AcDbObjectId>>> getSelectionMatrix( const ads_name& ssPoly, const int& pas, const double& tol )
{
    int poly_size = getSsLength( ssPoly );
    
    //Barre de progression
    ProgressBar prog0 = ProgressBar( _T( "Get datas:" ), poly_size );
    
    AcDbExtents matrice = AcDbExtents( AcGePoint3d( DBL_MAX, DBL_MAX, .0 ), AcGePoint3d( DBL_MIN, DBL_MIN, .0 ) );
    
    //Boucler sur les polylignes
    for( int i = 0; i < poly_size; i++ )
    {
        AcDbExtents ext;
        //Progresser
        prog0.moveUp( i );
        
        //Recuperer la i-eme polyline
        AcDb3dPolyline* poly = getPoly3DFromSs( ssPoly, i, AcDb::kForRead );
        poly->getGeomExtents( ext );
        
        if( ext.minPoint().x < matrice.minPoint().x )
            matrice = AcDbExtents( AcGePoint3d( ext.minPoint().x, matrice.minPoint().y, .0 ), matrice.maxPoint() );
            
        if( ext.minPoint().y < matrice.minPoint().y )
            matrice = AcDbExtents( AcGePoint3d( matrice.minPoint().x, ext.minPoint().y, .0 ), matrice.maxPoint() );
            
        if( ext.maxPoint().x > matrice.maxPoint().x )
            matrice = AcDbExtents( matrice.minPoint(), AcGePoint3d( ext.maxPoint().x, matrice.maxPoint().y, .0 ) );
            
        if( ext.maxPoint().y > matrice.maxPoint().y )
            matrice = AcDbExtents( matrice.minPoint(), AcGePoint3d( matrice.maxPoint().x, ext.maxPoint().y, .0 ) );
            
        //Liberer la mmoire
        poly->close();
    }
    
    //Mettre les bord de la matrice au multiple de 5
    AcGePoint3d ptExt1 = AcGePoint3d( int( matrice.minPoint().x ) - int( matrice.minPoint().x ) % pas,
            int( matrice.minPoint().y ) - int( matrice.minPoint().y ) % pas, .0 );
    AcGePoint3d ptExt2 = AcGePoint3d( int( matrice.maxPoint().x + 1 ) + pas - int( matrice.maxPoint().x + pas ) % pas,
            int( matrice.maxPoint().y ) + pas - int( matrice.maxPoint().y + pas ) % pas, .0 );
    matrice = AcDbExtents( ptExt1, ptExt2 );
    
    //Bien definir la taille de la matrice
    int taill1 = ( matrice.maxPoint().x - matrice.minPoint().x ) / pas;
    int taill2 = ( matrice.maxPoint().y - matrice.minPoint().y ) / pas;
    
    //Mettres les polylignes dans la matrice
    vector<vector<vector<AcDbObjectId>>> Matrice;
    
    Matrice.resize( taill1 );
    vector<vector<AcDbObjectId>> polyidList( taill2 );
    
    for( int iMt = 0; iMt < taill1; iMt++ )
        Matrice[iMt] = polyidList;
        
    //Bare de progression
    prog0 = ProgressBar( _T( "Calcul de la matrice:" ), poly_size );
    
    //Reboucler sur les polylignes
    for( int i = 0; i < poly_size; i++ )
    {
        //Faire progresser la barre de progression
        prog0.moveUp( i );
        
        //Recuperer la i-eme polyline
        AcDb3dPolyline* poly = getPoly3DFromSs( ssPoly, i, AcDb::kForRead );
        
        //Verifier poly
        if( !poly )
            continue;
            
        //Listes des points du polylignes
        vector<pair<int, int>> mindex;
        vector<AcGePoint3d> pointPolyList;
        AcDbExtents extnts;
        poly->getGeomExtents( extnts );
        //drawLine(extnts.minPoint(), extnts.maxPoint());
        //Vecteur pour recuperer le polyligne
        vector<AcDbObjectId> vecPpolyid;
        vecPpolyid.resize( 0 );
        vecPpolyid.shrink_to_fit();
        
        //Recuperer les case que se trouves le polyligne
        //pair<pair<int, int>, pair<int, int>> mcoord;
        int iMinX = int( ( extnts.minPoint().x - tol - matrice.minPoint().x ) / pas );
        int iMinY = int( ( extnts.minPoint().y - tol - matrice.minPoint().y ) / pas );
        int iMaxX = int( ( extnts.maxPoint().x + tol - matrice.minPoint().x ) / pas );
        int iMaxY = int( ( extnts.maxPoint().y + tol - matrice.minPoint().y ) / pas );
        
        //Si le point est exatement equal au box du dessin on recalcul le bounding
        if( iMinX < 0 ) iMinX = 0;
        
        if( iMinY < 0 ) iMinY = 0;
        
        if( iMaxX >= taill1 ) iMaxX = taill1 - 1;
        
        if( iMaxY >= taill2 ) iMaxY = taill2 - 1;
        
        //mcoord = { {iMinX, iMinY}, {iMaxX, iMaxY} };
        
        //Ajouter le polylignes parmis les polignes de la case
        for( int iminx = iMinX; iminx <= iMaxX; iminx++ )
        {
            for( int iminy = iMinY; iminy <= iMaxY; iminy++ )
            {
                if( Matrice[iminx][iminy].size() == 0 )
                {
                    vecPpolyid.push_back( poly->id() );
                    Matrice[iminx][iminy] = vecPpolyid;
                    vecPpolyid.resize( 0 );
                    vecPpolyid.shrink_to_fit();
                }
                
                else
                {
                    vecPpolyid = Matrice[iminx][iminy];
                    vecPpolyid.push_back( poly->id() );
                    Matrice[iminx][iminy] = vecPpolyid;
                    vecPpolyid.resize( 0 );
                    vecPpolyid.shrink_to_fit();
                }
            }
        }
    }
    
    return Matrice;
}

vector<vector<vector<int>>> getSelectionIntMatrix( const ads_name& ssPoly, const int& pas, const double& tol )
{
    int poly_size = getSsLength( ssPoly );
    
    //Barre de progression
    ProgressBar prog0 = ProgressBar( _T( "Get datas:" ), poly_size );
    
    AcDbExtents matrice = AcDbExtents( AcGePoint3d( DBL_MAX, DBL_MAX, .0 ), AcGePoint3d( DBL_MIN, DBL_MIN, .0 ) );
    
    //Boucler sur les polylignes
    for( int i = 0; i < poly_size; i++ )
    {
        AcDbExtents ext;
        //Progresser
        prog0.moveUp( i );
        
        //Recuperer la i-eme polyline
        AcDb3dPolyline* poly = getPoly3DFromSs( ssPoly, i, AcDb::kForRead );
        poly->getGeomExtents( ext );
        
        if( ext.minPoint().x < matrice.minPoint().x )
            matrice = AcDbExtents( AcGePoint3d( ext.minPoint().x, matrice.minPoint().y, .0 ), matrice.maxPoint() );
            
        if( ext.minPoint().y < matrice.minPoint().y )
            matrice = AcDbExtents( AcGePoint3d( matrice.minPoint().x, ext.minPoint().y, .0 ), matrice.maxPoint() );
            
        if( ext.maxPoint().x > matrice.maxPoint().x )
            matrice = AcDbExtents( matrice.minPoint(), AcGePoint3d( ext.maxPoint().x, matrice.maxPoint().y, .0 ) );
            
        if( ext.maxPoint().y > matrice.maxPoint().y )
            matrice = AcDbExtents( matrice.minPoint(), AcGePoint3d( matrice.maxPoint().x, ext.maxPoint().y, .0 ) );
            
        //Liberer la mmoire
        poly->close();
    }
    
    //Mettre les bord de la matrice au multiple de 5
    AcGePoint3d ptExt1 = AcGePoint3d( int( matrice.minPoint().x ) - int( matrice.minPoint().x ) % pas,
            int( matrice.minPoint().y ) - int( matrice.minPoint().y ) % pas, .0 );
    AcGePoint3d ptExt2 = AcGePoint3d( int( matrice.maxPoint().x + 1 ) + pas - int( matrice.maxPoint().x + pas ) % pas,
            int( matrice.maxPoint().y ) + pas - int( matrice.maxPoint().y + pas ) % pas, .0 );
    matrice = AcDbExtents( ptExt1, ptExt2 );
    
    //Bien definir la taille de la matrice
    int taill1 = ( matrice.maxPoint().x - matrice.minPoint().x ) / pas;
    int taill2 = ( matrice.maxPoint().y - matrice.minPoint().y ) / pas;
    
    //Mettres les polylignes dans la matrice
    vector<vector<vector<int>>> Matrice;
    
    Matrice.resize( taill1 );
    vector<vector<int>> polyidList( taill2 );
    
    for( int iMt = 0; iMt < taill1; iMt++ )
        Matrice[iMt] = polyidList;
        
    //Bare de progression
    prog0 = ProgressBar( _T( "Calcul de la matrice:" ), poly_size );
    
    //Reboucler sur les polylignes
    for( int i = 0; i < poly_size; i++ )
    {
        //Faire progresser la barre de progression
        prog0.moveUp( i );
        
        //Recuperer la i-eme polyline
        AcDb3dPolyline* poly = getPoly3DFromSs( ssPoly, i, AcDb::kForRead );
        
        //Verifier poly
        if( !poly )
            continue;
            
        //Listes des points du polylignes
        vector<pair<int, int>> mindex;
        vector<AcGePoint3d> pointPolyList;
        AcDbExtents extnts;
        poly->getGeomExtents( extnts );
        //drawLine(extnts.minPoint(), extnts.maxPoint());
        //Vecteur pour recuperer le polyligne
        vector<int> vecPpolyid;
        vecPpolyid.resize( 0 );
        vecPpolyid.shrink_to_fit();
        
        //Recuperer les case que se trouves le polyligne
        //pair<pair<int, int>, pair<int, int>> mcoord;
        int iMinX = int( ( extnts.minPoint().x - tol - matrice.minPoint().x ) / pas );
        int iMinY = int( ( extnts.minPoint().y - tol - matrice.minPoint().y ) / pas );
        int iMaxX = int( ( extnts.maxPoint().x + tol - matrice.minPoint().x ) / pas );
        int iMaxY = int( ( extnts.maxPoint().y + tol - matrice.minPoint().y ) / pas );
        
        //Si le point est exatement equal au box du dessin on recalcul le bounding
        if( iMinX < 0 ) iMinX = 0;
        
        if( iMinY < 0 ) iMinY = 0;
        
        if( iMaxX >= taill1 ) iMaxX = taill1 - 1;
        
        if( iMaxY >= taill2 ) iMaxY = taill2 - 1;
        
        //mcoord = { {iMinX, iMinY}, {iMaxX, iMaxY} };
        
        //Ajouter le polylignes parmis les polignes de la case
        for( int iminx = iMinX; iminx <= iMaxX; iminx++ )
        {
            for( int iminy = iMinY; iminy <= iMaxY; iminy++ )
            {
                if( Matrice[iminx][iminy].size() == 0 )
                {
                    vecPpolyid.push_back( i );
                    Matrice[iminx][iminy] = vecPpolyid;
                    vecPpolyid.resize( 0 );
                    vecPpolyid.shrink_to_fit();
                }
                
                else
                {
                    vecPpolyid = Matrice[iminx][iminy];
                    vecPpolyid.push_back( i );
                    Matrice[iminx][iminy] = vecPpolyid;
                    vecPpolyid.resize( 0 );
                    vecPpolyid.shrink_to_fit();
                }
            }
        }
        
        poly->close();
    }
    
    return Matrice;
}
/*
vector<vector<vector<int>>> getSelectionIntMatrix( const ads_name& ssPoly, const int& pas, const double& tol )
{
    int poly_size = getSsLength( ssPoly );

    //Barre de progression
    ProgressBar prog0 = ProgressBar( _T( "Get datas:" ), poly_size );

    AcDbExtents matrice = AcDbExtents( AcGePoint3d( DBL_MAX, DBL_MAX, .0 ), AcGePoint3d( DBL_MIN, DBL_MIN, .0 ) );

    //Boucler sur les polylignes
    for( int i = 0; i < poly_size; i++ )
    {
        AcDbExtents ext;
        //Progresser
        prog0.moveUp( i );

        //Recuperer la i-eme polyline
        AcDb3dPolyline* poly = getPoly3DFromSs( ssPoly, i, AcDb::kForRead );
        poly->getGeomExtents( ext );

        if( ext.minPoint().x < matrice.minPoint().x )
            matrice = AcDbExtents( AcGePoint3d( ext.minPoint().x, matrice.minPoint().y, .0 ), matrice.maxPoint() );

        if( ext.minPoint().y < matrice.minPoint().y )
            matrice = AcDbExtents( AcGePoint3d( matrice.minPoint().x, ext.minPoint().y, .0 ), matrice.maxPoint() );

        if( ext.maxPoint().x > matrice.maxPoint().x )
            matrice = AcDbExtents( matrice.minPoint(), AcGePoint3d( ext.maxPoint().x, matrice.maxPoint().y, .0 ) );

        if( ext.maxPoint().y > matrice.maxPoint().y )
            matrice = AcDbExtents( matrice.minPoint(), AcGePoint3d( matrice.maxPoint().x, ext.maxPoint().y, .0 ) );

        //Liberer la mmoire
        poly->close();
    }

    //Mettre les bord de la matrice au multiple de 5
    AcGePoint3d ptExt1 = AcGePoint3d( int( matrice.minPoint().x ) - int( matrice.minPoint().x ) % pas,
            int( matrice.minPoint().y ) - int( matrice.minPoint().y ) % pas, .0 );
    AcGePoint3d ptExt2 = AcGePoint3d( int( matrice.maxPoint().x + 1 ) + pas - int( matrice.maxPoint().x + pas ) % pas,
            int( matrice.maxPoint().y ) + pas - int( matrice.maxPoint().y + pas ) % pas, .0 );
    matrice = AcDbExtents( ptExt1, ptExt2 );

    //Bien definir la taille de la matrice
    int taill1 = ( matrice.maxPoint().x - matrice.minPoint().x ) / pas;
    int taill2 = ( matrice.maxPoint().y - matrice.minPoint().y ) / pas;

    //Mettres les polylignes dans la matrice
    vector<vector<vector<int>>> Matrice;

    Matrice.resize( taill1 );
    vector<vector<int>> polyidList( taill2 );

    for( int iMt = 0; iMt < taill1; iMt++ )
        Matrice[iMt] = polyidList;

    //Bare de progression
    prog0 = ProgressBar( _T( "Calcul de la matrice:" ), poly_size );

    //Reboucler sur les polylignes
    for( int i = 0; i < poly_size; i++ )
    {
        //Faire progresser la barre de progression
        prog0.moveUp( i );

        //Recuperer la i-eme polyline
        AcDb3dPolyline* poly = getPoly3DFromSs( ssPoly, i, AcDb::kForRead );

        //Verifier poly
        if( !poly )
            continue;

        //Listes des points du polylignes
        vector<pair<int, int>> mindex;
        vector<AcGePoint3d> pointPolyList;
        AcDbExtents extnts;
        poly->getGeomExtents( extnts );
        //drawLine(extnts.minPoint(), extnts.maxPoint());
        //Vecteur pour recuperer le polyligne
        vector<int> vecPpolyid;
        vecPpolyid.resize( 0 );
        vecPpolyid.shrink_to_fit();

        //Recuperer les case que se trouves le polyligne
        //pair<pair<int, int>, pair<int, int>> mcoord;
        int iMinX = int( ( extnts.minPoint().x - tol - matrice.minPoint().x ) / pas );
        int iMinY = int( ( extnts.minPoint().y - tol - matrice.minPoint().y ) / pas );
        int iMaxX = int( ( extnts.maxPoint().x + tol - matrice.minPoint().x ) / pas );
        int iMaxY = int( ( extnts.maxPoint().y + tol - matrice.minPoint().y ) / pas );

        //Si le point est exatement equal au box du dessin on recalcul le bounding
        if( iMinX < 0 ) iMinX = 0;

        if( iMinY < 0 ) iMinY = 0;

        if( iMaxX >= taill1 ) iMaxX = taill1 - 1;

        if( iMaxY >= taill2 ) iMaxY = taill2 - 1;

        //mcoord = { {iMinX, iMinY}, {iMaxX, iMaxY} };

        //Ajouter le polylignes parmis les polignes de la case
        for( int iminx = iMinX; iminx <= iMaxX; iminx++ )
        {
            for( int iminy = iMinY; iminy <= iMaxY; iminy++ )
            {
                if( Matrice[iminx][iminy].size() == 0 )
                {
                    vecPpolyid.push_back( i );
                    Matrice[iminx][iminy] = vecPpolyid;
                    vecPpolyid.resize( 0 );
                    vecPpolyid.shrink_to_fit();
                }

                else
                {
                    vecPpolyid = Matrice[iminx][iminy];
                    vecPpolyid.push_back( i );
                    Matrice[iminx][iminy] = vecPpolyid;
                    vecPpolyid.resize( 0 );
                    vecPpolyid.shrink_to_fit();
                }
            }
        }

        poly->close();
    }

    return Matrice;
}*/


bool are2bBoxIntersected( const AcGePoint3d& minPointBox1, const AcGePoint3d& maxPointBox1, const AcGePoint3d& minPointBox2, const AcGePoint3d& maxPointBox2 )
{
    bool xIntersected( false ), yIntersected( false ), zIntersected( false );
    xIntersected = isOverLapping1D( minPointBox1.x, maxPointBox1.x, minPointBox2.x, maxPointBox2.x );
    yIntersected = isOverLapping1D( minPointBox1.y, maxPointBox1.y, minPointBox2.y, maxPointBox2.y );
    zIntersected = isOverLapping1D( minPointBox1.z, maxPointBox1.z, minPointBox2.z, maxPointBox2.z );
    
    return ( xIntersected && yIntersected && zIntersected );
}


bool isOverLapping1D( const double& x1A, const double& x2A, const double& x1B, const double& x2B )
{

    //Verifier si les points se chevauchent
    if( x1B >= x1A && x1B <= x2A )
        return true;
    else if( x2B >= x1A && x2B <= x2A )
        return true;
    else if( x1A >= x1B && x1A <= x2B )
        return true;
    else if( x2A >= x1B && x2A <= x1B )
        return true;
        
    //Sinon retourner false
    return false;
}