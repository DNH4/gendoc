/**
 * \file poly2DEntity.h
 * \author Romain LEMETTAIS
 * \brief Fichier contenant des fonctions utiles pour travailler avec les polylignes 2d
 */


#pragma once
#include <dbents.h>
#include "selectEntity.h"
#include "pointEntity.h"
#include <vector>
#include "groupEntity.h"
#include "list.h"

struct poly2d
{
    AcDbPolyline* polyline;
    double xPos;
};

/**
  * \brief Fait une selection quelconque sur les polylignes 2d
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \param isClosed True si la polyligne est fermee, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSsPoly2D(
    ads_name         ssName,
    const AcString&   layerName = "",
    const bool&       isClosed = false );

/**
  * \brief Selectionne une seul polyligne dans le dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \param isClosed True si la polyligne est fermee, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSsOnePoly2D(
    ads_name          ssName,
    const AcString&   layerName = "",
    const bool&       isClosed = false );

/**
  * \brief Selectionne toutes les polylignes 2d du dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \param isClosed True si la polyligne est fermee, false sinon
  * \return Nombre d'elements selectionnes
  */
long getSsAllPoly2D(
    ads_name          ssName,
    const AcString&   layerName = "",
    const bool&       isClosed = false );

/**
 * \brief Renvoie un pointeur sur la polyligne 2d numero N dans la selection
 * \param ssName Pointeur sur la selection
 * \param iObject Index dans la selection de la polyligne a recuperer, par defaut 0
 * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
 * \return Pointeur sur une polyligne
 */
AcDbPolyline* getPoly2DFromSs(
    const ads_name&              ssName,
    const long&           iObject   =   0,
    const AcDb::OpenMode& opMode = AcDb::kForRead );

/**
  * \brief Renvoie un pointeur en LECTURE sur la polyligne numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de la polyligne a recuperer, par defaut 0
  * \return Pointeur sur une polyligne
  */
AcDbPolyline* readPoly2DFromSs(
    const ads_name&          ssName,
    const long&       iObject = 0 );

/**
  * \brief Renvoie un pointeur en ECRITURE sur la polyligne numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection de la polyligne a recuperer, par defaut 0
  * \return Pointeur sur une polyligne
  */
AcDbPolyline* writePoly2DFromSs(
    const ads_name&              ssName,
    const long&           iObject = 0 );

/**
  * \brief Renvoie un pointeur sur une polyligne 2d a partir d'une liste de points 2d
  * \param arPoint Liste de points 2d
  * \return Pointeur sur une polyligne
  */
AcDbPolyline* getPoly2D(
    const AcGePoint2dArray arPoint );

/**
  * \brief Renvoie un pointeur sur une polyligne 2d a partir d'une liste de points 3d
  * \param arPoint Liste de points 3d
  * \return Pointeur sur une polyligne
  */
AcDbPolyline* getPoly2D(
    const AcGePoint3dArray arPoint );

/**
  * \brief Renvoie un pointeur sur une polyligne 2d a partir d'une liste de points 2det d'une liste de bulge
  * \param arPoint Liste de points 3d
  *
  * \image html "C:\Futurmap\Dev-Outils\GStarCAD\GRX\FmapLib\documentation\images\bulge.png"
  *
  * \return Pointeur sur une polyligne
  */
AcDbPolyline* getPoly2D(
    const AcGePoint2dArray    arPoint,
    const AcGeDoubleArray     arBulge );

/**
  * \brief Renvoie un pointeur sur une polyligne 2d a partir d'une liste de points 3d et d'une liste de bulge
  * \param arPoint Liste de points 3d
  * \return Pointeur sur une polyligne
  */
AcDbPolyline* getPoly2D(
    const AcGePoint3dArray    arPoint,
    const AcGeDoubleArray     arBulge );

/**
  * \brief Renvoie l'abcisse de la polyligne au sommet demande
  * \param poly Polyligne pour laquelle on veut recuperer l'abcisse d'un sommet
  * \param index Index du sommet
  * \return Renvoie la valeur de l'abcisse
  */
double getAbscisse(
    const AcDbPolyline* poly,
    const unsigned int  index );

/**
  * \brief Renvoie l'abcisse du point de la polyligne le plus proche
  * \param poly Polyligne qui nous interesse
  * \param vertex Point pour lequel on veut recuperer l'abcisse sur la polyligne
  * \param toProj True s'il faut projeter le point sur la polyligne, false sinon
  * \return Renvoie la valeur de l'abcisse du point de la polyligne le plus proche
  */
double getAbscisse(
    const AcDbPolyline*  poly,
    const AcGePoint3d    vertex,
    const bool           toProj   = true );

/**
  * \brief Indique si le point est a droite ou a gauche de la polyligne
  * \param poly Polyligne qui nous interesse
  * \param pt Point qui nous interesse
  * \return Renvoie True si le point est a droite, false sinon
  */
bool getSide(
    const AcDbPolyline* poly,
    const AcGePoint3d   pt );

/**
  * \brief Calcule le bulge a partir de 3 points
  * \param p_ptStart Premier point de l'arc
  * \param p_ptMid Point au milieu de l'arc
  * \param p_ptEnd Dernier point de l'arc
  * \return Renvoie le bulge
  */
double getBulge(
    const AcGePoint3d p_ptStart,
    const AcGePoint3d p_ptMid,
    const AcGePoint3d p_ptEnd );

/**
  * \brief Calcule la longueur d'une polyligne
  * \param poly Polyligne 2D qui nous interesse
  * \return Renvoie la longueur
  */
double getLength(
    const AcDbCurve* const poly );

/**
  * \brief Indique si la polyligne est fermee ou non
  * \param poly Polyligne2D qui nous interesse
  * \return Renvoie true si la polyligne est fermee, false sinon
  */
bool isClosed(
    const AcDbPolyline* const poly );


/**
  * \brief Renvoie la liste des points pour les segments concatenee avec la liste des points discretises pour les arcs
  * \param poly Polyligne2D a discretiser
  * \param tol Tolerance sur la fleche de discretisation sur les arcs, par defaut 0.1
  * \return Renvoie true si la polyligne est fermee, false sinon
  */
AcGePoint2dArray discretize( AcDbPolyline* poly2D, const double& tol = 0.1 );

AcGePoint3dArray discretize3D( AcDbPolyline* poly2D, const double& tol = 0.1 );

/**
  * \brief V�rifier si la selection de polylignes 2D contient des polylignes avec arcs ou non
  * \param ss Selection de polylignes 2D
  * \param length Nombre de lignes de la s�lection
  * \return True s'il y a au moins une polyligne 2D avec arcs, false sinon
  */
bool hasArc( ads_name& ss, const long& length );

/**
  * \brief Tracer une polyligne2D passant par une liste de points 3D dans un meme plan
  * \param pointArray Liste des points de la polyligne 2D a tracer
  * \return Polyligne2D obtenue
  */
AcDbPolyline* draw3Dpoly2D( const AcGePoint3dArray pointArray );


/**
  * \brief Supprime les sommets superpos�s dans une polyligne 2D
  * \param poly La polyligne qui nous int�resse
  * \param tol Valeur de la tol�rance
  * \return Nombre de sommets superpos�s supprim�s dans la polyligne 2D
  */
int netPoly(
    AcDbPolyline* poly,
    const double& tol );


/**
  * \brief Fusionne les sommets colin�aires dans une polyligne 2D
  * \param poly La polyligne qui nous int�resse
  * \param tol Valeur de la tol�rance
  * \return Nombre de sommets colin�aires fusionn�s dans la polyligne 2D
  */
int coliPoly(
    AcDbPolyline* poly,
    const double& tol );


/**
  * \brief Supprime les sommets superpos�s dans une polyligne 2D
  * \param poly La polyligne qui nous int�resse
  * \param tol Valeur de la tol�rance
  * \return Nombre de sommets superpos�s supprim�s dans la polyligne 2D
  */
int netPoly2(
    AcDbPolyline* poly,
    const bool& erase = false,
    const double& tol = 0.01 );


/**
  * \brief Fusionne les sommets colin�aires dans une polyligne 2D
  * \param poly La polyligne qui nous int�resse
  * \param tol Valeur de la tol�rance
  * \return Nombre de sommets colin�aires fusionn�s dans la polyligne 2D
  */
int coliPoly2(
    AcDbPolyline* poly,
    const bool& erase = false,
    const double& tol = 0.01 );


/**
  * \brief Supprime les sommets superpos�s et/ou fusionne les sommets colin�aires dans une polyligne 2D
  * \param poly La polyligne qui nous int�resse
  * \param tol valeur de la tol�rance
  * \return Nombre de sommets superpos�s supprim�s et/ou sommets colin�aires fusionn�s dans la polyligne 2D
  */
int netColiPoly(
    AcDbPolyline* poly,
    const double& tol );

/**
  * \brief Verifier si un point est dans une polyligne 2D
  * \param poly2D la polyligne 2D
  * \param point2D � tester
  * \return booleen
  */
bool isPointInPoly( AcDbPolyline* poly2D, const AcGePoint2d& point );


/**
  * \brief V�rifier si un point est dans une polyligne 3D
  * \param poly3D la polyligne 3D
  * \param point3D � tester
  * \return booleen
  */
bool isPointInPolyByAngle( AcDbPolyline* poly2D,
    const AcGePoint3d& point );

/**
  * \brief Calcule le centro�de d'un polygone 2D ( polyligne 2D ferm�e )
  * \param poly Polyligne ferm�e
  * \param centroid Centro�de du polygone
  * \return ErrorStatus D�tail de la commande, si la polyligne n'est pas ferm�e retourne eInvalidInput
  */
Acad::ErrorStatus getCentroid( AcDbPolyline* poly,
    AcGePoint2d& centroid );

/**
  * \brief Calcule le centro�de d'un polygone 2D ( polyligne 2D ferm�e )
  * \param poly Polyligne ferm�e
  * \param centroid Centro�de du polygone
  * \return ErrorStatus D�tail de la commande, si la polyligne n'est pas ferm�e retourne eInvalidInput
  */
Acad::ErrorStatus getCentroid( AcDbPolyline* poly,
    AcGePoint3d& centroid );

/**
  * \brief Dessine une polyligne 2D grace � deux tableaux de vecteurs
  * \param tabx Vecteur Contenant les point X de la polyligne
  * \param taby Vecteur Contenant les point Y de la polyligne
  * \param layer Calque ou on veut inserer la polyligne
  * \param coli Par d�faut true : Permet de nettoyer la polyline i.e supprime les sommets colin�aires
  * \return void
  */
void drawPolyline( std::vector<double> tabx, std::vector<double> taby, AcString layer, const bool& coli = true, const bool& hasToZoom = false );


/**
  * \brief Dessine une polyligne 2D grace � une vecteur de point3d
  * \param tabx Vecteur contenant les coordonn�es de la polyligne � dessiner
  * \param layer Calque ou on veut ins�rer la polyligne dessin�e
  * \param coli Par d�faut true : Permet de nettoyer la polyline i.e supprime les sommets colin�aires
  * \return void
  */
void drawPolyline( std::vector<AcGePoint3d>& tabx, AcCmColor& colorPoly, AcString layer, const bool& coli = true, const bool& hasToZoom = false );

/**
  * \brief
  * \param
  * \param
  * \return void
  */
AcDbPolyline* drawGetPolyline( std::vector<AcGePoint3d>& tabx, AcCmColor& colorPoly, AcString layer, const bool& coli );



/**
  * \brief Dessine une polyligne 2D grace � une vecteur de point3d
  * \param tabx Vecteur contenant les coordonn�es de la polyligne � dessiner
  * \param layer Calque ou on veut ins�rer la polyligne dessin�e
  * \param isClosed Par d�faut false : Permet de fermer ou non la polyligne
  * \return AcDbPolyline
  */
AcDbPolyline* drawGetPolyline( std::vector<AcGePoint3d> tabx,  AcString layer, const bool& isClosed = false );


/**
  * \brief Zoom sur un objet polyligne 2D
  * \param polyline2D Pointeur sur un polyligne 2D
  * \param height Hauteur de zoom
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus zoomOn( AcDbPolyline* polyline2D,
    const double& height = 5.0 );


/**
  * \brief Retourne un pointeur sur une polyligne 2d
  * \param idPoly AcDbObjectId de la polyligne
  * \param poly Pointeur sur la polyligne
  * \param mode Mode d'ouverture (par d�faut en lecture)
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus
openPoly( const AcDbObjectId& idPoly,
    AcDbPolyline*& poly,
    const AcDb::OpenMode& mode = AcDb::kForRead );


/**
  * \brief Recuperer les sommets d'une polyligne 2d dans un vecteur de points
  * \param poly2D La Polyligne 2d
  * \param vertexArray Tableau qui va contenir les sommets de la polyligne2d
  * \param xVertexArray Vecteur qui va contenir les abscisses des sommets de la polyligne2d
  * \return ErrorStatus
  */
Acad::ErrorStatus getVertexOfPoly2D( AcDbPolyline* poly2D,
    AcGePoint2dArray& vertexArray,
    vector<double>& xVertexArray );

/**
  * \brief Recuperer les sommets en 3D (sommets en 2D et z = elevation de la polyligne) d'une polyligne 2d dans un vecteur de points
  * \param poly2D La Polyligne 2d
  * \param vertexArray Tableau qui va contenir les sommets en 3d de la polyligne2d
  * \param xVertexArray Vecteur qui va contenir les abscisses des sommets de la polyligne2d
  * \return ErrorStatus
  */
Acad::ErrorStatus getVertexOfPoly2D( AcDbPolyline* poly2D,
    AcGePoint3dArray& vertexArray,
    vector<double>& xVertexArray );


/**
  * \brief Recuperer les sommets d'une polyligne 2d dans un vecteur de Sommets
  * \param poly2D La Polyligne 2d
  * \param arNewPoint Tableau des points � ajouter dans la liste des sommets
  * \param vertexes Tableau qui va contenir les informations concernant le sommet
  * \param xVertexArray Vecteur qui va contenir les abscisses des sommets de la polyligne2d
  * \return ErrorStatus
  */
Acad::ErrorStatus getVertexesOfPoly2D( AcDbPolyline* poly2D,
    const AcGePoint3dArray& arNewPoint,
    vector<Sommet>& vertexes,
    vector<double>& xVertexArray );

/**
  * \brief Permet de copier et d'ajouter des vertexs dans la copie de la polyligne, mais l'index de chaque vertex depend de sa distance au premier point de la polyligne
  * \param arNewPoint Tableau contenant les nouveaux point � ajouter dans la polyligne
  * \param poly3D La polyligne 3d
  * \param delEntity Si true redessinne la polyligne et supprime l'ancien
  * \return ErrorStatus
  */
Acad::ErrorStatus addArVertexOnPoly( const AcGePoint3dArray& arNewPoint,
    AcDbPolyline*& poly,
    const bool& delEntity = false );


/**
  * \brief Permer de fusionner une selection de polyligne en une seule polyligne
  * \param ssPoly Selection sur les polylignes
  * \return Pointeur vers la nouvelle polyligne
  */
AcDbPolyline* mergePoly( const ads_name& ssPoly );


/**
  * \brief Permet de recuperer tous les polylignes de la selection
  * \param ssPoly Selection sur les polylignes
  * \param vectPolyPointer ???
  * \param vectX ???
  * \param layer ???
  * \param isLayer ???
  * \param op ???
  * \return ErrorStatus
  */
Acad::ErrorStatus  getAllPoly2DFromSs( const ads_name& ssPoly,
    vector<AcDbPolyline*>& vectPolyPointer,
    vector<double>& vectX,
    const AcString& layer = "",
    const bool isLayer = true,
    const AcDb::OpenMode& op = AcDb::kForRead );


/**
  * \brief
  * \param
  * \return
  */
void closePoly2dPtr( vector< AcDbPolyline* >& vectPoly );

/**
  * \brief Preparer les polylignes 2d i.e. trier les vecteurs
  * \param vectX Vecteur contenant les abscisses des vertex des polylignes
  * \param vectPoint2d Vecteur contenant les vertex des polylignes
  * \param vectObjId Vecteur contenant les objects ids des vertexs
  * \param vectPolyPointer Vecteur contenant les pointeurs sur les polylignes
  * \return Acad::ErrorStatus eOk si tout est Ok
  */
Acad::ErrorStatus preparePoly2d( std::vector<double>& vectX,
    AcGePoint2dArray& vectPoint2d,
    std::vector<AcDbObjectId>& vectObjId,
    std::vector<AcDb2dPolyline*>& vectPolyPointer );

/**
  * \brief
  * \param
  * \return
  */
Acad::ErrorStatus preparePoly2d( std::vector<double>& vectX,
    std::vector<AcDbPolyline*>& vectPolyPointer );


/**
  * \fn Acad::ErrorStatus projectThisPolyOnPLane( AcDbPolyline* poly2D,const AcGePlane& plane,const AcGeVectord3d& projectDir );
  * \brief Projete une polyligne 2D sur une plane
  * \param poly3D La polyligne 2d
  * \param plane Le plan de projection
  * \param projectDir Direction de la projection
  * \param erasePoly3d Booleen si on veut supprimer l'ancienne polyligne
  * \return La polyligne 2d projet�e
 */
Acad::ErrorStatus projectThisPolyOnPLane( AcDbPolyline* poly2D,
    const AcGePlane& plane,
    const AcGeVector3d& projectDir );


/**
  * \fn Acad::ErrorStatus projectPoly2DOnPlane( AcDbPolyline* poly2D, const AcGePlane& plane, const AcGeVector3d& projectDir, const bool& erasePoly2d )
  * \brief Projete une polyligne 2D sur une plane
  * \param poly3D La polyligne 2d
  * \param plane Le plan de projection
  * \param projectDir Direction de la projection
  * \param erasePoly3d Booleen si on veut supprimer l'ancienne polyligne
  * \return La polyligne 2d projet�e
 */
AcDbPolyline* getProjectedPoly2DOnPlane( AcDbPolyline* poly2D,
    const AcGePlane& plane,
    const AcGeVector3d& projectDir );

/**
  * \brief Fusionne les sommets colin�aires dans une polyligne 2D
  * \param poly La polyligne qui nous int�resse
  * \param ptarray Liste des sommets colin�aires fusionn�s
  * \param netPoly Bool�en verifiant si on nettoye la polyligne ou pas
  * \param erasePoly Bool�en verifiant si on supprime la polyligne ou pas
  * \param tol Valeur de la tol�rance
  * \return eOk si la fonction est r�ussie
  */
Acad::ErrorStatus coliPoly( AcDbPolyline* poly2D,
    AcGePoint2dArray& ptarray,
    const bool& netPoly = false,
    const bool& erasePoly = false,
    const double& tol = 0.01 );