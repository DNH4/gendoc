/**
 * @file userInput.h
 * @author Romain LEMETTAIS
 * @brief Fonctions pour demander une informations au dessinateur
 */


#pragma once
#include <fmapHeaders.h>
#include "getPoints.h"
#include "dbxutil.h"

/**
  * \brief Demande au dessinateur Oui ou Non
  * \param isYes true si Oui, false sinon
  * \param message Message � afficher dans la fen�tre
  * \return RTNORM si le dessinateur a repondu, RTNONE s'il a fait entree ( et dans ce cas isYes = true ), RTCAN s'il a annule
  */
int askYesOrNo( bool& isYes,
    const AcString& message );


/**
  * \brief Demande au dessinateur de saisir un reel
  * \param message Message � afficher
  * \param defaultValue Valeur par d�faut � proposer
  * \param value Valueur rentr�e par le dessinateur
  * \return RTNORM si le dessinateur a repondu, RTNONE s'il a fait entree ( et dans ce cas tol = defaultValue ), RTCAN s'il a annule
  */
int askForDouble( const ACHAR* message,
    ads_real& defaultValue,
    double& value );



/**
  * \brief Demande a l'utilisateur de rentrer un texte
  * \param textToPrint Texte a afficher dans la ligne de commande
  * \return Le texte rentre par l'utilisateur
  */
AcString askString(
    const AcString& textToPrint );

/**
  * \brief Demande a l'utilisateur de rentrer un texte
  * \param textToPrint Texte a afficher dans la ligne de commande
  * \return Le texte rentre par l'utilisateur
  */
AcString askString(
    const AcString& textToPrint, const bool& containSpaces );

/**
  * \brief Demande a l'utilisateur de rentrer un texte
  * \param textToPrint Texte a afficher dans la ligne de commande
  * \return int
  */
int askString( AcString& outputTxt,
    const AcString& textToPrint, const bool& containSpaces );


/**
  * \brief Demande a l'utilisateur de rentrer une cha�ne de caract�re
  * \param textToPrint Texte � afficher au dessinateur
  * \param txt Cha�ne de caract�re s�lectionn�e par le dessinateur
  * \param defaultValue Valeur par d�faut propos�e au dessinateur
  * \return Retourne un entier d�crivant le r�sultat de la commande: RTCAN pour ECHAP, RTNONE pour Entr�e, etc...
  */
int askString( const ACHAR* textToPrint,
    AcString& resultString,
    ACHAR* defaultValue );


/**
 * ouvre une boite de dialogue windows et demande a l'utilisateur de choisir une couleur
 * \return Renvoie un vecteur a 3 composantes comprises entre 0 et 255, la 1ere coordonnee est la composante en R, la seconde en G, la 3eme en B
 */
vector< unsigned char > askForColors();

/**
 * \fn  int askString( const AcString& textToPrint, AcString& txt );
 *
 * \brief   Demander la valeur de string
 *
 * \date    02/12/2019
 *
 * \param           Le texte a afficher.
 * \param [in,out]  La valeur de resultat text
 *
 * \returns Si la valeur est vraiment enregistrer ou pas.
 */
int askString( const AcString& textToPrint,
    AcString& txt );


/**
  * \brief Demande au dessinateur de saisir un entier
  * \param ACHAR : Message � afficher pour la demande
  * \param defaultValue : valeur par d�faut
  * \param value : valeur saisie par l'utilisateur
  * \return RTNORM si le dessinateur a repondu, RTNONE s'il a fait entree ( et dans ce cas tol = defaultValue ), RTCAN s'il a annule
  */
int askForInt( const ACHAR* message,
    int& defaultValue,
    int& value );
/**
  * \brief Demande au dessinateur de clicker dans un viewport et renvoie son Id
  * \param message Message � afficher pour la demande
  * \param vpId Id du viewport qui a �t� click�
  * \return ErrorsStatus pour d�tailler le d�roulement de la fonction
  */
Acad::ErrorStatus askToClickInVp( const ACHAR* message,
    AcDbObjectId& vpId );


/**
  * \brief Demander � l'utilisateur de choisir une string parmi une lsite de strings
  * \param message Message � afficher pour la demande
  * \param choices Liste de string que peut choisir le dessinateur, chaque string DOIT contenir une majuscule correspondant
  * au raccourci que peut utiliser le dessinateur pour choisir la string
  * \param choice String rentr� par le dessinateur
  * \return int R�sultat de la demande, RTNORM, RTNONE ...
  */
int askToChoose( const AcString& message,
    const AcStringArray& choices,
    AcString& choice );


/**
  * \brief Demander � l'utilisateur de choisir une string parmi un vecteur de strings
  * \param choix Vecteur contenant les choix
  * \param message Le message � afficher
  * \return Le resultat de la selection
  */
AcString askToChoose( const vector<AcString>& choix,
    const AcString& message );


/**
  * \brief Demande au dessinateur de clicker deux points et une �paisseur de coupe
  * \param pt1 Premier point de la coupe
  * \param pt2 Second point de la coupe
  * \param ep Epaisseur de la coupe
  * \param messageP1 Message � afficher lors du click du 1er point
  * \param messageP2 Message � afficher lors du click du 2nd point
  * \param messageEp Message � afficher lors du click de l'�paisseur
  * \param vpId ObjectId du viewport de coupe si on veut forcer de le dessinateur � clicker dans ce viewport
  * sinon on laisse le param�tre vide
  * \return ErrorsStatus eInvalidInput si Echap au moment de clicker, Acad::eOk si tout est bon
  */
Acad::ErrorStatus askSlice( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    double& ep,
    const AcString& messageP1 = _T( "\nClicker un point" ),
    const AcString& messageP2 = _T( "\nClicker un autre point" ),
    const AcString& messageEp = _T( "\nClicker une �paisseur" ) );


/**
  * \brief Demande au dessinateur de clicker deux points et une �paisseur de coupe
  * \param pt1 Premier point de la coupe
  * \param pt2 Second point de la coupe
  * \param ep Epaisseur de la coupe
  * \param idVp ObjectId d'un viewport
  * \param useIdVp Booleen si true on continue la commande quand on est dans le bon viewport du idVp, on termine la commande sinon
  * \param messageP1 Message � afficher lors du click du 1er point
  * \param messageP2 Message � afficher lors du click du 2nd point
  * \param messageEp Message � afficher lors du click de l'�paisseur
  * \param vpId ObjectId du viewport de coupe si on veut forcer de le dessinateur � clicker dans ce viewport
  * sinon on laisse le param�tre vide
  * \return ErrorsStatus eInvalidInput si Echap au moment de clicker, Acad::eOk si tout est bon
  */
Acad::ErrorStatus askSlice( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    double& ep,
    const AcDbObjectId& idVp,
    const bool& useIdVp = true,
    const AcString& messageP1 = _T( "\nClicker un point" ),
    const AcString& messageP2 = _T( "\nClicker un autre point" ),
    const AcString& messageEp = _T( "\nClicker une �paisseur" ) );


/**
  * \brief Demande au dessinateur de clicker deux points et une �paisseur de coupe
  * \param pt1 Premier point de la coupe
  * \param pt2 Second point de la coupe
  * \param messageP1 Message � afficher lors du click du 1er point
  * \param messageP2 Message � afficher lors du click du 2nd point
  * \param vpId ObjectId du viewport de coupe si on veut forcer de le dessinateur � clicker dans ce viewport
  * sinon on laisse le param�tre vide
  * \return ErrorsStatus eInvalidInput si Echap au moment de clicker, Acad::eOk si tout est bon
  */
Acad::ErrorStatus askDelim( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    const AcString& messageP1 = _T( "\n Clicker un point" ),
    const AcString& messageP2 = _T( "\n Clicker un autre point" ),
    const AcDbObjectId& vpId = AcDbObjectId::kNull );

/**
  * \brief Demander la d�limitation de la zone � l'utilisateur
  * \param pt1 Premier point du rectangle
  * \param pt2 Deuxi�me point du rectangle
  * \param pt3 Troisi�me point du rectangle
  * \param messageP1 Message � afficher pour la s�lection du premier point
  * \param messageP2 Message � afficher pour la s�lection du deuxi�me point
  * \param messageP3 Message � afficher pour la s�lection du troisi�me point
  * \return int d'entr�e utilisateur
  */
Acad::ErrorStatus askZoneDelim( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    AcGePoint3d& pt3,
    const AcString& messageP1 = _T( "Cliquer le premier point" ),
    const AcString& messageP2 = _T( "Cliquer le deuxieme point" ),
    const AcString& messageP3 = _T( "Cliquer le troisieme point" ),
    const AcGeVector3d& norm = AcGeVector3d::kZAxis );

/**
  * \brief Demander la d�limitation de la zone � l'utilisateur
  * \param pt1 Premier point du rectangle
  * \param pt2 Deuxi�me point du rectangle
  * \param pt3 Troisi�me point du rectangle
  * \param messageP1 Message � afficher pour la s�lection du premier point
  * \param messageP2 Message � afficher pour la s�lection du deuxi�me point
  * \param messageP3 Message � afficher pour la s�lection du troisi�me point
  * \return int d'entr�e utilisateur
  */
Acad::ErrorStatus askZoneDelim( AcGePoint3d& pt1,
    AcGePoint3d& pt2,
    AcGePoint3d& pt3,
    const AcDbObjectId& idVp,
    const bool& useVp = true,
    const AcString& messageP1 = _T( "Cliquer le premier point" ),
    const AcString& messageP2 = _T( "Cliquer le deuxieme point" ),
    const AcString& messageP3 = _T( "Cliquer le troisieme point" ),
    const AcGeVector3d& norm = AcGeVector3d::kZAxis );


/**
  * \brief Demander l'entr�e utilisateur avec les options
  * \param option Les options qu'on veut afficher
  * \param msg Le message � afficher
  * \param prevPt Le point precedent
  * \param newPt Le nouveau point
  * \param kwChoice Les choix
  * \return int d'entr�e utilisateur
  */
int askUserOpt( const AcString& option,
    const AcString& msg,
    const AcGePoint3d& prevPt,
    AcGePoint3d& newPt,
    AcString& kwChoice );


/**
  * \brief Demander � l'utilisateur de cliquer un point ou d'entrer un chiffre
  * \param res Le nombre entr� par l'utilisateur
  * \param message Le message
  * \param defValue Valeur par d�faut
  * \return ErrorStatus
  */
Acad::ErrorStatus askOrEnterDist( double& res,
    const AcString& message = _T( "Entrer une distance ou cliquer sur un point" ),
    const double& defValue = 0.1 );


/**
  * \brief Demander � l'utilisateur de cliquer un point ou d'entrer une valeur
  * \param res Le nombre entr� par l'utilisateur
  * \param ptRes Le point cliqu� par l'utilisateur
  * \param isDefault Est true si l'utilisateur a cliquer un point
  * \param d Le chiffre entr� par l'utilisateur
  * \param prevPoint le point pr�cedent
  * \return ErrorStatus
  */
Acad::ErrorStatus askPointOrEnterValue( double& res,
    AcGePoint3d& ptRes,
    bool& isDefault,
    const AcString& message = _T( "Cliquer un point ou entrer une valeur: " ),
    const double& defValue = 1,
    const AcGePoint3d& prevPoint = AcGePoint3d::kOrigin );