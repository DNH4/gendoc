#pragma once
#include "acString.h"


ACHAR* acStrToAcharPointeur( AcString& acStr )
{
    return acStr.getBuffer( acStr.getLength() );
}


AcString strToAcStr( const string& s )
{

    AcString  acString = AcString();
    int l = s.length();
    
    for( int i = 0; i < l; i++ )
    {
        char a = s[ i ];
        
        if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"i" )
            acString += _T( "i" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else if( a == *"�" )
            acString += _T( "�" );
        else
            acString += AcString( s[ i ] );
    }
    
    return acString;
}



AcString  acStrWithoutAccent( const AcString& s )
{
    AcString  acString = AcString();
    int l = s.length();
    
    for( int i = 0; i < l; i++ )
    {
        char a = s[ i ];
        
        //int temp = int(a);
        //
        //char ap = (char)25;
        
        if( a == *"�" )
            acString += _T( "e" );
        else if( a == *"�" )
            acString += _T( "e" );
        else if( a == *"�" )
            acString += _T( "e" );
        else if( a == ( ( char ) - 55 ) )
            acString += _T( "E" );
            
        else if( a == *"�" )
            acString += _T( "a" );
        else if( a == *"�" )
            acString += _T( "a" );
            
        else if( a == *"�" )
            acString += _T( "i" );
        else if( a == *"�" )
            acString += _T( "i" );
            
        else if( a == *"�" )
            acString += _T( "u" );
        else if( a == *"�" )
            acString += _T( "u" );
        else if( a == *"�" )
            acString += _T( "u" );
            
        else if( a == *"�" )
            acString += _T( "o" );
        else if( a == *"�" )
            acString += _T( "o" );
            
        else if( a == *"�" )
            acString += _T( "c" );
            
            
        else if( a == ( ( char )25 ) )
            acString += _T( "'" );
            
        else
            acString += AcString( s[ i ] );
    }
    
    return acString;
}

string acStrToStr( const AcString& acString )
{
    try
    {
        const OdChar* ptr = acString.constPtr();
        wstring ws( ptr );
        string str( ws.begin(), ws.end() );
        return str;
    }
    
    catch( exception e )
    {
        return "";
    }
}


AcString numberToAcString(
    const double&  value,
    const int&     numberOfDigits )
{
    AcString l_sString;
    
    if( numberOfDigits == 0 ) l_sString.format( _T( "%.0f" ), value );
    else if( numberOfDigits == 1 ) l_sString.format( _T( "%.1f" ), value );
    else if( numberOfDigits == 2 ) l_sString.format( _T( "%.2f" ), value );
    else if( numberOfDigits == 3 ) l_sString.format( _T( "%.3f" ), value );
    else if( numberOfDigits == 4 ) l_sString.format( _T( "%.4f" ), value );
    else if( numberOfDigits == 5 ) l_sString.format( _T( "%.5f" ), value );
    else if( numberOfDigits == 6 ) l_sString.format( _T( "%.6f" ), value );
    else if( numberOfDigits == 7 ) l_sString.format( _T( "%.7f" ), value );
    else if( numberOfDigits == 8 ) l_sString.format( _T( "%.8f" ), value );
    else if( numberOfDigits == 9 ) l_sString.format( _T( "%.9f" ), value );
    
    else if( numberOfDigits == 10 ) l_sString.format( _T( "%.10f" ), value );
    else if( numberOfDigits == 11 ) l_sString.format( _T( "%.11f" ), value );
    else if( numberOfDigits == 12 ) l_sString.format( _T( "%.12f" ), value );
    else if( numberOfDigits == 13 ) l_sString.format( _T( "%.13f" ), value );
    else if( numberOfDigits == 14 ) l_sString.format( _T( "%.14f" ), value );
    else if( numberOfDigits == 15 ) l_sString.format( _T( "%.15f" ), value );
    else if( numberOfDigits == 16 ) l_sString.format( _T( "%.16f" ), value );
    else if( numberOfDigits == 17 ) l_sString.format( _T( "%.17f" ), value );
    else if( numberOfDigits == 18 ) l_sString.format( _T( "%.18f" ), value );
    else if( numberOfDigits == 19 ) l_sString.format( _T( "%.19f" ), value );
    else                  l_sString.format( _T( "%.20f" ), value );
    
    return l_sString;
}


AcString numberToAcString( const int& d )
{
    return numberToAcString( d, 0 );
}


AcString numberToAcString( const unsigned __int64& d )
{
    return numberToAcString( d, 0 );
}


AcString numberToAcString( const float& d )
{
    return strToAcStr( to_string( long double ( d ) ) );
}



bool stringToDouble(
    const AcString    str,
    double* const     value )
{
    return RTNORM == acdbDisToF( str, 2, value );
}



bool stringToInt(
    const AcString    str,
    int* const        value )
{
    double l_dInt;
    
    if( RTNORM != acdbDisToF( str, 2, &l_dInt ) )
        return false;
    else
        *value = ( int )l_dInt;
        
    return true;
}



bool areEqualNoCase(
    const AcString& sText1,
    const AcString& sText2 )
{
    return
        //Verifier la longueur des deux textes
        sText1.compareNoCase( sText2 ) == 0
        &&
        //Verifier que les deux textes ont les meme caracteres
        sText1.matchNoCase( sText2 ) == sText1.length();
}


bool areEqualCase(
    const AcString& sText1,
    const AcString& sText2 )
{
    return ( sText1.compare( sText2 )  == 0 );
}

vector< string > splitString( string sentence,
    const string& sep )
{
    // Liste des sous-string
    vector< string > list( 0 );
    
    // Position du s�parateur dans la string
    int pos = sentence.find( sep );
    
    // Si la string ne contient pas ce s�parateur, retourner un vecteur contenant seulement cette string
    if( pos != string::npos )
    {
    
        // tant qu'on trouve le s�parateur dans la string on continue � la d�couper
        while( pos != string::npos )
        {
        
            // on ajoute la portion � la liste de sous-string
            list.push_back( sentence.substr( 0, pos ) );
            
            // On enl�ve la premi�re portion de string de la string totale
            sentence = sentence.substr( pos + 1, sentence.length() );
            
            // on recalcule la position du 1er it�rateur dans la string
            pos = sentence.find( sep );
        }
    }
    
    list.push_back( sentence );
    
    // Renvoie la liste
    return list;
}


bool containsNumber(
    const string& word )
{
    int length = word.length();
    
    bool digit = false;
    
    for( int i = 0; i < length; i++ )
    {
        if( isdigit( word[ i ] ) )
            digit = true;
            
        return( digit );
    }
}


string toUpperString(
    const string& word,
    const bool& hasAccent )
{
    //on declare un acString vide (pour simplifier la concatenation on utilise un acString :) )
    string result = "";
    
    //on parcourt la chaine de caract�re
    int length = word.length();
    
    for( int i = 0; i < length; i++ )
    {
        //on recup�re le i�me caract�re
        char c = word[i];
        
        //on verifie si on garde ou pas l'accents
        //on le convertit en majuscle
        if( hasAccent == false )
        {
            if( c == '�' || c == '�' || c == '�' || c == '�' )
                c = 'E';
            else if( c == '�' || c == '�' )
                c = 'I';
            else if( c == '�' || c == '�' || c == '�' )
                c =  'A';
            else if( c == '�' || c == '�' || c == '�' )
                c =  'U';
            else if( c == '�' || c == '�' )
                c =  'O';
            else if( c == '�' )
                c = 'C';
            else if( c == '�' )
                c = 'N';
            else
                c = toupper( c );
        }
        
        else
            c = toupper( c );
            
        //on le convertit en acstring
        //on l'affecte dans l'acstring
        result += c;
    }
    
    //on renvoie le result en string
    return result;
}



AcString toUpperString(
    const AcString& word,
    const bool& hasAccent )
{
    //on declare un acString vide (pour simplifier la concatenation on utilise un acString :) )
    AcString result = "";
    
    //on parcourt la chaine de caract�re
    int length = word.length();
    
    if( length == 0 )
        return result;
        
    for( int i = 0; i < length; i++ )
    {
        //on recup�re le i�me caract�re
        char c = word[i];
        
        //on verifie si on garde ou pas l'accents
        //on le convertit en majuscle
        if( hasAccent == false )
        {
            if( c == '�' || c == '�' || c == '�' || c == '�' )
                c = 'E';
            else if( c == '�' || c == '�' )
                c = 'I';
            else if( c == '�' || c == '�' || c == '�' )
                c =  'A';
            else if( c == '�' || c == '�' || c == '�' )
                c =  'U';
            else if( c == '�' || c == '�' )
                c =  'O';
            else if( c == '�' )
                c = 'C';
            else if( c == '�' )
                c = 'N';
            else
                c = toupper( c );
        }
        
        else
            c = toupper( c );
            
        //on le convertit en acstring
        //on l'affecte dans l'acstring
        result += c;
    }
    
    //on renvoie le result en string
    return result;
}


string trimLeftRight(
    const string& str )
{
    size_t first = str.find_first_not_of( ' ' );
    
    if( string::npos == first )
        return str;
        
    size_t last = str.find_last_not_of( ' ' );
    return str.substr( first, ( last - first + 1 ) );
}


AcString  trimLeftRight(
    const AcString& input )
{
    AcString output = "";
    //Boucler sur les caract�res de l'entr�e
    int length = input.length();
    
    if( length == 0 )
        output = input;
        
    for( int i = 0; i < length; i++ )
    {
        //Pour le premier caract�re
        if( i == 0 )
        {
            char carFirst = input[i];
            
            if( carFirst == ' ' )
                continue;
            else output.append( input[i] );
        }
        
        else if( i == length - 1 )
        {
            char carEnd = input[i];
            
            if( carEnd == ' ' )
                break;
            else output.append( input[i] );
        }
        
        else
            output.append( input[i] );
            
    }
    
    return output;
    
}


bool isEqualToCommand( const AcString& oneString,
    const AcString& command,
    const AcString& shortcutCmd )
{
    if( ( oneString.isEmpty() ) || ( command.isEmpty() ) || ( shortcutCmd.isEmpty() ) )
        return false;
        
    if( oneString.compareNoCase( shortcutCmd ) == 0 )
        return true;
        
    if( oneString.compareNoCase( command ) == 0 )
        return true;
        
    return false;
}



AcString numberReadable( const AcString& number, const AcString& sep )
{
    // Compter le nombre
    AcString numberreverse = number;
    numberreverse.makeReverse();
    int step = 1;
    int sz = numberreverse.length();
    AcString numberresult = "";
    
    for( int i = 0; i < sz; i++ )
    {
        numberresult += numberreverse[i];
        
        if( step % 3 == 0 && step != sz )
            numberresult += sep;
            
        step++;
    }
    
    numberresult.makeReverse();
    return numberresult;
}


bool isAcstringNumber(const AcString& s)
{
	string ac = acStrToStr(s);

	int nb_point = 0;

	int l = ac.length();

	if (!l)
		return false;

	for (int i = 0; i < l; i++)
	{
		if (ac[i] == '.')
			nb_point++;
		else if (!isdigit(ac[i]))
			return false;
	}

	if (nb_point <= 1)
		return true;
	else
		return false;
}