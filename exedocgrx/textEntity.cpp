#include "textEntity.h"


Acad::ErrorStatus getTexteStyleId( const AcString& styleName,
    AcDbObjectId& recordId )
{
    // R�cupere la database
    AcDbDatabase* pDB = acdbHostApplicationServices()->workingDatabase();
    
    // R�cup�rer la table des textes
    Acad::ErrorStatus es = Acad::eOk;
    AcDbSymbolTable* pTable;
    
    //Recuperer la table de style de texte
    if( es = pDB->getTextStyleTable( pTable, AcDb::kForRead ) )
    {
        print( es );
        return es;
    }
    
    //Recuperer l'id du style de texte
    if( es = pTable->getAt( styleName,
                recordId ) )
    {
        //Fermer les objets autocads
        print( "Le style de texte %s n'existe pas", styleName );
        pTable->close();
        return es;
    }
    
    pTable->close();
    return es;
}

Acad::ErrorStatus getStyleStringName( AcString& textStyle, const AcDbObjectId& obId )
{
    Acad::ErrorStatus es = eOk;
    AcDbTextStyleTableRecordPointer tableStyle;
    
    //Si la table des style ne march pas
    if( es = tableStyle.open( obId, AcDb::kForRead ) )
        return es;
        
    textStyle = tableStyle->getName();
    
    es = tableStyle->close();
    return es;
}


Acad::ErrorStatus drawTextAcString(AcString acTest, AcGePoint3d insertPoint, AcString layer) 
{
	AcDbText* text = new AcDbText();
	text->setTextString(acTest);
	if (isLayerExisting(layer))
		text->setLayer(layer);
	text->setPosition(insertPoint);
	addToModelSpace(text);
	text->close();
	return Acad::eOk;
}

/*AcDbText* drawTextAcString(AcString acTest, AcGePoint3d insertPoint)
{
	AcDbText* text = new AcDbText();
	text->setTextString(acTest);

	text->setPosition(insertPoint);
	addToModelSpace(text);
	return text;
}*/