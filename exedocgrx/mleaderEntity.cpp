#pragma once
#define _USE_MATH_DEFINES
#include "mleaderEntity.h"
#include "layer.h"

long getSsMLeader(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "", "MULTILEADER", layerName );
}



long getSsOneMLeader(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSingleSelection( ssName, "MULTILEADER", layerName );
}



long getSsAllMLeader(
    ads_name&        ssName,
    const AcString&  layerName )
{
    return getSelectionSet( ssName, "X", "MULTILEADER", layerName ) ;
}

AcDbMLeader* getMLeaderFromSs(
    const ads_name&        ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Recuperer l'entite
    AcDbEntity*  pEntityMLeader = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbMLeader::cast( pEntityMLeader );
}



AcDbMLeader* readMLeaderFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getMLeaderFromSs( ssName, iObject, AcDb::kForRead );
}


AcDbMLeader* writeMLeaderFromSs(
    const ads_name&        ssName,
    const long&            iObject )
{
    //Recuperer l'entite
    return getMLeaderFromSs( ssName, iObject, AcDb::kForWrite );
}




Acad::ErrorStatus drawSimpleMleader(
    AcDbMLeader*& mleader,
    const AcGePoint3d& ptFirst,
    const AcGePoint3d& textLocation,
    const AcString& textContent,
    const double& textHeight )
{
    // Cr�er  le texte
    AcDbMText* mtext = new AcDbMText();
    
    mtext->setContents( textContent );
    mtext->setLocation( textLocation );
    mtext->setDatabaseDefaults();
    mtext->setTextHeight( textHeight );
    
    // Cr�er le mleader
    mleader->setDatabaseDefaults();
    
    // Lier le mleader au texte
    mleader->setContentType( AcDbMLeaderStyle::kMTextContent );
    
    int idx = 0;
    mleader->addLeaderLine( textLocation, idx );
    mleader->addFirstVertex( idx, ptFirst );
    mleader->setMText( mtext );
    mtext->close();
    
    addToModelSpace( mleader );
    
    // Verifier l'id du mleader
    if( mleader->id() == AcDbObjectId::kNull )
        return eNotApplicable;
        
        
    return eOk;
}