#pragma once
#include "fmapHeaders.h"
#include "progressBar.h"
#include "pointEntity.h"
#include "lineEntity.h"


struct Sommet
{
    //index du point dans la curve
    int index;
    
    //Son coordonn�es en 3D
    AcGePoint3d point;
    
    //Bulge
    double bulge;
    
    //Distance curviligne par rapport au premier point de la courbe
    double distance;
};

/**
  * \brief Permet de Comparer un pair<>
  * \param p1 Le premier pair
  * \param p2 Le second pair
  * \return True si c'est le m�me pair
  */
bool comparePair( const pair<double, AcGePoint3d>& p1,
    const pair<double, AcGePoint3d>& p2 );


/**
  * \brief Permet de trouver si deux nombre sont les m�mes avec une certaine precision
  * \param num1 Le premier double
  * \param num2 Le deuxieme double
  * \param precision La tolerance d'equivalence
  * \return True si c'est le m�me nombre
  */
bool isTheSame( const double& num1,
    const double& num2,
    const double& precision );


/**
  * \brief Trouver le point le plus proche dans la liste par rapport au point pass� en argument de la fonction
  * \param ptRef Point que l'on recherche dans la liste
  * \param arPt Liste de points 3d
  * \param ptRes Point de la liste le plus proche de ptRef
  * \param tol Tol�race de recherche pour la distance euclidienne 3d
  * \return L'index de x dans le vecteur de double
  */
bool findClosestPointInArray( const AcGePoint3d& ptRef,
    AcGePoint3dArray& arPt,
    AcGePoint3d& ptRes,
    double tol );

/**
  * \brief Fonction de dichotomie
  * \param xPos Vecteur de double � d�couper
  * \param x Double � chercher dans le vecteur
  * \param epsilon Tolerance de recherche
  * \return L'index de x dans le vecteur de double
  */
int dichotom( const std::vector< double >& xPos,
    const double& x,
    const double& epsilon );


/**
  * \brief Fonction de dichotomie
  * \param xPos Vecteur de double � d�couper
  * \param x Double � chercher dans le vecteur
  * \param epsilon Tolerance de recherche
  * \return Vecteur contenant les indexs de x dans le vecteur de double
  */
std::vector<int> dichotomToVec( const std::vector<double>& xPos,
    const double& x,
    const double& epsilon );

/**
  * \brief Fonction de dichotomie
  * \param xPos Vecteur de double � d�couper
  * \param x Double � chercher dans le vecteur
  * \param epsilon Tolerance de recherche
  * \return Vecteur contenant les indexs de x dans le vecteur de double
  */
std::vector<int> dichotomToVecX( const std::vector<double>& xPos,
    const double& xmin,
    const double& xmax );


/**
  * \brief Permet de trier un AcGePoint3dArray
  * \param vectPoint Tableau contenant les points 3d
  * \param xPos Vecteur de double sortie tri�
  * \return ErrorStatus eOk si le filtrage est effectu�
  */
Acad::ErrorStatus sortList(
    AcGePoint3dArray& vectPoint,
    vector< double >& xPos );

/**
  * \brief Permet de trier un vecteur de point 3d
  * \param vectPoint Tableau contenant les points 3d
  * \param xPos Vecteur de double sortie tri�
  * \return ErrorStatus eOk si le filtrage est effectu�
  */
Acad::ErrorStatus sortList(
    vector<AcGePoint3d>& vectPoint,
    vector< double >& xPos );


/**
  * \brief Permet de trier un AcGePoint2dArray
  * \param vectPoint Tableau contenant les points 3d
  * \param xPos Vecteur de double sortie tri�
  * \return ErrorStatus eOk si le filtrage est effectu�
  */
Acad::ErrorStatus sortList(
    AcGePoint2dArray& vectPoint,
    vector<double>& xPos );


/**
  * \brief Permet de nettoyer les doublons dans un tableau de point 3d
  * \param pointArray Tableau contenant les points 3d
  * \return ErrorStatus
  */
Acad::ErrorStatus eraseDuplicate( AcGePoint3dArray& pointArray,
    const double& tol = 0.1 );


/**
  * \brief Permet de nettoyer les doublons dans un tableau de point 2d
  * \param pointArray Tableau contenant les points 2d
  * \return ErrorStatus
  */
Acad::ErrorStatus eraseDuplicate( AcGePoint2dArray& pointArray,
    const double& tol = 0.1 );

/**
  * \brief Permet de trouver si un point est dans une liste de AcArray de point 3d en utilisant les tris
  * \param point Le point 3d � rechercher dans le AcArray
  * \param vectDouble Vecteur tri� contenant les x de l'acArray
  * \param acPoint3d AcArray de point 3d o� on va rechercher le point en entr�e
  * \param index L'index du point trouv�
  * \param tolX La tolerance qu'on veut avoir en X
  * \param tolY La tolerance qu'on veut avoir en Y
  * \param tolZ La tolerance qu'on veut avoir en Z
  * \param del Si True on supprime le point trouv� dans la AcArray
  * \return True si le point est dans le vecteur
  */
bool isInList( const AcGePoint3d& pos,
    std::vector<double>& xPos,
    AcGePoint3dArray& acPoint3d,
    int& index,
    const double& tolXYZ = 0.01,
    const double& tolDichotom = 0.01,
    const bool& del = false );

bool isInList( const AcGePoint3d& pos,
    std::vector<double>& xPos,
    vector<AcGePoint3d>& acPoint3d,
    int& index,
    const double& tolXYZ = 0.01,
    const double& tolDichotom = 0.01,
    const bool& del = false );

/**
  * \brief Permet de trouver si un point est dans une liste de AcArray de point 3d en utilisant les tris
  * \param point Le point 3d � rechercher dans le AcArray
  * \param vectDouble Vecteur tri� contenant les x de l'acArray
  * \param acPoint3d AcArray de point 3d o� on va rechercher le point en entr�e
  * \param tolX La tolerance qu'on veut avoir en X
  * \param tolY La tolerance qu'on veut avoir en Y
  * \param del Si True on supprime le point trouv� dans la AcArray
  * \return True si le point est dans le vecteur
  */
bool isInList( const AcGePoint2d& point,
    std::vector<double>& xPos,
    AcGePoint3dArray& pos,
    std::vector<double>& vectZ,
    std::vector<int>& indexZ,
    const double& tolXY = 0.01,
    const double& tolDichotom = 0.01,
    const bool& del = false );


/**
  * \brief Permet de trouver si un point est dans une liste de AcArray de point 3d en utilisant les tris
  * \param point Le point 3d � rechercher dans le AcArray
  * \param vectDouble Vecteur tri� contenant les x de l'acArray
  * \param acPoint3d AcArray de point 3d o� on va rechercher le point en entr�e
  * \param neighbourPoint3d AcArray de point 3d o� on va mettre les points voisins
  * \tolerance tolerance de recherche
  * \return True si le point est dans le vecteur
  */
bool isInList( const AcGePoint3d& point3d,
    std::vector<double>& vectDouble,
    AcGePoint3dArray& acPoint3d,
    AcGePoint3dArray& neighbourPoint3d,
    const double& tolerance );


/**
  * \brief Permet de trouver si un point 2d est dans une liste de AcArray de point 3d en utilisant les tris
  * \param point Le point 2d � rechercher dans le AcArray
  * \param vectDouble Vecteur tri� contenant les x de l'acArray
  * \param acPoint3d AcArray de point 3d tri� est sans doublons o� on va chercher le point 2d
  * \param index L'index du point trouv� dans le vecteur de point 3d
  * \param tolerance tolerance de recherche
  * \return True si le point est dans le vecteur
  */
bool isInUniqueList2d( const AcGePoint2d& pt2d,
    std::vector<double>& vectDouble,
    AcGePoint3dArray& acPoint3d,
    int& index,
    const double& tolerance = 0.01 );



/**
  * \brief Permet d'ajouter un point dans une liste de point d�j� tri� en X
  * \param point Le point 3d � ajouter
  * \param vectPoint3d Vecteur de point 3d o� on va rechercher le point en entr�e
  * \param tolX La tolerance qu'on veut avoir en X
  * \return ErrorStatus
  */
Acad::ErrorStatus addInList( const AcGePoint3d& point,
    AcGePoint3dArray& acPoint3d,
    vector<double>& vectDouble,
    const double& tolX = 0.01 );


/**
  * \brief Permet d'ajouter un double dans un vecteur de double tri�
  * \param dc Le double � ajouter dans le vecteur
  * \param vectDouble Vecteur de double d�ja tri�
  * \param tol La tolerance
  * \return ErrorStatus
  */
Acad::ErrorStatus addDoubleInVector( const double& dc,
    std::vector< double >& vectDouble,
    const double& tol = 0.0001 );


/**
  * \brief Fonction qui permet de trier des points selon un point de reference
  * \param ptRef Point de Reference
  * \param arPoint Tableau de point � trier
  * \return ErrorStatus
  */
Acad::ErrorStatus sortListUsingDistance( const AcGePoint3d& ptRef,
    AcGePoint3dArray& arPoint );


/**
  * \brief Fonction qui permet de trier des points selon la distance curviligne d'une polyligne 3d de reference
  * \param poly3D Polyligne 3d de reference
  * \param arPoint Tableau de point � trier
  * \return ErrorStatus
  */
Acad::ErrorStatus sortListUsingCurveDistance( AcDb3dPolyline* poly3D,
    AcGePoint3dArray& arPoint );


/**
  * \brief Fonction qui permet de trier des points selon la distance curviligne d'une polyligne 2D de reference
  * \param poly2D L'entit� reference
  * \param vertexes Vecteur des sommets (struct Sommet)
  * \return ErrorStatus
  */
Acad::ErrorStatus sortListUsingCurveDistance( AcDbPolyline* poly2D,
    vector<Sommet>& vertexes );

/**
  * \brief
  * \param
  * \param
  * \return
  */
Acad::ErrorStatus sortList( vector<double>& xPos );

/**
  * \brief
  * \param
  * \param
  * \return
  */
Acad::ErrorStatus addInList( const AcGePoint3d& point,
    vector<AcGePoint3d>& acPoint3d,
    vector<double>& vectDouble,
    const double& tolX = 0.01 );