#include "acadVariable.h"

AcString getStringVariable( const AcString& sVarName )
{
    //Declarer le resbuf
    struct resbuf rbVariable;
    
    //Recuperer la valeur
    if( RTNORM != acedGetVar( sVarName, &rbVariable ) )
    {
        //Sortir
        return AcString::kEmpty;
    }
    
    //Verifier le type
    if( rbVariable.restype != RTSTR )
    {
        //Sortir
        return AcString::kEmpty;
    }
    
    //Renvoyer la valeur
    return rbVariable.resval.rstring;
}


void setStringVariable( const AcString& sVarName,
    const AcString& sVarValue )
{

    //Declarer le resbuf
    struct resbuf rbVariable;
    
    //Recuperer la valeur
    if( RTNORM != acedGetVar( sVarName, &rbVariable ) )
        return;
        
    //Verifier le type
    if( rbVariable.restype != RTSTR )
        return;
        
    //Stter la valeur la valeur
    AcString acstring = sVarValue;
    rbVariable.resval.rstring = acStrToAcharPointeur( acstring );
    
    acedSetVar( sVarName,
        &rbVariable );
}


double getRealVariable( const AcString& sVarName )
{
    //Declarer le resbuf
    struct resbuf rbVariable;
    
    //Recuperer la valeur
    if( RTNORM != acedGetVar( sVarName, &rbVariable ) )
    {
        //Sortir
        return 0.0;
    }
    
    //Verifier le type
    if( rbVariable.restype == RTREAL )
    {
        //Sortir
        return rbVariable.resval.rreal;
    }
    
    //Cas d'un integer
    else if( rbVariable.restype == RTSHORT )
    {
        //Sortir
        return rbVariable.resval.rint;
    }
    
    //Sortir
    return 0.0;
}

int getIntVariable( const AcString& sVarName )
{
    //Declarer le resbuf
    struct resbuf rbVariable;
    
    //Recuperer la valeur
    if( RTNORM != acedGetVar( sVarName, &rbVariable ) )
    {
        //Sortir
        return 0;
    }
    
    //Verifier le type
    if( rbVariable.restype == RTSHORT )
    {
        //Sortir
        return rbVariable.resval.rint;
    }
    
    //Sortir
    return 0;
}

bool getBoolVariable( const AcString& sVarName )
{
    //Declarer le resbuf
    struct resbuf rbVariable;
    
    //Recuperer la valeur
    if( RTNORM != acedGetVar( sVarName, &rbVariable ) )
    {
        //Sortir
        return false;
    }
    
    //Verifier le type
    if( rbVariable.restype == RTSHORT )
    {
        //Renvoyer la valeur
        return rbVariable.resval.rint == 1;
    }
    
    //Sortir
    return false;
}