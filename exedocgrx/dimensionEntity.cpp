﻿#pragma once
#include "dimensionEntity.h"
#include "list.h"




long getSsDimension(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSelectionSet( ssName, "", "DIMENSION", layerName );
}



long getSsOneDimension(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSingleSelection( ssName, "DIMENSION", layerName );
}



long getSsAllDimension(
    ads_name        ssName,
    const AcString  layerName )
{
    return getSelectionSet( ssName, "X", "DIMENSION", layerName );
}



AcDbDimension* getDimensionFromSs(
    const ads_name&               ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Récuperer l'entite
    AcDbEntity*  l_pEntityLine = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbDimension::cast( l_pEntityLine );
}


AcDbAlignedDimension* getAlignedDimensionFromSs(
    const ads_name&               ssName,
    const long&            iObject,
    const AcDb::OpenMode&  opMode )
{
    //Récuperer l'entite
    AcDbEntity*  l_pEntityLine = getEntityFromSs( ssName, iObject, opMode );
    
    //Convertir l'entite en ligne
    return AcDbAlignedDimension::cast( l_pEntityLine );
}


AcString getDimensionText( AcDbDimension* dimension )
{
    AcString text = "";
    //Recuperer l'id du dimension
    AcDbObjectId id = dimension->dimBlockId();
    AcDbBlockTableRecord* blockRecord;
    
    if( Acad::eOk != acdbOpenAcDbObject( ( AcDbObject*& )blockRecord, id, AcDb::kForRead ) )
        return "";
        
    AcDbBlockTableRecordIterator* pi;
    
    blockRecord->newIterator( pi );
    
    while( !pi->done() )
    {
        AcDbEntity* pEnt;
        
        if( Acad::eOk != pi->getEntity( pEnt, AcDb::kForRead ) )
            continue;
            
        if( pEnt->isKindOf( AcDbMText::desc() ) )
        
        {
        
            AcDbMText* pt = ( AcDbMText* ) pEnt;
            
            text = pt->contents();
            pEnt->close();
            break;
            
        }
        
        pEnt->close();
        pi->step();
        
    }
    
    blockRecord->close();
    
    return text;
}

//Acad::ErrorStatus insertAlignedDim( AcDbDimension* dimension,
//    AcDbHatch* hatch,
//    const AcString& patternName,
//    const AcString& layerName,
//    const AcCmColor& color,
//    const AcString& lineTypeName )