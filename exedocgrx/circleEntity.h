/**
  * \file       circleEntity.h
  * \author     Marielle H.R
  * \brief      Fichier contenant des fonctions utiles pour travailler avec les cercles
  * \date       16 juillet 2019
 */

#pragma once
#include <dbents.h>
#include "selectEntity.h"
#include "arcEntity.h"
#include <math.h>


/**
  * \brief Fait une selection quelconque sur les cercles
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsCircle(
    ads_name&  ssName,
    const AcString&  layerName = "" );


/**
  * \brief Selectionne un seul cercle dans le dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsOneCircle(
    const ads_name&    ssName,
    const AcString&    layerName = "" );



/**
  * \brief Selectionne tous les cercleS du dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsAllCircle(
    ads_name&  ssName,
    const AcString&  layerName = "" );


/**
  * \brief Renvoie un pointeur sur le cercle numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du cercle a recuperer, par defaut 0
  * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \return Pointeur sur un cercle
  */
AcDbCircle* getCircleFromSs(
    const ads_name& ssName,
    const long& iObject          = 0,
    const AcDb::OpenMode& opMode = AcDb::kForRead );


/**
  * \brief Renvoie un pointeur en LECTURE sur le cercle numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du cercle a recuperer, par defaut 0
  * \return Pointeur sur un cercle
  */
AcDbCircle* readCircleFromSs(
    const ads_name&    ssName,
    const long&        iObject = 0 );


/**
  * \brief Renvoie un pointeur en ECRITURE sur le cercle numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du cercle a recuperer, par defaut 0
  * \return Pointeur sur un cercle
  */
AcDbCircle* writeCircleFromSs(
    const ads_name&        ssName,
    const long&           iObject = 0 );

/**
  * \brief Renvoie un tableau de points 3D
  * \param circle Pointeur sur un cercle
  * \param fleche Fleche de discretisatin
  * \return Un tableau de points 3D
  */
AcGePoint3dArray discretize( AcDbCircle* circle, const double& fleche );

/**
  * \brief Renvoie l'objet cercle
  * \param ptCenter le centre du cercle
  * \param radius rayon du cercle
  * \param normal vecteur normal au plan du cercle
  * \return l'objet cercle
  */
AcDbCircle* insertCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcGeVector3d& normal = AcGeVector3d( 0, 0, 1 ) );


/**
  * \brief Cr�er un circle et ajout � la base de donn�e et renvoie l'ID
  * \param circleId l'ID du cercle cr�er
  * \param ptCenter le centre du cercle
  * \param radius rayon du cercle
  * \param normal vecteur normal au plan du cercle
  * \return ErrorStatus
  */
Acad::ErrorStatus insertCircle( AcDbObjectId& circleId,
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcGeVector3d& normal = AcGeVector3d( 0, 0, 1 ) );

/**
  * \brief Renvoie l'objet cercle
  * \param ptCentre le centre du cercle
  * \param radius rayon du cercle
  * \param normal Vecteur normal du plan du cercle
  * \param hasToZoom true si on zoom sur le circle false sinon
  * \return ErrorStatus
  */
Acad::ErrorStatus drawCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcGeVector3d& normal = AcGeVector3d( 0, 0, 1 ),
    const bool& hasToZoom = false );
/**
  * \brief Renvoie l'objet cercle
  * \param ptCentre le centre du cercle
  * \param radius rayon du cercle
  * \param normal Vecteur normal du plan du cercle
  * \param color indice de couleur Acad pour colorer le cercle
  * \param hasToZoom true si on zoom sur le circle false sinon
  * \return ErrorStatus
  */
Acad::ErrorStatus drawCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcString& layer,
    const int& indexColor,
    const AcGeVector3d& normal = AcGeVector3d( 0, 0, 1 ),
    const bool& hasToZoom = false );

/**
  * \brief Dessine et renvoie l'objet cercle
  * \param ptCentre le centre du cercle
  * \param radius rayon du cercle
  * \param layer nom de calque pour le nouveau cercle
  * \param normal Vecteur normal du plan du cercle
  * \param hasToZoom true si on zoom sur le circle false sinon
  * \return ErrorStatus
  */
Acad::ErrorStatus drawCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcString& layer,
    const AcGeVector3d& normal = AcGeVector3d( 0, 0, 1 ),
    const bool& hasToZoom = false );

/**
  * \brief Dessine et renvoie l'objet cercle avec un message (utile pour montrer des erreurs)
  * \param ptCentre le centre du cercle
  * \param radius rayon du cercle
  * \param layer nom de calque pour le nouveau cercle
  * \param message Message � afficher
  * \param normal Vecteur normal du plan du cercle
  * \param hasToZoom true si on zoom sur le circle false sinon
  * \return ErrorStatus
  */
Acad::ErrorStatus drawCircle(
    const AcGePoint3d& ptCenter,
    const double& radius,
    const AcString& layer,
    const AcString& message,
    const AcGeVector3d& normal = AcGeVector3d( 0, 0, 1 ),
    const bool& hasToZoom = false );


/**
  * \brief Zoom sur un objet cercle
  * \param circle Pointeur sur le cercle
  * \param height Hauteur de zoom
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus zoomOn( AcDbCircle* circle,
    const double& height = 5.0 );


/**
  * \brief Fonction pour r�cup�rer le pointeur sur un cercle � partir de son id
  * \param idCircle Id du cercle
  * \param circle Pointeur sur le cercle
  * \param mode Mode d'ouverture du cercle
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus
openCircle( const AcDbObjectId& idCircle,
    AcDbCircle*& circle,
    const AcDb::OpenMode& mode = AcDb::kForWrite );



/**
  * \brief R�cup�rer la liste des polylignes 3d d'un dwg sous forme de tableau d'objectId
  * \param objectIdArray Tableau d'objectId
  * \param dwgFile Chemin d'acc�s vers le fichier externe
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus importAcDbCircle( AcDbObjectIdArray& objectIdArray,
    const AcString& dwgFile );


/**
  * \brief Recuperer l'id et le  point d'insertion de tous les circle dans une selection
  * \param ssCircle S�lection sur les cercles
  * \return Vecteur contenant l'id et le  point d'insertion de tous les circle dans une selection
  */
std::vector<pair<AcDbObjectId, AcGePoint3d>> getIdAndInsertPtOfSSCircle( const ads_name& ssCircle );

/**
  * \brief Retourne l'entit� cercle qui contient le point et donc la distance du centre par rapport � ce point est la plus petite
  * \param ssCircle Selection sur les cercle
  * \param pt point de r�f�rence pour la recherche
  * \return Pointeur sur l'entit� cercle si existe sinon NULL
  */
AcDbCircle* getCircleAroundPoint( const ads_name& ssCircle, const AcGePoint3d& pt );


/**
  * \brief Retourne les Id des blocs se trouvant dans un cercle
  * \param arBlocksId ???
  * \param arBlocksPt ???
  * \param xPos ???
  * \param circle Le cercle
  * \return les Id des blocs
  */
AcDbObjectIdArray getSsBlocksInCircle( AcDbObjectIdArray& arBlocksId, AcGePoint3dArray& arBlocksPt,  vector<double>& xPos,  AcDbCircle*& circle );

/**
  * \brief
  * \param
  * \return
  */
AcDbObjectIdArray getSsPolyInCircle( const ads_name& ssPoly,  AcDbCircle*& circle );


/**
  * \brief Permet de comparer deux cercles ( rayon, centre, et calque )
  * \param areEqual Booleen sortie qui indique si c'est le m�me cercle
  * \param id1 ObjectId du premier cercle
  * \param id2 ObjectId du second cercle
  * \param precision Tolerance de comparaison
  * \return ErrorStatus
  */
Acad::ErrorStatus compareCircles(
    bool& areEqual,
    const AcDbObjectId& id1,
    const AcDbObjectId& id2,
    const double& precision = 0.01 );


/**
  * \brief Permet de valider le calque d'un cercle ( c'est � dire de mettre un OK au d�but du nom de la calque )
  * \param circle Le cercle o� on va valider le calque
  * \return ErrorStatus
  */
Acad::ErrorStatus changeCircleToOkLayer( AcDbCircle*& circle );