/**
 * @file pointEntity.h
 * @author Romain LEMETTAIS
 * @brief Fonctions utiles pour travailler avec les points et les vecteurs, 2d et 3d
 */


#pragma once
#include <dbents.h>
#include <geassign.h>
#include <dbobjptr.h>
#include <tchar.h>
#include "selectEntity.h"
#include "dataBase.h"
#include "poly3DEntity.h"
#include "progressBar.h"
#include <vector>
#include "geometry.h"


/**
 * \brief Convertit un ads_point en AcGePoint3d
 */
AcGePoint3d adsToAcGe( ads_point pt );



/**
 * \brief Convertit un AcGePoint3d en ads_point
 */
void acGeToAds( const AcGePoint3d& pt, ads_point& ads_pt );



/**
  * \brief Fait une selection quelconque sur un point
  * \param ssName Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsPoint(
    ads_name& ssName,
    const AcString& layerName = _T( "" ) );


/**
  * \brief Selectionne un seul point dans le dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsOnePoint(
    ads_name& ssName,
    const AcString& layerName = _T( "" ) );


/**
  * \brief Selectionne tous les points du dessin
  * \param ssName: Pointeur sur la selection
  * \param layerName  Calque a selectionner
  * \return Nombre d'elements selectionnes
  */
long getSsAllPoint(
    ads_name& ssName,
    const AcString& layerName = _T( "" ) );


/**
  * \brief Renvoie un pointeur sur le point numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du point a recuperer, par defaut 0
  * \param opMode Mode d'ouverture, en lecture ou en ecriture, par defaut en lecture
  * \return Pointeur sur point
  */
AcDbPoint* getPointFromSs(
    const ads_name& ssName,
    const long& iObject          = 0,
    const AcDb::OpenMode& opMode = AcDb::kForRead );



/**
  * \brief Renvoie un pointeur en LECTURE sur le point numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du point a recuperer, par defaut 0
  * \return Pointeur sur un point
  */
AcDbPoint* readPointFromSs(
    const ads_name&   ssName,
    const long&       iObject = 0 );



/**
  * \brief Renvoie un pointeur en ECRITURE sur le point numero N dans la selection
  * \param ssName Pointeur sur la selection
  * \param iObject Index dans la selection du point a recuperer, par defaut 0
  * \return Pointeur sur un point
  */
AcDbPoint* writePointFromSs(
    const ads_name&       ssName,
    const long&           iObject = 0 );

/**
  * \brief
  * \param
  * \param
  * \return
  */
void drawPoint( AcGePoint3d pt,
    const AcString& layer = _T( "0" ) );

/**
  * \brief
  * \param
  * \param
  * \return
  */
void drawPoint( AcGePoint2d pt,
    const AcString& layer = _T( "0" ) );


/**
  * \brief Dessine un point
  * \param pt Point 3d a dessiner
  * \param p_blocktable Pointeur sur la base de donnees
  * \return Pointeur sur un point 3d
  */
AcDbPoint* drawPoint(
    const AcGePoint3d& pt,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom = false );


/**
  * \brief Dessine un point
  * \param pt Point 3d a dessiner
  * \param p_blocktable Pointeur sur la base de donnees
  * \return Pointeur sur un point 3d
  */
AcDbPoint* drawPoint(
    const AcGePoint3d& pt,
    AcCmColor color,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom );

/**
  * \brief Dessine un point
  * \param pt Point 2d a dessiner
  * \param p_blocktable Pointeur sur la base de donnees
  * \return Pointeur sur un point 2d
  */
AcDbPoint* drawPoint(
    const AcGePoint2d&      pt,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom = false );




/** \brief Dessine un point 3d
  * \param dX Coordonnee X du point a dessiner
  * \param dY Coordonnee Y du point a dessiner
  * \param dZ Coordonnee Z du point a dessiner
  * \param p_blocktable Pointeur sur la base de donnees
  * \return Pointeur sur un point 3d
  */
AcDbPoint* drawPoint(
    const double&     dX,
    const double&     dY,
    const double&     dZ,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom = false );





/** \brief Dessine un point 2d
  * \param dX Coordonnee X du point a dessiner
  * \param dY Coordonnee Y du point a dessiner
  * \param p_blocktable Pointeur sur la base de donnees
  * \return Pointeur sur un point 2d
  */
AcDbPoint* drawPoint(
    const double&     dX,
    const double&     dY,
    AcDbBlockTableRecord* p_blocktable,
    const bool& hasToZoom = false );



/** \brief Dessine une croix rouge en 2D assez visible dans AutoCAD
  * \param pt Coordonn�es du point ou dessiner la croix
  */
void drawRedCross( const AcGePoint2d& pt );

/** \brief Dessine une croix rouge en 2D assez visible dans AutoCAD
  * \param pt Coordonn�es du point ou dessiner la croix
  */
void drawRedCross( const AcGePoint2d& pt,
    AcDbObjectId& idCross );


/** \brief Dessine une croix rouge en 3D assez visible dans AutoCAD
  * \param pt Coordonn�es du point ou dessiner la croix
  */
void drawRedCross( const AcGePoint3d& pt );

/** \brief Dessine une croix rouge en 3D assez visible dans AutoCAD
  * \param pt Coordonn�es du point ou dessiner la croix
  */
void drawRedCross( const AcGePoint3d& pt,
    AcDbObjectId& idCross );

/**
  * \brief Conversion d'un point 2d en un point 3d
  * \param pt Point a convertir
  * \param dZ Valeur de la coordonnee en Z
  * \return Point 3d
  */
AcGePoint3d getPoint3d(
    const AcGePoint2d&    pt,
    const double&         dZ = 0.0 );


/**
  * \brief Conversion d'un point 3d en un point 2d, la coordonnee en Z est supprimee
  * \param pt Point a convertir
  * \return Point 3d
  */
AcGePoint2d getPoint2d(
    const AcGePoint3d& pt );



/**
  * \brief Calcule la distance 2d entre 2 points 3d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Distance 2d entre les 2 points
  */
double getDistance2d(
    const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 );


/**
  * \brief Calcule la distance 2d entre un point 3d et un point 2d
  * \param pt1 Point 2d
  * \param pt2 Point 3d
  * \return Distance 2d entre les 2 points
  */
double getDistance2d(
    const AcGePoint2d&  pt1,
    const AcGePoint3d&  pt2 );

double getDistance2d(
    const AcGePoint2d&  pt1,
    const AcGePoint2d&  pt2 );


/**
  * \brief Conversion d'un vecteur 3d en 2d
  * \param p_vector Vecteur a convertir
  * \return Vecteur 2d
  */
AcGeVector2d getVector2d(
    const AcGeVector3d& p_vector );



/**
  * \brief Retourne un vecteur 2d a partir de 2 points 3d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Vecteur 2d partant de P1 vers P2
  */
AcGeVector2d getVector2d(
    const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 );



/**
  * \brief Retourne un vecteur 2d a partir de 2 points 2d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Vecteur 2d partant de P1 vers P2
  */
AcGeVector2d getVector2d(
    const AcGePoint2d&   pt1,
    const AcGePoint2d&   pt2 );



/**
  * \brief Retourne un vecteur 3d a partir d'un vecteur 2d
  * \param p_vector Vecteur 2d
  * \param dz Coordonnee en Z
  * \return Vecteur 3d
  */
AcGeVector3d getVector3d(
    const AcGeVector2d&    p_vector,
    const double&          dZ  = 0 );


/**
  * \brief Retourne un vecteur 3d a partir de 2 points 3d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Vecteur 3d partant de P1 vers P2
  */
AcGeVector3d getVector3d(
    const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 );



/**
  * \brief Retourne un vecteur 3d a partir de 2 points 2d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Vecteur 3d partant de P1 vers P2
  */
AcGeVector3d getVector3d(
    const AcGePoint2d& pt1,
    const AcGePoint2d& pt2 );


/**
  * \brief Cree un vecteur 3d
  * \param alpha Angle entre le vecteur et l'axe x
  * \param length Longueur du vecteur
  * \param dZ Coordonee en z du vecteur
  * \return Vecteur 3d
  */
AcGeVector3d getVector3d(
    const double&     alpha,
    const double&     length = 1.0,
    const double&     dZ  =    0.0 );



/**
  * \brief Retourne le vecteur 3d moyen
  * \param pt1 Premier point
  * \param pt2 Second point
  * \param pt3 Troisieme point
  * \return Vecteur moyen
  */
AcGeVector3d getMeanVector3d( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const AcGePoint3d& pt3 );


/**
  * \brief Retourne le vecteur 3d moyen
  * \param vec1 Premier vecteur
  * \param vec2 Second vecteur
  * \return Vecteur moyen
  */
AcGeVector3d getMeanVector3d( const AcGeVector3d& vec1,
    const AcGeVector3d& vec2 );


/**
  * \brief Retourne le vecteur 3d moyen
  * \param pt1 Premier point
  * \param pt2 Second point
  * \param pt3 Troisieme point
  * \return Vecteur moyen
  */
AcGeVector3d getMeanVector3d( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const AcGePoint3d& pt3 );


/**
  * \brief Retourne le vecteur 3d moyen
  * \param vec1 Premier vecteur
  * \param vec2 Second vecteur
  * \return Vecteur moyen
  */
AcGeVector3d getMeanVector3d( const AcGeVector3d& vec1,
    const AcGeVector3d& vec2 );



/**
  * \brief Retourne le vecteur 2d unitaire d'un vecteur 3d
  * \param p_vector Vecteur 3d
  * \return Vecteur 2d unitaire
  */
AcGeVector2d getUnitVector2d(
    const AcGeVector3d& p_vector );


/**
  * \brief Retourne le vecteur 2d unitaire entre 2 points 3d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Vecteur 2d unitaire partant de P1 vers P2
  */
AcGeVector2d getUnitVector2d(
    const AcGePoint3d&     pt1,
    const AcGePoint3d&     pt2 );


/**
  * \brief Retourne le vecteur 2d unitaire entre 2 points 2d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Vecteur 2d unitaire partant de P1 vers P2
  */
AcGeVector2d getUnitVector2d(
    const AcGePoint2d&     pt1,
    const AcGePoint2d&     pt2 );



/**
  * \brief Retourne le vecteur 3d unitaire entre 2 points 3d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Vecteur 3d unitaire partant de P1 vers P2
  */
AcGeVector3d getUnitVector3d(
    const AcGePoint3d&     pt1,
    const AcGePoint3d&     pt2 );


/**
  * \brief Retourne le vecteur 3d unitaire entre 2 points 2d
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Vecteur 3d unitaire partant de P1 vers P2
  */
AcGeVector3d getUnitVector3d(
    const AcGePoint2d& pt1,
    const AcGePoint2d& pt2 );



/**
  * \brief Renvoie le point moyen 3d de deux points 3d
  * \param pt1 Depart du vecteur
  * \param pt2 Fin du vecteur
  * \return Point moyen
  */
AcGePoint3d midPoint3d(
    const AcGePoint3d&     pt1,
    const AcGePoint3d&     pt2 );

/**
  * \brief Renvoie le point moyen 3d de deux points 2d, la commposante en Z est nulle
  * \param pt1 Depart du vecteur
  * \param pt2 Fin du vecteur
  * \return Point moyen
  */
AcGePoint3d midPoint3d(
    const AcGePoint2d& pt1,
    const AcGePoint2d& pt2 );


/**
  * \brief Renvoie le point moyen 2d de deux points 3d
  * \param pt1 Depart du vecteur
  * \param pt2 Fin du vecteur
  * \return Point moyen
  */
AcGePoint2d midPoint2d(
    const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 );


/**
  * \brief Renvoie le point moyen 2d de deux points 2d
  * \param pt1 Depart du vecteur
  * \param pt2 Fin du vecteur
  * \return Point moyen
  */
AcGePoint2d midPoint2d(
    const AcGePoint2d& pt1,
    const AcGePoint2d& pt2 );


/**
  * \brief Verifie si un nombre est nul ou pas a plus moins la tolerance
  * \param test Nombre a tester
  * \param tol Valuer de tolerance
  * \return true si c'est egal a zero plus ou moins la tolerance, false sinon
  */
bool isZero(
    const double& test,
    const double& tol );


/**
  * \brief Verifie si deux points sont egaux ou non a plus ou moins une tolerance
  * \param pt1 Premier point
  * \param pt2 Second point
  * \param tolXY Valeur de la tol�rance sur XY, par d�faut 1 cm
  * \return true si les points se chevauchent avec une distance de plus ou moins la tolerance, false sinon
  */
bool isEqual2d(
    const AcGePoint2d&  pt1,
    const AcGePoint2d&  pt2,
    const double&       tolXY = 0.01 );


/**
  * \brief Verifie si deux points 3d sont egaux ou non en 2d a plus ou moins une tolerance
  * \param pt1 Premier point
  * \param pt2 Second point
  * \param tolXY Valeur de la tol�rance sur XY, par d�faut 1 cm
  * \return true si les points se chevauchent avec une distance de plus ou moins la tolerance, false sinon
  */
bool isEqual2d(
    const AcGePoint3d&  pt1,
    const AcGePoint3d&  pt2,
    const double&       tolXY = 0.01 );


/**
  * \brief Verifie si deux points sont egaux ou non a plus ou moins une tolerance
  * \param pt1 Premier point
  * \param pt2 Second point
  * \param tolXYZ  Valeur de la tol�rance sur XY, par d�faut 1 cm
  * \param tolZ Valeur de la tol�rance sur Z, par d�faut 1 cm
  * \return true si les points se chevauchent avec une distance de plus ou moins la tolerance, false sinon
  */
bool isEqual3d(
    const AcGePoint3d&    pt1,
    const AcGePoint3d&    pt2,
    const double&         tolXY,
    const double&         tolZ );


/**
  * \brief Verifie si deux points sont egaux ou non a plus ou moins une tolerance sur xyz
  * \param pt1 Premier point
  * \param pt2 Second point
  * \param tolXYZ Valeur de la tolerance sur XYZ , par d�faut 1 cm
  * \return true si les points se chevauchent avec une distance de plus ou moins la tolerance, false sinon
  */
bool isEqual3d(
    const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const double& tolXYZ = 0.01 );


/**
  * \brief Ajoute les coordonnees de deux points 2D
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Somme des deux
  */
AcGePoint2d addPoints( const AcGePoint2d&    pt1,
    const AcGePoint2d&    pt2 );


/**
  * \brief Ajoute les coordonnees de deux points 3D
  * \param pt1 Premier point
  * \param pt2 Second point
  * \return Somme des deux
  */
AcGePoint3d addPoints( const AcGePoint3d&    pt1,
    const AcGePoint3d&    pt2 );



/**
  * \brief Zoom sur un objet point3D
  * \param pt3D Point 3D
  * \param height Hauteur de zoom
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus zoomOn( const AcGePoint3d& pt3D,
    const double& height = 5.0 );


/**
  * \brief Zoom sur un objet point3D
  * \param pt2D Point 2D
  * \param height Height de zoom
  * \param width Width du zoom
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus zoomOn( const AcGePoint3d& pt3D,
    const double& height,
    const double& width );



/**
  * \brief Zoom sur un objet point2D
  * \param pt2D Point 2D
  * \param height Hauteur de zoom
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus zoomOn( const AcGePoint2d& pt2D,
    const double& height = 5.0 );




/**
  * \brief Zoom sur un objet point
  * \param point Pointeur sur un AcDbPoint
  * \param height Hauteur de zoom
  * \return ErrorStatus Acad::eOk si l'op�ration s'effectue avec succ�s
  */
Acad::ErrorStatus zoomOn( AcDbPoint* point,
    const double& height = 5.0 );


/**
  * \brief Retourne le Z moyen
  * \param pt1 Premier point de r�f�rence
  * \param pt2 Second point de r�f�rence
  * \param ptZSearch point dont le Z est � cherch�
  * \return Z moyen
  */
double getMeanZ( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2,
    const AcGePoint3d& ptZSearch );

/**
  * \brief Retourne le X,Y,Z moyen
  * \param pt1 Premier point de r�f�rence
  * \param pt2 Second point de r�f�rence
  * \return point moyen
  */
AcGePoint3d getMeanPt( const AcGePoint3d& pt1,
    const AcGePoint3d& pt2 );

/**
  * \brief
  * \param
  * \param
  * \return
  */
bool isInsideBox( const AcGePoint3d& pt,
    const AcDbExtents& box );