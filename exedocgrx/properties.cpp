#pragma once
#include "properties.h"


double getCurrentScale()
{
    // Retouer l'echelle courante
    return  acdbHostApplicationServices()->workingDatabase()->celtscale();
}

Acad::ErrorStatus setCurrentScale( double scale )
{
    return  acdbHostApplicationServices()->workingDatabase()->setLtscale( scale );
}

AcCmColor getCurrentColor()
{
    // Retouer l'Id du type de ligne courant
    return  acdbHostApplicationServices()->workingDatabase()->cecolor();
}

Acad::ErrorStatus setCurrentColor( const AcCmColor& curColor )
{
    return  acdbHostApplicationServices()->workingDatabase()->setCecolor( curColor );
}

AcDbObjectId getCurrentLineTypeId()
{
    // Recuperer l'id du type de ligne  courant
    AcDbObjectId lienTypeId = acdbHostApplicationServices()->workingDatabase()->celtype();
    
    // Retouer l'Id du type de ligne courant
    return lienTypeId;
}

Acad::ErrorStatus setCurrentLineTypeId( const AcDbObjectId& lineTypeId )
{
    return  acdbHostApplicationServices()->workingDatabase()->setCeltype( lineTypeId );
}

AcDb::LineWeight getCurrentLineWeight()
{
    // Retouer l'Id du type de ligne courant
    return  acdbHostApplicationServices()->workingDatabase()->celweight();
}

Acad::ErrorStatus setCurrentLineWeight( AcDb::LineWeight lineWeight )
{
    return  acdbHostApplicationServices()->workingDatabase()->setCelweight( lineWeight );
}

Acad::ErrorStatus setCurrentLayer( const AcDbObjectId&  layerId )
{
    return acdbHostApplicationServices()->workingDatabase()->setClayer( layerId );
}

Acad::ErrorStatus setCurrentProperties( const AcDbEntity* entity )
{
    ErrorStatus es = eNotApplicable;
    
    if( !entity )
        return eNullEntityPointer;
        
    // Couleur de ligne
    if( es = setCurrentColor( entity->color() ) )
        return es;
        
    // Calque
    if( es = setCurrentLayer( entity->layerId() ) )
        return es;
        
    // Type de ligne
    if( es = setCurrentLineTypeId( entity->linetypeId() ) )
        return es;
        
    // Echelle
    if( es = setCurrentScale( entity->linetypeScale() ) )
        return es;
        
    // Epassiseur de ligne
    if( es = setCurrentLineWeight( entity->lineWeight() ) )
        return es;
        
    // Transparence
    acdbHostApplicationServices()->workingDatabase()->setCETRANSPARENCY( entity->transparency() );
    
    return es;
    
}